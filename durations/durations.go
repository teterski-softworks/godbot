package durations

import (
	"regexp"
	"strconv"
	"time"

	"github.com/pkg/errors"
)

const (
	Day  = time.Hour * 24
	Week = Day * 7
)

var (
	regexpWeeks = regexp.MustCompile(`(\d+)w`)
	regexpDays  = regexp.MustCompile(`(\d+)d`)
)

//Parse is a wrapper around time.Parse which also accepts `w` for weeks and `d` for days as valid time units.
//Supports only whole number weeks/days.
func Parse(s string) (time.Duration, error) {

	if len(s) == 0 {
		//return the same error we would get from time.ParseDuration for an empty string
		_, err := time.ParseDuration(s)
		return 0, errors.WithStack(err)
	}

	//Convert `w` and `d` time units into hours to add to the remaining duration
	var duration, sign time.Duration
	sign = +1

	//Check if this is a negative duration
	if s[0] == '-' {
		sign = -1
		s = s[1:]
	}

	if regexpWeeks.MatchString(s) {
		matches := regexpWeeks.FindAllString(s, -1)
		for _, match := range matches {
			weeksString := match[:len(match)-1] //trim `w` time unit
			weeks, err := strconv.ParseInt(weeksString, 10, 0)
			if err != nil {
				return 0, errors.WithStack(err)
			}
			duration += Week * time.Duration(weeks)
		}
	}

	if regexpDays.MatchString(s) {
		matches := regexpDays.FindAllString(s, -1)
		for _, match := range matches {
			daysString := match[:len(match)-1] //trim `d` time unit
			days, err := strconv.ParseInt(daysString, 10, 0)
			if err != nil {
				return 0, errors.WithStack(err)
			}
			duration += Day * time.Duration(days)
		}
	}

	//overflow check - time value can overflow into negative
	if duration < 0 {
		return 0, errors.New("overflow")
	}

	//Remove already processed time units.
	s = regexpWeeks.ReplaceAllString(s, "")
	s = regexpDays.ReplaceAllString(s, "")

	if s == "" {
		return sign * duration, nil
	}
	//Parse the remaining remainingDuration
	remainingDuration, err := time.ParseDuration(s)
	duration += remainingDuration

	return sign * duration, errors.WithStack(err)
}

//String is a wrapper around time.Duration.String which formats weeks and days using `w` and `d` time units.
func String(duration time.Duration) string {

	var sign, weeksString, daysString, remainingString string

	if duration < 0 {
		sign = "-"
		duration = -1 * duration
	}

	weeks := duration / Week
	duration %= Week
	if weeks != 0 {
		weeksString = strconv.FormatInt(int64(weeks), 10) + "w"
		daysString = "0d"
	}

	days := duration / Day
	duration %= Day
	if days != 0 {
		daysString = strconv.FormatInt(int64(days), 10) + "d"
	}

	remainingString = duration.String()
	if duration == 0 && (weeksString != "" || daysString != "") {
		remainingString = ""
	}

	return sign + weeksString + daysString + remainingString
}
