package durations

import (
	dg "github.com/bwmarrin/discordgo"
	"gitlab.com/teterski-softworks/godbot/embeds"
)

var (
	ResponseInvalidDurationError = func() *dg.MessageEmbed {
		embed := embeds.New("", "", "")
		embed.Title = "❌ Invalid Duration 🕒"
		embed.Color = embeds.ColorRed
		embed.Description = "Durations must be in the following format `0w0d0h0m0s`.\n\n"
		embed.Description += "Here are some examples of valid durations:\n"
		embed.Description += "`3h` - 3 hours\n"
		embed.Description += "`4w1s` - 4 weeks, 1 second\n"
		embed.Description += "`25m30s` - 25 minutes, 30 seconds\n"
		embed.Description += "`1d12h30m` - 1 day, 12 hours, 30 minutes\n"
		embed.Footer.Text += "Maximum duration is around 15000 weeks."
		return &embed.MessageEmbed
	}
)
