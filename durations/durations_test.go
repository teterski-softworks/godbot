package durations_test

import (
	"testing"
	"time"

	"gitlab.com/teterski-softworks/godbot/durations"
)

func TestParseDuration(t *testing.T) {

	t.Run("Simple", func(t *testing.T) {
		duration1, err := time.ParseDuration("10h13m50s60ms23ns")
		if err != nil {
			t.Fatalf("%+v", err)
		}

		duration2, err := durations.Parse("10h13m50s60ms23ns")
		if err != nil {
			t.Fatalf("%+v", err)
		}

		if duration1 != duration2 {
			t.Fatal("Durations not equal.")
		}
	})

	t.Run("Days", func(t *testing.T) {
		duration1, err := time.ParseDuration("610h13m50s60ms23ns")
		if err != nil {
			t.Fatalf("%+v", err)
		}

		duration2, err := durations.Parse("25d10h13m50s60ms23ns")
		if err != nil {
			t.Fatalf("%+v", err)
		}

		if duration1 != duration2 {
			t.Fatal("Durations not equal.", duration1, duration2)
		}

		if duration, err := durations.Parse("1d"); err != nil {
			t.Fatalf("%+v", err)
		} else if duration != durations.Day {
			t.Fatal("Durations not equal.", duration)
		}

		if duration, err := durations.Parse("1d3d"); err != nil {
			t.Fatalf("%+v", err)
		} else if duration != durations.Day*4 {
			t.Fatal("Durations not equal.")
		}
	})

	t.Run("Weeks", func(t *testing.T) {
		duration1, err := time.ParseDuration("514h13m50s60ms23ns")
		if err != nil {
			t.Fatalf("%+v", err)
		}

		duration2, err := durations.Parse("3w10h13m50s60ms23ns")
		if err != nil {
			t.Fatalf("%+v", err)
		}

		if duration1 != duration2 {
			t.Fatal("Durations not equal.")
		}

		if duration, err := durations.Parse("1w"); err != nil {
			t.Fatalf("%+v", err)
		} else if duration != durations.Week {
			t.Fatal("Durations not equal.")
		}

		if duration, err := durations.Parse("1w3w"); err != nil {
			t.Fatalf("%+v", err)
		} else if duration != durations.Week*4 {
			t.Fatal("Durations not equal.")
		}
	})

	t.Run("Days and Weeks", func(t *testing.T) {
		duration1, err := time.ParseDuration("610h13m50s60ms23ns")
		if err != nil {
			t.Fatalf("%+v", err)
		}

		duration2, err := durations.Parse("3w4d10h13m50s60ms23ns")
		if err != nil {
			t.Fatalf("%+v", err)
		}

		if duration1 != duration2 {
			t.Fatal("Durations not equal.")
		}

		//Changed order
		if _, err := durations.Parse("3d4w10h13m50s60ms23ns"); err != nil {
			t.Fatalf("%+v", err)
		}
		if _, err := durations.Parse("10h13m50s3d60ms23ns4w"); err != nil {
			t.Fatalf("%+v", err)
		}
	})

	t.Run("Negative durations", func(t *testing.T) {
		duration1, err := time.ParseDuration("-610h13m50s60ms23ns")
		if err != nil {
			t.Fatalf("%+v", err)
		}

		duration2, err := durations.Parse("-3w4d10h13m50s60ms23ns")
		if err != nil {
			t.Fatalf("%+v", err)
		}
		if duration1 != duration2 {
			t.Fatal("Durations not equal.")
		}
	})

	t.Run("Invalid durations", func(t *testing.T) {
		if _, err := durations.Parse("invalid duration"); err == nil {
			t.Fatal("Invalid durations did not result error.")
		}
		if _, err := durations.Parse(""); err == nil {
			t.Fatal("Invalid durations did not result error.")
		}
	})

	t.Run("Overflow", func(t *testing.T) {
		t.Run("Past max duration", func(t *testing.T) {
			if _, err := durations.Parse("999999w"); err == nil {
				t.Fatal("Overflow did not result error.")
			}
			if _, err := durations.Parse("999999d"); err == nil {
				t.Fatal("Overflow did not result error.")
			}
			if _, err := durations.Parse("-999999w"); err == nil {
				t.Fatal("Overflow did not result error.")
			}
			if _, err := durations.Parse("-999999d"); err == nil {
				t.Fatal("Overflow did not result error.")
			}
		})
		t.Run("ParseInt overflow", func(t *testing.T) {
			if _, err := durations.Parse("999999999999999999999999999999999999999999999999999999w"); err == nil {
				t.Fatal("Overflow did not result error.")
			}
			if _, err := durations.Parse("999999999999999999999999999999999999999999999999999999d"); err == nil {
				t.Fatal("Overflow did not result error.")
			}
			if _, err := durations.Parse("-999999999999999999999999999999999999999999999999999999w"); err == nil {
				t.Fatal("Overflow did not result error.")
			}
			if _, err := durations.Parse("-999999999999999999999999999999999999999999999999999999d"); err == nil {
				t.Fatal("Overflow did not result error.")
			}
		})
	})
}

func TestDurationString(t *testing.T) {

	testingTable := []struct {
		input, expected string
	}{
		{"0s", "0s"},
		{"0", "0s"},

		{"1h2m3s", "1h2m3s"},
		{"1h", "1h0m0s"},
		{"1m", "1m0s"},

		{"1w2d", "1w2d"},
		{"1w", "1w0d"},
		{"1d", "1d"},

		{"1w2d3h", "1w2d3h0m0s"},
		{"1w2d3h4m", "1w2d3h4m0s"},
		{"1w2d3h4m5s", "1w2d3h4m5s"},

		{"-1w2d3h4m5s", "-1w2d3h4m5s"},
	}

	for _, test := range testingTable {
		duration, err := durations.Parse(test.input)
		if err != nil {
			t.Fatalf("%+v", err)
		}
		result := durations.String(duration)
		if result != test.expected {
			t.Fatal(result)
		}
	}

}
