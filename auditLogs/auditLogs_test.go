package auditLogs_test

import (
	dg "github.com/bwmarrin/discordgo"
	"gitlab.com/teterski-softworks/godbot/auditLogs"
	"gitlab.com/teterski-softworks/godbot/db"
)

var guildID, channelID, actionType = "guildID", "channelID", dg.AuditLogActionMessageDelete
var entry = dg.AuditLogEntry{ActionType: &actionType}
var createTables = []func(db.ReadWriter) error{auditLogs.CreateDatabaseTables}

//TODO: Needs Delete method in order to work.
/*
func TestDatabaseAccess(t *testing.T) {

	expected := auditLogs.New(guildID, channelID, &entry)
	//TODO: set random values for updated
	updated := expected
	flags := tests.DefaultTestDatabaseAccessFlags
	flags.RetrieveEdited = false
	flags.StoreEdit = false

	retrieve := func(DB db.Reader) (auditLogs.AuditLogRecord, error) {
		return auditLogs.Retrieve(DB, guildID, channelID, actionType)
	}

	tests.TestDatabaseAccess(
		t,
		expected, updated,
		createTables,
		retrieve,
		flags,
	)
}
*/
