package auditLogs

var (
	SQLSelectMostRecentAuditLogOfType = `
	SELECT ID, count
	FROM auditLogs
	WHERE guildID=(?1) AND channelID=(?2) AND actionType=(?3);`

	SQLUpsertAuditLog = `
	INSERT INTO auditLogs(
		guildID,
		channelID,
		actionType,
		ID,
		count
	) VALUES (?1, ?2, ?3, ?4, ?5)
	ON CONFLICT (guildID, channelID, actionType) DO
	UPDATE SET

	ID    =(?4),
	count =(?5)

	WHERE guildID=(?1) AND channelID=(?2) AND actionType=(?3);`
)
