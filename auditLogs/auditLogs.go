package auditLogs

import (
	"database/sql"

	dg "github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
	"gitlab.com/teterski-softworks/godbot/channels"
	"gitlab.com/teterski-softworks/godbot/db"
	"gitlab.com/teterski-softworks/godbot/states"
)

//TODO: a way to delete audit log records for channels to which we no longer have access to or have been deleted.

//AuditLogRecord represents a Discord audit log entry.
//It is mostly used to keep track of what the most recently processed audit log entry of a specific type is for a specific channel.
type AuditLogRecord struct {
	//Primary keys
	channels.Channel
	ActionType dg.AuditLogAction

	ID    string
	Count string
}

//New returns a new AuditLogRecord based on the dg.AuditLogEntry provided.
func New(guildID string, channelID string, entry *dg.AuditLogEntry) AuditLogRecord {
	record := AuditLogRecord{
		Channel:    channels.New(guildID, channelID),
		ActionType: *entry.ActionType,
		ID:         entry.ID,
	}
	if entry.Options != nil {
		record.Count = entry.Options.Count
	}
	return record
}

//Retrieve returns the audit log record showing what the most recent audit log entry of the specified type was for the given channel.
func Retrieve(DB db.Reader, guildID string, channelID string, actionType dg.AuditLogAction) (AuditLogRecord, error) {

	r := AuditLogRecord{
		Channel:    channels.New(guildID, channelID),
		ActionType: actionType,
	}
	row, err := DB.QueryRow(SQLSelectMostRecentAuditLogOfType, r.GuildID, r.ChannelID, r.ActionType)
	if err != nil {
		return r, err
	}

	if err = row.Scan(
		&r.ID,
		&r.Count,
	); err != nil {
		return r, errors.WithStack(err)
	}

	return r, nil
}

//Store saves the AuditLogRecord to the DB.
func (r AuditLogRecord) Store(DB db.ReadWriter) error {

	t := DB.NewTransaction()
	if err := r.Channel.Store(t); err != nil {
		return err
	}
	t.Queue(SQLUpsertAuditLog,
		r.GuildID,
		r.ChannelID,
		r.ActionType,
		r.ID,
		r.Count,
	)
	return t.Commit()
}

//GetMessageDeleteInfo uses the guild's audit log to determine the ID of the member who deleted the author's message in the channel.
//If no audit log entry for the deletion is found (ex. member deleted their own message, or message was deleted by a bot), will return an empty ID.
func GetMessageDeleteInfo(s *states.State, guildID string, channelID string, authorID string) (deletedByID string, err error) {

	//We need to search for a matching audit log entry.
	//We can't just use the most recent one, because in the time it took the caller to call this function, another audit log entry might have been added.

	mostRecentlyProcessedEntry, err := Retrieve(s.DB, guildID, channelID, dg.AuditLogActionMessageDelete)
	if err != nil {
		if !errors.Is(err, sql.ErrNoRows) {
			return deletedByID, err
		}
		mostRecentlyProcessedEntry = AuditLogRecord{}
	}

	//Check if there is an audit log for the deletion and make best guess as to who deleted the message
	auditLogs, err := s.GuildAuditLog(guildID, "", "", int(dg.AuditLogActionMessageDelete), 100)
	if err != nil {
		return deletedByID, errors.WithStack(err)
	}

	endSearch := false
	for _, auditLogEntry := range auditLogs.AuditLogEntries {

		if endSearch {
			break
		}

		//Once we reach the most recently processed audit entry, stop the search.
		if auditLogEntry.ID == mostRecentlyProcessedEntry.ID {
			//If multiple messages have been deleted in a row, the count for the entry will be different.
			//In this case we want to process this entry and break after.
			if auditLogEntry.Options.Count == mostRecentlyProcessedEntry.Count {
				break
			}
			endSearch = true //Process this entry and then break the search
		}

		if auditLogEntry.Options.ChannelID != channelID {
			continue
		}
		if auditLogEntry.TargetID != authorID {
			continue
		}
		deletedByID = auditLogs.AuditLogEntries[0].UserID

		//set this auditLogEntry as the most recent and save it to the DB
		if err := New(guildID, channelID, auditLogEntry).Store(s.DB); err != nil {
			return deletedByID, err
		}

		break
	}
	return deletedByID, nil
}

//GetBanInfo uses the guild's audit log to get information about the recent ban of the provided member.
func GetBanInfo(s *states.State, guildID string, memberID string) (reason string, bannedByMemberID string, err error) {

	mostRecentlyProcessedEntry, err := Retrieve(s.DB, guildID, "", dg.AuditLogActionMemberBanAdd)
	if err != nil {
		if !errors.Is(err, sql.ErrNoRows) {
			return reason, bannedByMemberID, err
		}
		mostRecentlyProcessedEntry = AuditLogRecord{}
	}

	//Get the ban information from the audit log
	auditLogs, err := s.GuildAuditLog(guildID, "", "", int(dg.AuditLogActionMemberBanAdd), 100)
	if err != nil {
		return reason, bannedByMemberID, errors.WithStack(err)
	}
	for _, auditLogEntry := range auditLogs.AuditLogEntries {

		//Once we reach the most recently processed audit entry, stop the search.
		if auditLogEntry.ID == mostRecentlyProcessedEntry.ID {
			break
		}

		if auditLogEntry.TargetID != memberID {
			continue
		}
		reason = auditLogEntry.Reason
		bannedByMemberID = auditLogEntry.UserID

		//set this auditLogEntry as the most recent and save it to the DB
		if err := New(guildID, "", auditLogEntry).Store(s.DB); err != nil {
			return reason, bannedByMemberID, err
		}

		break
	}

	return reason, bannedByMemberID, nil
}
