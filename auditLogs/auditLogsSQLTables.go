package auditLogs

import "gitlab.com/teterski-softworks/godbot/db"

//CreateDatabaseTables creates the required database tables for the warnings package.
func CreateDatabaseTables(DB db.ReadWriter) error {
	t := DB.NewTransaction()
	t.Queue(SQLCreateTableAuditLogs)
	return t.Commit()
}

var (
	SQLCreateTableAuditLogs string = `
	CREATE TABLE IF NOT EXISTS auditLogs(
		guildID      TEXT NOT NULL,
		channelID    TEXT NOT NULL,
		ID           TEXT NOT NULL,
		actionType   INT  NOT NULL,
		count        INT  NOT NULL,

		PRIMARY KEY(guildID, channelID, actionType)
		FOREIGN KEY(guildID, channelID) REFERENCES channels(guildID, channelID) ON DELETE CASCADE
	);`
)
