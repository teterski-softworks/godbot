package guildRemovers

var (
	SQLSelectGuildRemover string = `
	SELECT endTime, timerID
	FROM guildRemovers
	WHERE guildID=?1;`

	SQLSelectGuildRunningTimers string = `
	SELECT endTime, timerID
	FROM guildRemovers WHERE guildID =?1 AND timerID !=0;`

	SQLUpsertGuildRemover string = `
	INSERT INTO guildRemovers
	VALUES(?1,?2,?3) ON CONFLICT DO
	UPDATE SET
	endTime =?2,
	timerID =?3
	WHERE guildID=?1;`

	SQLDeleteGuildRemover string = `
	DELETE FROM guildRemovers WHERE guildID=?1;`
)
