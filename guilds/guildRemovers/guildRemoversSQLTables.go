package guildRemovers

import "gitlab.com/teterski-softworks/godbot/db"

func CreateDatabaseTables(DB db.ReadWriter) error {
	t := DB.NewTransaction()
	t.Queue(SQLCreateTableGuildRemovers)
	return t.Commit()
}

var (
	SQLCreateTableGuildRemovers string = `
	CREATE TABLE IF NOT EXISTS guildRemovers(
		guildID TEXT 	 NOT NULL PRIMARY KEY REFERENCES guilds(guildID) ON DELETE CASCADE,
		endTime DATETIME NOT NULL,
		timerID INT 	 NOT NULL
	);`
)
