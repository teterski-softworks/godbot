package guildRemovers

import (
	"database/sql"
	"errors"
	"time"

	dg "github.com/bwmarrin/discordgo"
	"gitlab.com/teterski-softworks/godbot/commands"
	"gitlab.com/teterski-softworks/godbot/states"
)

//DefaultWaitTime is the default wait time before a guild's info is deleted.
//90Days, ~3 months.
const DefaultWaitTime = time.Hour * 24 * 90

//GuildCreateHandler stops and removes any pending GuildRemovers when a new guild is connected to the bot.
func GuildCreateHandler(state *states.State, commandManager commands.Manager, event *dg.GuildCreate) error {

	t := state.DB.NewTransaction()

	//Stop any existing timer to delete guild data
	remover, err := Retrieve(t, event.Guild.ID)
	if err != nil {
		if !errors.Is(err, sql.ErrNoRows) {
			return err
		}
		//No timer exists.
	} else {
		if stopped := state.TimerManagers.Get(event.Guild.ID).Stop(remover.timerID); !stopped {
			return errors.New("unable to stop guild remover timer")
		}
		if err := remover.Delete(t); err != nil {
			return err
		}
		state.Printf("Stopped delete timer for G[%s].", event.Guild.ID)
	}

	//Re-add commands for the guild
	if err := commandManager.AddCommandsToGuild(t, state.Session, event.Guild.ID); err != nil {
		return err
	}

	return t.Commit()
}

//GuildDeleteHandler creates a new GuildRemover and initializes its timer when the bot is removed from a guild.
func GuildDeleteHandler(state *states.State, commandManager commands.Manager, event *dg.GuildDelete) error {

	//Mark commands as not added for the guild
	t := state.DB.NewTransaction()

	if err := commandManager.ClearGuildCommandsHashes(t, event.Guild.ID); err != nil {
		return err
	}

	//Start timer to delete guild data
	waitDuration := DefaultWaitTime
	remover := GuildRemover{
		GuildID: event.Guild.ID,
		endTime: time.Now().Add(waitDuration),
	}
	state.Printf("Starting delete timer for G[%s] to fire on [%s].", event.Guild.ID, remover.endTime.Format("2006/01/02 15:04:05"))
	e := make(chan error, 1)
	timerManager := state.TimerManagers.Get(event.Guild.ID)
	remover.timerID = timerManager.New(waitDuration, state, remover.Alarm, e)
	if err := remover.Store(t); err != nil {
		return err
	}

	if err := t.Commit(); err != nil {
		return err
	}

	return <-e
}
