package guildRemovers

import (
	"database/sql"
	"time"

	"github.com/pkg/errors"
	"gitlab.com/teterski-softworks/godbot/db"
	"gitlab.com/teterski-softworks/godbot/guilds"
	"gitlab.com/teterski-softworks/godbot/states"
)

//GuildRemover are used to schedule the removal of guild's info from the DB.
type GuildRemover struct {
	GuildID string
	timerID int
	endTime time.Time
}

//New returns a new GuildRemover for the given guild ID.
func New(guildID string) GuildRemover {
	return GuildRemover{GuildID: guildID}
}

//Retrieve returns an existing GuildRemover from the DB.
func Retrieve(DB db.Reader, guildID string) (GuildRemover, error) {
	r := GuildRemover{GuildID: guildID}
	row, err := DB.QueryRow(SQLSelectGuildRemover, guildID)
	if err != nil {
		return r, err
	}
	if err := row.Scan(&r.endTime, &r.timerID); err != nil {
		return r, errors.WithStack(err)
	}
	return r, nil
}

//Store saves a GuildRemover to the DB.
func (r GuildRemover) Store(DB db.ReadWriter) error {
	return DB.QueueAndCommit(SQLUpsertGuildRemover, r.GuildID, r.endTime, r.timerID)
}

//Delete removes a GuildRemover from the DB.
func (r GuildRemover) Delete(DB db.ReadWriter) error {
	return DB.QueueAndCommit(SQLDeleteGuildRemover, r.GuildID)
}

//Alarm deletes all of a guild's information from the DB.
func (r GuildRemover) Alarm(s *states.State, e chan<- error) {
	guild, err := guilds.Retrieve(s.DB, r.GuildID)
	if err != nil {
		e <- err
		return
	}
	if err := guild.Delete(s.DB); err != nil {
		e <- err
		return
	}

	s.TimerManagers.Get(r.GuildID).Delete(r.timerID)

	e <- nil
}

/*
InitializeTimers resumes GuildRemover timers.

Any errors that the timers encounter AFTER they are resumed are returned through the channel.
Other errors related to resuming the timers themselves are returned through the regular error.
*/
func InitializeTimers(s *states.State, guildID string) (chan error, error) {
	rows, err := s.DB.Query(SQLSelectGuildRunningTimers, guildID)
	if err != nil {
		return nil, err
	}

	var remover []GuildRemover
	for rows.Next() {

		//We need to finish scanning the rows first before starting the timers
		//otherwise, the timers can fire and try to interact with the DB locking it.

		b := New(guildID)
		if err = rows.Scan(&b.endTime, &b.timerID); err != nil && !errors.Is(err, sql.ErrNoRows) {
			return nil, errors.WithStack(err)
		}

		remover = append(remover, b)
	}

	//We do not have any running timers.
	if len(remover) == 0 {
		return nil, nil
	}

	e := make(chan error, len(remover))
	for _, r := range remover {
		r.timerID = s.TimerManagers.Get(guildID).New(time.Until(r.endTime), s, r.Alarm, e)
		if err = r.Store(s.DB); err != nil {
			s.TimerManagers.Get(guildID).Delete(r.timerID)
			return e, err
		}
	}
	return e, nil

}
