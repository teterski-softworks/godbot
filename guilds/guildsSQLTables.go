package guilds

import "gitlab.com/teterski-softworks/godbot/db"

func CreateDatabaseTables(DB db.ReadWriter) error {
	t := DB.NewTransaction()
	t.Queue(SQLCreateTableGuilds)
	return t.Commit()
}

var (
	SQLCreateTableGuilds string = `
	CREATE TABLE IF NOT EXISTS guilds(
		guildID TEXT NOT NULL PRIMARY KEY
	);`
)
