package guilds

var (
	SQLSelectGuild string = `
	SELECT guildID FROM guilds WHERE guildID=?1;`

	SQLSelectAllGuilds string = `
	SELECT guildID FROM guilds;`

	SQLInsertGuild string = `
	INSERT OR IGNORE INTO guilds(guildID) VALUES(?1);`

	SQLDeleteGuild string = `
	DELETE FROM guilds WHERE guildID=?1;`
)
