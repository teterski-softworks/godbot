package guilds

import (
	"github.com/pkg/errors"
	"gitlab.com/teterski-softworks/godbot/db"
)

//Guild represents a Discord guild/server.
type Guild struct {
	GuildID string
}

//New returns a new Guild for the given ID.
func New(guildID string) Guild {
	return Guild{GuildID: guildID}
}

//Retrieve returns a Guild if it is in the DB.
func Retrieve(DB db.Reader, guildID string) (guild Guild, err error) {

	row, err := DB.QueryRow(SQLSelectGuild, guildID)
	if err != nil {
		return guild, err
	}
	if err = row.Scan(&guild.GuildID); err != nil {
		return guild, err
	}

	return guild, nil
}

//RetrieveAll returns a slice of all Guilds in the DB.
func RetrieveAll(DB db.Reader) (guilds []Guild, err error) {

	rows, err := DB.Query(SQLSelectAllGuilds)
	if err != nil {
		return guilds, err
	}

	var guild Guild
	for rows.Next() {
		if err := rows.Scan(&guild.GuildID); err != nil {
			return guilds, errors.WithStack(err)
		}
		guilds = append(guilds, guild)
	}

	return guilds, nil
}

//Store saves a Guild to the DB.
func (g Guild) Store(DB db.ReadWriter) error {
	return DB.QueueAndCommit(SQLInsertGuild, g.GuildID)
}

//Delete removes all Guild related data from the DB.
func (g Guild) Delete(DB db.ReadWriter) error {
	return DB.QueueAndCommit(SQLDeleteGuild, g.GuildID)
}
