package mutes

import "gitlab.com/teterski-softworks/godbot/db"

//CreateDatabaseTables creates the required database tables for the mutes package.
func CreateDatabaseTables(DB db.ReadWriter) error {
	t := DB.NewTransaction()
	t.Queue(SQLCreateTableMutes)
	t.Queue(SQLCreateTableMuteRoles)
	return t.Commit()
}

var (
	SQLCreateTableMutes string = `
	CREATE TABLE IF NOT EXISTS mutes(
		guildID  TEXT NOT NULL,
		memberID TEXT NOT NULL,
		endTime  DATETIME NOT NULL,
		timerID  INT,

		PRIMARY KEY(guildID, memberID),
		FOREIGN KEY(guildID, memberID) REFERENCES members(guildID, memberID) ON DELETE CASCADE
	);`

	SQLCreateTableMuteRoles string = `
	CREATE TABLE IF NOT EXISTS muteRoles(
		guildID    TEXT NOT NULL PRIMARY KEY REFERENCES guilds(guildID) ON DELETE CASCADE,
		muteRole   TEXT DEFAULT "",
		removeRole TEXT DEFAULT "",

		FOREIGN KEY (guildID, muteRole)   REFERENCES roles(guildID, roleID) ON DELETE SET DEFAULT,
		FOREIGN KEY (guildID, removeRole) REFERENCES roles(guildID, roleID) ON DELETE SET DEFAULT
	);`
)
