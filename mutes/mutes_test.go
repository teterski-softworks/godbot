package mutes_test

import (
	"testing"

	"gitlab.com/teterski-softworks/godbot/db"
	"gitlab.com/teterski-softworks/godbot/members"
	"gitlab.com/teterski-softworks/godbot/mutes"
	"gitlab.com/teterski-softworks/godbot/tests"
)

var guildID, memberID = "guildID", "memberID"
var createTables = []func(db.ReadWriter) error{members.CreateDatabaseTables, mutes.CreateDatabaseTables}

//TODO
func TestDatabaseAccess(t *testing.T) {

	expected := mutes.New(guildID, memberID)
	//TODO: random values for updated
	updated := expected

	flags := tests.DefaultTestDatabaseAccessFlags
	flags.StoreEdit = false
	flags.RetrieveEdited = false

	retrieve := func(DB db.Reader) (mutes.Mute, error) {
		return mutes.Retrieve(DB, guildID, memberID)
	}

	tests.TestDatabaseAccess(
		t,
		expected, updated,
		createTables,
		retrieve,
		flags,
	)
}
