package mutes

import (
	"database/sql"
	"fmt"
	"time"

	dg "github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
	"gitlab.com/teterski-softworks/godbot/db"
	"gitlab.com/teterski-softworks/godbot/durations"
	"gitlab.com/teterski-softworks/godbot/embeds"
	"gitlab.com/teterski-softworks/godbot/logs"
	"gitlab.com/teterski-softworks/godbot/members"
	"gitlab.com/teterski-softworks/godbot/states"
)

//Mute represents a mute of a member.
type Mute struct {
	members.Member
	EndTime time.Time
	timerID int

	manuallyRemoved bool //True only if the mute was manually removed before its duration has expired.
}

//New returns a new Mute for the given member.
func New(guildID string, memberID string) Mute {
	var m Mute
	m.GuildID = guildID
	m.MemberID = memberID
	return m
}

//Retrieves gets a member's mute from the database.
func Retrieve(DB db.Reader, guildID string, memberID string) (Mute, error) {

	m := New(guildID, memberID)

	row, err := DB.QueryRow(SQLSelectMuteOfMember, guildID, memberID)
	if err != nil {
		return m, err
	}
	if err = row.Scan(&m.EndTime, &m.timerID); err != nil {
		return m, err
	}
	return m, nil
}

//Store saves a member's mute to the database.
func (m Mute) Store(DB db.ReadWriter) error {

	t := DB.NewTransaction()

	if err := members.New(m.GuildID, m.MemberID).Store(t); err != nil {
		return err
	}

	t.Queue(SQLUpsertMute,
		m.GuildID,
		m.MemberID,
		m.EndTime,
		m.timerID,
	)

	return t.Commit()
}

//Delete removes a member's mute from the database.
//If the menu is already not in the provided database, then found returns false. Otherwise, returns true.
func (m Mute) Delete(DB db.ReadWriter) (found bool, err error) {

	if _, err = Retrieve(DB, m.GuildID, m.MemberID); err != nil {
		return false, err
	}
	if err = DB.QueueAndCommit(SQLDeleteMute, m.GuildID, m.MemberID); err != nil {
		return false, err
	}
	return true, nil
}

//SetEndTimeFromNow sets a new end time for a mute.
func (m *Mute) SetEndTimeFromNow(duration time.Duration) {
	m.EndTime = time.Now().Add(duration)
}

//ApplyMute adds and removes the required roles from the member in order for the mute to take functionally effect on the server.
func (m Mute) ApplyMute(s *states.State) error {

	//Get the mute settings
	muteRoleID, removeRoleID, err := getMuteRoles(s.DB, m.GuildID)
	if err != nil {
		return err
	}

	//Give the user the Muted role
	if muteRoleID != "" {
		if err := s.GuildMemberRoleAdd(m.GuildID, m.MemberID, muteRoleID); err != nil {
			return errors.WithMessagef(err, "unable to add role %s to %s", muteRoleID, m.GuildID)
		}
	}

	//Remove the Member role from the user
	if removeRoleID != "" {
		if err := s.GuildMemberRoleRemove(m.GuildID, m.MemberID, removeRoleID); err != nil {
			return errors.WithMessagef(err, "unable to add role %s to %s", removeRoleID, m.MemberID)
		}
	}

	return err
}

//RemoveMute removes a mute from a member.
func (m Mute) RemoveMute(s *states.State, unMutedByID string, reason string) error {
	e := make(chan error, 1)
	defer close(e)
	m.manuallyRemoved = true
	m.Alarm(s, e)
	if err := logs.LogMute(s, m.GuildID, m.UnMuteEmbed(s, reason, unMutedByID)); err != nil {
		return err
	}
	return <-e
}

//Alarm un-mutes the user when it fires.
func (m Mute) Alarm(s *states.State, e chan<- error) {

	//Get the mute settings
	muteRoleID, removeRoleID, err := getMuteRoles(s.DB, m.GuildID)
	if err != nil {
		e <- err
		return
	}
	if removeRoleID != "" {
		if err := s.GuildMemberRoleAdd(m.GuildID, m.MemberID, removeRoleID); err != nil {
			e <- errors.WithMessagef(err, "unable to remove role %s from %s", removeRoleID, m.MemberID)
			return
		}
	}
	if muteRoleID != "" {
		if err := s.GuildMemberRoleRemove(m.GuildID, m.MemberID, muteRoleID); err != nil {
			e <- errors.WithMessagef(err, "unable to remove role %s from %s", muteRoleID, m.MemberID)
			return
		}
	}
	if _, err := m.Delete(s.DB); err != nil {
		e <- err
		return
	}

	//Delete the associated timer
	s.TimerManagers.Get(m.GuildID).Delete(m.timerID)

	if !m.manuallyRemoved {
		//Log the un-mute if mute logging is enabled
		if err := logs.LogMute(s, m.GuildID, m.UnMuteEmbed(s, ReasonMuteExpired, s.State.User.ID)); err != nil {
			e <- err
			return
		}
	}

	e <- nil
}

//MuteEmbed returns an embed showing the mute.
func (m Mute) MuteEmbed(s *states.State, duration time.Duration, reason string, mutedByID string) *dg.MessageEmbed {
	embed := embeds.New("", "", "")
	embed.Color = embeds.ColorRed
	member, user, err := members.GetMemberOrUserFromID(s.Session, m.GuildID, m.MemberID)
	if err == nil {
		if member != nil {
			embed.Thumbnail.URL = member.AvatarURL("")
		} else if user != nil {
			embed.Thumbnail.URL = user.AvatarURL("")
		}
	}
	embed.Title = "🔇 Member Muted"
	embed.Timestamp = time.Now().Format(time.RFC3339)
	embed.Description += fmt.Sprintf("%s has been muted by %s.\n\n", members.Mention(m.MemberID), members.Mention(mutedByID))
	if duration == 0 {
		embed.Description += "**Duration:** `Indefinite`.\n"
	} else {
		embed.Description += fmt.Sprintf("**Duration:** for `%s` until <t:%d>.\n", durations.String(duration), time.Now().Add(duration).Unix())
	}
	embed.Description += fmt.Sprintf("**Reason:** `%s`", reason)

	return &embed.MessageEmbed
}

//UnMuteEmbed returns an embed showing the un-mute.
func (m Mute) UnMuteEmbed(s *states.State, reason string, unMutedByID string) *dg.MessageEmbed {
	embed := embeds.New("", "", "")
	embed.Color = embeds.ColorGreen
	member, user, err := members.GetMemberOrUserFromID(s.Session, m.GuildID, m.MemberID)
	if err == nil {
		if member != nil {
			embed.Thumbnail.URL = member.AvatarURL("")
		} else if user != nil {
			embed.Thumbnail.URL = user.AvatarURL("")
		}
	}
	embed.Title = "🔊 Member Unmuted"
	embed.Timestamp = time.Now().Format(time.RFC3339)
	embed.Description += fmt.Sprintf("%s has been unmuted by %s.\n\n", members.Mention(m.MemberID), members.Mention(unMutedByID))
	embed.Description += fmt.Sprintf("**Reason:** `%s`", reason)

	return &embed.MessageEmbed
}

//DeleteAllMemberTimers stops and deletes any mute timers for the member.
func DeleteAllMemberTimers(s *states.State, guildID string, memberID string) error {

	//Get all running timers for the member from the DB
	rows, err := s.DB.Query(SQLSelectMemberRunningTimers, guildID, memberID)
	if err != nil {
		return err
	}
	for rows.Next() {
		var timerID int
		if err = rows.Scan(&timerID); err != nil {
			return errors.WithStack(err)
		}
		//Delete the timers from the bot's timer manager
		s.TimerManagers.Get(guildID).Delete(timerID)
	}

	//Delete existing mute record for the member
	if _, err := New(guildID, memberID).Delete(s.DB); err != nil {
		return err
	}
	return nil
}

/*
InitializeTimers resumes mute timers.

Any errors that the timers encounter AFTER they are resumed are returned through the channel.
Other errors related to resuming the timers themselves are returned through the regular error.
*/
func InitializeTimers(s *states.State, guildID string) (chan error, error) {

	rows, err := s.DB.Query(SQLSelectGuildRunningTimers, guildID)
	if err != nil {
		return nil, err
	}

	var mutes []Mute
	for rows.Next() {

		//We need to finish scanning the rows first before starting the timers
		//otherwise, the timers can fire and try to interact with the DB locking it.

		mute := New(guildID, "")
		if err = rows.Scan(&mute.MemberID, &mute.EndTime, &mute.timerID); err != nil && !errors.Is(err, sql.ErrNoRows) {
			return nil, errors.WithStack(err)
		}

		mutes = append(mutes, mute)
	}

	//We do not have any running timers.
	if len(mutes) == 0 {
		return nil, nil
	}

	e := make(chan error, len(mutes))
	for _, m := range mutes {
		m.timerID = s.TimerManagers.Get(guildID).New(time.Until(m.EndTime), s, m.Alarm, e)
		if err = m.Store(s.DB); err != nil {
			s.TimerManagers.Get(guildID).Delete(m.timerID)
			return e, err
		}
	}
	return e, nil
}
