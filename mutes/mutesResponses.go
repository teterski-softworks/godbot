package mutes

import (
	"fmt"

	dg "github.com/bwmarrin/discordgo"
	"gitlab.com/teterski-softworks/godbot/embeds"
)

var (
	ResponseNotMuted = func(memberID string) *dg.MessageEmbed {
		embed := embeds.New("", "", "")
		embed.Title = "❌ Member Not Muted"
		embed.Color = embeds.ColorRed
		embed.Description = fmt.Sprintf("%s is not muted.", memberID)
		return &embed.MessageEmbed
	}

	ResponseUnMuted = func(memberID string) *dg.MessageEmbed {
		embed := embeds.New("", "", "")
		embed.Title = "✅ Member Unmuted"
		embed.Color = embeds.ColorGreen
		embed.Description = fmt.Sprintf("%s has been un-muted!", memberID)
		return &embed.MessageEmbed
	}
)
