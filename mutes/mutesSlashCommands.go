package mutes

import (
	"database/sql"
	"time"

	dg "github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
	"gitlab.com/teterski-softworks/godbot/commands"
	"gitlab.com/teterski-softworks/godbot/db"
	"gitlab.com/teterski-softworks/godbot/durations"
	"gitlab.com/teterski-softworks/godbot/guilds"
	"gitlab.com/teterski-softworks/godbot/logs"
	"gitlab.com/teterski-softworks/godbot/roles"
	"gitlab.com/teterski-softworks/godbot/states"
)

//SlashCommandMute mutes a member for the specified duration.
var SlashCommandMute = commands.New(
	&dg.ApplicationCommand{
		Type:        dg.ChatApplicationCommand,
		Name:        "mute",
		Description: "Mute related commands. Admin only.",
		Options: []*dg.ApplicationCommandOption{
			slashCommandMuteMember,
			slashCommandMuteRemove,
			slashCommandMuteSettings,
		},
	},
	false,
	func(intr states.Interaction, options ...*dg.ApplicationCommandInteractionDataOption) (err error) {

		if err := intr.Acknowledge(); err != nil {
			return err
		}

		switch options[0].Name {
		case slashCommandMuteMember.Name:
			return slashMuteMember(intr, options[0].Options)
		case slashCommandMuteRemove.Name:
			return slashMuteRemove(intr, options[0].Options)
		case slashCommandMuteSettings.Name:
			return slashMuteSettings(intr, options[0].Options)
		}
		return intr.EditResponseEmbeds(nil, states.ResponseGenericError(nil))
	},
)

//slashCommandMuteMember - A subcommand to mute members.
var slashCommandMuteMember = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionSubCommand,
	Name:        "member",
	Description: "Mutes a member.",
	Options: []*dg.ApplicationCommandOption{
		{
			Type:        dg.ApplicationCommandOptionUser,
			Name:        "member",
			Description: "The member who will be muted.",
			Required:    true,
		}, {
			Type:        dg.ApplicationCommandOptionString,
			Name:        "duration",
			Description: "Duration to mute the member for, in the format `0w0d0h0m0s`.",
		}, {
			Type:        dg.ApplicationCommandOptionString,
			Name:        "reason",
			Description: "The reason for the mute.",
		},
	},
}

//slashMuteMember mutes a member.
func slashMuteMember(intr states.Interaction, options []*dg.ApplicationCommandInteractionDataOption) (err error) {

	member := options[0].UserValue(intr.Session)

	var duration time.Duration
	reason := ReasonNone
	for _, option := range options {
		switch option.Name {
		case "reason":
			reason = option.StringValue()
		case "duration":
			duration, err = durations.Parse(option.StringValue())
			if err != nil {
				return intr.EditResponseEmbeds(nil, durations.ResponseInvalidDurationError())
			}
		}
	}

	//Create a new Mute
	mute := New(intr.GuildID, member.ID)

	//It is possible the member might have been muted already
	//So we need to stop all other temporary mutes from expiring.
	if err := DeleteAllMemberTimers(intr.State, mute.GuildID, mute.MemberID); err != nil {
		return err
	}

	e := make(chan error, 1)
	defer close(e)

	//Temporary mute
	if duration != 0 {

		mute.SetEndTimeFromNow(duration)
		mute.timerID = intr.TimerManagers.Get(mute.GuildID).New(duration, intr.State, mute.Alarm, e)

		//Add the Mute to the DB
		if err := mute.Store(intr.DB); err != nil {
			return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
		}
	}

	//Mute the member
	if err := mute.ApplyMute(intr.State); err != nil {
		if _, err1 := mute.Delete(intr.DB); err1 != nil {
			err = errors.WithMessage(err, "unable to remove mute on error")
		}
		return intr.EditResponseEmbeds(err, states.ResponseBotPermission())
	}

	//Log the mute if mute logging is enabled
	embed := mute.MuteEmbed(intr.State, duration, reason, intr.Member.User.ID)
	if err := logs.LogMute(intr.State, mute.GuildID, embed); err != nil {
		return intr.EditResponseEmbeds(err, states.ResponseGenericError(err))
	}

	//Respond to the user
	if err = intr.EditResponseEmbeds(nil, embed); err != nil {
		return err
	}

	return <-e
}

//slashCommandMuteRemove - A subcommand to remove a mute from a member.
var slashCommandMuteRemove = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionSubCommand,
	Name:        "remove",
	Description: "Unmutes a muted user.",
	Options: []*dg.ApplicationCommandOption{
		{
			Type:        dg.ApplicationCommandOptionUser,
			Name:        "member",
			Description: "The member who will be unmuted.",
			Required:    true,
		}, {
			Type:        dg.ApplicationCommandOptionString,
			Name:        "reason",
			Description: "The reason for the unmute.",
		},
	},
}

//slashMuteRemove un-mutes a member.
func slashMuteRemove(intr states.Interaction, options []*dg.ApplicationCommandInteractionDataOption) (err error) {

	member := options[0].UserValue(intr.Session)
	mute, err := Retrieve(intr.DB, intr.GuildID, member.ID)
	if err != nil {

		if !errors.Is(err, sql.ErrNoRows) {
			return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
		}
		//If member has been muted indefinitely, there will be no record of it.
		//So we create a temporary record for this mute, and then un-mute.
		mute = New(intr.GuildID, member.ID)
		mute.SetEndTimeFromNow(time.Hour)
		if err := mute.Store(intr.DB); err != nil {
			return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
		}

	}
	reason := ReasonNone
	if len(options) != 1 {
		reason = options[1].StringValue()
	}

	//Un-mute the user
	if err = mute.RemoveMute(intr.State, intr.Member.User.ID, reason); err != nil {
		return intr.EditResponseEmbeds(err, states.ResponseGenericError(err))
	}

	//Respond to the user
	return intr.EditResponseEmbeds(nil, mute.UnMuteEmbed(intr.State, reason, intr.Member.User.ID))
}

//slashCommandMuteSettings - A sub command group for mute settings.
var slashCommandMuteSettings = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionSubCommandGroup,
	Name:        "settings",
	Description: "Changes mute settings. If no options selected, shows current settings.",
	Options: []*dg.ApplicationCommandOption{
		slashCommandMuteSettingsEdit,
		slashCommandMuteSettingsClear,
		slashCommandMuteSettingsView,
	},
}

//slashMuteSettings handles mute settings commands.
func slashMuteSettings(intr states.Interaction, options []*dg.ApplicationCommandInteractionDataOption) (err error) {
	switch options[0].Name {
	case slashCommandMuteSettingsEdit.Name:
		return slashMuteSettingsEdit(intr, options[0].Options)
	case slashCommandMuteSettingsClear.Name:
		return slashMuteSettingsClear(intr, options[0].Options)
	case slashCommandMuteSettingsView.Name:
		return slashMuteSettingsView(intr, options[0].Options)
	}
	return intr.EditResponseEmbeds(nil, states.ResponseGenericError(nil))
}

//slashCommandMuteSettingsEdit - A subcommand to edit mute settings.
var slashCommandMuteSettingsEdit = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionSubCommand,
	Name:        "edit",
	Description: "Changes mute settings.",
	Options: []*dg.ApplicationCommandOption{
		{
			Type:        dg.ApplicationCommandOptionRole,
			Name:        "mute_role",
			Description: "The role to assign when a member gets muted.",
		}, {
			Type:        dg.ApplicationCommandOptionRole,
			Name:        "remove_role",
			Description: "A role to remove from the member when they get muted.",
		},
	},
}

//slashMuteSettingsEdit edits mute settings.
func slashMuteSettingsEdit(intr states.Interaction, options []*dg.ApplicationCommandInteractionDataOption) (err error) {

	t := intr.DB.NewTransaction()
	for _, option := range options {
		switch option.Name {
		case "mute_role":
			//TODO! make settings its own struct
			role := option.RoleValue(intr.Session, intr.GuildID)
			if err := guilds.New(intr.GuildID).Store(t); err != nil {
				return err
			}
			if err := roles.New(intr.GuildID, role.ID).Store(t); err != nil {
				return err
			}
			t.Queue(SQLUpsertMuteRolesMute, intr.GuildID, role.ID)
		case "remove_role":
			//TODO! make settings its own struct
			role := option.RoleValue(intr.Session, intr.GuildID)
			if err := guilds.New(intr.GuildID).Store(t); err != nil {
				return err
			}
			if err := roles.New(intr.GuildID, role.ID).Store(t); err != nil {
				return err
			}
			t.Queue(SQLUpsertMuteRolesRemove, intr.GuildID, role.ID)
		}
	}
	if err = t.Commit(); err != nil {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}

	return currentSettings(intr)
}

//slashCommandMuteSettingsClear - A subcommand to remove a mute setting.
var slashCommandMuteSettingsClear = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionSubCommand,
	Name:        "clear",
	Description: "Clears mute settings.",
	Options: []*dg.ApplicationCommandOption{
		{
			Type:        dg.ApplicationCommandOptionString,
			Name:        "setting",
			Description: "The role to assign when a member gets muted.",
			Required:    true,
			Choices: []*dg.ApplicationCommandOptionChoice{
				{Name: "Mute role", Value: "mute_role"},
				{Name: "Role to remove", Value: "remove_role"},
			},
		},
	},
}

//slashMuteSettingsClear removes mute settings.
func slashMuteSettingsClear(intr states.Interaction, options []*dg.ApplicationCommandInteractionDataOption) (err error) {

	t := intr.DB.NewTransaction()
	switch options[0].Value {
	case "mute_role":
		t.Queue(SQLUpsertMuteRolesMute, intr.GuildID, "")
	case "remove_role":
		t.Queue(SQLUpsertMuteRolesRemove, intr.GuildID, "")
	}

	if err := t.Commit(); err != nil {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}

	return currentSettings(intr)
}

//slashCommandMuteSettingsView - A subcommand to view mute settings.
var slashCommandMuteSettingsView = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionSubCommand,
	Name:        "view",
	Description: "Views mute settings.",
}

//slashMuteSettingsView shows mute settings.
func slashMuteSettingsView(intr states.Interaction, options []*dg.ApplicationCommandInteractionDataOption) (err error) {
	return currentSettings(intr)
}
