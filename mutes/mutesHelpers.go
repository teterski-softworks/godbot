package mutes

import (
	"database/sql"

	dg "github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
	"gitlab.com/teterski-softworks/godbot/db"
	"gitlab.com/teterski-softworks/godbot/embeds"
	"gitlab.com/teterski-softworks/godbot/roles"
	"gitlab.com/teterski-softworks/godbot/states"
)

//getMuteRoles returns the role to add and the role to remove from a member while they are muted.
func getMuteRoles(DB db.Reader, guildID string) (muteRoleID string, removeRoleID string, err error) {
	row, err := DB.QueryRow(SQLSelectMuteRoles, guildID)
	if err != nil {
		return muteRoleID, removeRoleID, err
	}

	if err = row.Scan(&muteRoleID, &removeRoleID); err != nil {
		if !errors.Is(err, sql.ErrNoRows) {
			return muteRoleID, removeRoleID, err
		}
	}
	return muteRoleID, removeRoleID, nil
}

//currentSettings shows a the current mute settings.
func currentSettings(intr states.Interaction) error {

	muteRole, removeRole, err := getMuteRoles(intr.DB, intr.GuildID)
	if err != nil {
		return err
	}

	embed := embeds.NewDefault(intr.DB, intr.GuildID)
	embed.Title = "🔇 Mute Settings ⚙️"
	embed.Description = "Mutes can work a multiple of ways, depending on your needs. "
	embed.Description += "When muted, a role can be given/taken from a user, and then the action is automatically reversed when the mute expires or is removed."

	var muteRoleField dg.MessageEmbedField
	muteRoleField.Name = "Mute role:"
	muteRoleField.Value = roles.Mention(muteRole)
	muteRoleField.Inline = true
	if muteRole == "" {
		muteRoleField.Value = "`None`"
	}
	embed.Fields = append(embed.Fields, &muteRoleField)

	var removeRoleField dg.MessageEmbedField
	removeRoleField.Name = "Remove role while muted:"
	removeRoleField.Value = roles.Mention(removeRole)
	removeRoleField.Inline = true
	if removeRole == "" {
		removeRoleField.Value = "`None`"
	}
	embed.Fields = append(embed.Fields, &removeRoleField)

	return intr.EditResponseEmbeds(nil, &embed.MessageEmbed)
}
