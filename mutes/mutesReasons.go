package mutes

var (
	ReasonNone        string = "No reason provided."
	ReasonMuteExpired string = "Mute duration expired."
)
