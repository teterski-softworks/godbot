package mutes

var (
	SQLUpsertMute string = `
	INSERT INTO mutes(
		guildID,
		memberID,
		endTime,
		timerID
	)
	VALUES (?1,?2,?3,?4)
	ON CONFLICT (guildID, memberID) DO
	UPDATE SET

	endTime = (?3),
	timerID = (?4)

	WHERE guildID=(?1) AND memberID=(?2);`

	SQLSelectMuteOfMember string = `
	SELECT endTime, timerID
	FROM mutes
	WHERE guildID=(?1) AND memberID=(?2);`

	SQLSelectMemberRunningTimers string = `
	SELECT timerID
	FROM bans
	WHERE guildID=(?1) AND memberID=(?2) AND timerID!=0;`

	SQLSelectGuildRunningTimers string = `
	SELECT memberID, endTime, timerID
	FROM mutes
	WHERE guildID=(?1) AND timerID!=0;`

	SQLDeleteMute string = `
	DELETE FROM mutes
	WHERE guildID=(?1) AND memberID=(?2);`

	SQLSelectMuteRoles string = `
	SELECT muteRole, removeRole
	FROM muteRoles
	WHERE guildID=(?1);`

	SQLUpsertMuteRolesMute string = `
	INSERT INTO muteRoles(guildID, muteRole)
	VALUES (?1, ?2)
	ON CONFLICT(guildID) DO
	UPDATE SET muteRole=(?2)
	WHERE guildID=(?1);`

	SQLUpsertMuteRolesRemove string = `
	INSERT INTO muteRoles(guildID, removeRole)
	VALUES (?1, ?2)
	ON CONFLICT(guildID) DO
	UPDATE SET removeRole=(?2)
	WHERE guildID=(?1);`
)
