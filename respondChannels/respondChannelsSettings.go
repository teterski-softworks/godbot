package respondChannels

import (
	"database/sql"
	"fmt"
	"time"

	dg "github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
	"gitlab.com/teterski-softworks/godbot/channels"
	"gitlab.com/teterski-softworks/godbot/db"
	"gitlab.com/teterski-softworks/godbot/durations"
	"gitlab.com/teterski-softworks/godbot/embeds"
	"gitlab.com/teterski-softworks/godbot/guilds"
	"gitlab.com/teterski-softworks/godbot/roles"
)

var defaultDuration = time.Minute * 2

//Settings represents a set of respond channel settings for the guild.
type Settings struct {
	GuildID string
	time.Duration

	ChannelIDs    []string
	IgnoreRoleIDs []string
}

//New returns a new set of respond channel Settings.
func NewSettings(guildID string, duration time.Duration) Settings {
	return Settings{
		GuildID:  guildID,
		Duration: duration,
	}
}

//RetrieveSettings returns a Settings from the DB.
func RetrieveSettings(DB db.Reader, guildID string) (Settings, error) {
	s := Settings{GuildID: guildID}

	row, err := DB.QueryRow(SQLSelectRespondChannelsSettings, guildID)
	if err != nil {
		return s, err
	}
	if err = row.Scan(&s.Duration); err != nil && !errors.Is(err, sql.ErrNoRows) {
		return s, errors.WithStack(err)
	}

	rows, err := DB.Query(SQLSelectGuildRespondChannels, guildID)
	if err != nil {
		return s, err
	}
	var channelID string
	for rows.Next() {
		if err = rows.Scan(&channelID); err != nil {
			return s, errors.WithStack(err)
		}
		s.ChannelIDs = append(s.ChannelIDs, channelID)
	}

	rows, err = DB.Query(SQLSelectGuildRespondChannelsIgnoreRoles, guildID)
	if err != nil {
		return s, err
	}
	var roleID string
	for rows.Next() {
		if err = rows.Scan(&roleID); err != nil {
			return s, errors.WithStack(err)
		}
		s.IgnoreRoleIDs = append(s.IgnoreRoleIDs, roleID)
	}

	return s, nil
}

//Store saves Settings to the DB.
func (s Settings) Store(DB db.ReadWriter) error {
	t := DB.NewTransaction()

	if err := guilds.New(s.GuildID).Store(t); err != nil {
		return err
	}

	t.Queue(SQLUpsertRespondChannelsSettings, s.GuildID, s.Duration)
	t.Queue(SQLDeleteGuildRespondChannels, s.GuildID)
	t.Queue(SQLDeleteGuildRespondChannelsIgnoreRoles, s.GuildID)
	for _, channelID := range s.ChannelIDs {
		if err := channels.New(s.GuildID, channelID).Store(t); err != nil {
			return err
		}
		t.Queue(SQLInsertRespondChannel, s.GuildID, channelID)
	}
	for _, roleID := range s.IgnoreRoleIDs {
		if err := roles.New(s.GuildID, roleID).Store(t); err != nil {
			return err
		}
		t.Queue(SQLInsertRespondChannelsIgnoreRole, s.GuildID, roleID)
	}
	return t.Commit()
}

//Embed returns a MessageEmbed showing the Settings.
func (s Settings) Embed(DB db.Reader) *dg.MessageEmbed {
	embed := embeds.NewDefault(DB, s.GuildID)
	embed.Title = "✉️ Respond Channel Settings ⚙️"

	if s.Duration == 0 {
		s.Duration = defaultDuration
	}
	DurationField := &dg.MessageEmbedField{
		Name:   "🕒 Duration",
		Value:  "The duration to wait before deleting a message: `" + durations.String(s.Duration) + "`",
		Inline: true,
	}

	channelsField := &dg.MessageEmbedField{
		Name:  "#️⃣ Channels",
		Value: "Channels listed here will delete messages which do not mention or reply to other users.\n ",
	}
	if len(s.ChannelIDs) != 0 {
		for i, channelID := range s.ChannelIDs {
			channelsField.Value += fmt.Sprintf("%d. %s\n", i+1, channels.Mention(channelID))
		}
	} else {
		channelsField.Value += "`None set`"
	}

	ignoredRolesField := &dg.MessageEmbedField{
		Name:  "❎ Ignored Roles",
		Value: "Roles listed here will not have their messages deleted.\n",
	}
	if len(s.IgnoreRoleIDs) != 0 {
		for i, roleID := range s.IgnoreRoleIDs {
			ignoredRolesField.Value += fmt.Sprintf("%d. %s\n", i+1, roles.Mention(roleID))
		}
	} else {
		ignoredRolesField.Value += "`None set`"
	}

	embed.Fields = append(embed.Fields, DurationField, channelsField, ignoredRolesField)
	return &embed.MessageEmbed
}
