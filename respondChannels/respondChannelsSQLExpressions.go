package respondChannels

var (
	SQLSelectRespondChannelsSettings = `
	SELECT duration	FROM respondChannelsSettings WHERE guildID =?1;`

	SQLSelectGuildRespondChannelsIgnoreRoles = `
	SELECT roleID FROM respondChannelsIgnoreRoles WHERE guildID =?1;`

	SQLSelectGuildRespondChannels = `
	SELECT channelID FROM respondChannels WHERE guildID =?1;`

	SQLInsertRespondChannel = `
	INSERT OR IGNORE INTO respondChannels(guildID, channelID) VALUES (?1,?2);`

	SQLInsertRespondChannelsIgnoreRole = `
	INSERT OR IGNORE INTO respondChannelsIgnoreRoles(guildID, roleID) VALUES (?1,?2);`

	SQLUpsertRespondChannelsSettings = `
	INSERT INTO respondChannelsSettings(guildID, duration)
	VALUES (?1, ?2)
	ON CONFLICT DO
	UPDATE SET

	duration =?2

	WHERE guildID =?1;`

	SQLDeleteGuildRespondChannelsIgnoreRoles = `
	DELETE FROM respondChannelsIgnoreRoles WHERE guildID =?1;`

	SQLDeleteGuildRespondChannels = `
	DELETE FROM respondChannels WHERE guildID =?1;`
)
