package respondChannels

import "gitlab.com/teterski-softworks/godbot/db"

func CreateDatabaseTables(DB db.ReadWriter) error {
	t := DB.NewTransaction()
	t.Queue(SQLCreateTableRespondChannelsSettings)
	t.Queue(SQLCreateTableRespondChannelsIgnoreRoles)
	t.Queue(SQLCreateTableRespondChannels)
	return t.Commit()
}

var (
	SQLCreateTableRespondChannelsSettings string = `
	CREATE TABLE IF NOT EXISTS respondChannelsSettings(
		guildID    TEXT NOT NULL PRIMARY KEY REFERENCES guilds(guildID) ON DELETE CASCADE,
		duration   INT  NOT NULL
	);`

	SQLCreateTableRespondChannelsIgnoreRoles string = `
	CREATE TABLE IF NOT EXISTS respondChannelsIgnoreRoles(
		guildID    TEXT NOT NULL REFERENCES respondChannelsSettings(guildID) ON DELETE CASCADE,
		roleID     TEXT NOT NULL,

		PRIMARY KEY (guildID, roleID),
		FOREIGN KEY (guildID, roleID) REFERENCES roles(guildID, roleID) ON DELETE CASCADE
	);`

	SQLCreateTableRespondChannels string = `
	CREATE TABLE IF NOT EXISTS respondChannels(
		guildID    TEXT NOT NULL REFERENCES respondChannelsSettings(guildID) ON DELETE CASCADE,
		channelID  TEXT NOT NULL REFERENCES channels(channelID) ON DELETE CASCADE,

		PRIMARY KEY (guildID, channelID)
	);`
)
