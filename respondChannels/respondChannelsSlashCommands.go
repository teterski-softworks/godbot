package respondChannels

import (
	dg "github.com/bwmarrin/discordgo"
	"gitlab.com/teterski-softworks/godbot/commands"
	"gitlab.com/teterski-softworks/godbot/db"
	"gitlab.com/teterski-softworks/godbot/durations"
	"gitlab.com/teterski-softworks/godbot/states"
)

//SlashCommandAdminRespondChannels - An admin-only command group for gag related commands.
var SlashCommandAdminRespondChannels = commands.New(
	&dg.ApplicationCommand{
		Type:        dg.ChatApplicationCommand,
		Name:        "admin_respond_channels",
		Description: "Admin-only respond channel commands.",
		Options: []*dg.ApplicationCommandOption{
			SlashCommandAdminRespondChannelsSettings,
		},
	},
	false,
	func(intr states.Interaction, options ...*dg.ApplicationCommandInteractionDataOption) (err error) {

		if err := intr.Acknowledge(); err != nil {
			return err
		}

		switch options[0].Name {
		case SlashCommandAdminRespondChannelsSettings.Name:
			return SlashAdminRespondChannelsSet(intr, options[0].Options)
		}
		return intr.EditResponseEmbeds(nil, states.ResponseGenericError(nil))
	},
)

//SlashCommandAdminRespondChannelsSettings - An admin-only subcommand for viewing and updating gag settings.
var SlashCommandAdminRespondChannelsSettings = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionSubCommand,
	Name:        "settings",
	Description: "View and change settings for respond/mention-only channels.",
	Options: []*dg.ApplicationCommandOption{
		{
			Type:        dg.ApplicationCommandOptionString,
			Name:        "duration",
			Description: "The duration after which messages get deleted in channels, in the format `0w0d0h0m0s`.",
		},
		{
			Type:         dg.ApplicationCommandOptionChannel,
			Name:         "channel_add",
			Description:  "The channel in which to delete messages if they are not a reply/mention.",
			ChannelTypes: []dg.ChannelType{dg.ChannelTypeGuildText},
		},
		{
			Type:        dg.ApplicationCommandOptionInteger,
			Name:        "channel_remove",
			Description: "The index of the channel to remove from the list.",
			MinValue:    &one,
		},
		{
			Type:         dg.ApplicationCommandOptionRole,
			Name:         "ignore_role_add",
			Description:  "The role whose messages will not be deleted.",
			ChannelTypes: []dg.ChannelType{dg.ChannelTypeGuildText},
		},
		{
			Type:        dg.ApplicationCommandOptionInteger,
			Name:        "ignore_role_remove",
			Description: "The index of the role to remove from the ignored roles list.",
			MinValue:    &one,
		},
	},
}
var one float64 = 1

//SlashAdminRespondChannelsSet views and updates a guild's gag settings.
func SlashAdminRespondChannelsSet(intr states.Interaction, options []*dg.ApplicationCommandInteractionDataOption) (err error) {

	settings, err := RetrieveSettings(intr.DB, intr.GuildID)
	if err != nil {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}
	clear := false
	for _, option := range options {
		switch option.Name {
		case "clear_settings":
			settings = NewSettings(intr.GuildID, defaultDuration)
			clear = true
		}
	}
	if !clear {
		for _, option := range options {
			switch option.Name {
			case "duration":
				duration, err := durations.Parse(option.StringValue())
				if err != nil {
					return intr.EditResponseEmbeds(nil, durations.ResponseInvalidDurationError())
				}
				settings.Duration = duration
			case "channel_add":
				settings.ChannelIDs = append(settings.ChannelIDs, option.ChannelValue(intr.Session).ID)
			case "channel_remove":
				index := int(option.IntValue()) - 1
				settings.ChannelIDs = append(settings.ChannelIDs[:index], settings.ChannelIDs[index+1:]...)
			case "ignore_role_add":
				settings.IgnoreRoleIDs = append(settings.IgnoreRoleIDs, option.RoleValue(intr.Session, intr.GuildID).ID)
			case "ignore_role_remove":
				index := int(option.IntValue()) - 1
				settings.IgnoreRoleIDs = append(settings.IgnoreRoleIDs[:index], settings.IgnoreRoleIDs[index+1:]...)
			}
		}
	}
	if err := settings.Store(intr.DB); err != nil {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}
	settings, err = RetrieveSettings(intr.DB, intr.GuildID)
	if err != nil {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}

	return intr.EditResponseEmbeds(nil, settings.Embed(intr.DB))
}
