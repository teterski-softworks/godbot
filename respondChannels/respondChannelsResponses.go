package respondChannels

import (
	"fmt"
	"time"

	dg "github.com/bwmarrin/discordgo"
	"gitlab.com/teterski-softworks/godbot/embeds"
)

var (
	ResponseMustRespond = func(endTime time.Time) *dg.MessageEmbed {
		embed := embeds.New("", "", "")
		embed.Title = "❌ Must Reply or Mention Another Member"
		embed.Color = embeds.ColorRed
		embed.Description = "Messages in this channel must either reply to or mention another member.\n"
		embed.Description += fmt.Sprintf("Your message will automatically be deleted <t:%d:R>.", endTime.Unix())
		embed.Footer.Text = "Read channel description for more info."
		embed.Timestamp = time.Now().Format(time.RFC3339)
		return &embed.MessageEmbed
	}
)
