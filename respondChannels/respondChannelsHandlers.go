package respondChannels

import (
	"database/sql"
	"time"

	"github.com/pkg/errors"
	"gitlab.com/teterski-softworks/godbot/members"
	"gitlab.com/teterski-softworks/godbot/states"
	"gitlab.com/teterski-softworks/godbot/webhooks"
)

//MessageCreateHandler checks if the message author is gagged and obfuscates their text according to the applied gag type.
func MessageCreateHandler(intr states.Interaction, webhookManager webhooks.Manager) (err error) {

	//Check if channel is marked as a respond channel
	settings, err := RetrieveSettings(intr.DB, intr.GuildID)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil
		}
		return err
	}
	match := false
	for _, channelID := range settings.ChannelIDs {
		if channelID == intr.Message.ChannelID {
			match = true
			break
		}
	}
	if !match {
		return nil
	}

	//Check if the message is a reply
	if intr.Message.ReferencedMessage != nil {
		//Check that user isn't replying or only mentioning themselves
		if intr.Message.ReferencedMessage.Author != intr.Message.Author {
			return nil
		}
	}

	//Check if the message has any mentions
	if len(intr.Message.Mentions) != 0 {
		//Check that the user isn't only mentioning themselves
		for _, mention := range intr.Message.Mentions {
			if mention.ID != intr.Message.Author.ID {
				return nil
			}
		}
	}

	//Check if author is in ignored roles
	member, _, err := members.GetMemberOrUserFromID(intr.Session, intr.GuildID, intr.Message.Author.ID)
	if err != nil {
		return err
	} else if member != nil {
		for _, roleID := range settings.IgnoreRoleIDs {
			for _, role := range member.Roles {
				if role == roleID {
					return
				}
			}
		}
	}

	//Respond to the user that they must mention/reply to another user.
	warning, err := intr.SendEmbeds(intr.ChannelID, intr.Message.Reference(), nil, ResponseMustRespond(time.Now().Add(settings.Duration)))
	if err != nil {
		return err
	}

	//Start timer
	e := make(chan error, 1)
	defer close(e)
	time.AfterFunc(settings.Duration, func() {
		e <- intr.ChannelMessagesBulkDelete(intr.ChannelID, []string{intr.Message.ID, warning.ID})
	})

	return <-e
}

//ChannelDeleteHandler checks if a channel used in respond channel settings was deleted.
func ChannelDeleteHandler(s *states.State, guildID string, channelID string) error {
	settings, err := RetrieveSettings(s.DB, guildID)
	if err != nil {
		return err
	}

	var channels []string
	for _, ID := range settings.ChannelIDs {
		if ID == channelID {
			continue
		}
		channels = append(channels, ID)
	}
	settings.ChannelIDs = channels

	return settings.Store(s.DB)
}
