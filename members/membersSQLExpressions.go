package members

var (
	SQLSelectMember string = `
	SELECT guildID, memberID
	FROM members
	WHERE guildID=(?1) AND memberID=(?2);`

	SQLInsertMemberID string = `
	INSERT OR IGNORE INTO members(guildID, memberID)
	VALUES (?1,?2);`

	SQLSelectAllGuildMembers string = `
	SELECT memberID
	FROM members
	WHERE guildID=(?1);`

	SQLDeleteMember string = `
	DELETE FROM members
	WHERE guildID=(?1) AND memberID=(?2);`

	SQLCleanMembers string = `
	DELETE FROM members WHERE guildID=(?1) AND
	memberID NOT IN (SELECT memberID FROM bans WHERE guildID=(?1)) AND
	memberID NOT IN (SELECT memberID FROM counterPerMember WHERE guildID=(?1)) AND
	memberID NOT IN (SELECT memberID FROM invitePeriods WHERE guildID=(?1)) AND
	memberID NOT IN (SELECT memberID FROM inviteRecords WHERE guildID=(?1)) AND
	memberID NOT IN (SELECT inviterID FROM invites WHERE guildID=(?1)) AND
	memberID NOT IN (SELECT memberID FROM levelingRecords WHERE guildID=(?1)) AND
	memberID NOT IN (SELECT memberID FROM inviteRecords WHERE guildID=(?1)) AND
	memberID NOT IN (SELECT memberID FROM mutes WHERE guildID=(?1)) AND
	memberID NOT IN (SELECT memberID FROM warnings WHERE guildID=(?1));`
)
