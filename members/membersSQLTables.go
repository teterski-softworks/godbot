package members

import "gitlab.com/teterski-softworks/godbot/db"

//CreateDatabaseTables creates the required database tables for the members package.
func CreateDatabaseTables(DB db.ReadWriter) error {
	t := DB.NewTransaction()
	t.Queue(SQLCreateTableMembers)
	return t.Commit()
}

var (
	SQLCreateTableMembers string = `
	CREATE TABLE IF NOT EXISTS members(
		guildID       TEXT NOT NULL REFERENCES guilds(guildID) ON DELETE CASCADE,
		memberID      TEXT NOT NULL,
		
		PRIMARY KEY (guildID, memberID)
	);`
)
