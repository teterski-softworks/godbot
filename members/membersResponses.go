package members

import (
	"time"

	dg "github.com/bwmarrin/discordgo"
)

var (
	ResponseMemberNotFound = func(subID string) *dg.MessageEmbed {
		//TODO: use embeds package, but resolve import cycle first
		embed := dg.MessageEmbed{}
		embed.Title = "❌ Member Not Found"
		embed.Color = 0xdd2e44
		embed.Description = Mention(subID) + " is not a member of the server."
		embed.Timestamp = time.Now().Format(time.RFC3339)
		return &embed
	}
)
