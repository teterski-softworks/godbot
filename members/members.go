package members

import (
	"database/sql"
	"fmt"

	dg "github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
	"gitlab.com/teterski-softworks/godbot/db"
	"gitlab.com/teterski-softworks/godbot/guilds"
)

//Member represents a server member.
type Member struct {
	GuildID  string
	MemberID string
}

//New returns a new Member for the giving guildID and memberID.
func New(guildID string, memberID string) Member {
	return Member{
		GuildID:  guildID,
		MemberID: memberID,
	}
}

//Retrieve returns a Member if one exists in the database.
func Retrieve(DB db.Reader, guildID string, memberID string) (Member, error) {
	var member Member
	row, err := DB.QueryRow(SQLSelectMember, guildID, memberID)
	if err != nil {
		return member, err
	}
	if err = row.Scan(&member.GuildID, &member.MemberID); err != nil {
		return member, errors.WithStack(err)
	}
	return member, nil
}

//RetrieveAll returns a list of all member IDs in the database.
func RetrieveAll(DB db.Reader, guildID string) ([]string, error) {

	var members []string
	var err error
	var rows *sql.Rows
	if rows, err = DB.Query(SQLSelectAllGuildMembers, guildID); err != nil {

		return members, err
	}

	var id string
	for rows.Next() {

		if err = rows.Scan(&id); err != nil {
			return nil, errors.WithStack(err)
		}
		members = append(members, id)
	}

	return members, nil
}

//Store saves the Member to the DB.
func (m Member) Store(DB db.ReadWriter) error {

	t := DB.NewTransaction()

	if err := guilds.New(m.GuildID).Store(t); err != nil {
		return err
	}

	t.Queue(SQLInsertMemberID, m.GuildID, m.MemberID)

	return t.Commit()
}

//Delete removes a member from the DB.
//If the member is already not in the provided database, then found returns false. Otherwise, returns true.
func (m Member) Delete(DB db.ReadWriter) (found bool, err error) {

	if _, err = Retrieve(DB, m.GuildID, m.MemberID); err != nil {
		return false, err
	}
	if err = DB.QueueAndCommit(SQLDeleteMember, m.GuildID, m.MemberID); err != nil {
		return false, err
	}
	return true, nil
}

//Mention returns a mention string for the provided user ID.
func Mention(ID string) string {
	return fmt.Sprintf("<@%s>", ID)
}

//GetMemberOrUserFromID returns either *dg.Member correlated to the given memberID if able to, or if not able to, returns a *dg.User instead.
func GetMemberOrUserFromID(session *dg.Session, guildID string, userID string) (member *dg.Member, user *dg.User, err error) {
	member, err = session.GuildMember(guildID, userID)
	if err == nil {
		return member, user, err
	}
	user, err = session.User(userID)
	return member, user, err
}

//HasRole returns true if the given member has the provided role.
func HasRole(member *dg.Member, roleID string) bool {
	hasRole := false
	for _, r := range member.Roles {
		if r == roleID {
			hasRole = true
			break
		}
	}
	return hasRole
}
