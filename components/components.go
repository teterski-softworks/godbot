package components

import (
	"encoding/json"

	dg "github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
)

//NewConfirmationMenu returns an action row with two buttons: a Yes button and a No button.
func NewConfirmationMenu(customIDYes string, customIDNo string) dg.ActionsRow {

	var actionRow dg.ActionsRow
	yesButton := dg.Button{
		Label:    "Yes",
		Style:    dg.SuccessButton,
		CustomID: customIDYes,
	}
	noButton := dg.Button{
		Label:    "No",
		Style:    dg.DangerButton,
		CustomID: customIDNo,
	}
	actionRow.Components = append(actionRow.Components, yesButton, noButton)
	return actionRow
}

//UnmarshalModalInput unmarshal an ActionsRow row from a modal window, containing a text input,
//and returns the TextInput struct contained within.
func UnmarshalModalInput(component dg.MessageComponent) (dg.TextInput, error) {

	var textInput dg.TextInput

	rawBytes, err := component.MarshalJSON()
	if err != nil {
		return textInput, errors.WithStack(err)
	}

	var actionsRow dg.ActionsRow
	if err := json.Unmarshal(rawBytes, &actionsRow); err != nil {
		return textInput, errors.WithStack(err)
	}

	rawBytes, err = actionsRow.Components[0].MarshalJSON()
	if err != nil {
		return textInput, errors.WithStack(err)
	}

	if err := json.Unmarshal(rawBytes, &textInput); err != nil {
		return textInput, errors.WithStack(err)
	}

	return textInput, nil
}
