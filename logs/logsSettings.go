package logs

import (
	"database/sql"
	"fmt"

	dg "github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
	"gitlab.com/teterski-softworks/godbot/channels"
	"gitlab.com/teterski-softworks/godbot/db"
	"gitlab.com/teterski-softworks/godbot/embeds"
	"gitlab.com/teterski-softworks/godbot/guilds"
	"gitlab.com/teterski-softworks/godbot/states"
)

//setting represents a log feature setting
//where channelID is the ID to log the feature too if Enabled is set to true.
type setting struct {
	ChannelID string
	enabled   bool
}

//Edit updates a setting from the given ApplicationCommandInteractionDataOptions.
func (s *setting) Edit(options []*dg.ApplicationCommandInteractionDataOption) {
	for _, option := range options {
		switch option.Name {
		case "channel":
			s.ChannelID = option.ChannelValue(nil).ID
		case "enabled":
			s.enabled = option.BoolValue()
		}
	}
}

//Settings represents a set of log options for a guild.
type Settings struct {
	GuildID string

	MessageEdits   setting
	MessageDeletes setting

	Mutes setting
	Warns setting
	Bans  setting

	Joins setting

	IgnoreChannelIDs []string
}

//RetrieveSettings returns saved LogSettings for the guild from the DB.
func RetrieveSettings(DB db.Reader, guildID string) (Settings, error) {
	var settings Settings
	settings.GuildID = guildID
	row, err := DB.QueryRow(SQLSelectLogSettings, guildID)
	if err != nil {
		return settings, err
	}
	if err = row.Scan(
		&settings.MessageEdits.ChannelID,
		&settings.MessageEdits.enabled,

		&settings.MessageDeletes.ChannelID,
		&settings.MessageDeletes.enabled,

		&settings.Mutes.ChannelID,
		&settings.Mutes.enabled,

		&settings.Warns.ChannelID,
		&settings.Warns.enabled,

		&settings.Bans.ChannelID,
		&settings.Bans.enabled,

		&settings.Joins.ChannelID,
		&settings.Joins.enabled,
	); !errors.Is(err, sql.ErrNoRows) && err != nil {
		return settings, errors.WithStack(err)
	}

	rows, err := DB.Query(SQLSelectLogIgnoreChannels, guildID)
	if err != nil {
		return settings, err
	}

	for rows.Next() {
		var channelID string
		if err = rows.Scan(&channelID); errors.Is(err, sql.ErrNoRows) {
			if err = rows.Close(); err != nil {
				return settings, errors.WithStack(err)
			}
			continue
		} else if err != nil {
			return settings, errors.WithStack(err)
		}

		settings.IgnoreChannelIDs = append(settings.IgnoreChannelIDs, channelID)
	}

	return settings, nil
}

//Store saves LogSettings to the DB.
func (settings Settings) Store(DB db.ReadWriter) error {
	t := DB.NewTransaction()

	if err := guilds.New(settings.GuildID).Store(t); err != nil {
		return err
	}

	for _, channelID := range []string{
		settings.MessageEdits.ChannelID,
		settings.MessageDeletes.ChannelID,
		settings.Mutes.ChannelID,
		settings.Warns.ChannelID,
		settings.Bans.ChannelID,
		settings.Joins.ChannelID,
	} {
		if err := channels.New(settings.GuildID, channelID).Store(t); err != nil {
			return err
		}
	}

	t.Queue(SQLUpsertLogSettings,
		settings.GuildID,

		settings.MessageEdits.ChannelID,
		settings.MessageEdits.enabled,

		settings.MessageDeletes.ChannelID,
		settings.MessageDeletes.enabled,

		settings.Mutes.ChannelID,
		settings.Mutes.enabled,

		settings.Warns.ChannelID,
		settings.Warns.enabled,

		settings.Bans.ChannelID,
		settings.Bans.enabled,

		settings.Joins.ChannelID,
		settings.Joins.enabled,
	)

	t.Queue(SQLDeleteLogIgnoreChannels, settings.GuildID)
	for _, channelID := range settings.IgnoreChannelIDs {
		t.Queue(SQLInsertLogIgnoreChannel, settings.GuildID, channelID)
	}
	return t.Commit()
}

//MessageLoggingEnabled returns true if any feature requiring message logging is enabled, has a channel set for it, and has the required permissions to log to the channel.
func (settings Settings) MessageLoggingEnabled(s *states.State) (bool, error) {

	messageEditLoggingEnabled, err := settings.MessageEdits.LoggingEnabled(s)
	if err != nil {
		return false, err
	}
	messageDeleteLoggingEnabled, err := settings.MessageDeletes.LoggingEnabled(s)
	if err != nil {
		return false, err
	}

	return messageEditLoggingEnabled || messageDeleteLoggingEnabled, nil
}

//Ignored returns true if a channel is ignored in the LogSettings.
func (settings Settings) Ignored(channel *dg.Channel) bool {

	for _, ignoredChannel := range settings.IgnoreChannelIDs {
		if channel.ID == ignoredChannel || channel.ParentID == ignoredChannel {
			return true
		}
	}

	return false
}

//LoggingEnabled returns true if logging is enabled and the bot has the
//required permissions to log to the set channel in the logSettingOption.
//Returns false otherwise.
func (option setting) LoggingEnabled(s *states.State) (bool, error) {

	loggingEnabled := option.enabled && option.ChannelID != ""
	if !loggingEnabled {
		return false, nil
	}

	channelPermissions, err := s.UserChannelPermissions(s.State.User.ID, option.ChannelID)
	if err != nil {
		return false, errors.WithStack(err)
	}
	if channelPermissions&dg.PermissionAdministrator == dg.PermissionAdministrator {
		return true, nil
	}
	if (channelPermissions&dg.PermissionSendMessages == dg.PermissionSendMessages) &&
		(channelPermissions&dg.PermissionViewChannel == dg.PermissionViewChannel) {
		return true, nil
	}

	return false, nil
}

//Embed returns an embed to showing the LogSettings.
func (settings Settings) Embed(intr states.Interaction) *dg.MessageEmbed {
	embed := embeds.NewDefault(intr.DB, intr.GuildID)
	embed.Title = "📜 Log Settings ⚙️"

	embed.Description += "\\▪️ In order to be able to log message edits/deletions, a record of each message needs to be saved (called a log record), "
	embed.Description += "because Discord does not provide what a message looked like before it was edited/deleted. "
	embed.Description += "All log records are encrypted before being saved and do not contain personally identifiable information.\n\n"

	embed.Description += "\\▪️ Changing log settings or permissions that can affect the bot's ability to log channels will schedule a log record update. "
	embed.Description += "This will either delete log records for channels no longer able to be logged by the bot, "
	embed.Description += "or create log records for all the messages in channels not previously logged.\n\n"

	embed.Description += "\\▪️ Once an update is scheduled, the bot will wait some time before applying the updates, "
	embed.Description += "in case any more updates are scheduled. Each time an update is scheduled, the wait timer is reset.\n\n"

	embed.Description += "\\▪️ When a log record update is running, the bot will wait for the update to finish before scheduling any more log record updates.\n\n"

	status := &dg.MessageEmbedField{
		Name:  "✅ Status:",
		Value: "Log records are up to date!",
	}
	if intr.LogRecordSchedulers.Get(intr.GuildID).Updating() {
		status.Name = "🔄 Status:"
		status.Value = "Log records update in progress..."
	} else if intr.LogRecordSchedulers.Get(intr.GuildID).UpdateScheduled() {
		status.Name = "⌛ Status:"
		status.Value = fmt.Sprintf("Next log record update scheduled <t:%d:R>.", intr.LogRecordSchedulers.Get(intr.GuildID).NextUpdateOn().Unix())
	}

	messageEdits := &dg.MessageEmbedField{
		Name:   "📝 Message Edits:",
		Value:  logSettingInfo(settings.MessageEdits),
		Inline: true,
	}

	messageDeletes := &dg.MessageEmbedField{
		Name:   "🗑️ Message Deletes:",
		Value:  logSettingInfo(settings.MessageDeletes),
		Inline: true,
	}

	joins := &dg.MessageEmbedField{
		Name:   "🚪 Joins/Leaves:",
		Value:  logSettingInfo(settings.Joins),
		Inline: true,
	}

	mutes := &dg.MessageEmbedField{
		Name:   "🔇 Mutes:",
		Value:  logSettingInfo(settings.Mutes),
		Inline: true,
	}

	warns := &dg.MessageEmbedField{
		Name:   "⚠️ Warns:",
		Value:  logSettingInfo(settings.Warns),
		Inline: true,
	}

	bans := &dg.MessageEmbedField{
		Name:   "⛔ Bans:",
		Value:  logSettingInfo(settings.Bans),
		Inline: true,
	}

	embed.Fields = append(embed.Fields, status, messageEdits, messageDeletes, joins, mutes, warns, bans)

	if len(settings.IgnoreChannelIDs) != 0 {
		ignore := &dg.MessageEmbedField{
			Name: "🔕 Ignored Channels/Categories:",
		}
		for _, channelID := range settings.IgnoreChannelIDs {
			ignore.Value += channels.Mention(channelID) + "\n"
		}
		embed.Fields = append(embed.Fields, ignore)
	}

	return &embed.MessageEmbed
}

//logSettingInfo returns a formatted string representation of a log's setting's data.
func logSettingInfo(option setting) string {
	enabled := "Enabled: ❌"
	if option.enabled {
		enabled = "Enabled: ✅"
	}
	channel := "Channel: `None`"
	if option.ChannelID != "" {
		channel = fmt.Sprintf("Channel: %s", channels.Mention(option.ChannelID))
	}
	return enabled + "\n" + channel
}
