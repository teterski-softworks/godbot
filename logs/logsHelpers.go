package logs

import (
	"time"

	dg "github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
	"gitlab.com/teterski-softworks/godbot/auditLogs"
	"gitlab.com/teterski-softworks/godbot/db"
	"gitlab.com/teterski-softworks/godbot/states"
)

//TODO: what if log channel is not accessible by the bot
//TODO: ignore message edits of messages from the bot

//LogBan logs a ban embed to the ban logging channel (if set and ban logging is enabled).
func LogBan(s *states.State, guildID string, embed *dg.MessageEmbed) error {
	//Get settings for the guild
	settings, err := RetrieveSettings(s.DB, guildID)
	if err != nil {
		return err
	}
	if enabled, err := settings.Bans.LoggingEnabled(s); err != nil {
		return err
	} else if !enabled {
		return nil
	}

	if _, err := s.ChannelMessageSendEmbed(settings.Bans.ChannelID, embed); err != nil {
		return errors.WithStack(err)
	}

	return nil
}

//LogWarning logs a warning embed to the warning logging channel (if set and warning logging is enabled).
func LogWarning(s *states.State, guildID string, embed *dg.MessageEmbed) error {
	//Get settings for the guild
	settings, err := RetrieveSettings(s.DB, guildID)
	if err != nil {
		return err
	}
	if enabled, err := settings.Warns.LoggingEnabled(s); err != nil {
		return err
	} else if !enabled {
		return nil
	}

	if _, err := s.ChannelMessageSendEmbed(settings.Warns.ChannelID, embed); err != nil {
		return errors.WithStack(err)
	}

	return nil
}

//LogMute logs a mute embed to the mute logging channel (if set and mute logging is enabled).
func LogMute(s *states.State, guildID string, embed *dg.MessageEmbed) error {

	//Get settings for the guild
	settings, err := RetrieveSettings(s.DB, guildID)
	if err != nil {
		return err
	}
	if enabled, err := settings.Mutes.LoggingEnabled(s); err != nil {
		return err
	} else if !enabled {
		return nil
	}

	if _, err := s.ChannelMessageSendEmbed(settings.Mutes.ChannelID, embed); err != nil {
		return errors.WithStack(err)
	}

	return nil
}

//logEditedMessage logs the deletion of a message.
//Caller needs to check if edit message logging is enabled.
func logEditedMessage(s *states.State, recordBefore LogRecord, message *dg.Message, settings Settings) error {

	//Create a new record to store the modified message data
	recordAfter := New(*message)
	if recordAfter.Content == "" && len(recordAfter.Attachments) == 0 {
		//Discord also sent a message edit event, but the message has been deleted.
		//In this case we do not log the message
		return nil
	}
	recordAfter.Timestamp = time.Now()

	var embeds []*dg.MessageEmbed
	messageLog := recordBefore.editedMessageEmbeds(s, recordAfter)
	if messageLog != nil {
		embeds = append(embeds, messageLog)
	}
	attachmentLog, files, indexes, err := recordBefore.deletedAttachmentsEmbeds(s, recordAfter, recordBefore.MemberID)
	if err != nil {
		return err
	}
	if attachmentLog != nil {
		embeds = append(embeds, attachmentLog)
	}
	//*NOTE: Preserve the same attachment data for the time being
	//*		 This is to ensure that attachment data is preserved if only text is edited.
	recordAfter.Attachments = recordBefore.Attachments

	if len(indexes) != 0 {
		recordAfter.RemoveAttachments(indexes...)
	}
	if err := recordAfter.Store(s.DB); err != nil {
		return err
	}

	if embeds != nil {
		if _, err := s.SendEmbeds(settings.MessageEdits.ChannelID, nil, nil, embeds...); err != nil {
			return errors.WithStack(err)
		}
	}
	if len(files) != 0 {
		message := &dg.MessageSend{Files: files}
		message.Files = files
		if _, err := s.ChannelMessageSendComplex(settings.MessageEdits.ChannelID, message); err != nil {
			return errors.WithStack(err)
		}
	}

	return nil
}

//logDeletedMessage logs the deletion of a message.
//Caller needs to check if delete message logging is enabled.
func logDeletedMessage(s *states.State, recordBefore LogRecord, settings Settings) error {

	deletedByID, err := auditLogs.GetMessageDeleteInfo(s, recordBefore.GuildID, recordBefore.ChannelID, recordBefore.MemberID)
	if err != nil {
		return err
	}

	var recordAfter LogRecord
	recordAfter.Message = recordBefore.Message
	recordAfter.MemberID = recordBefore.MemberID
	recordAfter.Timestamp = time.Now()

	var embeds []*dg.MessageEmbed
	messageLog := recordBefore.deletedMessageEmbeds(s, deletedByID)
	if messageLog != nil {
		embeds = append(embeds, messageLog)
	}
	attachmentLog, files, _, err := recordBefore.deletedAttachmentsEmbeds(s, recordAfter, recordBefore.MemberID)
	if err != nil {
		return err
	}
	if attachmentLog != nil {
		embeds = append(embeds, attachmentLog)
	}

	if embeds != nil {
		if _, err := s.SendEmbeds(settings.MessageEdits.ChannelID, nil, nil, embeds...); err != nil {
			return errors.WithStack(err)
		}
	}
	if len(files) != 0 {
		message := &dg.MessageSend{Files: files}
		message.Files = files
		if _, err := s.ChannelMessageSendComplex(settings.MessageEdits.ChannelID, message); err != nil {
			return errors.WithStack(err)
		}
	}

	_, err = recordBefore.Delete(s.DB)
	return err
}

//recordProgress returns the IDs of the earliest message with a logRecord, and the latest message with a logRecord.
func recordProgress(DB db.Reader, guildID string, channelID string) (earliest string, latest string, err error) {

	row, err := DB.QueryRow(SQLSelectLogRecordProgress, guildID, channelID)
	if err != nil {
		return earliest, latest, err
	}
	if err = row.Scan(&earliest, &latest); err != nil {
		return earliest, latest, errors.WithStack(err)
	}
	return earliest, latest, nil
}

//getGuildLogRecordChannels returns IDs of channels which have saved LogRecords in the DB.
func getGuildLogRecordChannels(DB db.Reader, guildID string) ([]string, error) {

	var channels []string
	rows, err := DB.Query(SQLSelectLogRecordChannels, guildID)
	if err != nil {
		return channels, err
	}

	for rows.Next() {
		var channel string
		if err := rows.Scan(&channel); err != nil {
			return channels, err
		}

		channels = append(channels, channel)
	}
	return channels, nil
}
