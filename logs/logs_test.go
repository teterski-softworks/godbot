package logs_test

import (
	"testing"
	"time"

	dg "github.com/bwmarrin/discordgo"
	"gitlab.com/teterski-softworks/godbot/db"
	"gitlab.com/teterski-softworks/godbot/logs"
	"gitlab.com/teterski-softworks/godbot/members"
	"gitlab.com/teterski-softworks/godbot/tests"
)

var guildID, channelID, messageID = "guildID", "channelID", "messageID"
var createTables = []func(db.ReadWriter) error{members.CreateDatabaseTables, logs.CreateDatabaseTables}

//TODO
func TestDatabaseAccess(t *testing.T) {

	expected := logs.New(dg.Message{
		GuildID:   guildID,
		ChannelID: channelID,
		ID:        messageID,
		Author: &dg.User{
			ID: "authorID",
		},
		Content: "content",
		Attachments: []*dg.MessageAttachment{
			{URL: "attachment1", ContentType: "image"},
			{URL: "attachment2", ContentType: "image"},
			{URL: "attachment3", ContentType: "image"},
			{URL: "attachment4", ContentType: "image"},
		},
		Timestamp: time.Now(), //.Truncate(time.Second).UTC(),
	})
	//TODO: generate random message values

	updated := expected
	updated.Content = "new content"
	updated.Timestamp = time.Now().Add(time.Hour).Truncate(time.Second).UTC()
	//updated.ImageURLs = []string{} //TODO: this fails the deep equal test

	retrieve := func(DB db.Reader) (logs.LogRecord, error) {
		return logs.Retrieve(DB, guildID, channelID, messageID)
	}

	tests.TestDatabaseAccess(
		t,
		expected, updated,
		createTables,
		retrieve,
	)

}
