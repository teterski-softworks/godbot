package logs

import (
	dg "github.com/bwmarrin/discordgo"
	"gitlab.com/teterski-softworks/godbot/commands"
	"gitlab.com/teterski-softworks/godbot/db"
	"gitlab.com/teterski-softworks/godbot/logs/logSchedulers"
	"gitlab.com/teterski-softworks/godbot/states"
)

//SlashCommandLog - A command group for log related commands.
var SlashCommandLog = commands.New(
	&dg.ApplicationCommand{
		Type:        dg.ChatApplicationCommand,
		Name:        "log",
		Description: "Log related commands. Admin only.",

		Options: []*dg.ApplicationCommandOption{
			slashCommandLogIgnore,
			slashCommandLogSettings,
		},
	},
	false,
	func(intr states.Interaction, options ...*dg.ApplicationCommandInteractionDataOption) (err error) {

		if err := intr.Acknowledge(); err != nil {
			return err
		}

		switch options[0].Name {

		case slashCommandLogIgnore.Name:
			return slashLogIgnore(intr, options[0].Options)

		case slashCommandLogSettings.Name:
			return slashLogSettings(intr, options[0].Options)
		}
		return intr.EditResponseEmbeds(nil, states.ResponseGenericError(nil))
	},
)

//slashCommandLogSettings - A subcommand group for log settings related commands.
var slashCommandLogSettings = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionSubCommandGroup,
	Name:        "settings",
	Description: "Log settings related commands.",
	Options: []*dg.ApplicationCommandOption{
		slashCommandLogSettingsEdit,
		slashCommandLogSettingsView,
	},
}

//slashLogSettings handles log settings.
func slashLogSettings(intr states.Interaction, options []*dg.ApplicationCommandInteractionDataOption) error {
	switch options[0].Name {

	case slashCommandLogSettingsEdit.Name:
		return slashLogSettingsEdit(intr, options[0].Options)

	case slashCommandLogSettingsView.Name:
		return slashLogSettingsView(intr, options[0].Options)
	}
	return intr.EditResponseEmbeds(nil, states.ResponseGenericError(nil))
}

//slashCommandLogSettingsEdit - A subcommand for log setting editing related commands.
var slashCommandLogSettingsEdit = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionSubCommand,
	Name:        "edit",
	Description: "Edits log settings.",
	Options: []*dg.ApplicationCommandOption{
		{
			Type:        dg.ApplicationCommandOptionString,
			Name:        "feature",
			Description: "The log feature to edit the settings for.",
			Choices: []*dg.ApplicationCommandOptionChoice{
				{Name: "All", Value: "all"},
				{Name: "Message Edits", Value: "edits"},
				{Name: "Message Deletes", Value: "deletes"},
				{Name: "Joins/Leaves", Value: "joins"},
				{Name: "Mutes", Value: "mutes"},
				{Name: "Warns", Value: "warns"},
				{Name: "Bans", Value: "bans"},
			},
			Required: true,
		}, {
			Type:        dg.ApplicationCommandOptionBoolean,
			Name:        "enabled",
			Description: "Whether or not this log feature is enabled.",
		}, {
			Type:        dg.ApplicationCommandOptionChannel,
			Name:        "channel",
			Description: "The channel used to for logging.",
			ChannelTypes: []dg.ChannelType{
				dg.ChannelTypeGuildText,
			},
		},
	},
}

//slashLogSettingsEdit edits log settings.
func slashLogSettingsEdit(intr states.Interaction, options []*dg.ApplicationCommandInteractionDataOption) error {

	feature := options[0].StringValue()

	settings, err := RetrieveSettings(intr.DB, intr.GuildID)
	if err != nil {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}
	messageLoggingEnabledBefore, err := settings.MessageLoggingEnabled(intr.State)
	if err != nil {
		return intr.EditResponseEmbeds(err, states.ResponseGenericError(err))
	}

	switch feature {
	case "all":
		settings.MessageEdits.Edit(options[1:])
		settings.MessageDeletes.Edit(options[1:])
		settings.Joins.Edit(options[1:])
		settings.Mutes.Edit(options[1:])
		settings.Warns.Edit(options[1:])
		settings.Bans.Edit(options[1:])
	case "edits":
		settings.MessageEdits.Edit(options[1:])
	case "deletes":
		settings.MessageDeletes.Edit(options[1:])
	case "joins":
		settings.Joins.Edit(options[1:])
	case "mutes":
		settings.Mutes.Edit(options[1:])
	case "warns":
		settings.Warns.Edit(options[1:])
	case "bans":
		settings.Bans.Edit(options[1:])

	}

	if err = settings.Store(intr.DB); err != nil {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}

	messageLoggingEnabledAfter, err := settings.MessageLoggingEnabled(intr.State)
	if err != nil {
		return intr.EditResponseEmbeds(err, states.ResponseGenericError(err))
	}

	//Re-populate / clean database records if record logging state has changed
	if messageLoggingEnabledBefore != messageLoggingEnabledAfter {
		intr.Printf("Logging for G[%s] has been set to [%t]. Update scheduled.", settings.GuildID, messageLoggingEnabledAfter)
		intr.LogRecordSchedulers.Get(intr.GuildID).ScheduleGuildUpdate(logSchedulers.UpdateTypeRefresh)
	}
	return intr.EditResponseEmbeds(nil, settings.Embed(intr))
}

//slashCommandLogSettingsView - A subcommand for viewing log settings.
var slashCommandLogSettingsView = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionSubCommand,
	Name:        "view",
	Description: "Shows the current log settings.",
}

//slashLogSettingsView shows the current log settings.
func slashLogSettingsView(intr states.Interaction, options []*dg.ApplicationCommandInteractionDataOption) error {
	settings, err := RetrieveSettings(intr.DB, intr.GuildID)
	if err != nil {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}
	return intr.EditResponseEmbeds(nil, settings.Embed(intr))
}

//slashCommandLogIgnore - A subcommand group for log ignoring related commands.
var slashCommandLogIgnore = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionSubCommandGroup,
	Name:        "ignore",
	Description: "Log ignore related commands.",
	Options: []*dg.ApplicationCommandOption{
		slashCommandLogIgnoreAdd,
		slashCommandLogIgnoreRemove,
	},
}

//slashLogIgnore handles log ignoring.
func slashLogIgnore(intr states.Interaction, options []*dg.ApplicationCommandInteractionDataOption) error {
	switch options[0].Name {
	case slashCommandLogIgnoreAdd.Name:
		return slashLogIgnoreAdd(intr, options[0].Options)
	case slashCommandLogIgnoreRemove.Name:
		return slashLogIgnoreRemove(intr, options[0].Options)
	}
	return intr.EditResponseEmbeds(nil, states.ResponseGenericError(nil))
}

//slashCommandLogIgnoreAdd - A subcommand for ignoring channels from logging.
var slashCommandLogIgnoreAdd = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionSubCommand,
	Name:        "add",
	Description: "Adds a channel/category to be ignored for logging.",
	Options: []*dg.ApplicationCommandOption{
		{
			Type:        dg.ApplicationCommandOptionChannel,
			Name:        "channel_or_category",
			Description: "The channel or category to ignore.",
			Required:    true,
		},
	},
}

//slashLogIgnoreAdd adds a channel to be ignored by logging.
func slashLogIgnoreAdd(intr states.Interaction, options []*dg.ApplicationCommandInteractionDataOption) error {

	channel := options[0].ChannelValue(intr.Session)

	settings, err := RetrieveSettings(intr.DB, intr.GuildID)
	if err != nil {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}

	settings.IgnoreChannelIDs = append(settings.IgnoreChannelIDs, channel.ID)
	if err = settings.Store(intr.DB); err != nil {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}

	//Remove records for the channel.
	intr.Printf("Logs have been ignored for C[%s] in G[%s]. Update scheduled.", intr.ChannelID, intr.GuildID)
	intr.LogRecordSchedulers.Get(intr.GuildID).ScheduleChannelUpdate(channel.ID, logSchedulers.UpdateTypeDelete)

	//Remove records from any child channels.
	if channel.Type == dg.ChannelTypeGuildCategory {
		channels := intr.GuildChannels(intr.GuildID)
		for _, childChannel := range channels {
			if childChannel.ParentID == channel.ID {
				intr.Printf("Logs have been ignored for C[%s] in G[%s]. Update scheduled.", childChannel.ID, intr.GuildID)
				intr.LogRecordSchedulers.Get(intr.GuildID).ScheduleChannelUpdate(channel.ID, logSchedulers.UpdateTypeDelete)
			}
		}
	}

	return intr.EditResponseEmbeds(nil, settings.Embed(intr))
}

//slashCommandLogIgnoreAdd - A subcommand to remove channels from being ignored by logging.
var slashCommandLogIgnoreRemove = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionSubCommand,
	Name:        "remove",
	Description: "Removes a channel/category to be ignored for logging",
	Options: []*dg.ApplicationCommandOption{
		{
			Type:        dg.ApplicationCommandOptionChannel,
			Name:        "channel_or_category",
			Description: "The channel or category to remove from the ignore list.",
			Required:    true,
		},
	},
}

//slashLogIgnoreRemove removes a channel from being ignored by logging.
func slashLogIgnoreRemove(intr states.Interaction, options []*dg.ApplicationCommandInteractionDataOption) error {

	removeChannel := options[0].ChannelValue(intr.Session)

	settings, err := RetrieveSettings(intr.DB, intr.GuildID)
	if err != nil {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}
	var ignoreChannelIDs []string
	for _, channelID := range settings.IgnoreChannelIDs {
		if channelID == removeChannel.ID {
			continue
		}
		ignoreChannelIDs = append(ignoreChannelIDs, channelID)
	}
	settings.IgnoreChannelIDs = ignoreChannelIDs
	if err = settings.Store(intr.DB); err != nil {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}

	//Schedule to re-populate records for this channel
	intr.Printf("Logs have been re-enabled for C[%s] in G[%s]. Update scheduled.", intr.ChannelID, intr.GuildID)
	intr.LogRecordSchedulers.Get(intr.GuildID).ScheduleChannelUpdate(removeChannel.ID, logSchedulers.UpdateTypeRefresh)

	//Schedule to re-populate records for any child channels
	if removeChannel.Type == dg.ChannelTypeGuildCategory {
		channels := intr.GuildChannels(intr.GuildID)
		for _, childChannel := range channels {
			if childChannel.ParentID == removeChannel.ID {
				intr.Printf("Logs have been re-enabled for C[%s] in G[%s]. Update scheduled.", childChannel.ID, intr.GuildID)
				intr.LogRecordSchedulers.Get(intr.GuildID).ScheduleChannelUpdate(childChannel.ID, logSchedulers.UpdateTypeRefresh)
			}
		}
	}

	return intr.EditResponseEmbeds(nil, settings.Embed(intr))
}
