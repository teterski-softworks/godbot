package logs

import (
	"database/sql"
	"strconv"

	dg "github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
	"gitlab.com/teterski-softworks/godbot/channels"
	"gitlab.com/teterski-softworks/godbot/logs/logSchedulers"
	"gitlab.com/teterski-softworks/godbot/states"
)

//MessageCreateHandler saves LogRecords of incoming messages if message edit/delete logging is enabled.
func MessageCreateHandler(s *states.State, event *dg.MessageCreate) error {

	//Get logging settings for the guild
	settings, err := RetrieveSettings(s.DB, event.GuildID)
	if err != nil {
		return err
	}

	//TODO: this block can probably be simplified into a single function =============================================
	//Do not save the message for logging if no form of message logging is enabled
	if enabled, err := settings.MessageLoggingEnabled(s); err != nil {
		return err
	} else if !enabled {
		return nil
	}

	//Check if channel is ignored in settings
	if ignored, err := channels.InCategoryList(s.Session, event.ChannelID, settings.IgnoreChannelIDs); err != nil {
		return err
	} else if ignored {
		//Do not save the message for logging
		return nil
	}

	//Ignore logging of messages in message edit/delete channels
	if event.ChannelID == settings.MessageDeletes.ChannelID || event.ChannelID == settings.MessageEdits.ChannelID {
		return nil
	}
	//TODO:===========================================================================================================

	logRecord := New(*event.Message)
	if err = logRecord.Download(); err != nil {
		return err
	}
	return logRecord.Store(s.DB)
}

//MessageEditHandler saves LogRecords of incoming messages if message edit logging is enabled.
func MessageEditHandler(s *states.State, event *dg.MessageUpdate) error {

	//Get logging settings for the guild
	settings, err := RetrieveSettings(s.DB, event.GuildID)
	if err != nil {
		return err
	}

	//TODO: this block can probably be simplified into a single function =============================================
	//Do not save the message for logging if no form of message logging is enabled
	if enabled, err := settings.MessageLoggingEnabled(s); err != nil {
		return err
	} else if !enabled {
		return nil
	}

	//Check if channel is ignored in settings
	if ignored, err := channels.InCategoryList(s.Session, event.ChannelID, settings.IgnoreChannelIDs); err != nil {
		return err
	} else if ignored {
		//Do not save the message for logging
		return nil
	}

	//Ignore logging of messages in message edit/delete channels
	if event.ChannelID == settings.MessageDeletes.ChannelID || event.ChannelID == settings.MessageEdits.ChannelID {
		return nil
	}
	//TODO:===========================================================================================================

	//Retrieve the original saved message
	recordBefore, err := Retrieve(s.DB, event.GuildID, event.ChannelID, event.Message.ID)
	if errors.Is(err, sql.ErrNoRows) {
		return nil
	} else if err != nil {
		return err
	}

	if err = logEditedMessage(s, recordBefore, event.Message, settings); err != nil {
		return err
	}
	return nil
}

//MessageDeleteHandler saves LogRecords of incoming messages if message delete logging is enabled.
func MessageDeleteHandler(s *states.State, event *dg.MessageDelete) error {

	//Get logging settings for the guild
	settings, err := RetrieveSettings(s.DB, event.GuildID)
	if err != nil {
		return err
	}

	//TODO: this block can probably be simplified into a single function =============================================
	//Do not save the message for logging if no form of message logging is enabled
	if enabled, err := settings.MessageLoggingEnabled(s); err != nil {
		return err
	} else if !enabled {
		return nil
	}

	//Check if channel is ignored in settings
	if ignored, err := channels.InCategoryList(s.Session, event.ChannelID, settings.IgnoreChannelIDs); err != nil {
		return err
	} else if ignored {
		//Do not save the message for logging
		return nil
	}

	//Ignore logging of messages in message edit/delete channels
	if event.ChannelID == settings.MessageDeletes.ChannelID || event.ChannelID == settings.MessageEdits.ChannelID {
		return nil
	}
	//TODO:===========================================================================================================

	//Retrieve the original saved message
	recordBefore, err := Retrieve(s.DB, event.GuildID, event.ChannelID, event.Message.ID)
	if errors.Is(err, sql.ErrNoRows) {
		//The log record is missing and without it the bot do not have sufficient information to log the event.
		return nil
	} else if err != nil {
		return err
	}

	return logDeletedMessage(s, recordBefore, settings)
}

//ChannelDeleteHandler clears a channel's scheduled updates and cache.
func ChannelDeleteHandler(s *states.State, event *dg.ChannelDelete) {
	//*NOTE: channel log records should be already deleted through the channels.ChannelDeleteHandler

	s.LogRecordSchedulers.Get(event.GuildID).Cache.ClearChannelCache(event.Channel.ID)
	s.LogRecordSchedulers.Get(event.GuildID).Updates.ClearChannelUpdate(event.Channel.ID)

	//TODO: remove
	//s.Printf("C[%s] deleted from G[%s]. Log update scheduled.", event.Channel.ID, event.GuildID)
	//s.LogRecordSchedulers.Get(event.GuildID).ScheduleChannelUpdate(event.ID, logSchedulers.UpdateTypeDelete)
}

//ChannelUpdateHandler checks if the channel update changes whether or not the channel is able to be logged.
//If so, it schedules a log record channel update.
func ChannelUpdateHandler(s *states.State, event *dg.ChannelUpdate) error {

	//Check the channels current message logging status.
	settings, err := RetrieveSettings(s.DB, event.GuildID)
	if err != nil {
		return err
	}
	loggedNow, err := channelLoggingEnabled(s, event.Channel, settings)
	if err != nil {
		return err
	}

	//If channel logging status has not changed, do nothing
	if loggedNow == s.LogRecordSchedulers.Get(event.GuildID).Cache.GetChannelCache(event.Channel.ID) {
		return nil
	}

	//TODO: simplify
	//Get a list of any child channels
	var channels []*dg.Channel
	if event.Channel.Type == dg.ChannelTypeGuildCategory {
		guildChannels := s.GuildChannels(event.GuildID)
		for _, channel := range guildChannels {
			if channel.ParentID != event.Channel.ID {
				continue
			}
			channels = append(channels, channel)
		}
	}
	channels = append(channels, event.Channel)

	for _, channel := range channels {
		s.Printf("C[%s] permissions changed to [%t] in G[%s]. Log update scheduled.", event.Channel.ID, loggedNow, event.GuildID)
		if loggedNow {
			//If channel logging has been enabled as a result of this update, populate missing records for the channel.
			s.LogRecordSchedulers.Get(event.GuildID).ScheduleChannelUpdate(channel.ID, logSchedulers.UpdateTypeRefresh)
			continue
		}
		//If channel logging has been disabled as a result of this update, remove any records for the channel.
		s.LogRecordSchedulers.Get(event.GuildID).ScheduleChannelUpdate(channel.ID, logSchedulers.UpdateTypeDelete)
	}

	return nil
}

//GuildRoleUpdateHandler checks if the role was assigned to the bot and if its permissions affect message logging.
//If so, schedules a log record recheck all channels update.
func GuildRoleUpdateHandler(s *states.State, event *dg.GuildRoleUpdate) error {

	//Check if the role is held by the bot
	member, err := s.GuildMember(event.GuildID, s.State.User.ID)
	if err != nil {
		return errors.WithStack(err)
	}
	hasRole := false
	for _, roleID := range member.Roles {
		if roleID != event.Role.ID {
			continue
		}
		hasRole = true
		break
	}

	//If the bot does not have the role that was updated, then the update will have no affect on it, so it does nothing
	if !hasRole {
		return nil
	}

	permissionsAffected := false

	//Check what about the role was changed through the audit log
	auditLogs, err := s.GuildAuditLog(event.GuildID, "", "", int(dg.AuditLogActionRoleUpdate), 1)
	if err != nil {
		return errors.WithStack(err)
	}
	for _, auditLogEntry := range auditLogs.AuditLogEntries {
		if auditLogEntry.TargetID != event.Role.ID {
			continue
		}

		for _, change := range auditLogEntry.Changes {
			//Check if any changes to permissions were made
			if *change.Key == dg.AuditLogChangeKeyPermissions {
				newPerms, err := strconv.ParseInt(change.NewValue.(string), 10, 64)
				if err != nil {
					continue
				}
				oldPerms, err := strconv.ParseInt(change.OldValue.(string), 10, 64)
				if err != nil {
					continue
				}
				permissionChange := newPerms - oldPerms
				both := int64(dg.PermissionViewChannel + dg.PermissionReadMessageHistory)
				oneAddedOneRemoved := int64(dg.PermissionViewChannel - dg.PermissionReadMessageHistory)

				//positive means role is added, negative means it was removed
				switch permissionChange {
				case
					dg.PermissionReadMessageHistory, -dg.PermissionReadMessageHistory,
					dg.PermissionViewChannel, -dg.PermissionViewChannel,
					oneAddedOneRemoved, -oneAddedOneRemoved,
					both, -both:
					permissionsAffected = true
				}
			}
		}
		break
	}

	if permissionsAffected {
		s.Printf("R[%s] permissions changed in G[%s]. Log update scheduled.", event.GuildRole.Role.ID, event.GuildID)
		s.LogRecordSchedulers.Get(event.GuildID).ScheduleAllChannelRecheck()
	}

	return nil
}

//GuildMemberUpdateHandler checks if a role assigned to / remove from the bot affects its permissions to log channels.
//If so, schedules a log record member update. This event is also triggered when role assigned to the bot is deleted.
func GuildMemberUpdateHandler(s *states.State, event *dg.GuildMemberUpdate) error {

	//Do not do anything if the member updated is not the bot.
	if event.Member.User.ID != s.State.User.ID {
		return nil
	}

	//TODO: is there a way we can keep track and ignore auditLogEntires that have already been actioned?
	//TODO: As it is right now, it will perform DB Maintenance if one of the last 100 events caused it.
	//TODO? does this even matter? can we get a audit log entry that SHOULD'NT result in maintenance given the current code?

	//Check to see if this is a role assignment related change
	roleAdded, roleRemoved := false, false
	var roleID string
	auditLogs, err := s.GuildAuditLog(event.GuildID, "", "", int(dg.AuditLogActionMemberRoleUpdate), 1)
	if err != nil {
		return errors.WithStack(err)
	}
	for _, auditLogEntry := range auditLogs.AuditLogEntries {
		if auditLogEntry.TargetID != event.Member.User.ID {
			continue
		}

		for _, change := range auditLogEntry.Changes {

			newValue, ok := change.NewValue.([]interface{})
			if !ok {
				return errors.New("invalid audit log entry")
			}
			partialRole, ok := newValue[0].(map[string]interface{})
			if !ok {
				return errors.New("invalid partial role")
			}
			roleID, ok = partialRole["id"].(string)
			if !ok {
				return errors.New("invalid role ID")
			}

			switch *change.Key {
			case dg.AuditLogChangeKeyRoleAdd:
				roleAdded = true
			case dg.AuditLogChangeKeyRoleRemove:
				roleRemoved = true
			}
		}
	}
	if roleID == "" {
		return nil
	}

	if roleAdded {
		//TODO: fires when role is deleted?! is that really a problem though?
		s.Printf("R[%s] added to bot in G[%s]. Log update scheduled.", roleID, event.GuildID)
		s.LogRecordSchedulers.Get(event.GuildID).ScheduleMemberUpdate(roleID, logSchedulers.UpdateTypeRefresh)
	}
	if roleRemoved {
		s.Printf("R[%s] removed from bot in G[%s]. Log update scheduled.", roleID, event.GuildID)
		s.LogRecordSchedulers.Get(event.GuildID).ScheduleMemberUpdate(roleID, logSchedulers.UpdateTypeDelete)
	}
	return nil
}
