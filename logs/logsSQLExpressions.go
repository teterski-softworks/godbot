package logs

const (
	SQLSelectLogSettings string = `
	SELECT
		message_edits_channelID, 	message_edits_enabled,
		message_deletes_channelID, 	message_deletes_enabled,
		mutes_channelID,			mutes_enabled,
		warns_channelID,			warns_enabled,
		bans_channelID,				bans_enabled,
		joins_channelID,			join_enabled
	FROM logSettings
	WHERE guildID=?1;`

	SQLSelectLogIgnoreChannels string = `
	SELECT channelID FROM logIgnoreChannels WHERE guildID=?1;`

	SQLSelectLogRecord string = `
	SELECT encryptedData FROM logRecords WHERE guildID=?1 AND channelID=?2 AND messageID=?3;`

	SQLSelectLogRecordChannels string = `
	SELECT DISTINCT	channelID FROM logRecords WHERE guildID=?1;`

	SQLSelectGuildChannelLogRecords string = `
	SELECT messageID, encryptedData FROM logRecords WHERE guildID=?1 AND channelID=?2;`

	SQLSelectLogRecordProgress string = `
	SELECT IFNULL(MIN(messageID+0),"") AS earliest, IFNULL(MAX(messageID+0),"") AS latest
	FROM logRecords
	WHERE guildID=?1 AND channelID=?2;`
	//Addition of +0 to messageID is required to convert messageID to an int so it can be numerically compared as a number, rather than as a string.

	SQLSelectLogRecordAttachments string = `
	SELECT attachmentIndex, encryptedFileName, encryptedURL, encryptedHash
	FROM logAttachments
	WHERE guildID=?1 AND channelID=?2 AND messageID=?3
	ORDER BY attachmentIndex;`

	SQLSelectAttachmentData string = `
	SELECT encryptedData FROM logAttachmentsData WHERE encryptedHash=?1;`

	SQLAttachmentExists string = `
	SELECT COUNT(*)>0 FROM logAttachmentsData WHERE encryptedHash=?1;`

	SQLUpsertLogSettings string = `
	INSERT INTO logSettings(
		guildID,
		message_edits_channelID,	message_edits_enabled,
		message_deletes_channelID,	message_deletes_enabled,
		mutes_channelID,			mutes_enabled,
		warns_channelID,			warns_enabled,
		bans_channelID,				bans_enabled,
		joins_channelID,			join_enabled
	) VALUES (
		?1,
		?2,  ?3,
		?4,  ?5,
		?6,  ?7,
		?8,  ?9,
		?10, ?11,
		?12, ?13
	)ON CONFLICT DO
	UPDATE SET
		message_edits_channelID   =?2,	message_edits_enabled     =?3,
		message_deletes_channelID =?4,	message_deletes_enabled   =?5,
		mutes_channelID           =?6,	mutes_enabled             =?7,
		warns_channelID           =?8,	warns_enabled             =?9,
		bans_channelID            =?10,	bans_enabled              =?11,
		joins_channelID           =?12,	join_enabled              =?13
	WHERE guildID=?1;`

	SQLInsertLogIgnoreChannel string = `
	INSERT OR IGNORE INTO logIgnoreChannels(guildID, channelID) VALUES (?1, ?2);`

	SQLUpsertLogRecord string = `
	INSERT INTO logRecords(guildID, channelID, messageID, encryptedData)
	VALUES(?1, ?2, ?3, ?4)
	ON CONFLICT (guildID, channelID, messageID) DO
	UPDATE SET encryptedData=?4;`

	SQLUpsertAttachment string = `
	INSERT INTO logAttachments(guildID, channelID, messageID, attachmentIndex, encryptedFileName, encryptedURL, encryptedHash)
	VALUES(?1, ?2, ?3, ?4, ?5, ?6, ?7) ON CONFLICT DO
	UPDATE SET
	encryptedFileName =?5,
	encryptedURL	  =?6,
	encryptedHash     =?7
	WHERE guildID=?1 AND channelID=?2 AND messageID=?3 AND attachmentIndex=?4;`

	SQLInsertAttachmentData string = `
	INSERT OR IGNORE INTO logAttachmentsData(encryptedHash, encryptedData) VALUES(?1, ?2);`

	SQLDeleteLogIgnoreChannels string = `
	DELETE FROM logIgnoreChannels WHERE guildID=?1;`

	SQLDeleteLogIgnoreChannel string = `
	DELETE FROM logIgnoreChannels WHERE guildID=?1 and channelID=?2;`

	SQLDeleteLogRecord string = `
	DELETE FROM logRecords WHERE guildID=?1 AND channelID=?2 AND messageID=?3;`

	SQLDeleteAttachment string = `
	DELETE FROM logAttachments WHERE guildID=?1 AND channelID=?2 AND messageID=?3 AND attachmentIndex=?4;`

	SQLDeleteChannelLogRecords string = `
	DELETE FROM logRecords WHERE guildID=?1 AND channelID=?2;`

	SQLDeleteGuildLogRecords string = `
	DELETE FROM logRecords WHERE guildID=?1;`
)
