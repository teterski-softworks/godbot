package logSchedulers

import (
	"time"
)

//Scheduler schedules log record updates.
//It waits for a period of time for updates to be scheduled or unscheduled, and then applies the all scheduled updates.
//This prevents unnecessarily updating log records if another change just a little later undoes the update.
//The wait timer resets each time an update is scheduled.
type Scheduler struct {
	Updates
	Cache
	ErrChan chan error
}

//New returns a new scheduler which runs `function` to apply its scheduled updates.
func New(function func() error) *Scheduler {
	var s Scheduler
	s.ErrChan = make(chan error, 1)
	s.Updates.waitPeriod = time.Minute * 2
	s.Updates.resetUpdates()
	s.Cache.reset()

	s.Timer = time.AfterFunc(s.Updates.waitPeriod, func() {
		//Lock the scheduler so that it doesn't get reset while the scheduler is doing work.
		s.Updates.mutex.Lock()
		defer s.Updates.mutex.Unlock()
		s.Updates.updating = true
		if err := function(); err != nil {
			s.ErrChan <- err
		}
		s.Updates.resetUpdates()
	})

	return &s
}

//Updating returns true if an update is currently running. Returns false otherwise.
func (s *Scheduler) Updating() bool {
	return s.Updates.updating
}

//UpdateScheduled returns true if an update is scheduled to run. Returns false otherwise.
func (s *Scheduler) UpdateScheduled() bool {
	return s.Updates.updateScheduled
}

//NextUpdateOn returns the time at which the next update will be ran.
func (s *Scheduler) NextUpdateOn() time.Time {
	return s.Updates.nextUpdateOn
}
