package logSchedulers

import (
	"sync"
	"time"
)

//UpdateType represents a type of schedule-able update.
type UpdateType int

const (
	UpdateTypeDelete UpdateType = iota - 1
	UpdateTypeNothing
	UpdateTypeRefresh
)

//Updates represents a collection of log record updates that are scheduled for a guild.
type Updates struct {
	//Whether or not it is needed to recheck all channels for changes in log state.
	//It will overwrite any scheduled channel updates or updates from member role assignment.
	RecheckAllChannels bool

	//Whether or not guild level logging has been enabled/disabled.
	//Guild updates overwrite any other scheduled updates.
	Guild UpdateType
	//Whether or not a channels logs need to be deleted or refreshed.
	Channel map[ /*channelID*/ string]UpdateType
	//Whether or not roles have been added/removed from the bot that can affect logging.
	Member map[ /*roleID*/ string]UpdateType

	//Used to lock the struct to ensure that updates are fully scheduled before scheduling the next update
	//and to ensure it waits until it is done applying pending updates
	//before starting applying a new set of updates.
	mutex sync.Mutex

	updating        bool      //True if an update is currently running. False otherwise.
	updateScheduled bool      //True if an update is scheduled to be run. False otherwise.
	nextUpdateOn    time.Time //The time at which the scheduled update will be ran.

	//The duration to wait before firing the timer after a change is scheduled.
	waitPeriod time.Duration
	*time.Timer
}

//resetUpdates clears all scheduled updates.
func (u *Updates) resetUpdates() {
	u.Guild = UpdateTypeNothing
	u.RecheckAllChannels = false
	u.Channel = make(map[string]UpdateType)
	u.Member = make(map[string]UpdateType)
	u.updating = false
	u.updateScheduled = false
	u.nextUpdateOn = time.Time{}
}

//resetTimer resets the timer to go off after the given duration.
func (u *Updates) resetTimer(duration time.Duration) {
	u.Timer.Stop()
	u.Timer.Reset(duration)
	u.nextUpdateOn = time.Now().Add(duration)
	u.updateScheduled = true
}

//ForceUpdate fires the timer as soon as Updates is unlocked,
//without waiting for the wait duration to expire.
//If unlocked already, fires immediately.
func (u *Updates) ForceUpdate() {
	u.mutex.Lock()
	defer u.mutex.Unlock()
	u.resetTimer(time.Duration(0))
}

//ScheduleGuildUpdate schedules an update for a guild.
func (u *Updates) ScheduleGuildUpdate(update UpdateType) {
	u.mutex.Lock()
	defer u.mutex.Unlock()
	u.Guild = update
	u.resetTimer(u.waitPeriod)
}

//ScheduleAllChannelRecheck schedules a recheck of all channels in a guild and
//will update those whose log state has changed (ex.: if it was not logged before but is now).
func (u *Updates) ScheduleAllChannelRecheck() {
	u.mutex.Lock()
	defer u.mutex.Unlock()
	u.RecheckAllChannels = true
	u.resetTimer(u.waitPeriod)
}

//ScheduleGuildUpdate schedules an update for a channel.
func (u *Updates) ScheduleChannelUpdate(channelID string, update UpdateType) {
	u.mutex.Lock()
	defer u.mutex.Unlock()
	u.Channel[channelID] = update
	u.resetTimer(u.waitPeriod)
}

//ScheduleMemberUpdate schedules an update to recheck channel log states if
//the bot's member roles have changed (and have affected which channels the bot has access to).
func (u *Updates) ScheduleMemberUpdate(roleID string, update UpdateType) {
	u.mutex.Lock()
	defer u.mutex.Unlock()
	u.Member[roleID] = update
	u.resetTimer(u.waitPeriod)
}

//TODO
func (u *Updates) ClearChannelUpdate(channelID string) {
	u.mutex.Lock()
	defer u.mutex.Unlock()
	delete(u.Channel, channelID)
}
