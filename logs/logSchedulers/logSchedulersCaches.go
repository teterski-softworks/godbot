package logSchedulers

import "sync"

//Cache is used to save certain information about channels and the bot at the time of the last update,
//to compare it at the time of the next update to see if the scheduled updates actually affect logging.
//It is made safe for concurrent use.
type Cache struct {

	//Channels keeps cached information on whether or not logging is enabled for a channel.
	Channels struct {
		mutex                 sync.Mutex
		messageLoggingEnabled map[ /*channelIDs*/ string]bool
	}

	//MemberRoles keeps a cached list of roles assigned to the bot.
	MemberRoles struct {
		mutex sync.Mutex
		List  []string //roleIDs
	}
}

//reset clears all saved cache.
func (c *Cache) reset() {
	c.Channels.mutex.Lock()
	c.Channels.messageLoggingEnabled = make(map[string]bool)
	c.Channels.mutex.Unlock()

	c.MemberRoles.mutex.Lock()
	c.MemberRoles.List = []string{}
	c.MemberRoles.mutex.Unlock()
}

//CacheChannel saves whether or not message logging is enabled for the channel for later use.
func (c *Cache) CacheChannel(channelID string, messageLoggingEnabled bool) {
	c.Channels.mutex.Lock()
	c.Channels.messageLoggingEnabled[channelID] = messageLoggingEnabled
	c.Channels.mutex.Unlock()
}

//GetChannelCache returns true if the channel was previously known (if it was cached) to have message logging enabled for it.
//Returns false otherwise (if not enabled, or if channel was not cached).
func (c *Cache) GetChannelCache(channelID string) bool {
	c.Channels.mutex.Lock()
	defer c.Channels.mutex.Unlock()
	enabled, exists := c.Channels.messageLoggingEnabled[channelID]
	if !exists {
		return false
	}
	return enabled
}

//CacheMemberPermissions saves a list of roles assigned to the bot.
func (c *Cache) CacheMemberPermissions(roles []string) {
	c.MemberRoles.mutex.Lock()
	c.MemberRoles.List = roles
	c.MemberRoles.mutex.Unlock()
}

//TODO
func (c *Cache) ClearChannelCache(channelID string) {
	c.Channels.mutex.Lock()
	delete(c.Channels.messageLoggingEnabled, channelID)
	c.Channels.mutex.Unlock()
}
