package logs

import "gitlab.com/teterski-softworks/godbot/db"

//CreateDatabaseTables creates the required database tables for the logs package.
func CreateDatabaseTables(DB db.ReadWriter) error {
	t := DB.NewTransaction()
	t.Queue(SQLCreateTableLogSettings)
	t.Queue(SQLCreateTableLogsIgnoreChannels)
	t.Queue(SQLCreateTableLogRecords)
	t.Queue(SQLCreateTableLogAttachments)
	t.Queue(SQLCreateTableLogAttachmentsData)
	return t.Commit()
}

var (
	SQLCreateTableLogSettings string = `
	CREATE TABLE IF NOT EXISTS logSettings(
		guildID                     TEXT    NOT NULL PRIMARY KEY REFERENCES guilds(guildID) ON DELETE CASCADE,

		message_edits_channelID     TEXT    DEFAULT "" REFERENCES channels(channelID) ON DELETE SET DEFAULT,
		message_edits_enabled       BOOLEAN DEFAULT FALSE,

		message_deletes_channelID   TEXT    DEFAULT "" REFERENCES channels(channelID) ON DELETE SET DEFAULT,
		message_deletes_enabled     BOOLEAN DEFAULT FALSE,

		mutes_channelID             TEXT    DEFAULT "" REFERENCES channels(channelID) ON DELETE SET DEFAULT,
		mutes_enabled               BOOLEAN DEFAULT FALSE,

		warns_channelID             TEXT    DEFAULT "" REFERENCES channels(channelID) ON DELETE SET DEFAULT,
		warns_enabled               BOOLEAN DEFAULT FALSE,
		
		bans_channelID              TEXT    DEFAULT "" REFERENCES channels(channelID) ON DELETE SET DEFAULT,
		bans_enabled                BOOLEAN DEFAULT FALSE,

		joins_channelID             TEXT    DEFAULT "" REFERENCES channels(channelID) ON DELETE SET DEFAULT,
		join_enabled                BOOLEAN DEFAULT FALSE
	);`

	SQLCreateTableLogsIgnoreChannels string = `
	CREATE TABLE IF NOT EXISTS logIgnoreChannels(
		guildID     TEXT NOT NULL REFERENCES logSettings(guildID) ON DELETE CASCADE,
		channelID   TEXT NOT NULL REFERENCES channels(channelID) ON DELETE CASCADE,

		PRIMARY KEY(guildID, channelID)
	);`

	SQLCreateTableLogRecords string = `
	CREATE TABLE IF NOT EXISTS logRecords(
		guildID       TEXT NOT NULL,
		channelID     TEXT NOT NULL,
		messageID     TEXT NOT NULL,
		encryptedData BLOB NOT NULL,

		PRIMARY KEY(guildID, channelID, messageID),
		FOREIGN KEY(guildID, channelID) REFERENCES channels(guildID, channelID) ON DELETE CASCADE
	);`

	SQLCreateTableLogAttachments string = `
	CREATE TABLE IF NOT EXISTS logAttachments(
		guildID             TEXT NOT NULL,
		channelID           TEXT NOT NULL,
		messageID           TEXT NOT NULL,
		attachmentIndex     INT  NOT NULL,
		encryptedFileName   BLOB NOT NULL,
		encryptedURL        BLOB NOT NULL,
		encryptedHash       BLOB NOT NULL REFERENCES logAttachmentsData(encryptedHash) ON DELETE CASCADE,

		PRIMARY KEY (guildID, channelID, messageID, attachmentIndex),
		FOREIGN KEY (guildID, channelID, messageID) REFERENCES logRecords(guildID, channelID, messageID) ON DELETE CASCADE
	);`

	SQLCreateTableLogAttachmentsData string = `
	CREATE TABLE IF NOT EXISTS logAttachmentsData(
		encryptedHash       BLOB NOT NULL,
		encryptedData       BLOB NOT NULL,
		timesUsed			INT DEFAULT 1,
		--//TODO: deletion trigger if timesUsed is 0
		PRIMARY KEY (encryptedHash)
	);`
)
