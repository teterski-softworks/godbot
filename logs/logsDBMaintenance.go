package logs

import (
	"fmt"
	"strings"
	"sync"

	dg "github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
	"gitlab.com/teterski-softworks/godbot/db"
	"gitlab.com/teterski-softworks/godbot/logs/logSchedulers"
	"gitlab.com/teterski-softworks/godbot/states"
)

//ApplyLogRecordGuildUpdates applies all scheduled log record updates for the guild.
func ApplyLogRecordGuildUpdates(s *states.State, guildID string) error {

	s.Printf("Log updating for G[%s] fired.", guildID)
	defer func() {
		s.Printf("Finished updating logs for G[%s].", guildID)
	}()

	scheduler := s.LogRecordSchedulers.Get(guildID)

	settings, err := RetrieveSettings(s.DB, guildID)
	if err != nil {
		return err
	}

	switch scheduler.Updates.Guild {

	case logSchedulers.UpdateTypeDelete:
		s.Printf("Deleting all logs for G[%s].", guildID)
		return DeleteAll(s.DB, guildID)

	case logSchedulers.UpdateTypeRefresh:
		return dbMaintenance(s, guildID, settings)

	case logSchedulers.UpdateTypeNothing:

		channelStates := make(map[string]bool)
		for _, channel := range s.GuildChannels(guildID) {
			enabled, err := channelLoggingEnabled(s, channel, settings)
			if err != nil {
				return err
			}
			channelStates[channel.ID] = enabled
		}

		if err := applyLogRecordMemberUpdates(s, scheduler, guildID, channelStates); err != nil {
			return err
		}

		if err := applyLogRecordRecheckChannels(s, scheduler, channelStates, settings); err != nil {
			return err
		}

		if err := applyLogRecordChannelUpdates(s, scheduler, channelStates, settings); err != nil {
			return err
		}

	}

	return nil
}

//applyLogRecordMemberUpdates checks if a role was assigned to the bot (that is a role that affects logging permissions)
//which was not assigned last time the scheduler was run, and if so, sets a recheck of all channels in the guild.
func applyLogRecordMemberUpdates(s *states.State, scheduler *logSchedulers.Scheduler, guildID string, channelStates map[string]bool) error {

	botMember, err := s.GuildMember(guildID, s.State.User.ID)
	if err != nil {
		return err
	}

	//If a recheck is already scheduled, there is no point in scheduling the check again
	if !scheduler.RecheckAllChannels {

		//Checks if a new role was added/removed and schedule a recheck of all channels if needed.
		for roleID, update := range scheduler.Updates.Member {

			roleWasPresentBefore := false

			//check if the bot had this role the last time the scheduler ran.
			for _, cachedRoleIDs := range scheduler.Cache.MemberRoles.List {
				if roleID != cachedRoleIDs {
					continue
				}
				roleWasPresentBefore = true
			}

			//if the state has changed in a meaningful way from the previous time the scheduler ran,
			//schedule an update.
			if (!roleWasPresentBefore && update == logSchedulers.UpdateTypeRefresh) ||
				(roleWasPresentBefore && update == logSchedulers.UpdateTypeDelete) {
				s.Printf("New assigned bot roles detected in G[%s]. Scheduling log record update.", guildID)
				scheduler.RecheckAllChannels = true
				break
			}

		}
	}

	//cache the bots current roles
	scheduler.CacheMemberPermissions(botMember.Roles)

	return nil
}

//applyLogRecordRecheckChannels, if scheduled by a recheck all channel log record update,
//checks which channels have had their message logging state changed
//(whether or not the channel can be logged for message edits or deletes).
//If so, sets a log record channel update.
func applyLogRecordRecheckChannels(s *states.State, scheduler *logSchedulers.Scheduler, channelStates map[string]bool, settings Settings) error {

	if !scheduler.RecheckAllChannels {
		return nil
	}

	//check which channels are affected and schedule the update for them
	for channelID, enabled := range channelStates {
		if scheduler.GetChannelCache(channelID) == enabled {
			continue
		}
		//Do not update the channel cache here. Do that only once the update is applied to the channel.

		//Schedule the channel log record's update
		if !enabled {
			scheduler.Updates.Channel[channelID] = logSchedulers.UpdateTypeDelete
			s.Printf("Can no longer log to C[%s] in G[%s]. Scheduling update to remove records.", channelID, settings.GuildID)
			continue
		}
		s.Printf("Scheduling refresh of logs in C[%s] of G[%s].", channelID, settings.GuildID)
		scheduler.Updates.Channel[channelID] = logSchedulers.UpdateTypeRefresh
	}
	return nil
}

//applyLogRecordChannelUpdates applies a log record update of the channel, if the channel's message logging state has been changed since last time the scheduler has ran.
func applyLogRecordChannelUpdates(s *states.State, scheduler *logSchedulers.Scheduler, channelStates map[string]bool, settings Settings) error {

	//TODO: parallelize
	for channelID, update := range scheduler.Updates.Channel {

		//Do not do anything if channel message logging state has not changed.
		if scheduler.GetChannelCache(channelID) == channelStates[channelID] {
			continue
		}

		switch update {

		case logSchedulers.UpdateTypeDelete:
			s.Printf("Deleting logs for C[%s] in G[%s].", channelID, settings.GuildID)
			if err := DeleteAllInChannel(s.DB, settings.GuildID, channelID); err != nil {
				return err
			}

		case logSchedulers.UpdateTypeRefresh:

			s.Printf("Refreshing logs for C[%s] in G[%s].", channelID, settings.GuildID)
			earliestRecordID, latestRecordID, err := recordProgress(s.DB, settings.GuildID, channelID)
			if err != nil {
				return err
			}
			if err := populateMissingMessageRecords(s, channelID, earliestRecordID, latestRecordID, settings); err != nil {
				return err
			}
		}

		//update cached channel log state
		scheduler.CacheChannel(channelID, channelStates[channelID])
	}
	return nil
}

//dbMaintenance runs database maintenance for any saved LogRecords
//It makes sure that the LogRecords are always up to date, and any unnecessary LogRecords are removed.
//
//It checks if the LogRecords are still needed, and deletes them if not
//(ex. if the channel for the LogRecords was deleted)
//It then checks if any messages that have existing LogRecords have been updated/deleted.
//If so, it logs the update/deletion and updated/removes the LogRecords.
//It then checks all the channels in the guild for new messages it can log,
//or messages it has not previously yet logged, and creates & saves LogRecords for them.
func dbMaintenance(s *states.State, guildID string, settings Settings) error {

	//Remove all records if message logging is disabled.
	if enabled, err := settings.MessageLoggingEnabled(s); err != nil {
		return err
	} else if !enabled {
		s.Printf("Logging has been disabled for G[%s]. Deleting all logs.", settings.GuildID)
		return DeleteAll(s.DB, guildID)
	}

	channels := s.GuildChannels(guildID)

	//Deleted records from channels that have been deleted.
	if err := removeRecordsFromDeletedChannels(s.DB, channels, guildID); err != nil {
		return err
	}

	var waitGroup sync.WaitGroup
	chanErr := make(chan error, len(channels))
	waitGroup.Add(len(channels))

	for _, channel := range channels {
		if channel.ID == settings.MessageDeletes.ChannelID || channel.ID == settings.MessageEdits.ChannelID {
			//Ignore messages in the message log channels.
			continue
		}
		go channelLogRecordMaintenance(s, guildID, channel, settings, &waitGroup, chanErr)
	}
	waitGroup.Wait()
	close(chanErr)

	//TODO: this will only return ONE error out of many possibly received errors...
	//Check for any errors from channelLogRecordMaintenance
	for err := range chanErr {
		if err != nil {
			return err
		}
	}

	return nil
}

//channelLogRecordMaintenance performs maintenance on LogRecords for a given channel.
func channelLogRecordMaintenance(s *states.State, guildID string, channel *dg.Channel, settings Settings, waitGroup *sync.WaitGroup, chanErr chan error) {

	defer waitGroup.Done()
	enabled, err := channelLoggingEnabled(s, channel, settings)
	if err != nil {
		chanErr <- err
		return
	}
	s.LogRecordSchedulers.Get(guildID).CacheChannel(channel.ID, enabled)

	if !enabled {
		//Delete any records still remaining in non-logged channels/categories
		if err := DeleteAllInChannel(s.DB, guildID, channel.ID); err != nil {
			chanErr <- err
			return
		}
		chanErr <- nil
		return
	}

	earliestRecordID, latestRecordID, err := recordProgress(s.DB, guildID, channel.ID)
	if err != nil {
		chanErr <- err
		return
	}
	if err := updateExistingRecords(s, channel.ID, earliestRecordID, latestRecordID, settings); err != nil {
		chanErr <- err
		return
	}
	if err := populateMissingMessageRecords(s, channel.ID, earliestRecordID, latestRecordID, settings); err != nil {
		chanErr <- err
		return
	}
	chanErr <- nil
}

//updateExistingRecords checks if any of the existing records for a channel have changed. If so, it logs the edit, and updates the record.
func updateExistingRecords(s *states.State, channelID string, earliestRecordID string, latestRecordID string, settings Settings) error {

	s.Printf("Updating logs for C[%s] in G[%s].", channelID, settings.GuildID)

	//Retrieve a list of all messages between the earliest recorded message and latest recorded.
	messages := make(map[ /*channelID*/ string]*dg.Message)

	if earliestRecordID == "" || latestRecordID == "" {
		return nil
	}

	//Get the earliest message, as it is not included in the batch retrieval process
	message, err := s.ChannelMessage(channelID, earliestRecordID)
	if err != nil {
		//fmt.Printf("Not found: https://discord.com/channels/%s/%s/%s\n", message.GuildID, message.ChannelID, message.ID)
		//TODO: delete the record then and try to find the next earliest one?
		return errors.WithStack(err) //TODO: retry on error? or ignore error?
	}
	message.GuildID = settings.GuildID
	messages[message.ID] = message

	for {
		batch, err := getChannelMessagesInBatch(s, channelID, latestRecordID, earliestRecordID, 100)
		if err != nil {
			return err
		}
		if len(batch) == 0 {
			break
		}

		earliestRecordID = batch[0].ID
		for _, message := range batch {
			message.GuildID = settings.GuildID
			messages[message.ID] = message
		}
	}
	return updateMessageRecordsAndLogs(s, channelID, messages, settings)
}

//populateMissingMessageRecords saves LogRecords for any messages that the bot does not yet have LogRecords for.
//This can include messages sent since the last logged message, as well as any messages sent before the earliest logged message.
func populateMissingMessageRecords(s *states.State, channelID string, earliestRecordID string, latestRecordID string, settings Settings) error {

	s.Printf("Populating logs for C[%s] in G[%s].", channelID, settings.GuildID)

	if err := recordMessagesAfterLatest(s, channelID, settings.GuildID, latestRecordID); err != nil {
		return err
	}
	if err := recordMessagesBeforeEarliest(s, channelID, settings.GuildID, earliestRecordID); err != nil {
		return err
	}

	return nil
}

//recordMessagesAfterLatest saves records for any messages that were sent after the latest message with a record in a channel.
func recordMessagesAfterLatest(s *states.State, channelID string, guildID string, latestRecordID string) error {

	//Retrieve a list of all messages since the latest recorded message.
	var messages []*dg.Message
	for {
		batch, err := getChannelMessagesInBatch(s, channelID, "", latestRecordID, 100)
		if err != nil {
			return err
		}
		if len(batch) == 0 {
			break
		}
		latestRecordID = batch[0].ID
		messages = append(messages, batch...)
	}

	return saveMessageRecords(s.DB, guildID, messages)
}

//recordMessagesBeforeEarliest saves records for any messages that were sent before the earliest message with the a record in a channel.
func recordMessagesBeforeEarliest(s *states.State, channelID string, guildID string, earliestRecordID string) error {

	//Retrieve a list of all messages before the last recorded message.
	var messages []*dg.Message
	for {
		batch, err := getChannelMessagesInBatch(s, channelID, earliestRecordID, "", 100)
		if err != nil {
			return err
		}
		if len(batch) == 0 {
			break
		}
		earliestRecordID = batch[len(batch)-1].ID
		messages = append(messages, batch...)
	}

	return saveMessageRecords(s.DB, guildID, messages)
}

//channelLoggingEnabled returns true if the given channel is not ignored for logging
//and the bot has the required permissions needed to log the channel.
func channelLoggingEnabled(s *states.State, channel *dg.Channel, settings Settings) (bool, error) {
	if channel.Type != dg.ChannelTypeGuildText {
		return false, nil
	}
	if settings.Ignored(channel) {
		return false, nil
	}
	requiredPermissions := int64(dg.PermissionViewChannel + dg.PermissionReadMessageHistory)
	channelPermissions, err := s.State.UserChannelPermissions(s.State.User.ID, channel.ID)
	if err != nil {
		return false, err
	} else if channelPermissions&dg.PermissionAdministrator == dg.PermissionAdministrator {
		return true, nil
	} else if channelPermissions&requiredPermissions != requiredPermissions {
		return false, nil
	}
	return true, nil
}

//saveMessageRecords saves LogRecords for the messages provided.
func saveMessageRecords(DB db.ReadWriter, guildID string, messages []*dg.Message) error {

	var records []LogRecord
	for _, message := range messages {
		message.GuildID = guildID
		record := New(*message)
		if err := record.Download(); err != nil {
			return err
		}
		records = append(records, record)
	}

	return StoreMultiple(DB, records)
}

//updateMessageRecordsAndLogs updates any LogRecords of a message that was edited and logs the edit.
func updateMessageRecordsAndLogs(s *states.State, channelID string, messages map[string]*dg.Message, settings Settings) error {

	//Get all records for the channel
	records, err := RetrieveAllInChannel(s.DB, settings.GuildID, channelID)
	if err != nil {
		return err
	}
	//TODO: some of these retrieved records have empty message IDs.

	for _, record := range records {
		//Check if each record has a message associated with it.
		message, ok := messages[record.MessageID]
		if !ok {
			//TODO: there seems to be an issue with some messages not having their messageIDs, which is causing this to fire.
			//TODO?: could this be the "latest" message in the channel, which is not part of the batch retrieval process? - NO
			//TODO! edited messages don't seem to have their ID preserved

			fmt.Println("FAIL - DOESN'T EXIST", record.MessageID) //TODO: remove
			//if not message has been deleted
			//		log the deletion and delete the record
			if err = logDeletedMessage(s, record, settings); err != nil {
				return err
			}
			continue
		}
		//Compare the message with the record
		compare := New(*message)
		//*NOTE: Do not need to re-download attachments for "compare"
		if !record.Equals(compare) {
			fmt.Println("FAIL - NOT EQUAL") //TODO: remove
			//if its not equal, the message was edited, so log the edit
			if err = logEditedMessage(s, record, message, settings); err != nil {
				return err
			}
		}
	}
	return nil
}

//removeRecordsFromDeletedChannels deletes all logRecords for a channel that has been deleted from a guild.
func removeRecordsFromDeletedChannels(DB db.ReadWriter, guildChannels []*dg.Channel, guildID string) error {

	loggedChannelIDs, err := getGuildLogRecordChannels(DB, guildID)
	if err != nil {
		return err
	}
	for _, channelID := range loggedChannelIDs {
		deleted := true
		for _, match := range guildChannels {
			if channelID == match.ID {
				deleted = false
				break
			}
		}
		if deleted {
			//TODO: add a progress log here.
			t := DB.NewTransaction()
			t.Queue(SQLDeleteLogIgnoreChannel, guildID, channelID)
			if err := DeleteAllInChannel(t, guildID, channelID); err != nil {
				return err
			}
			if err := t.Commit(); err != nil {
				return err
			}
		}
	}

	return nil
}

//getChannelMessagesInBatch gets a batch of messages from a channel.
//limit - max 100
//beforeID - If provided all messages returned will be before given ID.
//aroundID - If provided all messages returned will be around given ID.
func getChannelMessagesInBatch(s *states.State, channelID string, beforeID string, afterID string, limit int) ([]*dg.Message, error) {
	//TODO: what if channel perms change during scan and it just gets stuck here?
	batch, err := s.ChannelMessages(channelID, limit, beforeID, afterID, "")
	if err != nil {
		cause := errors.Cause(err).Error()
		if strings.HasPrefix(cause, "HTTP 403 Forbidden") {
			//TODO: can this even fire anymore?
			//The bot does not have access to view messages in this channel.
			return batch, nil
		}
		return batch, errors.WithStack(err) //TODO?: Do we want to retry if we get an error?
	}
	return batch, nil
}
