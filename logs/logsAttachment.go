package logs

import (
	"bytes"
	"crypto/md5"
	"encoding/gob"
	"encoding/hex"
	"io"
	"mime"
	"net/http"
	"net/url"
	"path/filepath"
	"strings"

	dg "github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
	"gitlab.com/teterski-softworks/godbot/db"
)

//Attachment represents a message file attachment.
type Attachment struct {
	dg.MessageReference
	Index int
	Hash  string

	EncryptedAttachment
}

//EncryptedAttachment represents the encrypted portion of an Attachment, containing the actual file data.
type EncryptedAttachment struct {
	FileName string
	URL      string
	Data     []byte
}

/*NewAttachment returns an Attachment for the given URL and message.

Note: The returned Attachment will not have any file data. This requires Attachment.Download() to be run.
*/
func NewAttachment(urlPath string, guildID string, channelID string, messageID string) (a Attachment, err error) {

	a.MessageReference = dg.MessageReference{
		GuildID:   guildID,
		ChannelID: channelID,
		MessageID: messageID,
	}

	// Get filename
	fileURL, err := url.Parse(urlPath) //TODO: do we need to do this considering we know that all urls will point to discord?
	if err != nil {
		return a, err
	}
	segments := strings.Split(fileURL.Path, "/")
	a.URL = urlPath
	a.FileName = segments[len(segments)-1]

	return a, nil
}

//Downloads the Attachment's file data.
func (a *Attachment) Download() error {
	//Download the file
	response, err := http.Get(a.URL)
	if err != nil {
		return errors.WithStack(err)
	}
	defer response.Body.Close()

	//Read file data
	var data bytes.Buffer
	if _, err = io.Copy(&data, response.Body); err != nil {
		return err
	}
	a.Data = data.Bytes()

	//Generate hash
	var buffer bytes.Buffer
	gob.NewEncoder(&buffer).Encode(data)
	hashBytes := md5.Sum(buffer.Bytes())
	a.Hash = hex.EncodeToString(hashBytes[:])

	return nil
}

//RetrieveAllAttachments returns all Attachments for a message from the DB.
func RetrieveAllAttachments(DB db.Reader, guildID string, channelID string, messageID string) (attachments []*Attachment, err error) {

	//Get list of all attachments in message
	rows, err := DB.Query(SQLSelectLogRecordAttachments, guildID, channelID, messageID)
	if err != nil {
		return attachments, err
	}
	for rows.Next() {
		a := Attachment{MessageReference: dg.MessageReference{
			GuildID:   guildID,
			ChannelID: channelID,
			MessageID: messageID,
		}}

		//NOTE: Reminder that a.File.Name and a.Hash are still encrypted at this point.
		//      they are decrypted in the loop below.
		if err = rows.Scan(&a.Index, &a.FileName, &a.URL, &a.Hash); err != nil {
			return attachments, errors.WithStack(err)
		}
		attachments = append(attachments, &a)
	}

	//Get attachment data
	for _, a := range attachments {
		//Decrypt file data
		row, err := DB.QueryRow(SQLSelectAttachmentData, a.Hash)
		//TODO: it is possible the file data for the attachment could be missing and that is alright, we just need to communicate that.
		if err != nil {
			return attachments, err
		}
		var encryptedData []byte
		if err = row.Scan(&encryptedData); err != nil {
			return attachments, errors.WithStack(err)
		}
		if err = DB.Decrypt(&a.EncryptedAttachment, encryptedData); err != nil {
			return attachments, err
		}

		//Decrypt hash, now that we do not need the encrypted one to query with
		if err = DB.Decrypt(&a.Hash, []byte(a.Hash)); err != nil {
			return attachments, err
		}

		//Decrypt file name
		if err = DB.Decrypt(&a.FileName, []byte(a.FileName)); err != nil {
			return attachments, err
		}

		//Decrypt URL
		if err = DB.Decrypt(&a.URL, []byte(a.URL)); err != nil {
			return attachments, err
		}

	}

	return attachments, err
}

//Stores the Attachment in the DB.
func (a Attachment) Store(DB db.ReadWriter) error {

	t := DB.NewTransaction()

	//Encrypt hash
	encryptedHash, err := t.Encrypt(a.Hash)
	if err != nil {
		return err
	}

	//Encrypt file name
	encryptedFileName, err := t.Encrypt(a.FileName)
	if err != nil {
		return err
	}

	//Encrypt URL
	encryptedURL, err := t.Encrypt(a.URL)
	if err != nil {
		return err
	}

	//Check if file hash is already in DB, before encrypting the file since that is expensive
	row, err := t.QueryRow(SQLAttachmentExists, encryptedHash)
	if err != nil {
		return err
	}
	var exists bool
	if err = row.Scan(&exists); err != nil {
		return errors.WithStack(err)
	}
	if !exists {
		//Save attachment file data
		encryptedData, err := t.Encrypt(a.Data)
		if err != nil {
			return err
		}
		t.Queue(SQLInsertAttachmentData, encryptedHash, encryptedData)
	}

	//Save attachment metadata
	t.Queue(SQLUpsertAttachment, a.GuildID, a.ChannelID, a.MessageID, a.Index, encryptedFileName, encryptedURL, encryptedHash)

	return t.Commit()
}

//Deletes the attachment from the DB.
func (a Attachment) Delete(DB db.ReadWriter) error {
	return DB.QueueAndCommit(SQLDeleteAttachment, a.GuildID, a.ChannelID, a.MessageID, a.Index)
}

//File returns a *dg.File representation of the Attachment.
func (a Attachment) File() (file *dg.File, err error) {

	var data bytes.Buffer
	//TODO: Err is always nil
	//TODO: could panic, we need to recover from that.
	if _, err = data.Write(a.Data); err != nil {
		return nil, errors.WithStack(err)
	}
	file = &dg.File{
		Reader:      &data,
		Name:        a.FileName,
		ContentType: mime.TypeByExtension(filepath.Ext(a.FileName)),
	}
	return file, nil
}
