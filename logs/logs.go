package logs

import (
	"database/sql"
	"fmt"
	"time"

	dg "github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
	"gitlab.com/teterski-softworks/godbot/channels"
	"gitlab.com/teterski-softworks/godbot/db"
	"gitlab.com/teterski-softworks/godbot/embeds"
	"gitlab.com/teterski-softworks/godbot/guilds"
	"gitlab.com/teterski-softworks/godbot/members"
	"gitlab.com/teterski-softworks/godbot/messages"
	"gitlab.com/teterski-softworks/godbot/states"
)

//LogRecord represents a record of a Discord message.
type LogRecord struct {
	messages.Message
	Encrypted
}

//Encrypted stores the encrypted portion of LogRecord.
type Encrypted struct {
	MemberID  string
	Timestamp time.Time //truncated to full seconds.
	Content   string

	Attachments []*Attachment
}

/*New returns a new record from the provided Discord message.

Note: The returned LogRecord's attachments will not have any file data. This requires LogRecord.Download() to be run.
*/
func New(message dg.Message) LogRecord {

	var record LogRecord
	record.GuildID = message.GuildID
	record.ChannelID = message.ChannelID
	record.MessageID = message.ID
	record.MemberID = message.Author.ID
	record.Timestamp = message.Timestamp
	record.Content = message.Content

	for _, a := range message.Attachments {
		attachment, err := NewAttachment(a.URL, message.GuildID, message.ChannelID, message.ID)
		if err != nil {
			fmt.Printf("%+v\n", err) //TODO: handle this
			return record
		}
		record.Attachments = append(record.Attachments, &attachment)
	}
	return record
}

//Downloads the attachment file data for the LogRecord.
func (record *LogRecord) Download() error {

	//TODO: what if can't download? retry a few times and then give up?

	for _, a := range record.Attachments {
		if err := a.Download(); err != nil {
			return err
		}
		fmt.Println("attachment", a.MessageReference, a.FileName, len(a.Data)) //TODO: remove
	}
	return nil
}

//Retrieve returns a saved LogRecord from the DB.
func Retrieve(DB db.Reader, guildID string, channelID string, messageID string) (LogRecord, error) {
	var record LogRecord
	record.GuildID = guildID
	record.ChannelID = channelID
	record.MessageID = messageID
	row, err := DB.QueryRow(SQLSelectLogRecord, guildID, channelID, messageID)
	if err != nil {
		return record, err
	}
	var encryptedData []byte
	if err = row.Scan(&encryptedData); err != nil {
		return record, errors.WithStack(err)
	}

	record.Attachments, err = RetrieveAllAttachments(DB, guildID, channelID, messageID)
	if err != nil && !errors.Is(err, sql.ErrNoRows) {
		return record, err
	}

	//Decrypt the log record.
	if err = DB.Decrypt(&record.Encrypted, encryptedData); err != nil {
		return record, err
	}

	return record, nil
}

//RetrieveAllInChannel returns all LogRecords for channel.
func RetrieveAllInChannel(DB db.Reader, guildID string, channelID string) ([]LogRecord, error) {

	var records []LogRecord
	rows, err := DB.Query(SQLSelectGuildChannelLogRecords, guildID, channelID)
	if err != nil {
		return records, err
	}

	var encryptedData []byte
	for rows.Next() {
		var record LogRecord
		record.GuildID = guildID
		record.ChannelID = channelID
		if err := rows.Scan(&record.MessageID, &encryptedData); err != nil {
			return records, errors.WithStack(err)
		}

		//Decrypt the log record.
		if err = DB.Decrypt(&record.Encrypted, encryptedData); err != nil {
			return records, err
		}

		records = append(records, record)
	}
	return records, nil
}

//Store saves a LogRecord to the DB.
func (record LogRecord) Store(DB db.ReadWriter) error {

	t := DB.NewTransaction()

	//encrypt the log record
	encryptedData, err := t.Encrypt(record.Encrypted)
	if err != nil {
		return err
	}

	if err = channels.New(record.GuildID, record.ChannelID).Store(t); err != nil {
		return err
	}

	t.Queue(SQLUpsertLogRecord,
		record.GuildID,
		record.ChannelID,
		record.MessageID,
		encryptedData,
	)

	//TODO: what if we removed an index in the middle? how do we get rid of it.
	for _, a := range record.Attachments {
		if err = a.Store(t); err != nil {
			return err
		}
	}

	return t.Commit()
}

//StoreMultiple stores a list of LogRecords as a single database transaction.
//This greatly helps prevent database transaction congestion if many records need to be saved at the same time.
func StoreMultiple(DB db.ReadWriter, records []LogRecord) error {

	t := DB.NewTransaction()

	for _, record := range records {
		//encrypt the log record
		encryptedData, err := t.Encrypt(record.Encrypted)
		if err != nil {
			return err
		}
		if err := guilds.New(record.GuildID).Store(t); err != nil {
			return err
		}

		t.Queue(SQLUpsertLogRecord,
			record.GuildID,
			record.ChannelID,
			record.MessageID,
			encryptedData,
		)

		//TODO: is this efficient?
		for _, a := range record.Attachments {
			if err = a.Store(t); err != nil {
				return err
			}
		}
	}
	return t.Commit()
}

//Delete removes a LogRecord from the DB.
//If the record is already not in the provided database, then found returns false. Otherwise, returns true.
func (record LogRecord) Delete(DB db.ReadWriter) (found bool, err error) {

	if _, err = Retrieve(DB, record.GuildID, record.ChannelID, record.MessageID); errors.Is(err, sql.ErrNoRows) {
		return false, nil
	} else if err != nil {
		return false, err
	}
	if err = DB.QueueAndCommit(SQLDeleteLogRecord, record.GuildID, record.ChannelID, record.MessageID); err != nil {
		return false, err
	}
	return true, nil
}

//DeleteAllInChannel deletes all LogRecords for a channel in a guild.
func DeleteAllInChannel(DB db.ReadWriter, guildID string, channelID string) error {
	return DB.QueueAndCommit(SQLDeleteChannelLogRecords, guildID, channelID)
}

//DeleteAll deletes all LogRecords for a guild.
func DeleteAll(DB db.ReadWriter, guildID string) error {
	return DB.QueueAndCommit(SQLDeleteGuildLogRecords, guildID)
}

//RemoveAttachments removes attachments from the record at the given indexes.
func (record *LogRecord) RemoveAttachments(indexes ...int) {
	var attachments []*Attachment
	for i := range record.Attachments {
		remove := false
		for _, index := range indexes {
			if i == index {
				remove = true
				break
			}
		}
		if remove {
			continue
		}
		attachments = append(attachments, record.Attachments[i])
	}
	record.Attachments = attachments
}

//Equals returns true if a LogRecord is equal to the one being compared.
//*NOTE: This does not check if the attachment data is equal, only if both have the same NUMBER of attachments.
//*NOTE: This ignores differences in timestamp.
func (record LogRecord) Equals(compare LogRecord) bool {

	return record.GuildID == compare.GuildID &&
		record.ChannelID == compare.ChannelID &&
		record.MessageID == compare.MessageID &&
		record.MemberID == compare.MemberID &&
		record.Content == compare.Content &&
		len(record.Attachments) == len(compare.Attachments)
}

//editedMessageEmbeds returns a MessageEmbed logging the edit of a message.
func (recordBefore LogRecord) editedMessageEmbeds(s *states.State, recordAfter LogRecord) *dg.MessageEmbed {
	if recordBefore.Content == recordAfter.Content {
		return nil
	}
	embed := embeds.New("", "", "")
	embed.Color = embeds.ColorYellow
	member, user, err := members.GetMemberOrUserFromID(s.Session, recordBefore.GuildID, recordBefore.MemberID)
	if err == nil {
		if member != nil {
			embed.Thumbnail.URL = member.AvatarURL("")
		} else if user != nil {
			embed.Thumbnail.URL = user.AvatarURL("")
		}
	}
	embed.Title = "📝 Message Edited"
	embed.Footer.Text = fmt.Sprintf("Message ID: %s\n", recordBefore.MessageID)
	embed.Timestamp = time.Now().Format(time.RFC3339)

	embed.Description += fmt.Sprintf("**Author:** %s\n", members.Mention(recordBefore.MemberID))
	embed.Description += fmt.Sprintf("**Channel:** %s\n", channels.Mention(recordBefore.ChannelID))
	embed.Description += fmt.Sprintf("[Link to message.](%s)\n", recordBefore.Link())

	var beforeField dg.MessageEmbedField
	beforeField.Name = fmt.Sprintf("Before from <t:%d:R>:", recordBefore.Timestamp.Unix())
	beforeField.Value = recordBefore.Content
	if beforeField.Value == "" {
		beforeField.Value = "`None`"
	} else if len(beforeField.Value) >= 1024 {
		beforeField.Value = beforeField.Value[:1020] + "..."
	}
	embed.Fields = append(embed.Fields, &beforeField)

	var afterField dg.MessageEmbedField
	afterField.Name = "After:"
	afterField.Value = recordAfter.Content
	if afterField.Value == "" {
		afterField.Value = "`None`"
	} else if len(afterField.Value) >= 1024 {
		afterField.Value = afterField.Value[:1020] + "..."
	}
	embed.Fields = append(embed.Fields, &afterField)
	return &embed.MessageEmbed
}

//deletedMessageEmbeds returns a MessageEmbed logging the deletion of a message.
func (recordBefore LogRecord) deletedMessageEmbeds(s *states.State, deletedByID string) *dg.MessageEmbed {

	if recordBefore.Content == "" {
		return nil
	}

	embed := embeds.New("", "", "")
	embed.Color = embeds.ColorRed
	member, user, err := members.GetMemberOrUserFromID(s.Session, recordBefore.GuildID, recordBefore.MemberID)
	if err == nil {
		if member != nil {
			embed.Thumbnail.URL = member.AvatarURL("")
		} else if user != nil {
			embed.Thumbnail.URL = user.AvatarURL("")
		}
	}
	embed.Title = "📄 Message Deleted 🗑️"
	embed.Footer.Text = fmt.Sprintf("Message ID: %s\n", recordBefore.MessageID)
	embed.Timestamp = time.Now().Format(time.RFC3339)

	if deletedByID != "" {
		embed.Description += fmt.Sprintf("**Deleted by:** %s\n", members.Mention(deletedByID))
	}
	embed.Description += fmt.Sprintf("**Author:** %s\n", members.Mention(recordBefore.MemberID))
	embed.Description += fmt.Sprintf("**Channel:** %s\n", channels.Mention(recordBefore.ChannelID))
	embed.Description += fmt.Sprintf("[Link to message.](%s)\n\n", recordBefore.Link())

	embed.Description += fmt.Sprintf("**Before from <t:%d:R>:**\n", recordBefore.Timestamp.Unix())
	embed.Description += recordBefore.Content

	if len(embed.Description) >= 4096 {
		embed.Description = embed.Description[:4096] + "..."
	}
	return &embed.MessageEmbed
}

//deletedAttachmentsEmbeds returns a MessageEmbed logging the deletion of an attachment from a message,
//as well as any deleted attachment files, and the indexes of deleted attachments.
func (recordBefore LogRecord) deletedAttachmentsEmbeds(s *states.State, recordAfter LogRecord, deletedByID string) (

	messageEmbed *dg.MessageEmbed, deletedFiles []*dg.File, deletedIndexes []int, err error,
) {

	if len(recordBefore.Attachments) == 0 {
		return nil, nil, nil, nil
	}

	embed := embeds.New("", "", "")
	embed.Color = embeds.ColorRed
	var deletedAttachments []*Attachment
	for i, attachmentBefore := range recordBefore.Attachments {
		deleted := true
		for _, attachmentAfter := range recordAfter.Attachments {
			if attachmentBefore.URL == attachmentAfter.URL {
				deleted = false
				break
			}
		}
		if deleted {
			deletedAttachments = append(deletedAttachments, attachmentBefore)
			deletedIndexes = append(deletedIndexes, i)
		}

	}
	if len(deletedAttachments) == 0 {
		return nil, nil, nil, nil
	}

	member, user, err := members.GetMemberOrUserFromID(s.Session, recordBefore.GuildID, recordBefore.MemberID)
	if err == nil {
		if member != nil {
			embed.Thumbnail.URL = member.AvatarURL("")
		} else if user != nil {
			embed.Thumbnail.URL = user.AvatarURL("")
		}
	}
	embed.Title = "🔗 Attachment Deleted 🗑️"

	if deletedByID != "" {
		embed.Description += fmt.Sprintf("**Deleted by:** %s\n", members.Mention(deletedByID))
	}
	embed.Description += fmt.Sprintf("**Author:** %s\n", members.Mention(recordBefore.MemberID))
	embed.Description += fmt.Sprintf("**Channel:** %s\n", channels.Mention(recordBefore.ChannelID))
	embed.Description += fmt.Sprintf("[Link to message.](%s)\n\n", recordBefore.Link())

	embed.Footer.Text = fmt.Sprintf("Message ID: %s\n", recordBefore.MessageID)
	embed.Timestamp = time.Now().Format(time.RFC3339)

	for i, a := range deletedAttachments {

		file, err := a.File()
		if err != nil {
			return nil, nil, nil, err
		}

		deletedFiles = append(deletedFiles, file)

		embed.Fields = append(embed.Fields, &dg.MessageEmbedField{
			Name:  fmt.Sprintf("Attachment %d:", i+1),
			Value: file.Name + " `" + file.ContentType + "`",
		})
	}
	return &embed.MessageEmbed, deletedFiles, deletedIndexes, nil
}
