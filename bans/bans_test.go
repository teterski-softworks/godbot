package bans_test

import (
	"database/sql"
	"testing"
	"time"

	"gitlab.com/teterski-softworks/godbot/bans"
	"gitlab.com/teterski-softworks/godbot/db"
	"gitlab.com/teterski-softworks/godbot/members"
	"gitlab.com/teterski-softworks/godbot/states"
	"gitlab.com/teterski-softworks/godbot/tests"
)

var testDBFilePath = "/db_test.db"
var testDBKey = "key"

var guildID, memberID, bannedByMemberID, bannedByMemberUsername, reason, unban = "guildID", "memberID", "bannedByMemberID", "bannedByMemberUsername", "reason", false
var createTables = []func(db.ReadWriter) error{members.CreateDatabaseTables, bans.CreateDatabaseTables}

//TODO
func TestDatabaseAccess(t *testing.T) {

	expected := bans.New(guildID, memberID, bannedByMemberID, bannedByMemberUsername, reason, unban)
	expected.Timestamp = expected.Timestamp.Round(time.Nanosecond)
	updated := bans.BanRecord{}

	flags := tests.DefaultTestDatabaseAccessFlags
	flags.StoreEdit = false
	flags.RetrieveEdited = false
	flags.Delete = false

	retrieve := func(DB db.Reader) (bans.BanRecord, error) {
		banList, err := bans.RetrieveAllMember(DB, guildID, memberID)
		if err != nil {
			return bans.BanRecord{}, err
		} else if len(banList) == 0 {
			return bans.BanRecord{}, sql.ErrNoRows
		}
		return banList[0], nil
	}

	tests.TestDatabaseAccess(
		t,
		expected, updated,
		createTables,
		retrieve,
		flags,
	)
}

func Test_Bans(t *testing.T) {

	testDBFilePath = t.TempDir() + testDBFilePath

	DB, err := db.Open(testDBFilePath, testDBKey)
	if err != nil {
		t.Fatalf("%+v", err)
	}
	defer func() {
		if err := DB.Close(); err != nil {
			t.Fatalf("%+v", err)
		}
	}()

	t.Run("Test_CreateDatabaseTables", func(t *testing.T) {
		if err = bans.CreateDatabaseTables(DB); err != nil {
			t.Fatalf("%+v", err)
		}
	})

	ban := bans.New("guildID", "memberID", "bannedByMemberID", "bannedByUsername", "ban reason", false)
	unban := bans.New("guildID", "memberID", "unbannedByMemberID", "unbannedByUsername", "unban reason", true)

	t.Run("Test_Store", func(t *testing.T) {
		if err := ban.Store(DB); err != nil {
			t.Fatalf("%+v", err)
		}
		if unban.Store(DB); err != nil {
			t.Fatalf("%+v", err)
		}
	})

	t.Run("Test_Retrieve", func(t *testing.T) {
		banRecords, err := bans.RetrieveAllMember(DB, "guildID", "memberID")
		if err != nil {
			t.Fatalf("%+v", err)
		}
		t.Run("Ban Record Check", func(t *testing.T) {
			if banRecords[0].GuildID != "guildID" ||
				banRecords[0].MemberID != "memberID" ||
				banRecords[0].BannedByMemberID != "bannedByMemberID" ||
				banRecords[0].BannedByMemberUsername != "bannedByUsername" ||
				banRecords[0].Reason != "ban reason" ||
				banRecords[0].Unban != false {
				t.Fatalf("Retrieve returned wrong value.")
			}

			if banRecords[1].GuildID != "guildID" ||
				banRecords[1].MemberID != "memberID" ||
				banRecords[1].BannedByMemberID != "unbannedByMemberID" ||
				banRecords[1].BannedByMemberUsername != "unbannedByUsername" ||
				banRecords[1].Reason != "unban reason" ||
				banRecords[1].Unban != true {
				t.Fatalf("Retrieve returned wrong value.")
			}
		})
	})

	t.Run("Test_Alarm", func(t *testing.T) {
		t.SkipNow()
		//result := time.After(time.Second * 5)
		expected := time.Now().Add(time.Second * 5)
		ban.EndTime = expected
		s := states.New(nil)
		e := make(chan error, 1)
		ban.Alarm(s, e)
		if <-e != nil {
			t.Fatalf("%+v", err)
		}
		//TODO
	})

}
