package bans

import (
	"database/sql"
	"fmt"
	"strings"
	"time"

	dg "github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
	"gitlab.com/teterski-softworks/godbot/db"
	"gitlab.com/teterski-softworks/godbot/durations"
	"gitlab.com/teterski-softworks/godbot/embeds"
	"gitlab.com/teterski-softworks/godbot/guilds"
	"gitlab.com/teterski-softworks/godbot/logs"
	"gitlab.com/teterski-softworks/godbot/members"
	"gitlab.com/teterski-softworks/godbot/states"
)

var (
	ListTitle             = "⛔ Ban Records of %s"
	CustomIDSeparatorList = "banList"
)

//BanRecord represents a record of a member's ban (or un-ban).
type BanRecord struct {
	members.Member

	Unban           bool //True if the record is for an un-ban. False otherwise.
	manuallyRemoved bool //Only true if the ban has been lifted before its temporary duration expired. False otherwise.

	timerID int //The ID of the timer that fires when the ban expires, if temporary is set to true.

	Encrypted
}

//Encrypted stores the encrypted portion of BanRecord.
type Encrypted struct {
	BannedByMemberID       string //The ID of the member who issued the record.
	BannedByMemberUsername string //The username of the member who issued the record.
	Reason                 string

	Timestamp time.Time //When the record was issued.
	EndTime   time.Time //If the ban is a temporary ban, contains the expiry time of the ban.
}

//New returns a new ban record from the provided information.
func New(guildID string, memberID string, bannedByMemberID string, bannedByMemberUsername string, reason string, unban bool) BanRecord {
	var b BanRecord
	b.GuildID = guildID
	b.MemberID = memberID
	b.BannedByMemberID = bannedByMemberID
	b.BannedByMemberUsername = bannedByMemberUsername
	b.Reason = reason
	b.Unban = unban
	b.Timestamp = time.Now()
	return b
}

//RetrieveAllMember gets a member's ban records for the given server from the database.
func RetrieveAllMember(DB db.Reader, guildID string, memberID string) ([]BanRecord, error) {

	var bans []BanRecord

	rows, err := DB.Query(SQLSelectBanRecordsOfMember, guildID, memberID)
	if err != nil {
		return bans, err
	}
	for rows.Next() {
		b := New(guildID, memberID, "", "", "", false)

		var encryptedData []byte
		if err = rows.Scan(
			&b.Unban,
			&encryptedData,
		); err != nil {
			return bans, errors.WithStack(err)
		}

		if err = DB.Decrypt(&b, encryptedData); err != nil {
			return bans, err
		}
		bans = append(bans, b)
	}

	return bans, nil
}

//Store saves a ban record in the given database.
func (b BanRecord) Store(DB db.ReadWriter) error {
	t := DB.NewTransaction()

	encryptedData, err := t.Encrypt(b)
	if err != nil {
		return err
	}

	if err := guilds.New(b.GuildID).Store(t); err != nil {
		return err
	}

	t.Queue(SQLInsertBanRecord,
		b.GuildID,
		b.MemberID,

		b.Unban,
		b.timerID,

		encryptedData,
	)

	return t.Commit()
}

//Alarm un-bans a member if the duration of their temporary ban has expired.
func (b *BanRecord) Alarm(s *states.State, e chan<- error) {

	//Un-ban the user
	if err := s.GuildBanDelete(b.GuildID, b.MemberID); err != nil {
		if !strings.HasPrefix(err.Error(), "HTTP 404 Not Found") {
			e <- errors.WithStack(err)
			return
		}
	}

	b.BannedByMemberUsername = s.State.User.String()
	b.BannedByMemberID = s.State.User.ID
	b.Reason = ReasonBanExpired
	b.Unban = true
	if err := b.Store(s.DB); err != nil {
		e <- err
		return
	}

	//Delete all ban timers the member might have
	if err := DeleteAllMemberTimers(s, b.GuildID, b.MemberID); err != nil {
		e <- err
		return
	}

	if !b.manuallyRemoved {
		//Log the un-ban if ban logging is enabled
		embed := b.UnBanEmbed(s.Session)
		if err := logs.LogBan(s, b.GuildID, embed); err != nil {
			e <- err
			return
		}
	}
}

//DeleteAllMemberTimers stops and deletes any ban timers for the member.
func DeleteAllMemberTimers(s *states.State, guildID string, memberID string) error {

	//Get all running timers for the member from the DB
	rows, err := s.DB.Query(SQLSelectMemberRunningTimers, guildID, memberID)
	if err != nil {
		return err
	}
	for rows.Next() {
		var timerID int
		if err = rows.Scan(&timerID); err != nil {
			return errors.WithStack(err)
		}
		//Delete the timers from the bot's timer manager
		s.TimerManagers.Get(guildID).Delete(timerID)
	}

	//Mark all the timers for the member in the DB as expired.
	return s.DB.QueueAndCommit(SQLStopAllMemberTimers, guildID, memberID)
}

/*
InitializeTimers resumes timers for temporary bans.

Any errors that the timers encounter AFTER they are resumed are returned through the channel.
Other errors related to resuming the timers themselves are returned through the regular error.
*/
func InitializeTimers(s *states.State, guildID string) (chan error, error) {

	rows, err := s.DB.Query(SQLSelectGuildRunningTimers, guildID)
	if err != nil {
		return nil, err
	}

	var bans []BanRecord
	for rows.Next() {

		var ban BanRecord
		ban.GuildID = guildID

		var encryptedData []byte
		if err = rows.Scan(
			&ban.MemberID,
			&encryptedData,
		); err != nil && !errors.Is(err, sql.ErrNoRows) {
			return nil, errors.WithStack(err)
		}

		if err = s.DB.Decrypt(&ban, encryptedData); err != nil {
			return nil, err
		}

		bans = append(bans, ban)
	}

	//We do not have any running timers.
	if len(bans) == 0 {
		return nil, nil
	}

	e := make(chan error, len(bans))
	for _, b := range bans {
		b.timerID = s.TimerManagers.Get(guildID).New(time.Until(b.EndTime), s, b.Alarm, e)
		if err = b.Store(s.DB); err != nil {
			s.TimerManagers.Get(guildID).Delete(b.timerID)
			return e, err
		}
	}
	return e, nil
}

//BanEmbed returns an embed showing the ban.
func (b BanRecord) BanEmbed(session *dg.Session, duration time.Duration) *dg.MessageEmbed {
	embed := embeds.New("", "", "")
	embed.Color = embeds.ColorRed
	member, user, err := members.GetMemberOrUserFromID(session, b.GuildID, b.MemberID)
	if err == nil {
		if member != nil {
			embed.Thumbnail.URL = member.AvatarURL("")
		} else if user != nil {
			embed.Thumbnail.URL = user.AvatarURL("")
		}
	}
	embed.Title = "⛔ Member Banned"
	embed.Timestamp = time.Now().Format(time.RFC3339)
	embed.Description += fmt.Sprintf("%s has been banned by %s.\n\n", members.Mention(b.MemberID), members.Mention(b.BannedByMemberID))
	if duration == 0 {
		embed.Description += "**Duration:** `Indefinite`.\n"
	} else {
		embed.Description += fmt.Sprintf("**Duration:** for `%s` until <t:%d>.\n", durations.String(duration), time.Now().Add(duration).Unix())
	}
	embed.Description += fmt.Sprintf("**Reason:** %s", b.Reason)
	return &embed.MessageEmbed
}

//UnBanEmbed returns an embed showing the un-ban.
func (b BanRecord) UnBanEmbed(session *dg.Session) *dg.MessageEmbed {
	embed := embeds.New("", "", "")
	embed.Color = embeds.ColorGreen
	member, user, err := members.GetMemberOrUserFromID(session, b.GuildID, b.MemberID)
	if err == nil {
		if member != nil {
			embed.Thumbnail.URL = member.AvatarURL("")
		} else if user != nil {
			embed.Thumbnail.URL = user.AvatarURL("")
		}
	}
	embed.Title = "✅ Member Unbanned"
	embed.Timestamp = time.Now().Format(time.RFC3339)
	embed.Description += fmt.Sprintf("%s has been unbanned by %s.\n\n", members.Mention(b.MemberID), members.Mention(b.BannedByMemberID))
	embed.Description += fmt.Sprintf("**Reason:** %s", b.Reason)
	return &embed.MessageEmbed
}

//Returns a formatted string with information about the BanRecord. Used in menus showing a list of all member bans.
func (b BanRecord) Info() string {
	var info string
	if b.Unban {
		info += fmt.Sprintf("**✅ Unbanned on:** <t:%d>\n", b.Timestamp.Unix())
		info += fmt.Sprintf("**Unbanned by:** %s `%s`\n", b.BannedByMemberUsername, b.BannedByMemberID)
	} else {
		info += fmt.Sprintf("**⛔ Banned on:** <t:%d>\n", b.Timestamp.Unix())
		info += fmt.Sprintf("**Banned by:** %s `%s`\n", b.BannedByMemberUsername, b.BannedByMemberID)
		if !b.EndTime.IsZero() {
			info += fmt.Sprintf("**Duration:** `%s`", durations.String(b.EndTime.Sub(b.Timestamp).Truncate(time.Second)))
			info += fmt.Sprintf(" Until: <t:%d>\n", b.EndTime.Unix())
		} else {
			info += "**Duration:** `Permanent`\n"
		}
	}
	info += fmt.Sprintf("**Reason:** %s\n", b.Reason)
	return info
}
