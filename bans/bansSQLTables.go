package bans

import "gitlab.com/teterski-softworks/godbot/db"

//CreateDatabaseTables creates the required database tables for the warnings package.
func CreateDatabaseTables(DB db.ReadWriter) error {
	t := DB.NewTransaction()
	t.Queue(SQLCreateTableBans)
	return t.Commit()
}

var (
	SQLCreateTableBans string = `
	CREATE TABLE IF NOT EXISTS bans(
		guildID         TEXT NOT NULL REFERENCES guilds(guildID) ON DELETE CASCADE,
		memberID        TEXT NOT NULL,
		unBan           BOOLEAN DEFAULT FALSE,
		timerID         INT,
		encryptedData   BLOB NOT NULL

		--No primary key
	);`
)
