package bans

import (
	"strings"
	"time"

	dg "github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
	"gitlab.com/teterski-softworks/godbot/commands"
	"gitlab.com/teterski-softworks/godbot/db"
	"gitlab.com/teterski-softworks/godbot/durations"
	"gitlab.com/teterski-softworks/godbot/embeds"
	"gitlab.com/teterski-softworks/godbot/logs"
	"gitlab.com/teterski-softworks/godbot/states"
)

//SlashCommandBan - A command group for ban related commands.
var SlashCommandBan = commands.New(
	&dg.ApplicationCommand{
		Type:        dg.ChatApplicationCommand,
		Name:        "ban",
		Description: "Ban related commands. Admin only.",
		Options: []*dg.ApplicationCommandOption{
			slashCommandBanMember,
			slashCommandBanRemove,
			slashCommandBanList,
		},
	},
	false,
	func(intr states.Interaction, options ...*dg.ApplicationCommandInteractionDataOption) (err error) {

		if err := intr.Acknowledge(); err != nil {
			return err
		}

		switch options[0].Name {
		case slashCommandBanMember.Name:
			return slashBanMember(intr, options[0].Options)
		case slashCommandBanRemove.Name:
			return slashBanRemove(intr, options[0].Options)
		case slashCommandBanList.Name:
			return slashBanList(intr, options[0].Options)
		}
		return intr.EditResponseEmbeds(nil, states.ResponseGenericError(nil))
	},
)

//slashCommandBanMember - A subcommand to ban a member.
var slashCommandBanMember = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionSubCommand,
	Name:        "member",
	Description: "Bans a member.",
	Options: []*dg.ApplicationCommandOption{
		{
			Type:        dg.ApplicationCommandOptionUser,
			Name:        "member",
			Description: "The member who will be banned.",
			Required:    true,
		}, {
			Type:        dg.ApplicationCommandOptionString,
			Name:        "reason",
			Description: "The reason for the ban.",
		}, {
			Type:        dg.ApplicationCommandOptionString,
			Name:        "duration",
			Description: "The duration of the ban.",
		}, {
			Type:        dg.ApplicationCommandOptionInteger,
			Name:        "days_to_remove",
			Description: "The number of days of previous messages to delete. Between 0 to 7 days.",
			MinValue:    &minDays,
			MaxValue:    7,
		},
	},
}
var minDays float64 = 0

//slashBanMember bans a member.
func slashBanMember(intr states.Interaction, options []*dg.ApplicationCommandInteractionDataOption) (err error) {

	member := options[0].UserValue(intr.State.Session)
	ban := New(
		intr.GuildID,
		member.ID,
		intr.Member.User.ID,
		intr.Member.User.String(),
		ReasonNone,
		false,
	)

	var duration time.Duration
	days := 0
	for _, option := range options {
		switch option.Name {
		case "reason":
			ban.Reason = option.StringValue()
		case "duration":
			duration, err = durations.Parse(option.StringValue())
			if err != nil {
				return intr.EditResponseEmbeds(nil, durations.ResponseInvalidDurationError())
			}
			ban.EndTime = time.Now().Add(duration)
		case "days_to_remove":
			days = int(option.IntValue())
		}
	}

	//It is possible the member might have been banned already
	//So we need to stop all other temporary bans from expiring.
	if err := DeleteAllMemberTimers(intr.State, ban.GuildID, ban.MemberID); err != nil {
		return err
	}

	//Ban the user
	if err := intr.GuildBanCreateWithReason(ban.GuildID, ban.MemberID, ban.Reason, days); err != nil {
		return intr.EditResponseEmbeds(err, states.ResponseBotPermission())
	}

	e := make(chan error, 1)
	defer close(e)
	if duration != 0 {
		ban.timerID = intr.TimerManagers.Get(ban.GuildID).New(duration, intr.State, ban.Alarm, e)
	}

	if err := ban.Store(intr.DB); err != nil {
		intr.TimerManagers.Get(ban.GuildID).Stop(ban.timerID)
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}

	//Log the ban if ban logging is enabled
	embed := ban.BanEmbed(intr.Session, duration)
	if err := logs.LogBan(intr.State, ban.GuildID, embed); err != nil {
		return intr.EditResponseEmbeds(err, states.ResponseGenericError(err))
	}

	//Respond to the user
	if err = intr.EditResponseEmbeds(nil, embed); err != nil {
		return err
	}

	return <-e
}

//slashCommandBanRemove - A subcommand to un-ban a member.
var slashCommandBanRemove = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionSubCommand,
	Name:        "remove",
	Description: "Un-bans a member.",
	Options: []*dg.ApplicationCommandOption{
		{
			Type:        dg.ApplicationCommandOptionUser,
			Name:        "member_id",
			Description: "The member to un-ban.",
			Required:    true,
		}, {
			Type:        dg.ApplicationCommandOptionString,
			Name:        "reason",
			Description: "The reason for the ban.",
		},
	},
}

//slashBanRemove un-bans a member.
func slashBanRemove(intr states.Interaction, options []*dg.ApplicationCommandInteractionDataOption) (err error) {

	member := options[0].UserValue(intr.Session)
	reason := ReasonNone
	if len(options) == 2 {
		reason = options[1].StringValue()
	}
	unban := New(
		intr.GuildID,
		member.ID,
		intr.Member.User.ID,
		intr.Member.User.String(),
		reason,
		true,
	)
	unban.manuallyRemoved = true
	//Delete all ban timers the member might have
	if err := DeleteAllMemberTimers(intr.State, unban.GuildID, unban.MemberID); err != nil {
		return err
	}

	//Un-ban the user
	if err := intr.GuildBanDelete(unban.GuildID, unban.MemberID); err != nil {
		if !strings.HasPrefix(err.Error(), "HTTP 404 Not Found") {
			return intr.EditResponseEmbeds(errors.WithStack(err), states.ResponseBotPermission())
		}
	}

	if err = unban.Store(intr.DB); err != nil {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}

	//Log the un-ban if ban logging is enabled
	embed := unban.UnBanEmbed(intr.Session)
	if err := logs.LogBan(intr.State, unban.GuildID, embed); err != nil {
		return intr.EditResponseEmbeds(err, states.ResponseGenericError(err))
	}

	//Respond to the user
	return intr.EditResponseEmbeds(nil, embed)
}

//slashCommandBanList - A sub command to view a members ban records.
var slashCommandBanList = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionSubCommand,
	Name:        "list",
	Description: "Lists a member's previous bans and un-bans.",
	Options: []*dg.ApplicationCommandOption{
		{
			Type:        dg.ApplicationCommandOptionUser,
			Name:        "member",
			Description: "The member whose bans/un-bans to list.",
			Required:    true,
		},
	},
}

//slashBanList views a member's ban records.
func slashBanList(intr states.Interaction, options []*dg.ApplicationCommandInteractionDataOption) (err error) {

	user := options[0].UserValue(intr.Session)
	list, err := embeds.NewMemberList(intr.State, intr.GuildID, user.ID, ListTitle, CustomIDSeparatorList, RetrieveAllMember)
	if err != nil {
		return intr.EditResponseEmbeds(err, states.ResponseGenericError(err))
	}

	response := &dg.WebhookEdit{
		Embeds:     list.Embeds(0),
		Components: list.Components(0),
	}
	if _, err := intr.InteractionResponseEdit(&intr.Interaction, response); err != nil {
		return errors.WithStack(err)
	}
	return nil
}
