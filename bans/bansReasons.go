package bans

var (
	ReasonNone       string = "No reason provided."
	ReasonBanExpired string = "Ban duration expired."
)
