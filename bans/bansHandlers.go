package bans

import (
	"strconv"
	"strings"

	dg "github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
	"gitlab.com/teterski-softworks/godbot/auditLogs"
	"gitlab.com/teterski-softworks/godbot/db"
	"gitlab.com/teterski-softworks/godbot/embeds"
	"gitlab.com/teterski-softworks/godbot/logs"
	"gitlab.com/teterski-softworks/godbot/states"
)

//GuildBanAddHandler saves and logs any bans made on the server made WITHOUT the use of the bot.
func GuildBanAddHandler(s *states.State, event *dg.GuildBanAdd) error {

	reason, bannedByMemberID, err := auditLogs.GetBanInfo(s, event.GuildID, event.User.ID)
	if err != nil {
		return err
	}
	if reason == "" {
		reason = ReasonNone
	}

	if bannedByMemberID == s.State.User.ID {
		//If the ban was initialized by the bot, do not handle the event, as it has already been handled
		return nil
	}

	user, err := s.User(bannedByMemberID)
	if err != nil {
		return err
	}
	bannedByMemberUsername := user.Username

	//It is possible the member might have been banned already
	//So we need to stop all other temporary bans from expiring.
	if err := DeleteAllMemberTimers(s, event.GuildID, event.User.ID); err != nil {
		return err
	}

	ban := New(
		event.GuildID,
		event.User.ID,
		bannedByMemberID,
		bannedByMemberUsername,
		reason,
		false,
	)

	//Save the ban
	if err := ban.Store(s.DB); err != nil {
		return err
	}

	//Log the ban if ban logging is enabled
	embed := ban.BanEmbed(s.Session, 0)
	if err := logs.LogBan(s, ban.GuildID, embed); err != nil {
		return err
	}

	return nil
}

//GuildBanRemoveHandler saves and logs any unbans made on the server made WITHOUT the use of the bot.
func GuildBanRemoveHandler(s *states.State, event *dg.GuildBanRemove) error {

	//Get the unban information from the audit log
	auditLogs, err := s.GuildAuditLog(event.GuildID, "", "", int(dg.AuditLogActionMemberBanRemove), 1)
	if err != nil {
		return errors.WithStack(err)
	}
	unbannedByMemberID, unbannedByMemberUsername := "", ""
	for _, auditLogEntry := range auditLogs.AuditLogEntries {
		if auditLogEntry.TargetID != event.User.ID {
			continue
		}
		unbannedByMemberID = auditLogEntry.UserID

		if unbannedByMemberID == s.State.User.ID {
			//If the unban was initialized by the bot, do not handle the event, as it has already been handled
			return nil
		}

		user, err := s.User(unbannedByMemberID)
		if err != nil {
			return err
		}
		unbannedByMemberUsername = user.Username
		break
	}

	unban := New(
		event.GuildID,
		event.User.ID,
		unbannedByMemberID,
		unbannedByMemberUsername,
		ReasonNone,
		true,
	)

	//Delete all ban timers the member might have
	if err := DeleteAllMemberTimers(s, unban.GuildID, unban.MemberID); err != nil {
		return err
	}

	//Save the unban
	if err := unban.Store(s.DB); err != nil {
		return err
	}

	//Log the ban if ban logging is enabled
	embed := unban.UnBanEmbed(s.Session)
	if err := logs.LogBan(s, unban.GuildID, embed); err != nil {
		return err
	}

	return nil
}

//ListPageSelectionHandler allows for switching between pages in an embed list.
func ListPageSelectionHandler(intr states.Interaction, customID string) error {

	splitID := strings.Split(customID, CustomIDSeparatorList)
	memberID := splitID[0]
	page, err := strconv.Atoi(splitID[1])
	if err != nil {
		return intr.RespondEphemeralEmbeds(err, states.ResponseGenericError(err))
	}
	list, err := embeds.NewMemberList(intr.State, intr.GuildID, memberID, ListTitle, CustomIDSeparatorList, RetrieveAllMember)
	if err != nil {
		return intr.RespondEphemeralEmbeds(err, db.ResponseDBError(err))
	}

	response := &dg.InteractionResponse{
		Type: dg.InteractionResponseUpdateMessage,
		Data: &dg.InteractionResponseData{
			Embeds:     list.Embeds(page),
			Components: list.Components(page),
		},
	}
	return intr.InteractionRespond(&intr.Interaction, response)
}
