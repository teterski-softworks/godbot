package bans

import (
	dg "github.com/bwmarrin/discordgo"
	"gitlab.com/teterski-softworks/godbot/embeds"
	"gitlab.com/teterski-softworks/godbot/members"
)

var (
	ResponseMemberAlreadyBanned = func(memberID string) *dg.MessageEmbed {
		embed := embeds.New("", "", "")
		embed.Title = "✅ Already Banned"
		embed.Color = embeds.ColorGreen
		embed.Description = members.Mention(memberID) + " is already banned."
		return &embed.MessageEmbed
	}

	ResponseMemberNotBanned = func(memberID string) *dg.MessageEmbed {
		embed := embeds.New("", "", "")
		embed.Title = "✅ Already Not Banned"
		embed.Color = embeds.ColorGreen
		embed.Description = members.Mention(memberID) + " is already not banned."
		return &embed.MessageEmbed
	}
)
