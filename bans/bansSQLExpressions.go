package bans

var (
	SQLSelectGuildRunningTimers string = `
	SELECT memberID, encryptedData
	FROM bans
	WHERE guildID=(?1) AND timerID!=0;`

	SQLSelectMemberRunningTimers string = `
	SELECT timerID
	FROM bans
	WHERE guildID=(?1) AND memberID=(?2) AND timerID!=0;`

	SQLStopAllMemberTimers string = `
	UPDATE bans
	SET timerID=0
	WHERE guildID=(?1) AND memberID=(?2);`

	SQLSelectBanRecordsOfMember string = `
	SELECT unBan, encryptedData
	FROM bans
	WHERE guildID=(?1) AND memberID=(?2)
	ORDER BY rowID ASC;`

	SQLInsertBanRecord string = `
	INSERT INTO bans(
		guildID,
		memberID,

		unBan,
		timerID,
		encryptedData
	) VALUES (?1, ?2, ?3, ?4, ?5);`
)
