package main

import (
	"fmt"
	"os"

	dg "github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
	"gitlab.com/teterski-softworks/godbot/auditLogs"
	"gitlab.com/teterski-softworks/godbot/autoReacts"
	"gitlab.com/teterski-softworks/godbot/bans"
	"gitlab.com/teterski-softworks/godbot/bookmarks"
	"gitlab.com/teterski-softworks/godbot/bots"
	"gitlab.com/teterski-softworks/godbot/channels"
	"gitlab.com/teterski-softworks/godbot/commands"
	"gitlab.com/teterski-softworks/godbot/confessions"
	"gitlab.com/teterski-softworks/godbot/counters"
	"gitlab.com/teterski-softworks/godbot/db"
	"gitlab.com/teterski-softworks/godbot/embeds"
	"gitlab.com/teterski-softworks/godbot/guilds"
	"gitlab.com/teterski-softworks/godbot/guilds/guildRemovers"
	"gitlab.com/teterski-softworks/godbot/info"
	"gitlab.com/teterski-softworks/godbot/invites"
	"gitlab.com/teterski-softworks/godbot/leveling"
	"gitlab.com/teterski-softworks/godbot/logs"
	"gitlab.com/teterski-softworks/godbot/members"
	"gitlab.com/teterski-softworks/godbot/messages"
	"gitlab.com/teterski-softworks/godbot/mutes"
	"gitlab.com/teterski-softworks/godbot/respondChannels"
	"gitlab.com/teterski-softworks/godbot/roleMenus"
	"gitlab.com/teterski-softworks/godbot/roles"
	"gitlab.com/teterski-softworks/godbot/starBoards"
	"gitlab.com/teterski-softworks/godbot/stats"
	"gitlab.com/teterski-softworks/godbot/warnings"
)

func main() {

	appID, ok := os.LookupEnv("APPID")
	if !ok {
		fmt.Printf("%+v\n", errors.New("no app ID provided"))
		return
	}

	token, ok := os.LookupEnv("BOTTOKEN")
	if !ok {
		fmt.Printf("%+v\n", errors.New("no bot token provided"))
		return
	}

	dbKey, ok := os.LookupEnv("DBKEY")
	if !ok {
		fmt.Printf("%+v\n", errors.New("no database key provided"))
		return
	}

	if len(os.Args) <= 2 {
		fmt.Printf("%+v\n", errors.New("no database path provided"))
		return
	}
	dbPath := os.Args[1]
	logPath := os.Args[2]

	//https://discord.com/developers/docs/topics/gateway#gateway-intents
	intents := dg.IntentGuilds +
		dg.IntentGuildMembers +
		dg.IntentGuildIntegrations +
		dg.IntentGuildWebhooks +
		dg.IntentGuildMessages +
		dg.IntentGuildMessageReactions +
		dg.IntentDirectMessages +
		dg.IntentGuildInvites +
		dg.IntentGuildBans

	tables := []func(DB db.ReadWriter) error{
		guilds.CreateDatabaseTables,
		guildRemovers.CreateDatabaseTables,
		channels.CreateDatabaseTables,
		messages.CreateDatabaseTables,
		roles.CreateDatabaseTables,
		members.CreateDatabaseTables,
		commands.CreateDatabaseTables,
		auditLogs.CreateDatabaseTables,
		logs.CreateDatabaseTables,
		bans.CreateDatabaseTables,
		warnings.CreateDatabaseTables,
		mutes.CreateDatabaseTables,
		counters.CreateDatabaseTables,
		embeds.CreateDatabaseTables,
		roleMenus.CreateDatabaseTables,
		invites.CreateDatabaseTables,
		leveling.CreateDatabaseTables,
		confessions.CreateDatabaseTables,
		autoReacts.CreateDatabaseTables,
		respondChannels.CreateDatabaseTables,
		starBoards.CreateDatabaseTables,
	}

	cmds := []commands.Commander{

		info.SlashCommandInfo,
		bookmarks.MessageCommandBookmark,
		invites.SlashCommandInvite,
	}

	cmdsAdmin := []commands.Commander{
		messages.SlashCommandMessage,
		embeds.SlashCommandEmbed,
		embeds.MessageCommandSaveEmbed,
		embeds.MessageCommandUnSaveEmbed,
		embeds.MessageCommandEmbedMessage,
		bans.SlashCommandBan,
		warnings.SlashCommandWarn,
		mutes.SlashCommandMute,
		counters.SlashCommandCounter,
		stats.SlashCommandStats,
		roleMenus.SlashCommandRoleMenu,
		roleMenus.MessageCommandRoleMenuNew,
		invites.SlashCommandInvites,
		logs.SlashCommandLog,
		leveling.SlashCommandLeveling,
		confessions.SlashCommandAdminConfession,
		autoReacts.SlashCommandAdminAutoReacts,
		respondChannels.SlashCommandAdminRespondChannels,
		starBoards.SlashCommandAdminStarBoard,
	}

	client, err := bots.New(token, appID, dbPath, dbKey, logPath, intents, tables, bots.GuildDBMaintenance, bots.AddHandlers, cmds, cmdsAdmin, bots.SetStatusBooting, bots.SetStatusReady)
	if err != nil {
		fmt.Printf("%+v\n", errors.Wrap(err, "unable to create client"))
		return
	}

	go client.SignalHandler()

	<-make(chan bool)
}
