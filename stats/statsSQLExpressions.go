package stats

const (
	SQLStats string = `
	SELECT * FROM (
		(SELECT COUNT(*) AS members FROM members WHERE guildID=(?1)) CROSS JOIN
		(SELECT COUNT(*) AS mutes FROM mutes WHERE guildID=(?1)) CROSS JOIN
		(SELECT COUNT(*) AS bans FROM bans WHERE guildID=(?1) AND temporary==TRUE)
	);`

	SQLSelectCommandStats string = `
	SELECT * FROM (
		SELECT REPLACE(cmd.name ||" "|| sub.groupName ||" "|| sub.name, "  ", " ") AS name, sub.timesUsed As timesUsed
		FROM subCommands sub, commands cmd
		WHERE cmd.guildID=(?1) AND cmd.commandID == sub.commandID
		UNION
		SELECT name, timesUsed
		FROM commands
		WHERE guildID=(?1) AND commandID NOT IN(
			SELECT DISTINCT	commandID
			FROM subCommands
		)
	)
	ORDER BY timesUsed DESC, name ASC
	LIMIT 10;`
)
