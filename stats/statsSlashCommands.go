package stats

import (
	"database/sql"
	"fmt"
	"time"

	dg "github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
	"gitlab.com/teterski-softworks/godbot/commands"
	"gitlab.com/teterski-softworks/godbot/counters"
	"gitlab.com/teterski-softworks/godbot/db"
	"gitlab.com/teterski-softworks/godbot/embeds"
	"gitlab.com/teterski-softworks/godbot/states"
)

//SlashCommandStats shows statistical information about the bot!
var SlashCommandStats = commands.New(
	&dg.ApplicationCommand{
		Type:        dg.ChatApplicationCommand,
		Name:        "stats",
		Description: "Shows statistical information about the bot. Admin only.",
	},
	false,
	func(intr states.Interaction, _ ...*dg.ApplicationCommandInteractionDataOption) (err error) {

		if err := intr.Acknowledge(); err != nil {
			return err
		}

		var row *sql.Row
		if row, err = intr.DB.QueryRow(SQLStats, intr.GuildID); err != nil {
			return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
		}
		var (
			members,
			mutes,
			bans int
		)
		if err := row.Scan(
			&members,
			&mutes,
			&bans); err != nil {
			return intr.EditResponseEmbeds(errors.WithStack(err), db.ResponseDBError(err))
		}

		generalField := &dg.MessageEmbedField{
			Name:   "**👥 General:**",
			Inline: true,
		}
		generalField.Value += fmt.Sprintf("`%d` Members in database\n", members)
		generalField.Value += fmt.Sprintln("\n**⏲️ Timers:**")
		generalField.Value += fmt.Sprintf("`%d` Members temp muted\n", mutes)
		generalField.Value += fmt.Sprintf("`%d` Members temp baned\n", bans)
		generalField.Value += fmt.Sprintln("\n**🔢 Counters:**")

		rows, err := intr.DB.Query(counters.SQLSelectAllCounterAllTimeCounts, intr.GuildID)
		if err != nil {
			return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
		}

		var channelID string
		var counterAllTime int
		for rows.Next() {

			if err = rows.Scan(&channelID, &counterAllTime); err != nil {
				return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
			}

			channel, err := intr.Channel(channelID)
			if err != nil {
				return intr.EditResponseEmbeds(err, states.ResponseGenericError(err))
			}

			generalField.Value += fmt.Sprintf("`%d` %s\n", counterAllTime, channel.Mention())

		}

		commandsField := &dg.MessageEmbedField{
			Name:   "**🏅 Top 10 Commands:**",
			Inline: true,
		}

		if rows, err = intr.DB.Query(SQLSelectCommandStats, intr.GuildID); err != nil {
			return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
		}
		var cmdName string
		var cmdUsed int
		for rows.Next() {

			if err = rows.Scan(&cmdName, &cmdUsed); err != nil && !errors.Is(err, sql.ErrNoRows) {
				return errors.WithStack(err)
			}

			if cmdUsed == 0 {
				continue
			}

			commandsField.Value += fmt.Sprintf("`%d` %s\n", cmdUsed, cmdName)
		}
		if commandsField.Value == "" {
			commandsField.Value = "No commands have yet been used."
		}

		embed := embeds.NewDefault(intr.DB, intr.GuildID)
		embed.Title = "📊 Bot Statistics"
		embed.Description = "Below are some statistics about the " + intr.State.State.User.Mention() + " bot."
		embed.Timestamp = time.Now().Format(time.RFC3339)
		embed.Fields = []*dg.MessageEmbedField{
			commandsField,
			generalField,
		}

		return intr.EditResponseEmbeds(nil, &embed.MessageEmbed)
	},
)
