package states

import (
	"fmt"

	dg "github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
	"gitlab.com/teterski-softworks/godbot/emojiSelectors"
)

//ErrUnableToRespond is an error which occurs when an interaction cannot be responded to.
var ErrUnableToRespond = fmt.Errorf("unable to respond")

//Interaction is a wrapper around discordgo.Interaction. It is state aware, and has methods to allowing to respond to it.
type Interaction struct {
	*State
	dg.Interaction
}

//NewInteraction wraps an existing discordgo.Interaction and returns the new Interaction object.
func NewInteraction(s *State, interaction dg.Interaction) Interaction {
	return Interaction{
		State:       s,
		Interaction: interaction,
	}
}

//acknowledge tells Discord that the stored interaction has been received and will be responded to later.
func (intr Interaction) acknowledge(flags dg.MessageFlags) error {

	response := &dg.InteractionResponse{
		Type: dg.InteractionResponseDeferredChannelMessageWithSource,
		Data: &dg.InteractionResponseData{
			Flags: uint64(flags),
		},
	}
	if err := intr.State.InteractionRespond(&intr.Interaction, response); err != nil {
		return errors.WithStack(err)
	}
	return nil
}

//Acknowledge lets Discord know that the interaction was received and will be responded to later.
func (intr Interaction) Acknowledge() error {
	return intr.acknowledge(0)
}

//AcknowledgeInteraction lets Discord know that the Interaction was received and will be responded to later.
//The user will receive an ephemeral message response.
func (intr Interaction) AcknowledgeEphemeral() error {
	return intr.acknowledge(dg.MessageFlagsEphemeral)
}

//respond is a helper function for responding to an Interaction with proper flags.
//If `e` is not nil, `e` is returned and wrapped with any errors encountered.
//Any values for `a` are formatted into the format specifiers in `content`.
func (intr Interaction) respond(content string, embeds []*dg.MessageEmbed, flags dg.MessageFlags, e error, a ...any) error {

	if content == "" {
		return e
	}

	var data dg.InteractionResponseData
	if content != "" {
		data.Content = fmt.Sprintf(content, a...)
	}
	data.Embeds = embeds
	data.Flags = uint64(flags)

	var response dg.InteractionResponse
	response.Type = dg.InteractionResponseChannelMessageWithSource
	response.Data = &data
	if err := intr.State.InteractionRespond(&intr.Interaction, &response); err != nil {
		e = errors.Wrapf(err, ErrUnableToRespond.Error())
	}

	return e
}

//Respond responds to an Interaction.
//If `e` is not nil, `e` is returned and wrapped with any errors encountered.
//Any values for `a` are formatted into the format specifiers in `content`.
func (intr Interaction) Respond(content string, e error, a ...any) error {
	return intr.respond(content, nil, 0, e, a...)
}

//RespondEmbeds responds to an Interaction with embeds.
//If `e` is not nil, `e` is returned and wrapped with any errors encountered.
//Any values for `a` are formatted into the format specifiers in `content`.
func (intr Interaction) RespondEmbeds(e error, embeds ...*dg.MessageEmbed) error {
	return intr.respond("", embeds, 0, e)
}

//RespondEphemeral responds to an Interaction with an ephemeral message.
//If `e` is not nil, `e` is returned and wrapped with any errors encountered.
//Any values for `a` are formatted into the format specifiers in `content`.
func (intr Interaction) RespondEphemeral(content string, e error, a ...any) error {
	return intr.respond(content, nil, dg.MessageFlagsEphemeral, e, a...)
}

//RespondEphemeralEmbeds responds to an Interaction with ephemeral embeds.
//If `e` is not nil, `e` is returned and wrapped with any errors encountered.
//Any values for `a` are formatted into the format specifiers in `content`.
func (intr Interaction) RespondEphemeralEmbeds(e error, embeds ...*dg.MessageEmbed) error {
	return intr.respond("", embeds, dg.MessageFlagsEphemeral, e)
}

//EditResponse edits a previously acknowledged Interaction.
//If `e` is not nil, `e` is returned and wrapped with any errors encountered.
//Any values for `a` are formatted into the format specifiers in `content`.
func (intr Interaction) EditResponse(content string, e error, a ...any) error {

	if content == "" {
		return e
	}

	var response dg.WebhookEdit
	response.Content = fmt.Sprintf(content, a...)
	_, err := intr.State.InteractionResponseEdit(&intr.Interaction, &response)
	if err != nil {
		e = errors.Wrapf(err, ErrUnableToRespond.Error())
	}

	return e
}

//EditResponseEmbeds edits a previously acknowledged Interaction with embeds.
//If `e` is not nil, `e` is returned and wrapped with any errors encountered.
//Any values for `a` are formatted into the format specifiers in `content`.
func (intr Interaction) EditResponseEmbeds(e error, embeds ...*dg.MessageEmbed) error {
	var response dg.WebhookEdit
	response.Embeds = embeds
	if _, err := intr.State.InteractionResponseEdit(&intr.Interaction, &response); err != nil {
		e = errors.Wrapf(err, "unable to respond")
	}
	return e
}

//TODO
func (intr Interaction) RequestEmoji() (emoji dg.Emoji, err error) {

	//TODO: what if emoji selector message gets deleted?

	embed := emojiSelectors.Embed(0)
	response := &dg.WebhookEdit{
		Embeds: []*dg.MessageEmbed{embed},
	}
	message, err := intr.State.InteractionResponseEdit(&intr.Interaction, response)
	if err != nil {
		return emoji, errors.Wrapf(err, "unable to respond")
	}
	intr.EmojiSelectors.Get(intr.GuildID).NewSelection(intr.ChannelID, message.ID)

	//Listen for a response
	emoji = intr.EmojiSelectors.Get(intr.GuildID).GetSelection(intr.ChannelID, message.ID)
	return emoji, nil
}
