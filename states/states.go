package states

import (
	"fmt"
	"log"

	dg "github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
	"gitlab.com/teterski-softworks/godbot/concurrentMaps"
	"gitlab.com/teterski-softworks/godbot/db"
	"gitlab.com/teterski-softworks/godbot/emojiSelectors"
	"gitlab.com/teterski-softworks/godbot/logs/logSchedulers"
	"gitlab.com/teterski-softworks/godbot/members"
	"gitlab.com/teterski-softworks/godbot/webhooks"
)

//State stores all required bot and Discord session information needed for a Command to run.
type State struct {
	*dg.Session
	*log.Logger
	DB db.Database

	//keeps track and schedules log record updates
	LogRecordSchedulers concurrentMaps.Map[ /*Guild ID*/ string, *logSchedulers.Scheduler]

	//keeps track of all running timers
	TimerManagers concurrentMaps.Map[ /*GuildID*/ string, TimerManager]

	//keeps track of any emoji selection menus.
	EmojiSelectors concurrentMaps.Map[ /*GuildID*/ string, emojiSelectors.EmojiSelection]
}

//New returns a new Session struct
func New(log *log.Logger) *State {
	return &State{
		DB:                  &db.DB{},
		Logger:              log,
		Session:             &dg.Session{},
		TimerManagers:       concurrentMaps.New[string, TimerManager](),
		LogRecordSchedulers: concurrentMaps.New[string, *logSchedulers.Scheduler](),
		EmojiSelectors:      concurrentMaps.New[string, emojiSelectors.EmojiSelection](),
	}
}

//SendMessage sends a message (or message reply to 'reference' if not nil) to the channel ID provided.
//If `e` is not nil, `e` is returned and wrapped with any errors encountered.
//Any values for `a` are formatted into the format specifiers in `content`.
func (s *State) SendMessage(content string, channelID string, reference *dg.MessageReference, e error, a ...any) (*dg.Message, error) {

	if content == "" {
		return nil, e
	}
	var message dg.MessageSend
	message.Content = fmt.Sprintf(content, a...)
	message.Reference = reference

	st, err := s.ChannelMessageSendComplex(channelID, &message)
	if err != nil {
		e = errors.Wrapf(err, "unable to send message")
	}
	return st, e
}

//SendEmbeds sends embeds (or replies with embeds to 'reference' if not nil) to the channel ID provided.
//If `e` is not nil, `e` is returned and wrapped with any errors encountered.
func (s *State) SendEmbeds(channelID string, reference *dg.MessageReference, e error, embeds ...*dg.MessageEmbed) (*dg.Message, error) {

	var message dg.MessageSend
	message.Embeds = embeds
	message.Reference = reference

	st, err := s.ChannelMessageSendComplex(channelID, &message)
	if err != nil {
		e = errors.Wrapf(err, "unable to send message")
	}
	return st, e
}

//impersonate sends a message to the connected Discord server that impersonates another member.
func (s *State) Impersonate(message dg.WebhookParams, guildID string, memberID string, channelID string, webhookManager webhooks.Manager) (st *dg.Message, err error) {

	member, user, err := members.GetMemberOrUserFromID(s.Session, guildID, memberID)
	if err != nil {
		return st, errors.WithStack(err)
	}
	if member != nil {
		message.AvatarURL = member.AvatarURL("")
	} else if user != nil {
		message.AvatarURL = user.AvatarURL("")
	}

	message.AvatarURL = member.AvatarURL("")

	nickname := member.Nick
	if nickname == "" {
		nickname = member.User.Username
	}

	webhook, err := webhookManager.GetWebhook(channelID, nickname, message.AvatarURL, s.Session)
	if err != nil {
		return st, errors.WithStack(err)
	}

	st, err = s.WebhookExecute(webhook.ID, webhook.Token, true, &message)
	if err != nil {
		return st, errors.WithStack(err)
	}
	return st, nil
}

//GuildChannels returns a list of all channels in a guild.
func (s *State) GuildChannels(guildID string) []*dg.Channel {
	// We can't get a full list of channels from s.GuildChannels(guildID), so this is a work around
	var channels []*dg.Channel
	for _, g := range s.State.Guilds {
		if g.ID != guildID {
			continue
		}
		channels = g.Channels
		break
	}
	return channels
}
