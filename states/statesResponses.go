package states

import (
	dg "github.com/bwmarrin/discordgo"
)

var (
	ResponseGenericError = func(e error) *dg.MessageEmbed {
		//TODO: use embeds package, but resolve import cycle first
		errorMessage := " "
		if e != nil {
			errorMessage = e.Error()
		}
		return &dg.MessageEmbed{
			Title:       "⚠️ Error",
			Color:       0xdd2e44,
			Description: "An error was encountered:\n```" + errorMessage + "```",
			Footer: &dg.MessageEmbedFooter{
				Text: "Please ask a bot administrator to look at the logs for more information.",
			},
		}
	}

	ResponseBotPermission = func() *dg.MessageEmbed {
		//TODO: use embeds package, but resolve import cycle first
		return &dg.MessageEmbed{
			Title:       "⛔ Missing Permission",
			Color:       0xdd2e44,
			Description: "The bot does not have the permissions it needs to fully run this command.",
		}
	}
)
