package states

import (
	"math/rand"
	"sync"
	"time"
)

/*
TimerManager represents a way to store *time.Timer in a concurrently safe way.
*/
type TimerManager interface {
	/*New generates a timer that fires the alarm after the specified duration.
	Any errors are returned through the error channel.
	Returns a key by which the timer can be accessed again.
	A valid key must be greater than 0.*/
	New(d time.Duration, s *State, alarm Alarm, e chan error) int
	//Stop prevents the timer from firing its alarm. Returns true if stop was successful. False otherwise.
	Stop(key int) bool
	//Reset sets a new duration for a timer without firing the timer. Returns true if reset was successful. False otherwise.
	Reset(key int, d time.Duration) bool
	//Fire runs the alarm function. Returns true if fire was successful. False otherwise.
	Fire(key int) bool
	//Delete stops and deletes a timer from the TimerManager.
	Delete(key int)
	//Count returns the number of timers in the TimerManager.
	Count() int
}

//timerManager stores *time.Timer in a concurrently safe way.
type timerManager struct {
	timers map[int]*time.Timer
	mutex  sync.Mutex
}

//NewTimerManager returns a new TimeManager.
func NewTimerManager() TimerManager {
	return &timerManager{
		timers: make(map[int]*time.Timer),
		mutex:  sync.Mutex{},
	}
}

//Alarm is a function that triggers when a timer runs out.
type Alarm func(s *State, e chan<- error)

/*New generates a timer that fires the alarm after the specified duration.
Any errors are returned through the error channel.
Returns a key by which the timer can be accessed again. */
func (m *timerManager) New(d time.Duration, s *State, alarm Alarm, e chan error) (key int) {

	timer := time.AfterFunc(d, func() {
		alarm(s, e)
	})

	return m.store(timer)
}

//Stop prevents the timer from firing its alarm. Returns true if stop was successful. False otherwise.
func (m *timerManager) Stop(key int) bool {
	timer, ok := m.get(key)
	if !ok {
		return false
	}
	if !timer.Stop() {
		//Drain the channel if needed
		go func() { <-timer.C }()
	}
	return true
}

//Reset sets a new duration for a timer without firing the timer. Returns true if reset was successful. False otherwise.
func (m *timerManager) Reset(key int, d time.Duration) bool {

	if !m.Stop(key) {
		return false
	}
	timer, ok := m.get(key)
	if !ok {
		return false
	}
	timer.Reset(d)
	return true
}

//Fire runs the alarm function. Returns true if fire was successful. False otherwise.
func (m *timerManager) Fire(key int) bool {

	timer, ok := m.get(key)
	if !ok {
		return false
	}
	timer.Reset(0)
	return true

}

/*get returns the timer that is assigned the specified key.
If no timer has the specified key, ok returns as false, otherwise ok returns true.*/
func (m *timerManager) get(key int) (timer *time.Timer, ok bool) {
	m.mutex.Lock()
	timer, ok = m.timers[key]
	m.mutex.Unlock()
	return timer, ok
}

//Delete stops and deletes a timer from the TimerManager. Returns true if deletion was successful. False otherwise.
func (m *timerManager) Delete(key int) {
	m.Stop(key)
	m.mutex.Lock()
	delete(m.timers, key)
	m.mutex.Unlock()
}

//Count returns the number of timers in the TimerManager.
func (m *timerManager) Count() int {
	return len(m.timers)
}

//store saves the timer and returns the unique key assigned to it. A valid key must be greater than 0.
func (m *timerManager) store(timer *time.Timer) (key int) {

	m.mutex.Lock()
	//generate a random key that is not in use.
	for _, taken := m.timers[key]; taken || key == 0; key = rand.Int() {
		_, taken = m.timers[key]
	}
	m.timers[key] = timer
	m.mutex.Unlock()

	return key
}
