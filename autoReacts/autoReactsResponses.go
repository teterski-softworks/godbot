package autoReacts

import (
	"time"

	dg "github.com/bwmarrin/discordgo"
	"gitlab.com/teterski-softworks/godbot/embeds"
)

var (
	//TODO: this could be useful to have in the godbot commands package.
	ResponseIncorrectUseOfOption = func() *dg.MessageEmbed {
		embed := embeds.New("", "", "")
		embed.Title = "❌ Incorrect Use of Option"
		embed.Color = embeds.ColorRed
		embed.Description = "The `react_mode` option can only be used together with the `channel_add` option."
		embed.Timestamp = time.Now().Format(time.RFC3339)
		return &embed.MessageEmbed
	}
)
