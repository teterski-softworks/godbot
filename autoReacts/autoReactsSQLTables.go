package autoReacts

import "gitlab.com/teterski-softworks/godbot/db"

func CreateDatabaseTables(DB db.ReadWriter) error {
	t := DB.NewTransaction()
	t.Queue(SQLCreateTableAutoReacts)
	return t.Commit()
}

var (
	SQLCreateTableAutoReacts string = `
	CREATE TABLE IF NOT EXISTS autoReacts(
		guildID   TEXT NOT NULL,
		channelID TEXT NOT NULL,
		emojiID   TEXT NOT NULL,
		emojiName TEXT NOT NULL,
		mode      INT DEFAULT 0,

		PRIMARY KEY (guildID, channelID),
		FOREIGN KEY (guildID, channelID) REFERENCES channels(guildID, channelID) ON DELETE CASCADE
	);`
)
