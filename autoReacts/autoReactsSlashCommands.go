package autoReacts

import (
	"database/sql"

	dg "github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
	"gitlab.com/teterski-softworks/godbot/commands"
	"gitlab.com/teterski-softworks/godbot/db"
	"gitlab.com/teterski-softworks/godbot/states"
)

//SlashCommandAdminAutoReacts - An admin-only command group for auto reacts related commands.
var SlashCommandAdminAutoReacts = commands.New(
	&dg.ApplicationCommand{
		Type:        dg.ChatApplicationCommand,
		Name:        "admin_auto_reacts",
		Description: "Admin-only auto reacts commands.",
		Options: []*dg.ApplicationCommandOption{
			SlashCommandAdminAutoReactsSettings,
		},
	},
	false,
	func(intr states.Interaction, options ...*dg.ApplicationCommandInteractionDataOption) (err error) {

		if err := intr.Acknowledge(); err != nil {
			return err
		}

		switch options[0].Name {
		case SlashCommandAdminAutoReactsSettings.Name:
			return SlashAdminAutoReactsSettings(intr, options[0].Options)
		}
		return intr.EditResponseEmbeds(nil, states.ResponseGenericError(nil))
	},
)

//SlashCommandAdminAutoReactsSettings - An admin-only subcommand for adding/editing auto reacts to a channel.
var SlashCommandAdminAutoReactsSettings = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionSubCommand,
	Name:        "settings",
	Description: "View and change settings for automatic emoji reactions.",
	Options: []*dg.ApplicationCommandOption{
		{
			Type:         dg.ApplicationCommandOptionChannel,
			Name:         "channel_add",
			Description:  "Adds a channel to auto react to. The bot will ask for an emoji after.",
			ChannelTypes: []dg.ChannelType{dg.ChannelTypeGuildText},
		},
		{
			Type:        dg.ApplicationCommandOptionInteger,
			Name:        "react_mode",
			Description: "Sets the react mode when adding a channel.",
			Choices: []*dg.ApplicationCommandOptionChoice{
				{Name: "All", Value: ModeAll},
				{Name: "Text Only", Value: ModeText},
				{Name: "Attachments Only", Value: ModeAttachments},
			},
		},
		{
			Type:        dg.ApplicationCommandOptionInteger,
			Name:        "channel_remove",
			Description: "The index of the channel to remove automatic reactions from.",
			MinValue:    &one,
		},
	},
}
var one float64 = 1

//SlashAdminAutoReactsSettings adds/edits auto reacts for a channel.
func SlashAdminAutoReactsSettings(intr states.Interaction, options []*dg.ApplicationCommandInteractionDataOption) (err error) {

	var autoReact *AutoReact
	for _, option := range options {
		switch option.Name {
		case "channel_add":

			channel := option.ChannelValue(intr.Session)

			//TODO
			emoji, err := intr.RequestEmoji()
			if err != nil {
				return err
			}

			//Save the auto react
			autoReact = &AutoReact{}
			*autoReact = New(intr.GuildID, channel.ID, emoji.ID, emoji.Name, ModeAll)
			if err := autoReact.Store(intr.DB); err != nil {
				return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
			}

		case "react_mode":
			if autoReact == nil {
				return intr.EditResponseEmbeds(err, ResponseIncorrectUseOfOption())
			}
			autoReact.Mode = Mode(option.IntValue())
			if err := autoReact.Store(intr.DB); err != nil {
				return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
			}

		case "channel_remove":
			index := int(option.IntValue())
			autoReacts, err := RetrieveAll(intr.DB, intr.GuildID)
			if err != nil {
				return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
			}
			if len := len(autoReacts); len >= index-1 && len != 0 {
				if _, err := autoReacts[index-1].Delete(intr.DB); err != nil {
					return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
				}
			}
		}
	}

	autoReacts, err := RetrieveAll(intr.DB, intr.GuildID)
	if err != nil && !errors.Is(err, sql.ErrNoRows) {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}

	return intr.EditResponseEmbeds(nil, autoReacts.Embed(intr.DB, intr.GuildID))
}
