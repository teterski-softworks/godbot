package autoReacts

import (
	dg "github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
	"gitlab.com/teterski-softworks/godbot/states"
)

//MessageCreateHandler adds a reaction to the new message if the channel has an auto react set up for it.
func MessageCreateHandler(intr states.Interaction) error {

	autoReacts, err := RetrieveAll(intr.DB, intr.GuildID)
	if err != nil {
		return err
	}

	for _, a := range autoReacts {
		if a.GuildID != intr.GuildID || a.ChannelID != intr.ChannelID {
			continue
		}
		e := dg.Emoji{
			ID:   a.EmojiID,
			Name: a.EmojiName,
		}

		react := false
		switch a.Mode {
		case ModeText:
			react = intr.Message.Content != ""
		case ModeAttachments:
			react = len(intr.Message.Attachments) != 0
		default:
			react = true
		}
		if !react {
			continue
		}

		if err := intr.MessageReactionAdd(intr.ChannelID, intr.Message.ID, e.APIName()); err != nil {
			return errors.WithStack(err)
		}
	}

	return nil
}

//TODO: ChannelDeleteHandler
