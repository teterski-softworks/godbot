package autoReacts

import (
	"database/sql"
	"fmt"

	dg "github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
	"gitlab.com/teterski-softworks/godbot/channels"
	"gitlab.com/teterski-softworks/godbot/db"
	"gitlab.com/teterski-softworks/godbot/embeds"
)

//Mode specifies what type of messages are reacted to.
type Mode int

const (
	//React to all messages
	ModeAll Mode = iota
	//React only to messages with text in them.
	ModeText
	//React only to messages with attachments in them.
	ModeAttachments
)

//AutoReact represents an automatic emoji reaction to messages in a channel.
type AutoReact struct {
	channels.Channel
	EmojiID   string
	EmojiName string
	Mode
}

//AutoReactLists is a slice of AutoReacts with its own methods.
type AutoReactLists []AutoReact

//New returns an AutoReact.
func New(guildID string, channelID string, emojiID string, emojiName string, mode Mode) AutoReact {
	return AutoReact{
		Channel:   channels.New(guildID, channelID),
		EmojiID:   emojiID,
		EmojiName: emojiName,
		Mode:      mode,
	}
}

//Retrieve returns an AutoReact for the given channel from the DB.
func Retrieve(DB db.Reader, guildID string, channelID string) (AutoReact, error) {

	a := New(guildID, channelID, "", "", ModeAll)
	row, err := DB.QueryRow(SQLSelectAutoReact, guildID, channelID)
	if err != nil {
		return a, err
	}
	if err = row.Scan(&a.EmojiID, &a.EmojiName, &a.Mode); err != nil && !errors.Is(err, sql.ErrNoRows) {
		return a, errors.WithStack(err)
	}
	return a, nil
}

//RetrieveAll returns all AUtoReacts for the guild from the DB.
func RetrieveAll(DB db.Reader, guildID string) (AutoReactLists, error) {

	var autoReacts []AutoReact

	rows, err := DB.Query(SQLSelectGuildAutoReacts, guildID)
	if err != nil {
		return autoReacts, err
	}

	for rows.Next() {
		a := New(guildID, "", "", "", ModeAll)
		if err := rows.Scan(&a.ChannelID, &a.EmojiID, &a.EmojiName, &a.Mode); err != nil {
			return autoReacts, err
		}
		autoReacts = append(autoReacts, a)

	}
	return autoReacts, nil
}

//Store saves an AutoReact to the DB.
func (a AutoReact) Store(DB db.ReadWriter) error {
	t := DB.NewTransaction()
	if err := a.Channel.Store(t); err != nil {
		return err
	}
	t.Queue(SQLUpsertAutoReact, a.GuildID, a.ChannelID, a.EmojiID, a.EmojiName, a.Mode)
	return t.Commit()
}

//Delete removes an AutoReact from the DB.
//If the AutoReact is already not in the provided database, then found returns false. Otherwise, returns true.
func (a AutoReact) Delete(DB db.ReadWriter) (found bool, err error) {
	if _, err = Retrieve(DB, a.GuildID, a.ChannelID); err != nil {
		return false, err
	}
	if err = DB.QueueAndCommit(SQLDeleteAutoReact, a.GuildID, a.ChannelID); err != nil {
		return false, err
	}
	return true, nil
}

//Embed returns a dg.MessageEmbed showing the AutoReacts in the list.
func (l AutoReactLists) Embed(DB db.Reader, guildID string) *dg.MessageEmbed {
	embed := embeds.NewDefault(DB, guildID)
	embed.Title = "👍 Auto React Settings ⚙️"
	embed.Description = "The bot will automatically react to messages in the channels below with their selected emoji:\n"

	if len(l) == 0 {
		embed.Description += "`None set`"
	}

	for i, autoReact := range l {
		emoji := dg.Emoji{
			ID:   autoReact.EmojiID,
			Name: autoReact.EmojiName,
		}

		mode := "all"
		switch autoReact.Mode {
		case ModeText:
			mode = "text only"
		case ModeAttachments:
			mode = "attachments only"
		}

		embed.Description += fmt.Sprintf("%d. %s Reacts to `%s` with %s\n", i+1, channels.Mention(autoReact.ChannelID), mode, emoji.MessageFormat())
	}

	return &embed.MessageEmbed
}
