package autoReacts

var (
	SQLSelectAutoReact string = `
	SELECT emojiID, emojiName, mode FROM autoReacts WHERE guildID = ?1 AND channelID = ?2;`

	SQLSelectGuildAutoReacts string = `
	SELECT channelID, emojiID, emojiName, mode FROM autoReacts WHERE guildID = ?1 ORDER BY channelID;`

	SQLUpsertAutoReact string = `
	INSERT OR IGNORE INTO autoReacts(guildID, channelID, emojiID, emojiName, mode)
	VALUES (?1,?2,?3,?4,?5)
	ON CONFLICT DO
	UPDATE SET

	emojiID   =?3,
	emojiName =?4,
	mode      =?5

	WHERE guildID=?1 AND channelID=?2;`

	SQLDeleteAutoReact string = `
	DELETE FROM autoReacts WHERE guildID = ?1 AND channelID = ?2;`
)
