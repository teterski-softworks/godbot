package messages

import (
	"fmt"

	dg "github.com/bwmarrin/discordgo"
)

var (
	ResponseMessageSent = func(messageLink string) *dg.MessageEmbed {
		//TODO: use embeds package, but resolve import cycle first
		return &dg.MessageEmbed{
			Title:       "✅ Message Sent",
			Color:       0xdd2e44,
			Description: fmt.Sprintf("The message has been sent. [Link](%s)", messageLink),
		}
	}

	ResponseMessageEdited = func(messageLink string) *dg.MessageEmbed {
		//TODO: use embeds package, but resolve import cycle first
		return &dg.MessageEmbed{
			Title:       "✅ Message Edited",
			Color:       0xdd2e44,
			Description: fmt.Sprintf("The message has been edited. [Link](%s)", messageLink),
		}
	}
)
