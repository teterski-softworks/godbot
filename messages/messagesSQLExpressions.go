package messages

var (
	SQLSelectMessage string = `
	SELECT guildID, channelID, messageID FROM messages WHERE guildID=?1 AND channelID=?2 AND messageID=?3;`

	SQLSelectGuildMessages string = `
	SELECT channelID, messageID FROM messages WHERE guildID=?1;`

	SQLInsertMessage string = `
	INSERT OR IGNORE INTO messages(guildID, channelID, messageID) VALUES(?1, ?2, ?3);`

	SQLDeleteMessage string = `
	DELETE FROM messages WHERE guildID=?1 AND channelID=?2 AND messageID=?3;`
)
