package messages

import (
	dg "github.com/bwmarrin/discordgo"
	"gitlab.com/teterski-softworks/godbot/commands"
	"gitlab.com/teterski-softworks/godbot/states"
)

//SlashCommandMessage - A command group for message related commands.
var SlashCommandMessage = commands.New(
	&dg.ApplicationCommand{
		Type:        dg.ChatApplicationCommand,
		Name:        "message",
		Description: "Message related commands. Admin only.",
		Options: []*dg.ApplicationCommandOption{
			slashCommandMessageSend,
			slashCommandMessageEdit,
		},
	},
	false,
	func(intr states.Interaction, options ...*dg.ApplicationCommandInteractionDataOption) (err error) {

		if err := intr.AcknowledgeEphemeral(); err != nil {
			return err
		}

		switch options[0].Type {
		case dg.ApplicationCommandOptionSubCommand:
			switch options[0].Name {
			case slashCommandMessageSend.Name:
				return slashMessageSend(intr, options[0].Options)
			case slashCommandMessageEdit.Name:
				return slashMessageEdit(intr, options[0].Options)
			}
		}

		return intr.EditResponseEmbeds(nil, states.ResponseGenericError(nil))
	},
)

//slashCommandMessageSend - A subcommand to send a message as the bot.
var slashCommandMessageSend = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionSubCommand,
	Name:        "send",
	Description: "Sends/replies to a message as the bot.",
	Options: []*dg.ApplicationCommandOption{
		{
			Type:         dg.ApplicationCommandOptionChannel,
			Name:         "channel",
			Description:  "The channel to send the message/reply to.",
			Required:     true,
			ChannelTypes: []dg.ChannelType{dg.ChannelTypeGuildText},
		}, {
			Type:        dg.ApplicationCommandOptionString,
			Name:        "message",
			Description: "The message content to send.",
			Required:    true,
		}, {
			Type:        dg.ApplicationCommandOptionString,
			Name:        "replyto",
			Description: "The message to reply to.",
			Required:    false,
		},
	},
}

//slashMessageSend sends a message as the bot.
func slashMessageSend(intr states.Interaction, options []*dg.ApplicationCommandInteractionDataOption) error {

	var reference *dg.MessageReference
	channel := options[0].ChannelValue(intr.Session)
	content := options[1].StringValue()

	if len(options) > 2 {

		replyToID := options[2].StringValue()

		replyToMessage, err := intr.ChannelMessage(channel.ID, replyToID)
		if err != nil {
			return intr.EditResponseEmbeds(err, states.ResponseGenericError(err))
		}
		reference = replyToMessage.Reference()
	}
	message, err := intr.SendMessage(content, channel.ID, reference, nil)
	if err != nil {
		return intr.EditResponseEmbeds(err, states.ResponseGenericError(err))
	}
	return intr.EditResponseEmbeds(nil, ResponseMessageSent(Link(message.Reference())))
}

//slashCommandMessageEdit - A subcommand to edit a message previously sent by the bot.
var slashCommandMessageEdit = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionSubCommand,
	Name:        "edit",
	Description: "Edits any message sent by the bot.",
	Options: []*dg.ApplicationCommandOption{
		{
			Type:         dg.ApplicationCommandOptionChannel,
			Name:         "channel",
			Description:  "The channel that the message is in.",
			Required:     true,
			ChannelTypes: []dg.ChannelType{dg.ChannelTypeGuildText},
		}, {
			Type:        dg.ApplicationCommandOptionString,
			Name:        "messageid",
			Description: "The ID of the message to edit.",
			Required:    true,
		}, {
			Type:        dg.ApplicationCommandOptionString,
			Name:        "message",
			Description: "The new message content to send.",
			Required:    true,
		},
	},
}

//slashMessageEdit edits a message previously send by the bot.
func slashMessageEdit(intr states.Interaction, options []*dg.ApplicationCommandInteractionDataOption) error {

	channel := options[0].ChannelValue(intr.Session)
	messageID := options[1].StringValue()
	content := options[2].StringValue()

	message, err := intr.ChannelMessageEdit(channel.ID, messageID, content)
	if err != nil {
		return intr.EditResponseEmbeds(err, states.ResponseGenericError(err))
	}
	return intr.EditResponseEmbeds(nil, ResponseMessageEdited(Link(message.Reference())))
}
