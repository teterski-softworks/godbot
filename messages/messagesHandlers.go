package messages

import (
	dg "github.com/bwmarrin/discordgo"
	"gitlab.com/teterski-softworks/godbot/states"
)

//MessageDeleteHandler deletes a message from the DB when it is deleted from a server.
func MessageDeleteHandler(s *states.State, event *dg.MessageDelete) error {
	if err := NewFromReference(event.Reference()).Delete(s.DB); err != nil {
		return err
	}
	return nil
}
