package messages

import "gitlab.com/teterski-softworks/godbot/db"

func CreateDatabaseTables(DB db.ReadWriter) error {
	t := DB.NewTransaction()
	t.Queue(SQLCreateTableMessages)
	return t.Commit()
}

var (
	SQLCreateTableMessages string = `
	CREATE TABLE IF NOT EXISTS messages(
		guildID   TEXT NOT NULL,
		channelID TEXT NOT NULL,
		messageID TEXT NOT NULL,

		PRIMARY KEY(guildID, channelID, messageID),
		FOREIGN KEY(guildID, channelID) REFERENCES channels(guildID, channelID) ON DELETE CASCADE
	);`
)
