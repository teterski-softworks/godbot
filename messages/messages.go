package messages

import (
	"database/sql"
	"fmt"
	"strings"

	dg "github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
	"gitlab.com/teterski-softworks/godbot/channels"
	"gitlab.com/teterski-softworks/godbot/db"
	"gitlab.com/teterski-softworks/godbot/states"
)

//Message represents the location of a Discord message.
type Message struct {
	channels.Channel
	MessageID string
}

//New returns a new Message for the given message ID.
func New(guildID string, channelID string, messageID string) Message {
	return Message{
		Channel:   channels.New(guildID, channelID),
		MessageID: messageID,
	}
}

//New returns a new Message for the given message reference.
func NewFromReference(reference *dg.MessageReference) Message {
	return Message{
		Channel:   channels.New(reference.GuildID, reference.ChannelID),
		MessageID: reference.MessageID,
	}
}

//Retrieve returns a Message if it is in the DB.
func Retrieve(DB db.Reader, guildID string, channelID string, messageID string) (message Message, err error) {

	row, err := DB.QueryRow(SQLSelectMessage, guildID, channelID, messageID)
	if err != nil {
		return message, err
	}
	if err = row.Scan(&message.GuildID, &message.ChannelID, &message.MessageID); err != nil {
		return message, err
	}

	return message, nil
}

//RetrieveAll returns a list of all messages stored in the DB for a guild.
func RetrieveAll(DB db.Reader, guildID string) (messages []Message, err error) {
	rows, err := DB.Query(SQLSelectGuildMessages, guildID)
	if err != nil {
		return messages, err
	}

	var message Message
	message.GuildID = guildID
	for rows.Next() {
		if err := rows.Scan(&message.ChannelID, &message.MessageID); err != nil {
			return messages, errors.WithStack(err)
		}
		messages = append(messages, message)
	}

	return messages, nil
}

//Store saves a Message to the DB.
func (m Message) Store(DB db.ReadWriter) error {
	t := DB.NewTransaction()
	if err := m.Channel.Store(t); err != nil {
		return err
	}
	t.Queue(SQLInsertMessage, m.GuildID, m.ChannelID, m.MessageID)
	return t.Commit()
}

//Delete removes all Channel related data from the DB.
func (m Message) Delete(DB db.ReadWriter) error {
	return DB.QueueAndCommit(SQLDeleteMessage, m.GuildID, m.ChannelID, m.MessageID)
}

//Mention returns a mention string for the provided Channel ID.
func Mention(channelID string) string {
	return fmt.Sprintf("<#%s>", channelID)
}

//Link returns th URL of the Message
func (m Message) Link() string {
	return fmt.Sprintf("https://discord.com/channels/%s/%s/%s", m.GuildID, m.ChannelID, m.MessageID)
}

//Link returns th URL of the Message
func Link(reference *dg.MessageReference) string {
	return fmt.Sprintf("https://discord.com/channels/%s/%s/%s", reference.GuildID, reference.ChannelID, reference.MessageID)
}

//RemoveDeletedMessages deletes messages from the DB which have been removed from the given guild.
func RemoveDeletedMessages(s *states.State, guildID string) error {
	dbMessages, err := RetrieveAll(s.DB, guildID)
	if errors.Is(err, sql.ErrNoRows) {
		return nil
	}
	if err != nil {
		return err
	}

	for _, message := range dbMessages {
		if message.MessageID == "" {
			//Ignore blank message. They are required to have empty settings options.
			continue
		}

		match := false
		_, err = s.ChannelMessage(message.ChannelID, message.MessageID)
		if err != nil {
			cause := errors.Cause(err)
			if strings.HasPrefix(cause.Error(), "HTTP 404") {
				match = false
			} else if strings.HasPrefix(cause.Error(), "HTTP 403") {
				match = true
			} else {
				return err
			}
		} else {
			match = true
		}

		if !match {
			if err := message.Delete(s.DB); err != nil {
				return err
			}
		}
	}
	return nil
}
