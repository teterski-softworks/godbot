package starBoards

import (
	dg "github.com/bwmarrin/discordgo"
	"gitlab.com/teterski-softworks/godbot/channels"
	"gitlab.com/teterski-softworks/godbot/states"
)

//TODO: ChannelDeleteHandler

//MessageReactionAddHandler checks if the reacted to message is eligible to be posted in a StarBoard.
func MessageReactionAddHandler(state *states.State, event *dg.MessageReactionAdd) error {

	settings, err := RetrieveSettings(state.DB, event.GuildID)
	if err != nil {
		return err
	}

	//Check if the emoji reacted with was is in a starboard
	ignore := false
	for _, starBoard := range settings.StarBoards {
		if event.Emoji.ID != starBoard.EmojiID || event.Emoji.Name != starBoard.EmojiName {
			ignore = true
			break
		}
	}
	if ignore {
		return nil
	}

	//Check if the channel/category is ignored
	//Check this after the emoji check, as this check is quiet expensive.
	if ignore, err = channels.InCategoryList(state.Session, event.ChannelID, settings.IgnoredChannelIDs); err != nil {
		return err
	} else if ignore {
		return nil
	}

	for _, starBoard := range settings.StarBoards {
		//Check if the message has the required number of reacts
		message, err := state.ChannelMessage(event.ChannelID, event.MessageID)
		message.GuildID = event.GuildID
		if err != nil {
			return err
		}
		for _, reaction := range message.Reactions {
			//Find the reaction with the right emoji
			if event.Emoji.ID != reaction.Emoji.ID || event.Emoji.Name != reaction.Emoji.Name {
				continue
			}
			//Check if the react count is reached.
			//Do not post to starboard if count is greater, as it was already posted.
			if reaction.Count != starBoard.Count {
				continue
			}
			//Post the post to the starboard
			state.SendEmbeds(starBoard.ChannelID, nil, nil, Embed(state, message, event.Emoji)...)
		}
	}
	return nil
}
