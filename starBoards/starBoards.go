package starBoards

import (
	"database/sql"
	"strings"
	"time"

	dg "github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
	"gitlab.com/teterski-softworks/godbot/channels"
	"gitlab.com/teterski-softworks/godbot/db"
	"gitlab.com/teterski-softworks/godbot/embeds"
	"gitlab.com/teterski-softworks/godbot/messages"
	"gitlab.com/teterski-softworks/godbot/states"
)

//StarBoard represents a channel where messages will be posted if they are reacted to enough times with a specific emoji.
type StarBoard struct {
	channels.Channel        //The channel to post the message in.
	EmojiID          string //The emoji to be reacted with.
	EmojiName        string //The emoji to be reacted with.
	Count            int    //The required amount of times a message needs to be reacted to.
}

//New returns a new StarBoard for the given channel.
func New(guildID string, channelID string, emojiID string, emojiName string, count int) StarBoard {
	return StarBoard{
		Channel:   channels.New(guildID, channelID),
		EmojiID:   emojiID,
		EmojiName: emojiName,
		Count:     count,
	}
}

//Retrieves an existing StarBoard from the DB.
func Retrieve(DB db.Reader, guildID string, channelID string) (StarBoard, error) {
	s := New(guildID, channelID, "", "", 1)
	row, err := DB.QueryRow(SQLSelectStarBoard, guildID, channelID)
	if err != nil {
		return s, err
	}
	if err = row.Scan(&s.EmojiID, &s.EmojiName, &s.Count); err != nil && !errors.Is(err, sql.ErrNoRows) {
		return s, errors.WithStack(err)
	}
	return s, nil
}

//Stores a StarBoard in the DB.
func (s StarBoard) Store(DB db.ReadWriter) error {
	t := DB.NewTransaction()
	if err := s.Channel.Store(t); err != nil {
		return err
	}
	t.Queue(SQLUpsertStarBoard, s.GuildID, s.ChannelID, s.EmojiID, s.EmojiName, s.Count)
	return t.Commit()
}

//Deletes a StarBoard from the DB.
func (s StarBoard) Delete(DB db.ReadWriter) (found bool, err error) {
	if _, err = Retrieve(DB, s.GuildID, s.ChannelID); err != nil {
		return false, err
	}
	if err = DB.QueueAndCommit(SQLDeleteStarBoard, s.GuildID, s.ChannelID); err != nil {
		return false, err
	}
	return true, nil
}

//Embed returns an embed representation of a message to be posted in the StarBoard's channel.
//This includes not only the message's text, but also it's own embeds as part of the returned embed list.
func Embed(state *states.State, message *dg.Message, emoji dg.Emoji) (embedList []*dg.MessageEmbed) {

	m := *message

	embed := embedMessage(state, message)
	embed.Title = emoji.MessageFormat() + " " + embed.Title
	embed.Description += "\n\n**Channel:** " + channels.Mention(message.ChannelID)
	embedList = append(embedList, &embed.MessageEmbed)
	if m.ReferencedMessage != nil {
		m.ReferencedMessage.GuildID = m.GuildID
		embed := embedMessage(state, m.ReferencedMessage)
		embed.Title = "✉️ Replying to " + embed.Title
		embedList = append(embedList, &embed.MessageEmbed)
	}
	embedList = append(embedList, m.Embeds...)

	return embedList
}

//embedMessage returns an embed of the given message's text.
func embedMessage(state *states.State, message *dg.Message) embeds.Embed {
	embed := embeds.NewDefault(state.DB, message.GuildID)
	if member, err := state.GuildMember(message.GuildID, message.Author.ID); err == nil {
		embed.Title = member.Nick + " [" + member.User.String() + "]"
		embed.Author.IconURL = member.AvatarURL("")
	} else {
		embed.Title = message.Author.String()
		embed.Author.IconURL = message.Author.AvatarURL("")
	}

	embed.Author.Name = "Link to message"
	embed.Author.URL = messages.Link(message.Reference())
	embed.Description = message.Content
	embed.Timestamp = message.Timestamp.Format(time.RFC3339)
	embed.Footer.Text = "Message ID: " + message.ID
	for _, a := range message.Attachments {
		if !strings.HasPrefix(a.ContentType, "image") {
			continue
		}
		embed.Image.URL = a.URL
		break
	}

	return embed
}
