package starBoards

import "gitlab.com/teterski-softworks/godbot/db"

func CreateDatabaseTables(DB db.ReadWriter) error {
	t := DB.NewTransaction()
	t.Queue(SQLCreateTableStarBoardsSettings)
	t.Queue(SQLCreateTableStarBoards)
	t.Queue(SQLCreateTableStarBoardsIgnoredChannels)
	return t.Commit()
}

var (
	SQLCreateTableStarBoardsSettings string = `
	CREATE TABLE IF NOT EXISTS starBoardsSettings(
		guildID   TEXT NOT NULL	PRIMARY KEY REFERENCES guilds(guildID) ON DELETE CASCADE
	);`

	SQLCreateTableStarBoards string = `
	CREATE TABLE IF NOT EXISTS starBoards(
		guildID   TEXT NOT NULL REFERENCES starBoardsSettings(guildID) ON DELETE CASCADE,
		channelID TEXT NOT NULL REFERENCES channels(channelID) ON DELETE CASCADE,
		emojiID   TEXT NOT NULL,
		emojiName TEXT NOT NULL,
		count     INT  NOT NULL,

		CHECK (count > 0),

		PRIMARY KEY (guildID, channelID)
	);`

	SQLCreateTableStarBoardsIgnoredChannels string = `
	CREATE TABLE IF NOT EXISTS starBoardsIgnoredChannels(
		guildID   TEXT NOT NULL REFERENCES starBoardsSettings(guildID) ON DELETE CASCADE,
		channelID TEXT NOT NULL REFERENCES channels(channelID) ON DELETE CASCADE,

		PRIMARY KEY (guildID, channelID)
	);`
)
