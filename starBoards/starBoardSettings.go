package starBoards

import (
	"database/sql"
	"fmt"

	dg "github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
	"gitlab.com/teterski-softworks/godbot/channels"
	"gitlab.com/teterski-softworks/godbot/db"
	"gitlab.com/teterski-softworks/godbot/embeds"
	"gitlab.com/teterski-softworks/godbot/guilds"
)

//Settings represents a set of starboard Settings for the guild.
type Settings struct {
	GuildID           string
	StarBoards        []StarBoard
	IgnoredChannelIDs []string
}

//NewSettings returns a new default set of Settings for the guild.
func NewSettings(guildID string) Settings {
	return Settings{GuildID: guildID}
}

//RetrieveSettings returns a set of existing settings from the DB.
//If none exists, returns a default set of settings.
func RetrieveSettings(DB db.Reader, guildID string) (Settings, error) {
	settings := NewSettings(guildID)

	rows, err := DB.Query(SQLSelectGuildStarBoards, guildID)
	if err != nil && !errors.Is(err, sql.ErrNoRows) {
		return settings, err
	}
	for rows.Next() {
		s := New(guildID, "", "", "", 1)
		if err := rows.Scan(&s.ChannelID, &s.EmojiID, &s.EmojiName, &s.Count); err != nil {
			return settings, errors.WithStack(err)
		}
		settings.StarBoards = append(settings.StarBoards, s)
	}

	rows, err = DB.Query(SQLSelectGuildIgnoredChannels, guildID)
	if err != nil && !errors.Is(err, sql.ErrNoRows) {
		return settings, err
	}
	var channelID string
	for rows.Next() {
		if err := rows.Scan(&channelID); err != nil {
			return settings, errors.WithStack(err)
		}
		settings.IgnoredChannelIDs = append(settings.IgnoredChannelIDs, channelID)
	}

	return settings, nil
}

//Stores Settings to the DB.
func (s Settings) Store(DB db.ReadWriter) error {

	t := DB.NewTransaction()

	if err := guilds.New(s.GuildID).Store(t); err != nil {
		return err
	}

	t.Queue(SQLUpsertStarBoardSettings, s.GuildID)

	t.Queue(SQLDeleteGuildStarBoards, s.GuildID)
	for _, s := range s.StarBoards {
		if err := s.Store(t); err != nil {
			return err
		}
	}

	t.Queue(SQLDeleteGuildIgnoredChannels, s.GuildID)
	for _, channelID := range s.IgnoredChannelIDs {
		if err := channels.New(s.GuildID, channelID).Store(t); err != nil {
			return err
		}
		t.Queue(SQLInsertIgnoredChannel, s.GuildID, channelID)
	}
	return t.Commit()
}

//Embed returns an embed containing the starboard Settings.
func (s Settings) Embed(DB db.Reader) *dg.MessageEmbed {
	embed := embeds.NewDefault(DB, s.GuildID)
	embed.Title = "⭐ Starboard Settings ⚙️"
	embed.Description = "Posts will be shown in the channels bellow if they get reacted to the corresponding number of times with the corresponding emoji:\n"

	if len(s.StarBoards) == 0 {
		embed.Description += "`None set`"
	}

	for i, starBoard := range s.StarBoards {
		emoji := dg.Emoji{
			ID:   starBoard.EmojiID,
			Name: starBoard.EmojiName,
		}
		embed.Description += fmt.Sprintf("%d. %s `%d` %s\n", i+1, channels.Mention(starBoard.ChannelID), starBoard.Count, emoji.MessageFormat())
	}

	ignoredChannels := dg.MessageEmbedField{
		Name:  "❎ Ignored Channels",
		Value: "The following channels/categories will have their messages excluded from appearing in any starboard:\n",
	}
	if len(s.IgnoredChannelIDs) == 0 {
		ignoredChannels.Value += "`None set`"
	}
	for i, channel := range s.IgnoredChannelIDs {
		ignoredChannels.Value += fmt.Sprintf("%d. %s\n", i+1, channels.Mention(channel))
	}

	embed.Fields = append(embed.Fields, &ignoredChannels)

	return &embed.MessageEmbed
}
