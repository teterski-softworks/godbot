package starBoards

import (
	"database/sql"

	dg "github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
	"gitlab.com/teterski-softworks/godbot/commands"
	"gitlab.com/teterski-softworks/godbot/db"
	"gitlab.com/teterski-softworks/godbot/states"
)

//SlashCommandAdminStarBoard - An admin-only command group for star board related commands.
var SlashCommandAdminStarBoard = commands.New(
	&dg.ApplicationCommand{
		Type:        dg.ChatApplicationCommand,
		Name:        "admin_starboard",
		Description: "Admin-only starboard commands.",
		Options: []*dg.ApplicationCommandOption{
			SlashCommandStarBoardAdd,
			SlashCommandStarBoardSettings,
			SlashCommandStarBoardRemove,
		},
	},
	false,
	func(intr states.Interaction, options ...*dg.ApplicationCommandInteractionDataOption) (err error) {

		if err := intr.Acknowledge(); err != nil {
			return err
		}

		switch options[0].Name {
		case SlashCommandStarBoardAdd.Name:
			return SlashAdminStarBoardAdd(intr, options[0].Options)
		case SlashCommandStarBoardSettings.Name:
			return SlashAdminStarBoardSettings(intr, options[0].Options)
		case SlashCommandStarBoardRemove.Name:
			return SlashAdminStarBoardRemove(intr, options[0].Options)
		}
		return intr.EditResponseEmbeds(nil, states.ResponseGenericError(nil))
	},
)

//SlashCommandStarBoardAdd - An admin-only subcommand for adding star boards.
var SlashCommandStarBoardAdd = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionSubCommand,
	Name:        "add",
	Description: "Adds a starboard to a channel. The bot will ask for an emoji after.",
	Options: []*dg.ApplicationCommandOption{
		{
			Type:         dg.ApplicationCommandOptionChannel,
			Name:         "channel",
			Description:  "Adds a channel as a starboard.",
			ChannelTypes: []dg.ChannelType{dg.ChannelTypeGuildText},
			Required:     true,
		},
		{
			Type:        dg.ApplicationCommandOptionInteger,
			Name:        "count",
			Description: "Sets the required emoji count needed for a post to show up in the starboard.",
			MinValue:    &one,
			Required:    true,
		},
	},
}
var one float64 = 1

//SlashAdminStarBoardAdd adds a starboard to a channel.
func SlashAdminStarBoardAdd(intr states.Interaction, options []*dg.ApplicationCommandInteractionDataOption) (err error) {

	channel := options[0].ChannelValue(intr.Session)
	count := int(options[1].IntValue())

	//TODO
	emoji, err := intr.RequestEmoji()
	if err != nil {
		return err
	}

	settings, err := RetrieveSettings(intr.DB, intr.GuildID)
	if err != nil {
		return err
	}

	//Save the starboard
	starBoard := New(intr.GuildID, channel.ID, emoji.ID, emoji.Name, count)
	settings.StarBoards = append(settings.StarBoards, starBoard)
	if err := settings.Store(intr.DB); err != nil {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}

	settings, err = RetrieveSettings(intr.DB, intr.GuildID)
	if err != nil && !errors.Is(err, sql.ErrNoRows) {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}
	return intr.EditResponseEmbeds(nil, settings.Embed(intr.DB))
}

//SlashCommandStarBoardSettings - An admin-only subcommand for showing and editing star boards settings.
var SlashCommandStarBoardSettings = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionSubCommand,
	Name:        "settings",
	Description: "View and change settings for starboards.",
	Options: []*dg.ApplicationCommandOption{
		{
			Type:        dg.ApplicationCommandOptionChannel,
			Name:        "ignore_channel_add",
			Description: "The channel or category in which starboard reacts will be ignored.",
		},
		{
			Type:        dg.ApplicationCommandOptionInteger,
			Name:        "ignore_channel_remove",
			Description: "The index of the channel to no longer ignore.",
			MinValue:    &one,
		},
	},
}

//SlashAdminStarBoardSettings shows and edits star boards settings.
func SlashAdminStarBoardSettings(intr states.Interaction, options []*dg.ApplicationCommandInteractionDataOption) (err error) {

	settings, err := RetrieveSettings(intr.DB, intr.GuildID)
	if err != nil && !errors.Is(err, sql.ErrNoRows) {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}
	for _, option := range options {
		switch option.Name {
		case "ignore_channel_add":
			channel := option.ChannelValue(intr.Session)
			settings.IgnoredChannelIDs = append(settings.IgnoredChannelIDs, channel.ID)
		case "ignore_channel_remove":
			index := int(option.IntValue())
			settings.IgnoredChannelIDs = append(settings.IgnoredChannelIDs[:index], settings.IgnoredChannelIDs[index+1:]...)
		}
	}

	if err = settings.Store(intr.DB); err != nil {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}

	settings, err = RetrieveSettings(intr.DB, intr.GuildID)
	if err != nil && !errors.Is(err, sql.ErrNoRows) {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}

	return intr.EditResponseEmbeds(nil, settings.Embed(intr.DB))
}

//SlashCommandStarBoardRemove - An admin-only subcommand for removing star boards.
var SlashCommandStarBoardRemove = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionSubCommand,
	Name:        "remove",
	Description: "Removes a starboard from a channel.",
	Options: []*dg.ApplicationCommandOption{
		{
			Type:        dg.ApplicationCommandOptionInteger,
			Name:        "channel_index",
			Description: "The index of the channel to remove from being a starboard.",
			MinValue:    &one,
			Required:    true,
		},
	},
}

//SlashAdminStarBoardRemove removes a starboard from a channel.
func SlashAdminStarBoardRemove(intr states.Interaction, options []*dg.ApplicationCommandInteractionDataOption) (err error) {

	index := int(options[0].IntValue()) - 1
	settings, err := RetrieveSettings(intr.DB, intr.GuildID)
	if err != nil {
		intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}
	if len := len(settings.StarBoards); len >= index && len != 0 {
		if _, err := settings.StarBoards[index].Delete(intr.DB); err != nil {
			return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
		}
	}

	settings, err = RetrieveSettings(intr.DB, intr.GuildID)
	if err != nil && !errors.Is(err, sql.ErrNoRows) {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}

	return intr.EditResponseEmbeds(nil, settings.Embed(intr.DB))
}
