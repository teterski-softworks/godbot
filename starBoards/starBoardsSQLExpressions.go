package starBoards

var (
	SQLSelectStarBoard string = `
	SELECT emojiID, emojiName, count
	FROM starBoards
	WHERE  guildID = ?1 AND channelID = ?2;`

	SQLSelectGuildStarBoards string = `
	SELECT channelID, emojiID, emojiName, count
	FROM starBoards
	WHERE  guildID = ?1;`

	SQLSelectGuildIgnoredChannels string = `
	SELECT channelID FROM starBoardsIgnoredChannels WHERE  guildID = ?1;`

	SQLUpsertStarBoardSettings string = `
	INSERT OR IGNORE INTO starBoardsSettings(guildID) VALUES (?1);`

	SQLUpsertStarBoard string = `
	INSERT INTO starBoards(guildID, channelID, emojiID, emojiName, count)
	VALUES (?1, ?2, ?3, ?4, ?5)
	ON CONFLICT DO UPDATE SET

	emojiID   =?3,
	emojiName =?4,
	count     =?5

	WHERE guildID = ?1 AND channelID = ?2;`

	SQLInsertIgnoredChannel string = `
	INSERT INTO starBoardsIgnoredChannels(guildID, channelID) VALUES (?1, ?2);`

	SQLDeleteGuildStarBoards string = `
	DELETE FROM starBoards WHERE guildID = ?1;`

	SQLDeleteStarBoard string = `
	DELETE FROM starBoards WHERE guildID = ?1 AND channelID = ?2;`

	SQLDeleteGuildIgnoredChannels string = `
	DELETE FROM starBoardsIgnoredChannels WHERE guildID = ?1;`
)
