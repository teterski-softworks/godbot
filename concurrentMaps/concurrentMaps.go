package concurrentMaps

import "sync"

//Map is a generic concurrently safe map.
type Map[K comparable, V any] struct {
	data  map[K]V
	mutex sync.Mutex
}

//New returns a new Map.
func New[K comparable, V any]() Map[K, V] {
	return Map[K, V]{
		data: make(map[K]V),
	}
}

//Store sets the value for a key.
func (m *Map[K, V]) Store(key K, value V) {
	m.mutex.Lock()
	m.data[key] = value
	m.mutex.Unlock()
}

//Get returns the value stored in the map for a key.
//If the value is not present, returns the same result as what is returned from a map where the key is not present.
func (m *Map[K, V]) Get(key K) (value V) {
	m.mutex.Lock()
	value = m.data[key]
	m.mutex.Unlock()
	return value
}

//Get returns the value stored in the map for a key.
//If key is present, `ok` returns as true, false otherwise.
func (m *Map[K, V]) GetOK(key K) (value V, ok bool) {
	m.mutex.Lock()
	value, ok = m.data[key]
	m.mutex.Unlock()
	return value, ok
}

//Delete deletes the value for a key.
func (m *Map[K, V]) Delete(key K) {
	m.mutex.Lock()
	delete(m.data, key)
	m.mutex.Unlock()
}

//Range calls f sequentially for each key and value present in the map.
func (m *Map[K, V]) Range(f func(k K, v V)) {
	for k, v := range m.data {
		f(k, v)
	}
}
