package concurrentMaps_test

import (
	"math/rand"
	"sync"
	"testing"

	"gitlab.com/teterski-softworks/godbot/concurrentMaps"
	"gitlab.com/teterski-softworks/godbot/tests"
)

func TestConcurrentMaps(t *testing.T) {
	m := concurrentMaps.New[int, int]()

	var wg sync.WaitGroup

	size := 4
	randArray1, randArray2 := make([]int, size+1), make([]int, size+1)
	for i := 0; i < size; i++ {
		randArray1[i] = rand.Int()
		randArray2[i] = rand.Int()
	}

	t.Run("Non overlapping insert and get", func(t *testing.T) {
		wg.Add(size)
		for i := 0; i < size; i++ {
			go func(index int) {
				m.Store(index, randArray1[index])
				wg.Done()
			}(i)
		}
		wg.Wait()

		results := make([]int, size+1)
		wg.Add(size)
		for i := 0; i < size; i++ {
			go func(i int) {
				results[i] = m.Get(i)
				wg.Done()
			}(i)
		}
		wg.Wait()
		for i, v := range results {
			if v != randArray1[i] {
				t.Fatalf(tests.WrongValue, "(Map).Get()", randArray1[i], v)
			}
		}

	})

	t.Run("Range", func(t *testing.T) {
		m.Range(func(k, v int) {
			if v != randArray1[k] {
				t.Fatalf(tests.WrongValue, "(Map).Get()", randArray1[k], v)
			}
		})
	})

	t.Run("Overlapping insert and get", func(t *testing.T) {

		wg.Add(size)
		for i := 0; i < size; i++ {
			go func(index int) {
				go m.Store(index, randArray1[index])
				go m.Store(index, randArray2[index])
				wg.Done()
			}(i)
		}
		wg.Wait()

		resultsValues, resultsOK := make([]int, size+1), make([]bool, size+1)
		wg.Add(size)
		for i := 0; i < size; i++ {
			go func(i int) {
				resultsValues[i], resultsOK[i] = m.GetOK(i)
				wg.Done()
			}(i)
		}
		wg.Wait()

		for i := 0; i < size; i++ {
			if !resultsOK[i] {
				t.Fatalf(tests.WrongValue, "(Map).GetOK()", true, resultsOK[i])
			} else if resultsValues[i] != randArray1[i] && resultsValues[i] != randArray2[i] {
				t.Fatalf("(Map).Get() resulted in wrong value.\nExpected %d or %d,\nReceived %d", randArray1[i], randArray2[i], resultsValues[i])
			}
		}

	})

	t.Run("Delete", func(t *testing.T) {
		m.Delete(1)
		_, ok := m.GetOK(1)
		if ok {
			t.Fatalf(tests.WrongValue, "(Map).GetOK()", false, ok)
		}
	})
}
