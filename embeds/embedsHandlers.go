package embeds

import (
	"strconv"
	"strings"

	dg "github.com/bwmarrin/discordgo"
	"gitlab.com/teterski-softworks/godbot/db"
	"gitlab.com/teterski-softworks/godbot/states"
)

//ListPageSelectionHandler allows for switching between pages in an embed list.
func ListPageSelectionHandler(intr states.Interaction, customID string) error {

	list, err := NewGuildList(intr.DB, intr.GuildID, ListTitle, CustomIDPrefixList, RetrieveAll)
	if err != nil {
		return intr.RespondEphemeralEmbeds(err, db.ResponseDBError(err))
	}

	page, err := strconv.Atoi(strings.TrimPrefix(customID, CustomIDPrefixList))
	if err != nil {
		return intr.RespondEphemeralEmbeds(err, states.ResponseGenericError(err))
	}

	response := &dg.InteractionResponse{
		Type: dg.InteractionResponseUpdateMessage,
		Data: &dg.InteractionResponseData{
			Embeds:     list.Embeds(page),
			Components: list.Components(page),
		},
	}
	return intr.InteractionRespond(&intr.Interaction, response)
}
