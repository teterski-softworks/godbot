package embeds

import (
	dg "github.com/bwmarrin/discordgo"
	"gitlab.com/teterski-softworks/godbot/commands"
	"gitlab.com/teterski-softworks/godbot/db"
	"gitlab.com/teterski-softworks/godbot/members"
	"gitlab.com/teterski-softworks/godbot/states"
)

//MessageCommandEmbedMessage - turns a text message into an embed.
var MessageCommandEmbedMessage = commands.New(
	&dg.ApplicationCommand{
		Type: dg.MessageApplicationCommand,
		Name: "Embed - New 📄",
	},
	false,
	func(intr states.Interaction, _ ...*dg.ApplicationCommandInteractionDataOption) error {

		if err := intr.AcknowledgeEphemeral(); err != nil {
			return err
		}

		commandData := intr.ApplicationCommandData()
		message := commandData.Resolved.Messages[commandData.TargetID]

		embed := New(intr.GuildID, intr.ChannelID, "")
		embed.Description = message.Content

		//Retrieve the full member object if author is still a member of the guild.
		member, user, err := members.GetMemberOrUserFromID(intr.Session, intr.GuildID, message.Author.ID)
		if err != nil {
			return intr.EditResponseEmbeds(err, states.ResponseGenericError(err))
		}
		if member != nil {
			if member.Nick == "" {
				embed.Author.Name = member.User.Username
			} else {
				embed.Author.Name = member.Nick
			}

			if member.User.AvatarURL("") == "" {
				embed.Author.IconURL = member.AvatarURL("")
				embed.Author.ProxyIconURL = member.AvatarURL("")
			} else {
				embed.Author.IconURL = member.User.AvatarURL("")
				embed.Author.ProxyIconURL = member.User.AvatarURL("")
			}
		} else {
			embed.Author.Name = user.Username
			embed.Author.IconURL = user.AvatarURL("")
			embed.Author.ProxyIconURL = user.AvatarURL("")
		}

		//Post the new embed.
		message, err = intr.ChannelMessageSendEmbed(embed.ChannelID, &embed.MessageEmbed)
		if err != nil {
			return intr.EditResponseEmbeds(err, states.ResponseGenericError(err))
		}
		embed.MessageID = message.ID
		//Save the embed in the DB.
		err = embed.Store(intr.DB)
		if err != nil {
			return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
		}
		return intr.EditResponseEmbeds(nil, ResponseEmbedPosted(embed.Link()))
	},
	"Any message can be turned into an embed by left clicking on it and selecting `Apps > Embed - New 📄`.",
)

//MessageCommandSaveEmbed - saves the first embed attached to the message.
var MessageCommandSaveEmbed = commands.New(
	&dg.ApplicationCommand{
		Type: dg.MessageApplicationCommand,
		Name: "Embed - Save 💾",
	},
	false,
	func(intr states.Interaction, _ ...*dg.ApplicationCommandInteractionDataOption) error {
		commandData := intr.ApplicationCommandData()
		message := commandData.Resolved.Messages[commandData.TargetID]
		message.GuildID = intr.GuildID
		if err := intr.AcknowledgeEphemeral(); err != nil {
			return err
		}

		return embedSave(intr, message, 0)
	},
	"Any embed can be saved to the internal database by left clicking on the message and going to"+
		" `Apps > Embed - Save 💾`. Only saved the first embed in the message if it has multiple ones.",
)

//MessageCommandUnSaveEmbed - removes the first embed from the DB if it was previously saved.
var MessageCommandUnSaveEmbed = commands.New(
	&dg.ApplicationCommand{
		Type: dg.MessageApplicationCommand,
		Name: "Embed - Unsave ❌",
	},
	false,
	func(intr states.Interaction, _ ...*dg.ApplicationCommandInteractionDataOption) error {

		if err := intr.AcknowledgeEphemeral(); err != nil {
			return err
		}

		commandData := intr.ApplicationCommandData()
		message := commandData.Resolved.Messages[commandData.TargetID]

		t := intr.DB.NewTransaction()

		embed := New(intr.GuildID, message.ChannelID, message.ID)

		if found, err := embed.Delete(intr.DB); err != nil {
			return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
		} else if !found {
			return intr.EditResponseEmbeds(nil, ResponseEmbedNotFound())
		}

		if err := intr.EditResponseEmbeds(nil, ResponseEmbedUnSaved()); err != nil {
			return err
		}
		return t.Commit()
	},
	"Any embed can be removed from the internal database by left clicking on the message and going to"+
		" `Apps > Embed - Unsave ❌`. If the message has multiple embeds, all weill be unsaved.",
)
