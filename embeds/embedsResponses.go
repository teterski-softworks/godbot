package embeds

import (
	"fmt"

	dg "github.com/bwmarrin/discordgo"
)

var (
	ResponseEmbedPosted = func(messageLink string) *dg.MessageEmbed {
		embed := New("", "", "")
		embed.Title = "✅ Embed Posted"
		embed.Color = ColorGreen
		embed.Description = fmt.Sprintf("Embed posted. [Link](%s)", messageLink)
		return &embed.MessageEmbed
	}

	ResponseEmbedUpdated = func(messageLink string) *dg.MessageEmbed {
		embed := New("", "", "")
		embed.Title = "✅ Embed Updated"
		embed.Color = ColorGreen
		embed.Description = fmt.Sprintf("Embed updated. [Link](%s)", messageLink)
		return &embed.MessageEmbed
	}

	ResponseEmbedNotFound = func() *dg.MessageEmbed {
		embed := New("", "", "")
		embed.Title = "❌ Embed Not Found"
		embed.Color = ColorRed
		embed.Description = "Unable to find embed with the provided information."
		return &embed.MessageEmbed
	}

	ResponseEmbedSaved = func() *dg.MessageEmbed {
		embed := New("", "", "")
		embed.Title = "✅ Embed Saved 💾"
		embed.Color = ColorGreen
		embed.Description = "Embed has been saved."
		return &embed.MessageEmbed
	}

	ResponseEmbedUnSaved = func() *dg.MessageEmbed {
		embed := New("", "", "")
		embed.Title = "✅ Embed Unsaved 🚮"
		embed.Color = ColorGreen
		embed.Description = "Embed save information was deleted."
		return &embed.MessageEmbed
	}

	ResponseInvalidColorCode = func() *dg.MessageEmbed {
		embed := New("", "", "")
		embed.Title = "❌ Invalid Color Code 🎨"
		embed.Color = ColorRed
		embed.Description = "Please provide a valid hexadecimal color code."
		return &embed.MessageEmbed
	}

	ResponseInvalidMessageID = func(optionName string) *dg.MessageEmbed {
		embed := New("", "", "")
		embed.Title = "❌ Invalid Message ID"
		embed.Color = ColorRed
		embed.Description = fmt.Sprintf("Please provide a valid message ID in the `%s` option.", optionName)
		return &embed.MessageEmbed
	}
)
