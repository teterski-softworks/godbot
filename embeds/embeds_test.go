package embeds_test

import (
	"reflect"
	"strconv"
	"testing"
	"time"

	dg "github.com/bwmarrin/discordgo"
	"gitlab.com/teterski-softworks/godbot/db"
	"gitlab.com/teterski-softworks/godbot/embeds"
	"gitlab.com/teterski-softworks/godbot/tests"
)

var guildID, channelID, messageID = "guildID", "channelID", "messageID"
var createTables = []func(db.ReadWriter) error{embeds.CreateDatabaseTables}

//TODO
func TestDatabaseAccess(t *testing.T) {

	expected := embeds.New(guildID, channelID, messageID)

	//TODO: generate random values for counter.

	//TODO: generate new random values for counter
	updated := expected
	updated.Author.Name = "newAuthor"
	updated.Footer.Text = "newFooterText"
	updated.Image.URL = "newImageURL"
	updated.Description = "newDescription"
	updated.Timestamp = time.Now().Add(3 * time.Hour).Format(time.RFC1123)

	retrieve := func(DB db.Reader) (embeds.Embed, error) {
		return embeds.Retrieve(DB, guildID, channelID, messageID)
	}

	tests.TestDatabaseAccess(
		t,
		expected, updated,
		createTables,
		retrieve,
	)
}

//TODO
func TestFields(t *testing.T) {
	DB := tests.InitDatabase(t, createTables)
	defer tests.CloseDatabase(t, DB)

	t.Run("Add Field", func(t *testing.T) {

		name, value, inline := "name", "value", true

		expected := embeds.New(guildID, channelID, messageID)
		expected.Fields = []*dg.MessageEmbedField{
			{
				Name:   name,
				Value:  value,
				Inline: inline,
			},
		}
		result := embeds.New(guildID, channelID, messageID)
		result.AddField(name, value, inline)

		if !reflect.DeepEqual(expected, result) {
			t.Fatalf(tests.WrongValue, "AddField()", expected, result)
		}
	})

	t.Run("Remove Field", func(t *testing.T) {
		expected := embeds.New(guildID, channelID, messageID)
		result := embeds.New(guildID, channelID, messageID)
		for i := 0; i <= 10; i++ {
			result.AddField(strconv.Itoa(i), strconv.Itoa(i*2), i%2 == 0)
			if i == 0 || i == 10 || i == 5 {
				continue
			}
			expected.AddField(strconv.Itoa(i), strconv.Itoa(i*2), i%2 == 0)
		}

		result.RemoveField(10)
		result.RemoveField(5)
		result.RemoveField(0)

		//should not remove any fields
		result.RemoveField(-1)
		result.RemoveField(len(result.Fields) + 10)

		if !reflect.DeepEqual(expected, result) {
			t.Fatalf(tests.WrongValue, "RemoveField()", expected, result)
		}
	})

}

//TODO
func TestEmbedSave(t *testing.T) {

	DB := tests.InitDatabase(t, createTables)
	defer tests.CloseDatabase(t, DB)

	/*message := &dg.Message{
		GuildID:   keys.GuildID,
		ChannelID: keys.ChannelID,
		ID:        keys.MessageID,
	}*/

	t.Run("Embed not found", func(t *testing.T) {
		/*expected := embeds.ResponseEmbedNotFound
		if result, err := embeds.EmbedSave(DB, message, 0); err != nil {
			t.Fatalf("%+v", err)
		} else if result != expected {
			t.Fatalf(tests.WrongValue, "EmbedSave()", expected, result)
		}*/
	})

	embed1 := embeds.New(guildID, channelID, messageID)
	//TODO: generate random embed values
	embed1.Title = "One"
	embed1.Description = "This is the first embed."
	embed1.AddField("field1", "a field", false)

	embed2 := embeds.New(guildID, channelID, messageID)
	//TODO: generate random embed values
	embed2.Title = "Two"
	embed2.Description = "This is the second embed."
	embed2.AddField("field2", "another field", true)

	embed3 := embeds.New(guildID, channelID, messageID)
	//TODO: generate random embed values
	embed3.Title = "Three"
	embed3.Description = "This is the third embed."
	embed3.AddField("field3", "yet another field", true)

	/*embedList := []embeds.Embed{embed1, embed2, embed3}

	t.Run("Embed saved", func(t *testing.T) {

		for i := range embedList {
			expected := embeds.ResponseEmbedSaved
			message.Embeds = append(message.Embeds, &embedList[i].MessageEmbed)
			if result, err := embeds.EmbedSave(DB, message, i); err != nil {
				t.Fatalf("%+v", err)
			} else if result != expected {
				t.Fatalf(tests.WrongValue, "EmbedSave()", expected, result)
			}

			if result, err := embeds.Retrieve(DB, keys); err != nil {
				t.Fatalf("%+v", err)
			} else if expected := embedList[i]; !reflect.DeepEqual(result, expected) {
				t.Fatalf(tests.WrongValue, "EmbedSave()", expected, result)
			}
		}
	})*/

}
