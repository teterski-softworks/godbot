package embeds

import (
	dg "github.com/bwmarrin/discordgo"
	"gitlab.com/teterski-softworks/godbot/db"
	"gitlab.com/teterski-softworks/godbot/states"
)

//slashCommandEmbedField - A subcommand option group for embed field related commands.
var slashCommandEmbedField = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionSubCommandGroup,
	Name:        "field",
	Description: "Embed field related commands.",
	Options: []*dg.ApplicationCommandOption{
		slashCommandEmbedFieldNew,
		slashCommandEmbedFieldEdit,
		slashCommandEmbedFieldClear,
	},
}

//slashEmbedField is a subcommand group for embed field related commands.
func slashEmbedField(intr states.Interaction, options []*dg.ApplicationCommandInteractionDataOption) error {

	if err := intr.AcknowledgeEphemeral(); err != nil {
		return err
	}

	switch options[0].Name {
	case slashCommandEmbedFieldNew.Name:
		return slashEmbedFieldNew(intr, options[0].Options)
	case slashCommandEmbedFieldEdit.Name:
		return slashEmbedFieldEdit(intr, options[0].Options)
	case slashCommandEmbedFieldClear.Name:
		return slashEmbedFieldClear(intr, options[0].Options)
	}
	return intr.EditResponseEmbeds(nil, states.ResponseGenericError(nil))
}

//slashCommandEmbedFieldNew - A subcommand for creating new fields in an embed.
var slashCommandEmbedFieldNew = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionSubCommand,
	Name:        "new",
	Description: "Adds a new field to an existing embed.",
	Options: []*dg.ApplicationCommandOption{
		optionChannel,
		optionMessage,
		{
			Type:        dg.ApplicationCommandOptionString,
			Name:        "name",
			Description: "The name of the field.",
			Required:    true,
		}, {
			Type:        optionCopyMessage.Type,
			Name:        optionCopyMessage.Name,
			Description: optionCopyMessage.Description,
			Required:    true,
		}, {
			Type:        optionCopyChannel.Type,
			Name:        optionCopyChannel.Name,
			Description: optionCopyChannel.Description,
			Required:    true,
		}, {
			Type:        dg.ApplicationCommandOptionBoolean,
			Name:        "inline",
			Description: "Whether or not this field should display inline.",
			Required:    true,
		},
	},
}

//slashEmbedFieldNew adds a field to an embed.
func slashEmbedFieldNew(intr states.Interaction, options []*dg.ApplicationCommandInteractionDataOption) error {

	embed, err := getEmbedFromOptions(intr, options)
	if err != nil {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}

	//Get the content from the message ID
	fieldMsgID := options[3].StringValue()
	fieldChannel := options[4].ChannelValue(intr.Session)
	message, err := intr.ChannelMessage(fieldChannel.ID, fieldMsgID)
	if err != nil {
		return intr.EditResponseEmbeds(err, states.ResponseGenericError(err))
	}

	//Add the new field
	embed.AddField(options[2].StringValue(), message.Content, options[5].BoolValue())
	embed.Store(intr.DB)

	return updateEmbedAndRespond(intr, embed)
}

//slashCommandEmbedFieldEdit - A subcommand for editing fields in an embed.
var slashCommandEmbedFieldEdit = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionSubCommand,
	Name:        "edit",
	Description: "Edits a field in an existing embed.",
	Options: []*dg.ApplicationCommandOption{
		optionChannel,
		optionMessage,
		optionEmbedFieldIndex,
		{
			Type:        dg.ApplicationCommandOptionString,
			Name:        "name",
			Description: "The name of the field.",
		},
		optionCopyMessage,
		optionCopyChannel,
		{
			Type:        dg.ApplicationCommandOptionBoolean,
			Name:        "inline",
			Description: "Whether or not this field should display inline.",
		},
	},
}

//slashEmbedFieldEdit edits a field of an embed.
func slashEmbedFieldEdit(intr states.Interaction, options []*dg.ApplicationCommandInteractionDataOption) error {

	embed, err := getEmbedFromOptions(intr, options)
	if err != nil {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}
	index := int(options[2].IntValue()) - 1

	var fieldMsgID string
	for _, option := range options {
		switch option.Name {
		case "name":
			embed.Fields[index].Name = option.StringValue()
		case "text_message_id":
			fieldMsgID = option.StringValue()
		case "text_channel":
			if fieldMsgID == "" {
				continue
			}
			fieldChannel := option.ChannelValue(intr.Session)

			if fieldChannel != nil {
				message, err := intr.ChannelMessage(fieldChannel.ID, fieldMsgID)

				if err != nil {
					return intr.EditResponseEmbeds(err, states.ResponseGenericError(err))
				}
				if message.Content != "" {
					embed.Fields[index].Value = message.Content
				}
			}
		case "inline":
			embed.Fields[index].Inline = option.BoolValue()
		}
	}

	return updateEmbedAndRespond(intr, embed)
}

//slashCommandEmbedFieldClear - A subcommand to erase an embed's field.
var slashCommandEmbedFieldClear = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionSubCommand,
	Name:        "clear",
	Description: "Clears a field in an existing embed.",
	Options: []*dg.ApplicationCommandOption{
		optionChannel,
		optionMessage,
		optionEmbedFieldIndex,
	},
}

//slashEmbedFieldClear erases an embed's field.
func slashEmbedFieldClear(intr states.Interaction, options []*dg.ApplicationCommandInteractionDataOption) error {

	embed, err := getEmbedFromOptions(intr, options)
	if err != nil {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}
	index := int(options[2].IntValue()) - 1

	//Get the embed
	embed, err = Retrieve(intr.DB, embed.GuildID, embed.ChannelID, embed.MessageID)
	if err != nil {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}

	//Remove the field at the index from the embed
	embed.RemoveField(index)

	return updateEmbedAndRespond(intr, embed)
}
