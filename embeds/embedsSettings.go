package embeds

import (
	"database/sql"

	dg "github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
	"gitlab.com/teterski-softworks/godbot/db"
)

//TODO
type Settings struct {
	GuildID      string
	DefaultColor int
}

//TODO
func NewSettings(guildID string) Settings {
	return Settings{GuildID: guildID}
}

//RetrieveSettings returns the Settings for the given Guild.
func RetrieveSettings(DB db.Reader, guildID string) (settings Settings, err error) {
	settings = NewSettings(guildID)
	row, err := DB.QueryRow(SQLSelectSettings, guildID)
	if err != nil {
		return settings, err
	}
	if err := row.Scan(&settings.DefaultColor); err != nil && !errors.Is(err, sql.ErrNoRows) {
		return settings, errors.WithStack(err)
	}
	return settings, nil
}

//Stores Settings to the DB.
func (s Settings) Store(DB db.ReadWriter) error {
	return DB.QueueAndCommit(SQLUpsertSettings, s.GuildID, s.DefaultColor)
}

//TODO
func (s Settings) Embed() *dg.MessageEmbed {
	embed := New("", "", "")
	embed.Color = s.DefaultColor
	embed.Title = "📄 Embed Settings ⚙️"

	embed.AddField("✍️ Author", "Name: `None set`\nIcon: `None set`\nURL: `None set`", true)

	embed.AddField("📄 Body", "Title: `None set`\nURL: `None set`\nDescription: `None set`", true)

	embed.AddField("🦶 Footer", "Text: `None set`\nIcon: `None set`", true)

	embed.AddField("🎨 Color",
		"The accent color of this embed will be used as the default accent color for other embeds. "+
			"Note that `#000000` will effectively remove the accent color.", false)

	return &embed.MessageEmbed
}
