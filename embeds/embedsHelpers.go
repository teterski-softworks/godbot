package embeds

import (
	dg "github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
	"gitlab.com/teterski-softworks/godbot/db"
	"gitlab.com/teterski-softworks/godbot/states"
)

//embedSave saves a message as an Embed to the DB.
func embedSave(intr states.Interaction, message *dg.Message, index int) error {

	embed := New(message.GuildID, message.ChannelID, message.ID)

	if len(message.Embeds) == 0 {
		return intr.EditResponseEmbeds(nil, ResponseEmbedNotFound())
	}

	embed.Title = message.Embeds[index].Title
	embed.URL = message.Embeds[index].URL
	embed.Description = message.Embeds[index].Description
	embed.Color = message.Embeds[index].Color

	if message.Embeds[index].Author != nil {
		embed.Author = message.Embeds[index].Author
	}
	if message.Embeds[index].Footer != nil {
		embed.Footer = message.Embeds[index].Footer
	}
	if message.Embeds[index].Image != nil {
		embed.Image = message.Embeds[index].Image
	}
	if message.Embeds[index].Thumbnail != nil {
		embed.Thumbnail = message.Embeds[index].Thumbnail
	}
	if message.Embeds[index].Video != nil {
		embed.Video = message.Embeds[index].Video
	}
	if message.Embeds[index].Fields != nil {
		embed.Fields = message.Embeds[index].Fields
	}

	if err := embed.Store(intr.DB); err != nil {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}

	return intr.EditResponseEmbeds(nil, ResponseEmbedSaved())
}

//getEmbedFromOptions returns the embed from the DB given the channel in options[0] and message ID in options[1].
func getEmbedFromOptions(intr states.Interaction, options []*dg.ApplicationCommandInteractionDataOption) (Embed, error) {
	channel := options[0].ChannelValue(intr.Session)
	messageID := options[1].StringValue()
	return Retrieve(intr.DB, intr.GuildID, channel.ID, messageID)
}

//updateEmbedAndRespond saves the changes to the embed to the DB
//edits the original embed in the guild, and sends a response to the interaction.
func updateEmbedAndRespond(intr states.Interaction, embed Embed) (err error) {
	t := intr.DB.NewTransaction()

	//Store the updated embed info
	if err = embed.Store(t); err != nil {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}
	//Update the embed
	if _, err = intr.ChannelMessageEditEmbed(embed.ChannelID, embed.MessageID, &embed.MessageEmbed); err != nil {
		return intr.EditResponseEmbeds(err, states.ResponseGenericError(err))
	}

	if err = intr.EditResponseEmbeds(errors.WithStack(err), ResponseEmbedUpdated(embed.Link())); err != nil {
		return err
	}
	return t.Commit()
}
