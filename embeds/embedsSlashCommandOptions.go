package embeds

import dg "github.com/bwmarrin/discordgo"

//optionChannel - a required option for a text channel.
var optionChannel = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionChannel,
	Name:        "channel",
	Description: "The channel that the embed is in.",
	Required:    true,
	ChannelTypes: []dg.ChannelType{
		dg.ChannelTypeGuildText,
	},
}

//optionMessage - a required option for a message ID string.
var optionMessage = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionString,
	Name:        "message_id",
	Description: "The ID of the message the embed is in.",
	Required:    true,
}

//optionCopyMessage - an option for a message ID string.
var optionCopyMessage = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionString,
	Name:        "copy_text_from_message_id",
	Description: "ID of the message with the desired text to be copied.",
}

//optionCopyChannel - an option for a text channel.
var optionCopyChannel = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionChannel,
	Name:        "copy_text_from_channel",
	Description: "Channel ID of the message with the desired text to be copied.",
	ChannelTypes: []dg.ChannelType{
		dg.ChannelTypeGuildText,
	},
}

//optionEmbedFieldIndex - A required option for an field's index in an embed.
var optionEmbedFieldIndex = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionInteger,
	Name:        "index",
	Description: "The index of the field in the embed.",
	Required:    true,
	MinValue:    &minFieldIndex,
	MaxValue:    25,
}
var minFieldIndex float64 = 1
