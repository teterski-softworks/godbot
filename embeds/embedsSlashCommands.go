package embeds

import (
	"database/sql"
	"fmt"
	"strings"

	dg "github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
	"gitlab.com/teterski-softworks/godbot/commands"
	"gitlab.com/teterski-softworks/godbot/db"
	"gitlab.com/teterski-softworks/godbot/states"
)

//SlashCommandEmbed - A command group for embed related commands.
var SlashCommandEmbed = commands.New(
	&dg.ApplicationCommand{
		Type:        dg.ChatApplicationCommand,
		Name:        "embed",
		Description: "Embed related commands. Admin only.",
		Options: []*dg.ApplicationCommandOption{
			slashCommandEmbedNew,
			slashCommandEmbedEdit,
			slashCommandEmbedList,
			slashCommandEmbedSave,
			slashCommandEmbedUnSave,
			slashCommandEmbedRepost,
			slashCommandEmbedField,
			slashCommandEmbedSettings,
		},
	},
	false,
	func(intr states.Interaction, options ...*dg.ApplicationCommandInteractionDataOption) (err error) {

		if err := intr.AcknowledgeEphemeral(); err != nil {
			return err
		}

		switch options[0].Name {
		case slashCommandEmbedNew.Name:
			return slashEmbedNew(intr, options[0].Options)
		case slashCommandEmbedEdit.Name:
			return slashEmbedEdit(intr, options[0].Options)
		case slashCommandEmbedList.Name:
			return slashEmbedList(intr, options[0].Options)
		case slashCommandEmbedSave.Name:
			return slashEmbedSave(intr, options[0].Options)
		case slashCommandEmbedUnSave.Name:
			return slashEmbedUnSave(intr, options[0].Options)
		case slashCommandEmbedRepost.Name:
			return slashEmbedRepost(intr, options[0].Options)
		case slashCommandEmbedField.Name:
			return slashEmbedField(intr, options[0].Options)
		case slashCommandEmbedSettings.Name:
			return slashEmbedSettings(intr, options[0].Options)
		}

		return intr.EditResponseEmbeds(nil, states.ResponseGenericError(nil))
	},
)

//slashCommandEmbedNew - A subcommand to make new embeds.
var slashCommandEmbedNew = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionSubCommand,
	Name:        "new",
	Description: "Creates a new embed.",
}

//slashEmbedNew creates a new embed in the guild.
func slashEmbedNew(intr states.Interaction, options []*dg.ApplicationCommandInteractionDataOption) error {

	embed := NewDefault(intr.DB, intr.GuildID)
	embed.ChannelID = intr.ChannelID
	embed.Title = "Edit me with the `/embed edit` commands!"

	//Post the new embed.
	message, err := intr.ChannelMessageSendEmbed(embed.ChannelID, &embed.MessageEmbed)
	if err != nil {
		return intr.EditResponseEmbeds(err, states.ResponseGenericError(err))
	}
	embed.MessageID = message.ID

	//Update the embed with its message ID.
	embed.Description = fmt.Sprintf("**Message ID: `%s`**", embed.MessageID)
	_, err = intr.ChannelMessageEditEmbed(embed.ChannelID, embed.MessageID, &embed.MessageEmbed)
	if err != nil {
		return intr.EditResponseEmbeds(err, states.ResponseGenericError(err))
	}
	//Save the embed in the DB.
	err = embed.Store(intr.DB)
	if err != nil {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}
	return intr.EditResponseEmbeds(nil, ResponseEmbedPosted(embed.Link()))
}

//slashCommandEmbedList - A subcommand to view a list of all saved embeds.
var slashCommandEmbedList = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionSubCommand,
	Name:        "list",
	Description: "Lists all saved embeds.",
}

//slashEmbedList sends a list of all saved embeds.
func slashEmbedList(intr states.Interaction, options []*dg.ApplicationCommandInteractionDataOption) error {

	list, err := NewGuildList(intr.DB, intr.GuildID, ListTitle, CustomIDPrefixList, RetrieveAll)
	if err != nil {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}

	page := 0
	var response dg.WebhookEdit
	response.Embeds = list.Embeds(page)
	response.Components = list.Components(page)
	if _, err = intr.InteractionResponseEdit(&intr.Interaction, &response); err != nil {
		return errors.Wrapf(err, "unable to respond")
	}
	return nil
}

//slashCommandEmbedSave - A subcommand to save an embed for later use.
var slashCommandEmbedSave = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionSubCommand,
	Name:        "save",
	Description: "Saves an embed for later use.",
	Options: []*dg.ApplicationCommandOption{
		optionChannel,
		optionMessage,
		{
			Type:        dg.ApplicationCommandOptionInteger,
			Name:        "index",
			Description: "The index of the embed if the message has multiple.",
			MaxValue:    10,
		},
	},
}

//slashEmbedSave saves an embed to the DB.
func slashEmbedSave(intr states.Interaction, options []*dg.ApplicationCommandInteractionDataOption) error {

	channel := options[0].ChannelValue(intr.Session)
	messageID := strings.ReplaceAll(options[1].StringValue(), " ", "")
	index := 0
	if len(options) > 2 {
		index = int(options[2].IntValue()) - 1
	}

	//Get the embed from the message
	message, err := intr.ChannelMessage(channel.ID, messageID)
	if err != nil {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}
	message.GuildID = intr.GuildID
	return embedSave(intr, message, index)
}

//slashCommandEmbedUnSave - A subcommand to remove embed save information.
var slashCommandEmbedUnSave = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionSubCommand,
	Name:        "unsave",
	Description: "Removes the saved embed information.",
	Options: []*dg.ApplicationCommandOption{
		optionChannel,
		optionMessage,
	},
}

//slashEmbedUnSave removes an embed from the DB.
func slashEmbedUnSave(intr states.Interaction, options []*dg.ApplicationCommandInteractionDataOption) error {

	channel := options[0].ChannelValue(intr.Session)
	messageID := strings.ReplaceAll(options[1].StringValue(), " ", "")

	embed := New(intr.GuildID, channel.ID, messageID)
	if found, err := embed.Delete(intr.DB); err != nil {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	} else if !found {
		return intr.EditResponseEmbeds(nil, ResponseEmbedNotFound())
	}

	return intr.EditResponseEmbeds(nil, ResponseEmbedUnSaved())
}

//slashCommandEmbedRepost - A subcommand to repost a saved embed.
var slashCommandEmbedRepost = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionSubCommand,
	Name:        "repost",
	Description: "Reposts a saved embed.",
	Options: []*dg.ApplicationCommandOption{
		optionChannel,
		optionMessage,
	},
}

//slashEmbedRepost posts a saved embed from the DB and saves it as a new duplicate.
func slashEmbedRepost(intr states.Interaction, options []*dg.ApplicationCommandInteractionDataOption) error {

	channel := options[0].ChannelValue(intr.Session)
	messageID := strings.ReplaceAll(options[1].StringValue(), " ", "")

	//Get the embed
	embed, err := Retrieve(intr.DB, intr.GuildID, channel.ID, messageID)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return intr.EditResponseEmbeds(nil, ResponseEmbedNotFound())
		}
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}

	//Repost the embed
	message, err := intr.ChannelMessageSendEmbed(channel.ID, &embed.MessageEmbed)
	if err != nil {
		return intr.EditResponseEmbeds(err, states.ResponseGenericError(err))
	}

	//Save the reposted embed in the DB
	embed.MessageID = message.ID
	if err = embed.Store(intr.DB); err != nil {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}

	return intr.EditResponseEmbeds(nil, ResponseEmbedPosted(embed.Link()))
}
