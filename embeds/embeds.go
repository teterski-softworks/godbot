package embeds

import (
	"fmt"

	dg "github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
	"gitlab.com/teterski-softworks/godbot/channels"
	"gitlab.com/teterski-softworks/godbot/db"
	"gitlab.com/teterski-softworks/godbot/messages"
)

var (
	ListTitle          = "💾 Saved Embeds 📄"
	CustomIDPrefixList = "embedList"

	ColorGreen  int = 0x78b159
	ColorYellow int = 0xfdcb58
	ColorRed    int = 0xdd2e44
)

//Embed represents a Discord rich embed.
type Embed struct {
	messages.Message
	Encrypted
}

//Encrypted stores the encrypted portion of Embed.
type Encrypted struct {
	dg.MessageEmbed
}

//New returns a new Embed.
func New(guildID string, channelID string, messageID string) Embed {
	var e Embed
	e.Footer = &dg.MessageEmbedFooter{}
	e.Image = &dg.MessageEmbedImage{}
	e.Thumbnail = &dg.MessageEmbedThumbnail{}
	e.Video = &dg.MessageEmbedVideo{} //We can't use this, only Discord itself can, but its here in case that changes in the future.
	e.Author = &dg.MessageEmbedAuthor{}
	return e
}

//TODO
func NewDefault(DB db.Reader, guildID string) Embed {
	settings, _ := RetrieveSettings(DB, guildID) //Ignore error
	e := New(guildID, "", "")
	e.Color = settings.DefaultColor
	return e
}

//Retrieve gets an Embed from the DB.
func Retrieve(DB db.Reader, guildID string, channelID string, messageID string) (Embed, error) {

	e := New(guildID, channelID, messageID)

	var encryptedData []byte

	//Get embed from DB.
	row, err := DB.QueryRow(SQLSelectEmbed, e.GuildID, e.ChannelID, e.MessageID)
	if err != nil {
		return e, err
	}
	if err = row.Scan(&encryptedData); err != nil {
		return e, errors.WithStack(err)
	}
	if err = DB.Decrypt(&e.Encrypted, encryptedData); err != nil {
		return e, err
	}

	return e, nil
}

func RetrieveAll(DB db.Reader, guildID string) (embeds []Embed, err error) {

	//TODO: change this to be one SQL Query instead.

	//Retrieve a list of all embeds in the DB
	rows, err := DB.Query(SQLSelectGuildEmbeds, guildID)
	if err != nil {
		return embeds, err
	}

	for rows.Next() {
		var channelID, messageID string
		if err = rows.Scan(&channelID, &messageID); err != nil {
			return embeds, errors.WithStack(err)
		}
		embed := New(guildID, channelID, "")
		embed.MessageID = messageID
		embeds = append(embeds, embed)
	}

	for i := range embeds {
		embed, err := Retrieve(DB, embeds[i].GuildID, embeds[i].ChannelID, embeds[i].MessageID)
		if err != nil {
			return embeds, errors.WithStack(err)
		}
		embeds[i] = embed
	}
	return embeds, nil
}

//Store saves the embed to the DB.
func (e Embed) Store(DB db.ReadWriter) (err error) {

	t := DB.NewTransaction()

	encryptedData, err := t.Encrypt(e.Encrypted)
	if err != nil {
		return err
	}

	if err := channels.New(e.GuildID, e.ChannelID).Store(t); err != nil {
		return err
	}

	//Upsert the embed
	t.Queue(SQLUpsertEmbed, e.GuildID, e.ChannelID, e.MessageID, encryptedData)

	return t.Commit()
}

//Delete removes a saved Embed from the DB.
//If the embed is already not in the provided database, then found returns false. Otherwise, returns true.
func (e Embed) Delete(DB db.ReadWriter) (found bool, err error) {

	if _, err = Retrieve(DB, e.GuildID, e.ChannelID, e.MessageID); err != nil {
		return false, err
	}
	if err = DB.QueueAndCommit(SQLDeleteEmbed, e.GuildID, e.ChannelID, e.MessageID); err != nil {
		return false, err
	}
	return true, nil
}

//AddField adds an embed field to the Embed.
func (e *Embed) AddField(name string, value string, inline bool) {
	e.Fields = append(e.Fields,
		&dg.MessageEmbedField{
			Name:   name,
			Value:  value,
			Inline: inline,
		})
}

//RemoveField removes a field from an Embed.
func (e *Embed) RemoveField(index int) {
	//Remove the field at the index from the embed
	var fields []*dg.MessageEmbedField
	for i := 0; i < len(e.Fields); i++ {
		if i != index {
			fields = append(fields, e.Fields[i])
		}
	}
	e.Fields = fields
}

//Returns a formatted string with information about the Embed. Used in menus showing a list of all embeds.
func (e Embed) Info() string {

	//TODO: sanitize the text from the embed
	//embed.Title = strings.ReplaceAll(embed.Title, "*", "")
	//embed.Description = strings.ReplaceAll(embed.Description, "*", "")
	//embed.Description = strings.ReplaceAll(embed.Description, "`", "")

	var info string

	image, thumbnail := "", ""
	if e.Image.URL != "" {
		image = fmt.Sprintf("[[Image]](%s)", e.Image.URL)
	}
	if e.Thumbnail.URL != "" {
		thumbnail = fmt.Sprintf("[[Thumbnail]](%s)", e.Thumbnail.URL)
	}

	info += fmt.Sprintf("**ID:** `%s` %s [[Link]](%s) %s %s\n", e.MessageID, channels.Mention(e.ChannelID), e.Link(), image, thumbnail)
	if e.Title != "" {
		link := ""
		if e.URL != "" {
			link = fmt.Sprintf("[[Title Link]](%s)", e.URL)
		}
		info += fmt.Sprintf("Title: `%s` %s\n", e.Title, link)
	}
	if e.Description != "" {
		info += fmt.Sprintf("Description: `%d` characters\n", len(e.Description))
	}

	if e.Author.Name != "" {
		link, avatar := "", ""
		if e.Author.URL != "" {
			link = fmt.Sprintf("[[Link]](%s)", e.Author.URL)
		}
		if e.Author.IconURL != "" {
			avatar = fmt.Sprintf("[[Avatar]](%s)", e.Author.IconURL)
		}
		info += fmt.Sprintf("Author: `%s` %s %s\n", e.Author.Name, link, avatar)
	}

	if e.Footer.Text != "" {
		avatar := ""
		if e.Footer.IconURL != "" {
			avatar = fmt.Sprintf("[[Avatar]](%s)", e.Footer.IconURL)
		}
		info += fmt.Sprintf("Footer: `%s` %s\n", e.Footer.Text, avatar)
	}

	if len(e.Fields) != 0 {
		info += fmt.Sprintf("Fields: `%d`\n", len(e.Fields))
	}
	return info
}
