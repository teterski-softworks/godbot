package embeds

import (
	"fmt"
	"strconv"
	"time"

	dg "github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
	"gitlab.com/teterski-softworks/godbot/db"
	"gitlab.com/teterski-softworks/godbot/states"
)

//The maximum length of an Embed's description.
const maxEmbedDescriptionLength = 4096

//Book is an Embed which has multiple pages which can be flipped through.
type Book struct {
	embed    Embed    //The Embed used in displaying the Book.
	lastPage int      //The number of the last page in the Book.
	customID string   //The customID used in event handling for switching pages.
	pages    []string //An array of strings representing the text of each of the Book's pages.
}

//NewBook returns an embed with pages.
func NewBook(embed Embed, customID string) Book {
	return Book{
		pages:    make([]string, 1),
		embed:    embed,
		customID: customID,
	}
}

//Add adds the provided text to the last page of the book.
//If the page exceeds the max text length of a Discord embed, a new page is created
//and the overflowed text is added there instead.
func (b *Book) Add(text string) {

	//TODO: If text itself is greater than maxEmbedDescriptionLength, what do we do?
	if len(text) > maxEmbedDescriptionLength {
		b.pages[b.lastPage] += text[:maxEmbedDescriptionLength]
		b.pages = append(b.pages, text[maxEmbedDescriptionLength:]) //TODO: we need to loop this because this text could still be too big
		b.lastPage++
		return
	}

	if len(b.pages[b.lastPage])+len(text) < maxEmbedDescriptionLength {
		b.pages[b.lastPage] += text
		return
	}
	b.pages = append(b.pages, text)
	b.lastPage++

}

//Embeds returns an array of MessageEmbeds containing an embed representation of the Book.
func (b Book) Embeds(page int) []*dg.MessageEmbed {
	if page > b.lastPage {
		page = b.lastPage
	}
	b.embed.Description = b.pages[page]
	if b.lastPage != 0 {
		b.embed.Footer.Text = fmt.Sprintf("Page %d/%d", page+1, b.lastPage+1)
	}
	return []*dg.MessageEmbed{&b.embed.MessageEmbed}
}

//Components returns an action row containing buttons allowing to switch Book pages.
func (b Book) Components(page int) []dg.MessageComponent {
	if len(b.pages) == 1 {
		return []dg.MessageComponent{}
	}
	if page > b.lastPage {
		page = b.lastPage
	}
	return []dg.MessageComponent{
		dg.ActionsRow{
			Components: []dg.MessageComponent{
				dg.Button{
					Label:    "◀️",
					Style:    dg.PrimaryButton,
					Disabled: page == 0,
					CustomID: b.customID + strconv.Itoa(page-1),
				},
				dg.Button{
					Label:    "▶️",
					Style:    dg.PrimaryButton,
					Disabled: page == b.lastPage,
					CustomID: b.customID + strconv.Itoa(page+1),
				},
			},
		},
	}
}

type info interface {
	//Info returns of a formatted string with information about the underlining object.
	//Used in menus showing a list of objects.
	Info() string //TODO: rename?
}

//NewGuildList returns a Book containing a list of `I`s`. This is primarily used to display guild level information in menu lists.
func NewGuildList[I info](
	DB db.Reader,
	guildID string,
	title string,
	CustomIDPrefix string,
	RetrieveAll func(DB db.Reader, guildID string) ([]I, error),

) (book Book, err error) {

	listEmbed := NewDefault(DB, guildID)
	listEmbed.Title = title
	listEmbed.Timestamp = time.Now().Format(time.RFC3339)

	items, err := RetrieveAll(DB, guildID)
	if err != nil {
		return book, err
	}

	book = NewBook(listEmbed, CustomIDPrefix)
	for _, i := range items {
		book.Add(i.Info() + "\n")
	}

	//If book is empty, let the user know
	if book.pages[0] == "" {
		book.Add("There are no records on file.")
	}

	return book, nil
}

//NewGuildList returns a Book containing a list of `I`s`. This is primarily used to display member level information in menu lists.
func NewMemberList[I info](
	s *states.State,
	guildID string,
	memberID string,
	title string,
	CustomIDPrefix string,
	RetrieveAllMember func(DB db.Reader, guildID string, memberID string) ([]I, error),

) (book Book, err error) {

	listEmbed := NewDefault(s.DB, guildID)
	listEmbed.Timestamp = time.Now().Format(time.RFC3339)
	member, err := s.GuildMember(guildID, memberID)
	username := "Member"
	if err != nil {
		user, err := s.User(memberID)
		if err != nil {
			return Book{}, errors.WithStack(err)
		}
		username = user.Username + "#" + user.Discriminator
		listEmbed.Thumbnail.URL = user.AvatarURL("")
	} else {
		username = member.User.Username + "#" + member.User.Discriminator
		listEmbed.Thumbnail.URL = member.AvatarURL("")
	}
	listEmbed.Title = fmt.Sprintf(title, username)
	listEmbed.Footer.Text = "User ID: " + memberID

	items, err := RetrieveAllMember(s.DB, guildID, memberID)
	if err != nil {
		return Book{}, err
	}

	book = NewBook(listEmbed, memberID+CustomIDPrefix)
	for _, i := range items {
		book.Add(i.Info() + "\n")
	}

	//If book is empty, let the user know
	if book.pages[0] == "" {
		book.Add("There are no records on file.")
	}

	return book, nil
}
