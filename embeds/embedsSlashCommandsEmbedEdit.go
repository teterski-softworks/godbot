package embeds

import (
	"database/sql"
	"strconv"
	"strings"

	dg "github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
	"gitlab.com/teterski-softworks/godbot/db"
	"gitlab.com/teterski-softworks/godbot/states"
)

//slashCommandEmbedEdit - A subcommand group for embed editing related commands.
var slashCommandEmbedEdit = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionSubCommandGroup,
	Name:        "edit",
	Description: "Edits an existing embed.",
	Options: []*dg.ApplicationCommandOption{
		slashCommandEmbedEditClear,
		slashCommandEmbedEditAuthor,
		slashCommandEmbedEditBody,
		slashCommandEmbedEditFooter,
	},
}

//slashEmbedEdit edits an embed.
func slashEmbedEdit(intr states.Interaction, options []*dg.ApplicationCommandInteractionDataOption) error {

	switch options[0].Name {
	case slashCommandEmbedEditAuthor.Name:
		return slashEmbedEditAuthor(intr, options[0].Options)
	case slashCommandEmbedEditBody.Name:
		return slashEmbedEditBody(intr, options[0].Options)
	case slashCommandEmbedEditFooter.Name:
		return slashEmbedEditFooter(intr, options[0].Options)
	case slashCommandEmbedEditClear.Name:
		return slashEmbedEditClear(intr, options[0].Options)
	}
	return intr.EditResponseEmbeds(nil, states.ResponseGenericError(nil))
}

//slashCommandEmbedEditAuthor - A subcommand to edit an embed's author information.
var slashCommandEmbedEditAuthor = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionSubCommand,
	Name:        "author",
	Description: "Edits an existing embed's author information.",
	Options: []*dg.ApplicationCommandOption{
		optionChannel,
		optionMessage,
		{
			Type:        dg.ApplicationCommandOptionString,
			Name:        "name",
			Description: "Author's name.",
		},
		{
			Type:        dg.ApplicationCommandOptionString,
			Name:        "link",
			Description: "URL of the author's name.",
		},
		{
			Type:        dg.ApplicationCommandOptionString,
			Name:        "icon_url",
			Description: "The URL to the author's icon.",
		},
	},
}

//slashEmbedEditAuthor edits an embed's author information.
func slashEmbedEditAuthor(intr states.Interaction, options []*dg.ApplicationCommandInteractionDataOption) error {

	embed, err := getEmbedFromOptions(intr, options)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return intr.EditResponseEmbeds(nil, ResponseEmbedNotFound())
		}
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}

	for _, option := range options {
		switch option.Name {
		case "name":
			embed.Author.Name = option.StringValue()
		case "link":
			embed.Author.URL = option.StringValue()
		case "icon_url":
			embed.Author.IconURL = option.StringValue()
			embed.Author.ProxyIconURL = option.StringValue()
		}
	}

	return updateEmbedAndRespond(intr, embed)
}

//slashCommandEmbedEditBody - A subcommand to edit an embed's body.
var slashCommandEmbedEditBody = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionSubCommand,
	Name:        "body",
	Description: "Edits an existing embed's body information.",
	Options: []*dg.ApplicationCommandOption{
		optionChannel,
		optionMessage,
		{
			Type:        dg.ApplicationCommandOptionString,
			Name:        "title",
			Description: "The title of the embed.",
		},
		{
			Type:        dg.ApplicationCommandOptionString,
			Name:        "url",
			Description: "The title's link.",
		},
		optionCopyMessage,
		optionCopyChannel,
		{
			Type:        dg.ApplicationCommandOptionString,
			Name:        "color",
			Description: "A hexadecimal color value.",
		},
		{
			Type:        dg.ApplicationCommandOptionString,
			Name:        "image_url",
			Description: "The URL to the embed's image.",
		},
		{
			Type:        dg.ApplicationCommandOptionString,
			Name:        "thumbnail_url",
			Description: "The ULR to the embed's thumbnail.",
		},
	},
}

//slashEmbedEditBody edits the embed's body.
func slashEmbedEditBody(intr states.Interaction, options []*dg.ApplicationCommandInteractionDataOption) error {

	embed, err := getEmbedFromOptions(intr, options)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return intr.EditResponseEmbeds(nil, ResponseEmbedNotFound())
		}
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}

	var descriptionMsgId string
	for _, option := range options {
		switch option.Name {
		case "title":
			embed.Title = option.StringValue()
		case "url":
			embed.URL = option.StringValue()
		case optionCopyMessage.Name:
			descriptionMsgId = option.StringValue()
		case optionCopyChannel.Name:
			if descriptionMsgId == "" {
				return intr.EditResponseEmbeds(errors.WithStack(err), ResponseInvalidMessageID(optionCopyMessage.Name))
			}
			descriptionChannel := option.ChannelValue(intr.Session)
			message, err := intr.ChannelMessage(descriptionChannel.ID, descriptionMsgId)
			if err != nil {
				return intr.EditResponseEmbeds(err, states.ResponseGenericError(err))
			}
			embed.Description = message.Content
		case "color":
			color := strings.ReplaceAll(option.StringValue(), "#", "")
			colorInt, err := strconv.ParseInt(color, 16, 32)
			if err != nil {
				return intr.EditResponseEmbeds(errors.WithStack(err), ResponseInvalidColorCode())
			}
			embed.Color = int(colorInt)
		case "image_url":
			embed.Image.URL = option.StringValue()
			embed.Image.ProxyURL = option.StringValue()
		case "thumbnail_url":
			embed.Thumbnail.URL = option.StringValue()
			embed.Thumbnail.ProxyURL = option.StringValue()
		}
	}

	return updateEmbedAndRespond(intr, embed)
}

//slashCommandEmbedEditFooter - A subcommand to edit an embed's footer.
var slashCommandEmbedEditFooter = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionSubCommand,
	Name:        "footer",
	Description: "Edits an existing embed's footer information.",
	Options: []*dg.ApplicationCommandOption{
		optionChannel,
		optionMessage,
		{
			Type:        dg.ApplicationCommandOptionString,
			Name:        "text",
			Description: "The footer text.",
		},
		{
			Type:        dg.ApplicationCommandOptionString,
			Name:        "icon_url",
			Description: "The URL to icon used in the footer.",
		},
	},
}

//slashEmbedEditFooter edits a an embed's footer information.
func slashEmbedEditFooter(intr states.Interaction, options []*dg.ApplicationCommandInteractionDataOption) error {

	embed, err := getEmbedFromOptions(intr, options)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return intr.EditResponseEmbeds(nil, ResponseEmbedNotFound())
		}
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}

	for _, option := range options {
		switch option.Name {
		case "text":
			embed.Footer.Text = option.StringValue()
		case "icon_url":
			embed.Footer.IconURL = option.StringValue()
			embed.Footer.ProxyIconURL = option.StringValue()

		}
	}

	return updateEmbedAndRespond(intr, embed)
}

//slashCommandEmbedEditClear - A subcommand to erase information from an embed.
var slashCommandEmbedEditClear = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionSubCommand,
	Name:        "clear",
	Description: "Clears a value in an embed message.",
	Options: []*dg.ApplicationCommandOption{
		optionChannel,
		optionMessage,
		{
			Type:        dg.ApplicationCommandOptionString,
			Name:        "element",
			Description: "An element of the embed's body to clear.",
			Required:    true,
			Choices: []*dg.ApplicationCommandOptionChoice{
				{Value: "URL", Name: "URL"},
				{Value: "Title", Name: "Title"},
				{Value: "Description", Name: "Description"},
				{Value: "Color", Name: "Color"},
				{Value: "Image", Name: "Image"},
				{Value: "Thumbnail", Name: "Thumbnail"},
				{Value: "Video", Name: "Video"},

				{Value: "Author Name", Name: "Author Name"},
				{Value: "Author URL", Name: "Author URL"},
				{Value: "Author Icon", Name: "Author Icon"},

				{Value: "Footer Text", Name: "Footer Text"},
				{Value: "Footer Icon", Name: "Footer Icon"},
			},
		},
	},
}

//slashEmbedEditClear erases selected information from an embed.
func slashEmbedEditClear(intr states.Interaction, options []*dg.ApplicationCommandInteractionDataOption) error {

	embed, err := getEmbedFromOptions(intr, options)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return intr.EditResponseEmbeds(nil, ResponseEmbedNotFound())
		}
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}

	switch options[2].StringValue() {
	case "URL":
		embed.URL = ""
	case "Title":
		embed.Title = ""
	case "Description":
		embed.Description = ""
	case "Color":
		embed.Color = 0
	case "Image":
		embed.Image = &dg.MessageEmbedImage{}
	case "Thumbnail":
		embed.Thumbnail = &dg.MessageEmbedThumbnail{}
	case "Video":
		embed.Video = &dg.MessageEmbedVideo{}
	case "Author Name":
		embed.Author.Name = ""
	case "Author URL":
		embed.Author.URL = ""
	case "Author Icon":
		embed.Author.IconURL = ""
		embed.Author.ProxyIconURL = ""
	case "Footer Text":
		embed.Footer.Text = ""
	case "Footer Icon":
		embed.Footer.IconURL = ""
		embed.Footer.ProxyIconURL = ""
	}

	return updateEmbedAndRespond(intr, embed)
}
