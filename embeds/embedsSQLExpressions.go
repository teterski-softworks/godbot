package embeds

const (
	SQLSelectEmbed string = `
	SELECT encryptedData FROM embeds WHERE guildID=?1 AND channelID=?2 AND messageID=?3;`

	SQLSelectGuildEmbeds string = `
	SELECT channelID, messageID	FROM embeds	WHERE guildID=?1 ORDER BY messageID;`

	SQLSelectSettings string = `
	SELECT defaultColor FROM embedsSettings WHERE guildID=?1;`

	SQLUpsertEmbed string = `
	INSERT INTO embeds( 
		guildID,
		channelID,
		messageID,
		encryptedData
	)
	VALUES(?1, ?2, ?3, ?4) ON CONFLICT DO
	UPDATE SET encryptedData=?4
	WHERE guildID=?1 AND channelID=?2 AND messageID=?3;`

	SQLUpsertSettings string = `
	INSERT INTO embedsSettings(guildID, defaultColor)
	VALUES(?1, ?2) ON CONFLICT DO
	UPDATE SET defaultColor=?2 WHERE guildID=?1;`

	SQLDeleteEmbed string = `
	DELETE FROM embeds
	WHERE guildID=?1 AND channelID=?2 AND messageID=?3;`
)
