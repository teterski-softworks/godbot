package embeds

import "gitlab.com/teterski-softworks/godbot/db"

//CreateDatabaseTables creates the required database tables for the embeds package.
func CreateDatabaseTables(DB db.ReadWriter) error {
	t := DB.NewTransaction()
	t.Queue(SQLCreateTableEmbeds)
	t.Queue(SQLCreateTableEmbedsSettings)
	return t.Commit()
}

const (
	SQLCreateTableEmbeds string = `
	CREATE TABLE IF NOT EXISTS embeds(
		guildID             TEXT NOT NULL,
		channelID           TEXT NOT NULL,
		messageID           TEXT NOT NULL,
		encryptedData       BLOB NOT NULL,
		
		PRIMARY KEY (guildID, channelID, messageID),
		--//TODO: do we really want to delete embeds if a channel gets deleted? could be annoying...
		FOREIGN KEY (guildID, channelID) REFERENCES channels(guildID, channelID) ON DELETE CASCADE
	);`

	SQLCreateTableEmbedsSettings string = `
	CREATE TABLE IF NOT EXISTS embedsSettings(
		guildID        TEXT NOT NULL PRIMARY KEY REFERENCES guilds(guildID) ON DELETE CASCADE,
		defaultColor   INT  DEFAULT 0
	);`
)
