package embeds

import (
	"strconv"
	"strings"

	dg "github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
	"gitlab.com/teterski-softworks/godbot/db"
	"gitlab.com/teterski-softworks/godbot/states"
)

//TODO
var slashCommandEmbedSettings = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionSubCommand,
	Name:        "settings",
	Description: "Commands for setting default embed values.",
	Options: []*dg.ApplicationCommandOption{
		{
			Type:        dg.ApplicationCommandOptionString,
			Name:        "color",
			Description: "Sets the default accent color used. Hexadecimal color in the form #FFFFFF.",
		},
	},
}

//TODO
func slashEmbedSettings(intr states.Interaction, options []*dg.ApplicationCommandInteractionDataOption) error {

	settings, err := RetrieveSettings(intr.DB, intr.GuildID)
	if err != nil {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}

	if len(options) != 0 {
		color := strings.ReplaceAll(options[0].StringValue(), "#", "")
		colorInt, err := strconv.ParseInt(color, 16, 32)
		if err != nil {
			return intr.EditResponseEmbeds(errors.WithStack(err), ResponseInvalidColorCode())
		}
		settings.DefaultColor = int(colorInt)

		if err := settings.Store(intr.DB); err != nil {
			return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
		}
	}

	return intr.EditResponseEmbeds(err, settings.Embed())
}
