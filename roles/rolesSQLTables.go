package roles

import "gitlab.com/teterski-softworks/godbot/db"

func CreateDatabaseTables(DB db.ReadWriter) error {
	t := DB.NewTransaction()
	t.Queue(SQLCreateTableRoles)
	return t.Commit()
}

var (
	SQLCreateTableRoles string = `
	CREATE TABLE IF NOT EXISTS roles(
		guildID   TEXT NOT NULL REFERENCES guilds(guildID) ON DELETE CASCADE,
		roleID    TEXT NOT NULL UNIQUE,

		PRIMARY KEY (guildID, roleID)
	);`
)
