package roles

var (
	SQLSelectRole string = `
	SELECT guildID, channelID FROM roles WHERE guildID=?1 AND roleID=?2;`

	SQLSelectGuildRoles string = `
	SELECT roleID FROM roles WHERE guildID=?1;`

	SQLInsertRole string = `
	INSERT OR IGNORE INTO roles(guildID, roleID) VALUES(?1, ?2);`

	SQLDeleteRole string = `
	DELETE FROM roles WHERE guildID=?1 AND roleID=?2;`
)
