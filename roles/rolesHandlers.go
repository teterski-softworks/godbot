package roles

import (
	dg "github.com/bwmarrin/discordgo"
	"gitlab.com/teterski-softworks/godbot/states"
)

//GuildRoleDeleteHandler deletes a role from the DB when it is deleted from a server.
func GuildRoleDeleteHandler(s *states.State, event *dg.GuildRoleDelete) error {
	if err := New(event.GuildID, event.RoleID).Delete(s.DB); err != nil {
		return err
	}
	return nil
}
