package roles

import (
	"database/sql"
	"fmt"

	"github.com/pkg/errors"
	"gitlab.com/teterski-softworks/godbot/db"
	"gitlab.com/teterski-softworks/godbot/guilds"
	"gitlab.com/teterski-softworks/godbot/states"
)

//Role represents a Discord role.
type Role struct {
	guilds.Guild
	RoleID string
}

//New returns a new Role for the given channelID and guild.
func New(guildID string, roleID string) Role {
	return Role{
		Guild:  guilds.Guild{GuildID: guildID},
		RoleID: roleID,
	}
}

//Retrieve returns a Role if it is in the DB.
func Retrieve(DB db.Reader, guildID string, channelID string) (role Role, err error) {

	row, err := DB.QueryRow(SQLSelectRole, guildID, channelID)
	if err != nil {
		return role, err
	}
	if err = row.Scan(&role.GuildID, &role.RoleID); err != nil {
		return role, err
	}

	return role, nil
}

//RetrieveAll returns a list of all Roles stored in the DB for a guild.
func RetrieveAll(DB db.Reader, guildID string) (roles []Role, err error) {
	rows, err := DB.Query(SQLSelectGuildRoles, guildID)
	if err != nil {
		return roles, err
	}

	role := Role{Guild: guilds.New(guildID)}
	for rows.Next() {
		if err := rows.Scan(&role.RoleID); err != nil {
			return roles, errors.WithStack(err)
		}
		roles = append(roles, role)
	}

	return roles, nil
}

//Store saves a Role to the DB.
func (r Role) Store(DB db.ReadWriter) error {
	t := DB.NewTransaction()
	if err := r.Guild.Store(t); err != nil {
		return err
	}
	t.Queue(SQLInsertRole, r.GuildID, r.RoleID)
	return t.Commit()
}

//Delete removes all Role related data from the DB.
func (r Role) Delete(DB db.ReadWriter) error {
	return DB.QueueAndCommit(SQLDeleteRole, r.GuildID, r.RoleID)
}

//Mention returns a mention string for the provided role ID.
func Mention(ID string) string {
	return fmt.Sprintf("<@&%s>", ID)
}

//RemoveDeletedRoles deletes roles from the DB which have been removed from the given guild.
func RemoveDeletedRoles(s *states.State, guildID string) error {
	dbRoles, err := RetrieveAll(s.DB, guildID)
	if err != nil {
		return err
	}
	if errors.Is(err, sql.ErrNoRows) {
		return nil
	}

	guildRoles, err := s.GuildRoles(guildID)
	if err != nil {
		return errors.WithStack(err)
	}

	for _, dbRole := range dbRoles {
		if dbRole.RoleID == "" {
			//Ignore blank roles. They are required to have empty role settings option.
			continue
		}

		match := false
		for _, guildRole := range guildRoles {
			if guildRole.ID == dbRole.RoleID {
				match = true
				break
			}
		}
		if !match {
			if err := dbRole.Delete(s.DB); err != nil {
				return err
			}
		}
	}
	return nil
}
