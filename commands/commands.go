package commands

import (
	"bytes"
	"crypto/md5"
	"encoding/gob"
	"encoding/hex"

	dg "github.com/bwmarrin/discordgo"
	"gitlab.com/teterski-softworks/godbot/states"
)

//TODO: encrypt

//Commander represents a command that can be used by an end user through the bot.
type Commander interface {
	Name() string
	Description() string
	ApplicationCommand() *dg.ApplicationCommand
	ApplicationCommandMD5Hash() string
	Execute(states.Interaction, ...*dg.ApplicationCommandInteractionDataOption) error
}

//CmdFunc is a function that is run by a Command.
type CmdFunc func(states.Interaction, ...*dg.ApplicationCommandInteractionDataOption) error

//Command is a command that can be used by a Console.
type Command struct {
	appCommand  *dg.ApplicationCommand
	description string
	function    CmdFunc
}

//New returns a new Commander
func New(applicationCommand *dg.ApplicationCommand, defaultPermissions bool, function CmdFunc, helpMenuDescription ...string) Commander {
	applicationCommand.DefaultPermission = &defaultPermissions
	command := Command{
		function:   function,
		appCommand: applicationCommand,
	}
	if len(helpMenuDescription) != 0 {
		command.description = helpMenuDescription[0]
	}
	return &command
}

//Name returns the name of the Command.
func (c Command) Name() string {
	return c.appCommand.Name
}

//Description returns a description of the Command.
func (c Command) Description() string {
	if c.description == "" {
		return c.appCommand.Description
	}
	return c.description
}

//ApplicationCommand returns the Discord application command of the Command.
func (c Command) ApplicationCommand() *dg.ApplicationCommand {
	return c.appCommand
}

//Get the MD5 hash of Command's ApplicationCommand.
func (c Command) ApplicationCommandMD5Hash() string {
	var buffer bytes.Buffer
	gob.NewEncoder(&buffer).Encode(*c.ApplicationCommand())
	gob.NewEncoder(&buffer).Encode(c.ApplicationCommand().Options)
	hashBytes := md5.Sum(buffer.Bytes())
	return hex.EncodeToString(hashBytes[:])
}

//Execute runs the Command's function.
func (c Command) Execute(intr states.Interaction, options ...*dg.ApplicationCommandInteractionDataOption) error {

	err := c.function(intr, options...)
	if err != nil {
		return err
	}

	return nil
}

/*
CalledCommandName returns the full name of the command called in interactionData
The returned array contains the following info:

0 - the command name.
1 - the subcommand group name (if any).
2 - the subcommand name (if any).
*/
func CalledCommandName(interactionData dg.ApplicationCommandInteractionData) [3]string {
	var names [3]string
	names[0] = interactionData.Name
	for _, option := range interactionData.Options {
		if option.Type != dg.ApplicationCommandOptionSubCommand && option.Type != dg.ApplicationCommandOptionSubCommandGroup {
			continue
		}
		names[2] = option.Name
		for _, option := range option.Options {
			if option.Type != dg.ApplicationCommandOptionSubCommand && option.Type != dg.ApplicationCommandOptionSubCommandGroup {
				continue
			}
			//The previously set name at [2] is a group name
			//We want to move it to its proper position.
			names[1] = names[2]
			names[2] = option.Name
			break
		}
		break
	}
	return names
}
