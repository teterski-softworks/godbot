package commands_test

import (
	"reflect"
	"strings"
	"testing"

	dg "github.com/bwmarrin/discordgo"
	"gitlab.com/teterski-softworks/godbot/commands"
	"gitlab.com/teterski-softworks/godbot/states"
)

//TODO: incomplete

func Test_Commander(t *testing.T) {
	var _ commands.Commander = new(commands.Command)
}

//TODO!: 81.8% coverage

func Test_New_Name_Description_ApplicationCommand_Execute(t *testing.T) {

	name := "Test name"
	description := "This is a test description"
	defaultPermissions := false

	success := false
	appCommand := dg.ApplicationCommand{
		Type:              dg.ChatApplicationCommand,
		Name:              strings.ToLower(name),
		Description:       description,
		DefaultPermission: &defaultPermissions,
		Version:           "0",
	}

	cmd := commands.New(&appCommand, false,
		func(_ states.Interaction, _ ...*dg.ApplicationCommandInteractionDataOption) error {
			success = true
			return nil
		},
	)

	if cmd.Name() != strings.ToLower(name) {
		t.Fatalf("Name returned wrong value. Expected: %s. Received; %s.", strings.ToLower(name), cmd.Name())
	}
	if cmd.Description() != description {
		t.Fatalf("Description returned wrong value. Expected: %s. Received; %s.", description, cmd.Description())
	}
	appCommandTest := cmd.ApplicationCommand()
	if reflect.DeepEqual(&appCommandTest, appCommand) {
		t.Fatalf("ApplicationCommand returned unexpected value.")
	}

	if cmd.Execute(states.Interaction{}); !success {
		t.Fatalf("Execute did not run.")
	}

}
