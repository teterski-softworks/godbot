package commands

const (
	SQLSelectCommand string = `
	SELECT commandID, hash, added, timesUsed
	FROM commands
	WHERE guildID=?1 AND name=?2;`

	SQLSelectSubCommands string = `
	SELECT name, groupName, timesUsed
	FROM subCommands
	WHERE commandID=?1;`

	SQLCommandInfoExist string = `
	SELECT COUNT(*)>0 FROM commands WHERE guildID=?1;`

	SQLUpsertCommand string = `
	INSERT INTO commands(commandID, name, guildID, hash, added, timesUsed)
	VALUES (?1, ?2, ?3, ?4, ?5, ?6) ON CONFLICT DO
	UPDATE SET

	name      =?2,
	guildID   =?3,
	hash      =?4,
	added     =?5,
	timesUsed =?6

	WHERE commandID=?1;`

	SQLUpsertSubCommand string = `
	INSERT INTO subCommands(commandID, name, groupName, timesUsed)
	VALUES (?1, ?2, ?3, ?4) ON CONFLICT DO
	UPDATE SET
	timesUsed =?4
	WHERE commandID=?1 AND name=?2 AND groupName=?3;`

	SQLDeleteCommand string = `
	DELETE FROM commands
	WHERE commandID=?1;`

	//TODO: rewrite
	SQLUpdateCommandTimesUsed string = `
	UPDATE commands
	SET timesUsed=timesUsed+1
	WHERE name=?1 AND guildID=?2;`

	//TODO: rewrite
	SQLUpdateSubCommandTimesUsed string = `
	UPDATE subCommands
	SET timesUsed = timesUsed+1
	WHERE commandID IN (
		SELECT commandID
		FROM commands
		WHERE name=?1 AND guildID=?4
	)
	AND groupName=?2 AND name=?3;`

	SQLDeleteNotAddedCommands string = `
	DELETE FROM commands
	WHERE added=FALSE;`

	SQLClearAllCommandsAddedStatus string = `
	UPDATE commands	SET added=FALSE;`

	SQLClearGuildCommandsHashes string = `
	UPDATE commands	SET hash="" WHERE guildID=?1;`
)
