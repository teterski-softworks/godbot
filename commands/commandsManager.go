package commands

import (
	"database/sql"
	"log"

	dg "github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
	"gitlab.com/teterski-softworks/godbot/db"
	"gitlab.com/teterski-softworks/godbot/states"
)

//TODO! change to interface
//Manager adds commands to guilds when they join, and updates command DB entries when the guilds disconnect the bot.
type Manager struct {
	session       *dg.Session
	log           *log.Logger
	applicationID string
	Commands      map[ /*Name*/ string]Commander
}

//NewManager returns a new Manager.
func NewManager(session *dg.Session, log *log.Logger, applicationID string, commandLists ...[]Commander) Manager {
	m := Manager{
		session:       session,
		log:           log,
		applicationID: applicationID,
		Commands:      map[string]Commander{},
	}

	for _, commandList := range commandLists {
		for _, cmd := range commandList {
			m.Commands[cmd.Name()] = cmd
		}
	}

	return m
}

//ClearAllCommandsAddedStatus marks all the commands in the DB as not added.
func (m Manager) ClearAllCommandsAddedStatus(DB db.ReadWriter) error {
	return DB.QueueAndCommit(SQLClearAllCommandsAddedStatus)
}

//ClearAllCommandsAddedStatus marks all the commands for guild in the DB as not added.
func (m Manager) ClearGuildCommandsHashes(DB db.ReadWriter, guildID string) error {
	return DB.QueueAndCommit(SQLClearGuildCommandsHashes, guildID)
}

//AddCommandsToGuild adds commands to a guild.
func (m Manager) AddCommandsToGuild(DB db.ReadWriter, session *dg.Session, guildID string) error {

	m.log.Printf("Adding application commands for G[%s].", guildID)

	//Check if guild had its commands removed (bot was removed and re-added while offline)
	if err := m.CheckIfCommandsHaveBeenRemoved(DB, session, guildID); err != nil {
		return err
	}

	//Add regular commands
	for _, cmd := range m.Commands {
		//Get the current unique hash of the command.
		//This is used to check if the command has changed since it was last added to the guild and DB.
		hashCurrent := cmd.ApplicationCommandMD5Hash()

		//Check if DB has an entry for the command.
		command, err := RetrieveCommandInfo(DB, guildID, cmd.Name())
		if errors.Is(err, sql.ErrNoRows) {
			//If no entry exists, add command to server. If command already on server overwrite it anyway
			//since without a hash, we can't be certain about how up to date the command on the server is.
			if err = m.addNewGuildCommand(DB, cmd, guildID); err != nil {
				return err
			}
			continue
		} else if err != nil {
			return err
		}

		if command.Hash != hashCurrent {
			//The command has been updated since the last time it was added.
			//We need to push the new version of the command.
			if err = m.addNewGuildCommand(DB, cmd, guildID); err != nil {
				return err
			}
			continue
		}

		//Mark the command as already added
		command.Added = true
		if err = command.Store(DB); err != nil {
			return err
		}

	}

	return nil
}

//CheckIfCommandsHaveBeenRemoved checks if the guild is missing previously added commands,
//and if so, clears the guild's commands' DB hashes, so that the commands are marked as "out of date" and may be re-added.
func (m Manager) CheckIfCommandsHaveBeenRemoved(DB db.ReadWriter, session *dg.Session, guildID string) error {

	m.log.Printf("Checking for missing commands in G[%s].", guildID)

	//Check if there are any command records for the guild at all.
	//If so, we know that commands for this guild previously existed.
	if commandInfoExist, err := CommandInfoExist(DB, guildID); err != nil {
		return err
	} else if !commandInfoExist {
		return nil
	}

	//Then check if any commands are present in the server.
	commandsInServer, err := session.ApplicationCommands(m.applicationID, guildID)
	if err != nil {
		return errors.WithStack(err)
	}
	//If NOT, we know that ALL GUILD COMMANDS need to be re-added.
	if len(commandsInServer) == 0 {
		m.log.Printf("Detected missing commands in G[%s].", guildID)
		//Clear hashes so we can distinguish these commands in AddCommandsToGuild and then re-add them as if the command was updated.
		return m.ClearGuildCommandsHashes(DB, guildID)
	}

	//If commands existed before AND after the bot offline,
	//then the commands must be the same as there is no way to update them during the offline period
	m.log.Printf("No missing commands in G[%s].", guildID)

	return nil
}

//addNewGuildCommand handles the addition of a command that does not exists on the server.
func (m Manager) addNewGuildCommand(DB db.ReadWriter, cmd Commander, guildID string) error {

	m.log.Printf("Adding CMD[%s] to G[%s].", cmd.Name(), guildID)

	//If command is not in DB, add it to both the guild and the DB
	addedCmd, err := m.session.ApplicationCommandCreate(m.applicationID, guildID, cmd.ApplicationCommand())
	if err != nil {
		return errors.WithStack(err)
	}

	commandInfo := NewCommandInfo(cmd, addedCmd.ID, guildID)
	commandInfo.Added = true
	if err = commandInfo.Store(DB); err != nil {
		return err
	}

	return nil
}

//DeleteNotAddedCommands deletes all previously added commands that are no longer part of the bot, both from guild's and from the DB.
func (m Manager) DeleteNotAddedCommands(DB db.ReadWriter, guilds []*dg.Guild) error {

	for _, guild := range guilds {

		m.log.Printf("Checking if extra commands need to be removed from G[%s].", guild.ID)

		//It is possible that we previously added commands to the guild that no longer exist.
		//We need to remove them from the guild.
		guildCommands, err := m.session.ApplicationCommands(m.applicationID, guild.ID)
		if err != nil {
			return err
		}
		noExtra := true
		for _, cmd := range guildCommands {

			//Remove the application command if we do not have a database record of it.
			if _, err := RetrieveCommandInfo(DB, guild.ID, cmd.Name); errors.Is(err, sql.ErrNoRows) {
				m.log.Printf("Removed extra CMD[%s] from G[%s].", cmd.Name, guild.ID)
				noExtra = false
				if err := m.session.ApplicationCommandDelete(m.applicationID, guild.ID, cmd.ID); err != nil {
					return err
				}
				continue
			} else if err != nil {
				return err
			}
		}
		if noExtra {
			m.log.Printf("No extra commands in G[%s].", guild.ID)
		}
	}
	//Remove any database entries of commands not added to any guild.
	return DB.QueueAndCommit(SQLDeleteNotAddedCommands)
}

//CommandHandler handles the use of application commands by an end user.
func (m Manager) CommandHandler(intr states.Interaction) error {
	commandNames := CalledCommandName(intr.ApplicationCommandData())
	options := intr.ApplicationCommandData().Options

	//Regular commands
	for _, cmd := range m.Commands {
		if cmd.Name() != commandNames[0] {
			continue
		}

		m.log.Printf("CMD%s used by U[%s] in C[%s] in G[%s].", commandNames, intr.Member.User.ID, intr.ChannelID, intr.GuildID)

		//Update usage stats for the command
		intr.DB.QueueAndCommit(SQLUpdateCommandTimesUsed, commandNames[0], intr.GuildID)
		intr.DB.QueueAndCommit(SQLUpdateSubCommandTimesUsed, commandNames[0], commandNames[1], commandNames[2], intr.GuildID)

		if err := cmd.Execute(intr, options...); err != nil {
			return err
		}
	}
	return nil
}
