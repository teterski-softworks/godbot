package commands

import (
	dg "github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
	"gitlab.com/teterski-softworks/godbot/db"
)

//CommandInfo is used to store command info in the DB.
type CommandInfo struct {
	CommandID string

	Name      string
	GuildID   string
	Hash      string
	Added     bool
	TimesUsed int

	SubCommands []SubCommandInfo
}

//NewCommandInfo returns a new CommandInfo struct for the given Commander.
func NewCommandInfo(commander Commander, commandID string, guildID string) CommandInfo {
	c := CommandInfo{
		Name:      commander.Name(),
		CommandID: commandID,
		GuildID:   guildID,
		Hash:      commander.ApplicationCommandMD5Hash(),
	}

	options := commander.ApplicationCommand().Options
	for _, commandGroup := range options {
		if commandGroup.Type == dg.ApplicationCommandOptionSubCommand {
			subCommand := SubCommandInfo{
				Name:      commandGroup.Name,
				CommandID: commandID,
			}
			c.SubCommands = append(c.SubCommands, subCommand)
			continue
		}

		for _, subCommand := range commandGroup.Options {
			if subCommand.Type == dg.ApplicationCommandOptionSubCommand {
				subCommand := SubCommandInfo{
					Name:      commandGroup.Name,
					GroupName: commandGroup.Name,
					CommandID: commandID,
				}
				c.SubCommands = append(c.SubCommands, subCommand)
			}
		}
	}

	return c
}

//CommandInfoExist returns true if the given guild has at least one CommandInfo entry in the DB.
func CommandInfoExist(DB db.ReadWriter, guildID string) (exists bool, err error) {
	row, err := DB.QueryRow(SQLCommandInfoExist, guildID)
	if err != nil {
		return false, err
	}
	if err := row.Scan(&exists); err != nil {
		return false, errors.WithStack(err)
	}

	return exists, err
}

//RetrieveCommandInfo returns the CommandInfo for the given command name and guild.
func RetrieveCommandInfo(DB db.Reader, guildID string, name string) (CommandInfo, error) {

	c := CommandInfo{
		GuildID: guildID,
		Name:    name,
	}
	row, err := DB.QueryRow(SQLSelectCommand, guildID, name)
	if err != nil {
		return c, err
	}
	if err := row.Scan(&c.CommandID, &c.Hash, &c.Added, &c.TimesUsed); err != nil {
		return c, errors.WithStack(err)
	}
	c.SubCommands, err = RetrieveSubCommands(DB, c.CommandID)
	if err != nil {
		return c, err
	}

	return c, nil
}

//Store saves the CommandInfo the DB.
func (c CommandInfo) Store(DB db.ReadWriter) error {
	t := DB.NewTransaction()

	t.Queue(SQLUpsertCommand,
		c.CommandID,
		c.Name,
		c.GuildID,
		c.Hash,
		c.Added,
		c.TimesUsed,
	)

	for _, subCommand := range c.SubCommands {
		subCommand.Store(t)
	}

	return t.Commit()
}

//TODO: do we need this? - No, doesn't look like it.
//Delete removes the CommandInfo from the DB.
func (c CommandInfo) Delete(DB db.ReadWriter) error {
	return DB.QueueAndCommit(SQLDeleteCommand, c.CommandID)
}

//SubCommandInfo is used to store subcommands in the DB.
type SubCommandInfo struct {
	CommandID string

	Name      string
	GroupName string
	TimesUsed int

	SubCommands []SubCommandInfo
}

//RetrieveSubCommands returns all SubCommandInfo for the given commandInfo.
func RetrieveSubCommands(DB db.Reader, commandID string) (commands []SubCommandInfo, err error) {

	rows, err := DB.Query(SQLSelectSubCommands, commandID)
	if err != nil {
		return commands, err
	}
	for rows.Next() {
		c := SubCommandInfo{CommandID: commandID}
		if err := rows.Scan(&c.Name, &c.GroupName, &c.TimesUsed); err != nil {
			return commands, errors.WithStack(err)
		}
		commands = append(commands, c)
	}
	return commands, nil
}

//Store saves the SubCommandInfo to the DB.
func (c SubCommandInfo) Store(DB db.ReadWriter) error {
	return DB.QueueAndCommit(SQLUpsertSubCommand,
		c.CommandID,
		c.Name,
		c.GroupName,
		c.TimesUsed,
	)
}
