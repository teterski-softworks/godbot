package commands

import "gitlab.com/teterski-softworks/godbot/db"

//CreateDatabaseTables creates the required database tables for the command package.
func CreateDatabaseTables(DB db.ReadWriter) error {
	t := DB.NewTransaction()
	t.Queue(SQLCreateTableCommands)
	t.Queue(SQLCreateTableSubCommands)
	return t.Commit()
}

var (
	SQLCreateTableCommands string = `
	CREATE TABLE IF NOT EXISTS commands(
		commandID   TEXT NOT NULL PRIMARY KEY,
		name        TEXT NOT NULL,
		guildID     TEXT NOT NULL REFERENCES guilds(guildID) ON DELETE CASCADE,
		hash        TEXT NOT NULL,
		added		BOOL DEFAULT FALSE,
		timesUsed   INT  DEFAULT 0,
		
		UNIQUE (commandID, name, guildID)
	);`

	SQLCreateTableSubCommands string = `
	CREATE TABLE IF NOT EXISTS subCommands(
		commandID   TEXT NOT NULL PRIMARY KEY REFERENCES commands(commandID) ON DELETE CASCADE,
		name        TEXT NOT NULL,
		groupName   TEXT DEFAULT "",
		timesUsed   INT DEFAULT 0,

		UNIQUE (commandID, groupName, name)
	);`
)
