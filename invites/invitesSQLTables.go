package invites

import "gitlab.com/teterski-softworks/godbot/db"

//CreateDatabaseTables creates the required database tables for the warnings package.
func CreateDatabaseTables(DB db.ReadWriter) error {
	t := DB.NewTransaction()
	t.Queue(SQLCreateTableInvites)
	t.Queue(SQLCreateTableInvitePeriods)
	t.Queue(SQLCreateTableInviteRecord)
	t.Queue(SQLCreateTableInviteSettings)
	return t.Commit()
}

var (
	SQLCreateTableInvites string = `
	CREATE TABLE IF NOT EXISTS invites(
		guildID     TEXT NOT NULL REFERENCES guilds(guildID) ON DELETE CASCADE,
		channelID   TEXT NOT NULL,
		code        TEXT NOT NULL,

		inviterID   TEXT NOT NULL,
		endTime     DATETIME NOT NULL,
		timerID     INT NOT NULL,

		PRIMARY KEY (guildID, channelID, code)
	);`

	SQLCreateTableInvitePeriods string = `
	CREATE TABLE IF NOT EXISTS invitePeriods(
		guildID        TEXT NOT NULL,
		memberID       TEXT NOT NULL,
		count          INT DEFAULT 0,
		periodic_end   DATETIME NOT NULL,

		PRIMARY KEY (guildID, memberID),
		FOREIGN KEY (guildID, memberID) REFERENCES members(guildID, memberID) ON DELETE CASCADE
	);`

	SQLCreateTableInviteRecord string = `
	CREATE TABLE IF NOT EXISTS inviteRecords(
		guildID       TEXT NOT NULL,
		memberID      TEXT NOT NULL,
		invitedByID   TEXT NOT NULL,

		PRIMARY KEY (guildID, memberID),
		FOREIGN KEY (guildID, memberID) REFERENCES members(guildID, memberID) ON DELETE CASCADE
	);`

	SQLCreateTableInviteSettings string = `
	CREATE TABLE IF NOT EXISTS inviteSettings(
		guildID             TEXT NOT NULL PRIMARY KEY REFERENCES guilds(guildID) ON DELETE CASCADE,
		channelID           TEXT NOT NULL,
		periodic_duration   TEXT DEFAULT "1w",
		maxAge              TEXT DEFAULT "1d",
		maxInvites          INT  DEFAULT 1,
		enabled             BOOL DEFAULT TRUE
	);`
)
