package invites

import (
	"database/sql"
	"time"

	"github.com/pkg/errors"
	"gitlab.com/teterski-softworks/godbot/db"
	"gitlab.com/teterski-softworks/godbot/members"
	"gitlab.com/teterski-softworks/godbot/states"
)

//Invite represents a Discord guild invite
type Invite struct {
	//Primary keys
	GuildID   string
	ChannelID string
	Code      string

	InviterID string //TODO?: Encrypt
	EndTime   time.Time
	timerID   int
}

//Invite returns a new Invite struct based on the information provided
func New(guildID string, channelID string, code string) Invite {
	var i Invite
	i.GuildID = guildID
	i.ChannelID = channelID
	i.Code = code
	return i
}

//Retrieve returns an Invite from the DB that matches the given parameters.
func Retrieve(DB db.Reader, guildID string, channelID string, code string) (Invite, error) {

	invite := New(guildID, channelID, code)

	row, err := DB.QueryRow(SQLSelectInvite, invite.GuildID, invite.ChannelID, invite.Code)
	if err != nil {
		return invite, err
	}

	if err = row.Scan(
		&invite.InviterID,
		&invite.EndTime,
		&invite.timerID,
	); err != nil {
		return invite, errors.WithStack(err)
	}

	return invite, nil
}

//RetrieveMemberInvites returns all saved invites made by a member for the guild.
func RetrieveMemberInvites(DB db.Reader, guildID string, memberID string) ([]Invite, error) {
	rows, err := DB.Query(SQLSelectMemberInvites, guildID, memberID)
	if err != nil {
		return nil, err
	}

	var invites []Invite
	for rows.Next() {

		var invite Invite
		invite.GuildID = guildID
		invite.InviterID = memberID

		if err = rows.Scan(
			&invite.ChannelID,
			&invite.Code,
			&invite.EndTime,
			&invite.timerID,
		); err != nil && !errors.Is(err, sql.ErrNoRows) {
			return nil, errors.WithStack(err)
		}

		invites = append(invites, invite)
	}
	return invites, nil
}

//RetrieveAll returns all Invites to the guild saved in the DB.
func RetrieveAll(DB db.Reader, guildID string) ([]Invite, error) {
	rows, err := DB.Query(SQLSelectGuildInvites, guildID)
	if err != nil {
		return nil, err
	}

	var invites []Invite
	for rows.Next() {

		var invite Invite
		invite.GuildID = guildID

		if err = rows.Scan(
			&invite.ChannelID,
			&invite.Code,
			&invite.InviterID,
			&invite.EndTime,
			&invite.timerID,
		); err != nil && !errors.Is(err, sql.ErrNoRows) {
			return nil, errors.WithStack(err)
		}
		invites = append(invites, invite)
	}
	return invites, nil
}

//Store saves an Invite to the DB.
func (i Invite) Store(DB db.ReadWriter) error {
	t := DB.NewTransaction()

	//Needed for settings.Period
	settings, err := RetrieveSettings(t, i.GuildID)
	if err != nil {
		return err
	}

	inviter := members.New(i.GuildID, i.InviterID)
	if err := inviter.Store(t); err != nil {
		return err
	}
	t.Queue(SQLUpsertInvite,
		i.GuildID,
		i.ChannelID,
		i.Code,
		i.InviterID,
		i.EndTime,
		i.timerID,
	)

	t.Queue(SQLUpsertInvitePeriod,
		i.GuildID,
		i.InviterID,
		time.Now().Add(settings.Period),
	)
	return t.Commit()
}

//Delete removes an Invite from the DB.
//If the invite is already not in the provided database, then found returns false. Otherwise, returns true.
func (i Invite) Delete(DB db.ReadWriter) (found bool, err error) {

	if _, err = Retrieve(DB, i.GuildID, i.ChannelID, i.Code); err != nil {
		return false, err
	}
	if err = DB.QueueAndCommit(SQLDeleteInvite, i.Code, i.GuildID, i.ChannelID); err != nil {
		return false, err
	}
	return true, nil
}

//Alarm is fired with an invite expires. This deletes it from the DB.
func (i Invite) Alarm(s *states.State, e chan<- error) {

	//Delete the associated timer
	s.TimerManagers.Get(i.GuildID).Delete(i.timerID)

	_, err := i.Delete(s.DB)
	e <- err
}

//RetrieveMemberPeriodInfo returns when the member's period ends and how many invites the user has made in the current period.
func RetrieveMemberPeriodInfo(DB db.Reader, guildID string, memberID string) (periodEndTime time.Time, count int, err error) {

	row, err := DB.QueryRow(SQLSelectMemberInvitePeriod, guildID, memberID)
	if err != nil {
		return time.Time{}, 0, err
	}
	if err = row.Scan(&count, &periodEndTime); errors.Is(err, sql.ErrNoRows) {
		return time.Now(), 0, nil
	} else if err != nil {
		return time.Time{}, 0, errors.WithStack(err)
	}

	return periodEndTime, count, nil
}

//ResetMemberPeriod resets the period for a specific member.
func ResetMemberPeriod(DB db.ReadWriter, guildID string, memberID string) error {
	return DB.QueueAndCommit(SQLDeleteInvitePeriod, guildID, memberID)
}

//ResetAllMemberPeriod resets the period for a all members.
func ResetAllMemberPeriod(DB db.ReadWriter, guildID string) error {
	return DB.QueueAndCommit(SQLDeleteAllInvitePeriod, guildID)
}

//GetInviteTimerIDs returns a list of timerIDs for every invite in the database.
func GetInviteTimerIDs(DB db.Reader, guildID string) ([]int, error) {
	rows, err := DB.Query(SQLSelectInvitesTimerIDs, guildID)
	if err != nil {
		return nil, err
	}
	var timerIDs []int
	for rows.Next() {
		var timerID int
		if err = rows.Scan(&timerID); err != nil && !errors.Is(err, sql.ErrNoRows) {
			return nil, errors.WithStack(err)
		}

		timerIDs = append(timerIDs, timerID)
	}
	return timerIDs, err
}

/*
InitializeTimers resumes invite timers.

Any errors that the timers encounter AFTER they are resumed are returned through the channel.
Other errors related to resuming the timers themselves are returned through the regular error.
*/
func InitializeTimers(s *states.State, guildID string) (chan error, error) {

	invites, err := RetrieveAll(s.DB, guildID)
	if err != nil {
		return nil, err
	}

	//We do not have any running timers.
	if len(invites) == 0 {
		return nil, nil
	}

	e := make(chan error, len(invites))
	for _, i := range invites {
		i.timerID = s.TimerManagers.Get(guildID).New(time.Until(i.EndTime), s, i.Alarm, e)
		if err := i.Store(s.DB); err != nil {
			s.TimerManagers.Get(guildID).Delete(i.timerID)
			return e, err
		}
	}
	return e, nil
}
