package invites

import (
	"fmt"
	"time"

	dg "github.com/bwmarrin/discordgo"
	"gitlab.com/teterski-softworks/godbot/embeds"
)

var (
	ResponseInviteCreated = func(inviteCode string) *dg.MessageEmbed {
		embed := embeds.New("", "", "")
		embed.Title = "✅ Invite Created"
		embed.Color = embeds.ColorGreen
		embed.Description = fmt.Sprintf("A new one time use invite has been created:\n\n**https://discord.gg/%s**", inviteCode)
		return &embed.MessageEmbed
	}

	ResponseInviteAlreadyExists = func(inviteCode string) *dg.MessageEmbed {
		embed := embeds.New("", "", "")
		embed.Title = "❎ Invite Already Exists"
		embed.Color = embeds.ColorYellow
		embed.Description = fmt.Sprintf("An unused invite already exists:\n\n**https://discord.gg/%s**", inviteCode)
		return &embed.MessageEmbed
	}

	ResponseOutOfInvites = func(nextInviteTime time.Time) *dg.MessageEmbed {
		embed := embeds.New("", "", "")
		embed.Title = "❌ Out Of Invites"
		embed.Color = embeds.ColorRed
		embed.Description = fmt.Sprintf("You have used up your allowed invites for the time being.\n\nAnother invite can be made <t:%[1]d:R> on <t:%[1]d>.", nextInviteTime.Unix())
		return &embed.MessageEmbed
	}

	ResponseInvitesDisabled = func() *dg.MessageEmbed {
		embed := embeds.New("", "", "")
		embed.Title = "❌ Invites Disabled"
		embed.Color = embeds.ColorRed
		embed.Description = "Sorry, but invite creation is currently disabled."
		return &embed.MessageEmbed
	}
)
