package invites

var (
	SQLSelectGuildInvites string = `
	SELECT
	channelID,
	code,
	inviterID,
	endTime,
	timerID
	FROM invites
	WHERE guildID=(?1);`

	SQLSelectMemberInvites string = `
	SELECT
	channelID,
	code,
	endTime,
	timerID
	FROM invites
	WHERE guildID=(?1) AND inviterID=(?2);`

	SQLSelectInvitesTimerIDs string = `
	SELECT timerID
	FROM invites
	WHERE guildID=(?1);`

	SQLSelectInvite string = `
	SELECT inviterID, endTime, timerID
	FROM invites
	WHERE
	guildID=(?1) AND 
	channelID=(?2) AND
	code=(?3);`

	SQLSelectMemberInvitePeriod string = `
	SELECT count, periodic_end
	FROM invitePeriods
	WHERE guildID=(?1) AND memberID=(?2);`

	SQLResetMemberPeriodicInviteCount string = `
	UPDATE invitePeriods
	SET count = 0
	WHERE guildID=(?1) AND memberID=(?2);`

	SQLSelectInviteSettings string = `
	SELECT
		channelID,
		periodic_duration,
		maxAge,
		maxInvites,
		enabled
	FROM inviteSettings
	WHERE guildID=(?1);`

	SQLSelectInviteRecord string = `
	SELECT invitedByID
	FROM inviteRecords
	WHERE guildID=(?1) AND memberID=(?2);`

	SQLUpsertInvite string = `
	INSERT INTO invites(
		guildID,
		channelID,
		code,
		inviterID,
		endTime,
		timerID
	) VALUES (?1, ?2, ?3, ?4, ?5, ?6)
	ON CONFLICT (guildID, channelID, code) DO
	UPDATE SET timerID=(?6)
	WHERE guildID=(?1) AND channelID=(?2) AND code=(?3);`

	SQLUpsertInviteRecord string = `
	INSERT INTO inviteRecords(
		guildID,
		memberID,
		invitedByID
	) VALUES (?1, ?2, ?3)
	ON CONFLICT (guildID, memberID) DO
	UPDATE SET invitedByID=(?3)
	WHERE guildID=(?1) AND memberID=(?2);`

	SQLUpsertInviteSettings string = `
	INSERT INTO inviteSettings(
		guildID,
		channelID,
		periodic_duration,
		maxAge,
		maxInvites,
		enabled
	) VALUES (?1, ?2, ?3, ?4, ?5, ?6)
	ON CONFLICT (guildID) DO
	UPDATE SET
		channelID         = (?2),
		periodic_duration = (?3),
		maxAge            = (?4),
		maxInvites        = (?5),
		enabled           = (?6)
	WHERE guildID=(?1);`

	SQLUpsertInvitePeriod string = `
	INSERT INTO invitePeriods(
		guildID,
		memberID,
		count,
		periodic_end
	)
	VALUES (?1, ?2, 1, ?3)
	ON CONFLICT(guildID, memberID) DO
	UPDATE SET

	count = count+1

	WHERE guildID=(?1) AND memberID=(?2);`

	SQLDeleteInvitePeriod string = `
	DELETE FROM invitePeriods WHERE
	guildID  =(?1) AND
	memberID =(?2);`

	SQLDeleteAllInvitePeriod string = `
	DELETE FROM invitePeriods
	WHERE guildID=(?1);`

	SQLDeleteInvite string = `
	DELETE FROM invites	WHERE
	code      =(?1) AND 
	guildID   =(?2) AND 
	channelID =(?3);`
)
