package invites

import (
	"database/sql"
	"time"

	"github.com/pkg/errors"
	"gitlab.com/teterski-softworks/godbot/db"
	"gitlab.com/teterski-softworks/godbot/durations"
	"gitlab.com/teterski-softworks/godbot/guilds"
)

//InviteSettings represents a collection of settings for creating invites to a guild.
type InviteSettings struct {
	GuildID    string
	ChannelID  string
	Period     time.Duration
	MaxAge     time.Duration
	MaxInvites int
	Enabled    bool
}

//NewSettings returns a set of new default invite settings for a guild.
func NewSettings(guildID string) InviteSettings {
	return InviteSettings{
		GuildID:    guildID,
		ChannelID:  "",
		Period:     time.Hour * 24 * 7,
		MaxAge:     time.Hour * 24,
		MaxInvites: 1,
		Enabled:    true,
	}
}

//RetrieveSettings returns saved invite settings for a guild from the DB.
func RetrieveSettings(DB db.Reader, guildID string) (InviteSettings, error) {

	settings := NewSettings(guildID)
	row, err := DB.QueryRow(SQLSelectInviteSettings, guildID)
	if err != nil {
		return settings, err
	}
	var period, maxAge string
	if err = row.Scan(
		&settings.ChannelID,
		&period,
		&maxAge,
		&settings.MaxInvites,
		&settings.Enabled,
	); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return settings, nil
		}
		return settings, errors.WithStack(err)
	}
	settings.Period, err = durations.Parse(period)
	if err != nil {
		return settings, err
	}
	settings.MaxAge, err = durations.Parse(maxAge)
	if err != nil {
		return settings, err
	}

	return settings, nil

}

//Store saves InviteSettings to the DB.
func (s InviteSettings) Store(DB db.ReadWriter) error {

	t := DB.NewTransaction()

	if err := guilds.New(s.GuildID).Store(t); err != nil {
		return err
	}

	t.Queue(SQLUpsertInviteSettings,
		s.GuildID,
		s.ChannelID,
		durations.String(s.Period),
		durations.String(s.MaxAge),
		s.MaxInvites,
		s.Enabled,
	)

	return t.Commit()
}
