package invites

import (
	"database/sql"
	"errors"

	dg "github.com/bwmarrin/discordgo"
	"gitlab.com/teterski-softworks/godbot/members"
	"gitlab.com/teterski-softworks/godbot/states"
)

//GuildMemberAddHandler handles incoming members to the guild and tries to find out what invite they might have used to get in.
func GuildMemberAddHandler(s *states.State, event *dg.GuildMemberAdd) error {

	trackedInvites, err := RetrieveAll(s.DB, event.GuildID)
	if err != nil {
		return err
	}

	guildInvites, err := s.GuildInvites(event.GuildID)
	if err != nil {
		return err
	}

	var inviter string
	for _, trackedInvite := range trackedInvites {

		//Check if the tracked invite is still active on the server. (this assumes its a one time invite...)
		found := false
		for _, invite := range guildInvites {
			if trackedInvite.Code == invite.Code {
				found = true
				break
			}
		}
		if found {
			//The invite has not been used, so this is not the invite we are looking for
			continue
		}

		//We have found another potentially used invite before
		//this means there are multiple possible invites the user could have used
		//so we clear the inviter (since we do not know which of the invites is the correct one)
		//and stop the search.
		if inviter != "" {
			inviter = ""
			break
		}

		//The invite is no longer active on the server, so its a potential match for it have been used to invite the member.
		inviter = trackedInvite.InviterID
	}

	//Save member record only if inviter is known
	if inviter != "" {
		inviteRecord := InviteRecord{
			Member:      members.New(event.GuildID, event.User.ID),
			InvitedByID: inviter,
		}
		return inviteRecord.Store(s.DB)
	}

	return nil
}

//InviteDeleteHandler handles invites being deleting in the guild, and removes them from the DB.
func InviteDeleteHandler(s *states.State, event *dg.InviteDelete) error {

	invite, err := Retrieve(s.DB, event.GuildID, event.ChannelID, event.Code)
	if !errors.Is(err, sql.ErrNoRows) && err != nil {
		return err
	}

	if _, err = invite.Delete(s.DB); err != nil {
		return err
	}

	//Delete the associated timer
	s.TimerManagers.Get(event.GuildID).Delete(invite.timerID)

	return nil
}
