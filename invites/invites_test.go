package invites_test

import (
	"testing"
	"time"

	"gitlab.com/teterski-softworks/godbot/db"
	"gitlab.com/teterski-softworks/godbot/invites"
	"gitlab.com/teterski-softworks/godbot/members"
	"gitlab.com/teterski-softworks/godbot/tests"
)

var guildID, channelID, code = "guildID", "channelID", "codeID"

var createTables = []func(db.ReadWriter) error{members.CreateDatabaseTables, invites.CreateDatabaseTables}

//TODO
func TestDatabaseAccess(t *testing.T) {

	expected := invites.New(guildID, channelID, code)

	updated := expected
	updated.GuildID = "newGuildID"
	updated.ChannelID = "newChannelID"
	updated.InviterID = "newInviterID"
	updated.Code = "newCode"
	updated.EndTime = time.Now().Add(3 * time.Hour).Truncate(time.Second).UTC()

	flags := tests.DefaultTestDatabaseAccessFlags
	flags.StoreEdit = false
	flags.RetrieveEdited = false

	retrieve := func(DB db.Reader) (invites.Invite, error) {
		return invites.Retrieve(DB, guildID, channelID, code)
	}

	tests.TestDatabaseAccess(
		t,
		expected, updated,
		createTables,
		retrieve,
		flags,
	)

}
