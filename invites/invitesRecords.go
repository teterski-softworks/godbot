package invites

import (
	"database/sql"

	"github.com/pkg/errors"
	"gitlab.com/teterski-softworks/godbot/db"
	"gitlab.com/teterski-softworks/godbot/members"
)

//InviteRecord represents a record of who invited a member to a guild.
type InviteRecord struct {
	members.Member
	InvitedByID string
}

//RetrieveRecord returns a saved InviteRecord from the DB.
func RetrieveRecord(DB db.Reader, guildID string, memberID string) (InviteRecord, error) {
	var record InviteRecord
	record.GuildID = guildID
	record.MemberID = memberID

	row, err := DB.QueryRow(SQLSelectInviteRecord, guildID, memberID)
	if err != nil {
		return record, err
	}
	if err = row.Scan(&record.InvitedByID); errors.Is(err, sql.ErrNoRows) {
		record.InvitedByID = "" //TODO? Change this to smthng else?
	} else if err != nil {
		return record, errors.WithStack(err)
	}

	return record, nil
}

//Store saves a InviteRecord to the DB.
func (record InviteRecord) Store(DB db.ReadWriter) error {
	t := DB.NewTransaction()

	if err := record.Member.Store(t); err != nil {
		return err
	}
	t.Queue(SQLUpsertInviteRecord, record.GuildID, record.MemberID, record.InvitedByID)

	return t.Commit()
}
