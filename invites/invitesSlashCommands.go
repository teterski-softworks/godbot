package invites

import (
	"database/sql"
	"fmt"
	"strings"
	"time"

	dg "github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
	"gitlab.com/teterski-softworks/godbot/channels"
	"gitlab.com/teterski-softworks/godbot/commands"
	"gitlab.com/teterski-softworks/godbot/db"
	"gitlab.com/teterski-softworks/godbot/durations"
	"gitlab.com/teterski-softworks/godbot/embeds"
	"gitlab.com/teterski-softworks/godbot/members"
	"gitlab.com/teterski-softworks/godbot/states"
)

//SlashCommandInvite - A command that allows members to create invites to the guild.
var SlashCommandInvite = commands.New(
	&dg.ApplicationCommand{
		Type:        dg.ChatApplicationCommand,
		Name:        "invite",
		Description: "Creates a one time use invite to the server.",
	},
	false,
	func(intr states.Interaction, options ...*dg.ApplicationCommandInteractionDataOption) (err error) {

		if err := intr.AcknowledgeEphemeral(); err != nil {
			return err
		}

		//Get current Invite settings
		settings, err := RetrieveSettings(intr.DB, intr.GuildID)
		if errors.Is(err, sql.ErrNoRows) || settings.ChannelID == "" {
			//If no settings are saved for this server, or no channelID has been saved, use the current channel.
			settings.ChannelID = intr.ChannelID
		} else if err != nil {
			return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
		}

		//Check if invite creation is enabled
		if !settings.Enabled {
			return intr.EditResponseEmbeds(nil, ResponseInvitesDisabled())
		}

		//Check if an invite already exists
		memberInvites, err := RetrieveMemberInvites(intr.DB, intr.GuildID, intr.Member.User.ID)
		if err != nil {
			return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
		}
		if len(memberInvites) != 0 {
			return intr.EditResponseEmbeds(nil, ResponseInviteAlreadyExists(memberInvites[0].Code))
		}

		//Check if user can generate a new invite
		periodEndTime, inviteCount, err := RetrieveMemberPeriodInfo(intr.DB, intr.GuildID, intr.Member.User.ID)
		if err != nil {
			return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
		}

		if inviteCount >= settings.MaxInvites {
			return intr.EditResponseEmbeds(nil, ResponseOutOfInvites(periodEndTime))
		}

		if periodEndTime.After(time.Now()) {
			//Reset the member's associated period info if the period has expired
			if err := ResetMemberPeriod(intr.DB, intr.GuildID, intr.Member.User.ID); err != nil {
				return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
			}
		}

		channelInvite := &dg.Invite{
			MaxAge:    int(settings.MaxAge.Seconds()),
			MaxUses:   1,
			Temporary: false,
		}

		channelInvite, err = intr.ChannelInviteCreate(settings.ChannelID, *channelInvite)
		if err != nil {
			return intr.EditResponseEmbeds(err, states.ResponseGenericError(err))
		}
		invite := Invite{
			GuildID:   intr.GuildID,
			ChannelID: settings.ChannelID,
			Code:      channelInvite.Code,
			InviterID: intr.Member.User.ID,
			EndTime:   time.Now().Add(time.Duration(channelInvite.MaxAge * int(time.Second))),
		}

		e := make(chan error, 1)
		defer close(e)
		invite.timerID = intr.TimerManagers.Get(invite.GuildID).New(time.Until(invite.EndTime), intr.State, invite.Alarm, e)
		if err := invite.Store(intr.DB); err != nil {
			intr.TimerManagers.Get(invite.GuildID).Stop(invite.timerID)
			cause := errors.Cause(err).Error()

			//If for whatever reason our previous checks for these failed, we still want to show a proper error message for it.
			if strings.HasPrefix(cause, "UNIQUE") { //An invite for this channel already exists
				return intr.EditResponseEmbeds(nil, ResponseInviteAlreadyExists(invite.Code))
			}

			return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
		}
		if err = intr.EditResponseEmbeds(nil, ResponseInviteCreated(channelInvite.Code)); err != nil {
			return err
		}
		return <-e
	},
)

//SlashCommandInvites - A command group for invite related commands.
var SlashCommandInvites = commands.New(
	&dg.ApplicationCommand{
		Type:        dg.ChatApplicationCommand,
		Name:        "invites",
		Description: "Invite related commands. Admin only.",
		Options: []*dg.ApplicationCommandOption{
			slashCommandInvitesSettings,
			slashCommandInvitesInviter,
		},
	},
	false,
	func(intr states.Interaction, options ...*dg.ApplicationCommandInteractionDataOption) (err error) {

		if err := intr.Acknowledge(); err != nil {
			return err
		}

		switch options[0].Name {
		case slashCommandInvitesSettings.Name:
			return slashInvitesSettings(intr, options[0].Options)
		case slashCommandInvitesInviter.Name:
			return slashInvitesInviter(intr, options[0].Options)

		}
		return intr.EditResponseEmbeds(err, states.ResponseGenericError(err))
	},
)

//slashCommandInvitesSettings - A subcommand to view and edit invite settings.
var slashCommandInvitesSettings = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionSubCommand,
	Name:        "settings",
	Description: "Sets and shows the settings for invite creation.",
	Options: []*dg.ApplicationCommandOption{
		{
			Type:        dg.ApplicationCommandOptionBoolean,
			Name:        "enabled",
			Description: "Whether or not invite creation is enabled.",
		}, {
			Type:        dg.ApplicationCommandOptionInteger,
			Name:        "max_invites",
			Description: "The max number of invites a user can create.",
			MinValue:    &minInvites,
		}, {
			Type:        dg.ApplicationCommandOptionInteger,
			Name:        "max_age",
			Description: "The maximum age of the created invites.",
			Choices: []*dg.ApplicationCommandOptionChoice{
				{Name: "30 minutes",
					Value: time.Minute * 30},
				{Name: "1 hour",
					Value: time.Hour},
				{Name: "6 hours",
					Value: time.Hour * 6},
				{Name: "12 hours",
					Value: time.Hour * 12},
				{Name: "1 day",
					Value: time.Hour * 24},
				{Name: "7 day",
					Value: time.Hour * 24 * 7},
			},
		}, {
			Type:         dg.ApplicationCommandOptionChannel,
			Name:         "channel",
			Description:  "The channel to which people will be invited to.",
			ChannelTypes: []dg.ChannelType{dg.ChannelTypeGuildText},
		}, {
			Type:        dg.ApplicationCommandOptionString,
			Name:        "period",
			Description: "The amount of time before a user can generate new invites.",
		},
	},
}
var minInvites float64 = 0

//slashInvitesSettings views and edits invite settings.
func slashInvitesSettings(intr states.Interaction, options []*dg.ApplicationCommandInteractionDataOption) error {

	embed := embeds.NewDefault(intr.DB, intr.GuildID)
	embed.Title = "🛂 Invite Settings ⚙️"
	embed.Timestamp = time.Now().Format(time.RFC3339)

	settings, err := RetrieveSettings(intr.DB, intr.GuildID)
	if !errors.Is(err, sql.ErrNoRows) && err != nil {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}

	for _, option := range options {
		switch option.Name {
		case "max_age":
			settings.MaxAge = time.Duration(option.IntValue())
		case "max_invites":
			settings.MaxInvites = int(option.IntValue())
		case "channel":
			settings.ChannelID = option.ChannelValue(intr.Session).ID
		case "period":
			period, err := durations.Parse(option.StringValue())
			if err != nil {
				return intr.EditResponseEmbeds(nil, durations.ResponseInvalidDurationError())
			}
			//Reset timers on all member records - we do this in case timer was previously set way too high.
			if err := ResetAllMemberPeriod(intr.DB, intr.GuildID); err != nil {
				return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
			}
			timerIDs, err := GetInviteTimerIDs(intr.DB, intr.GuildID)
			if err != nil {
				return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
			}
			for _, timerID := range timerIDs {
				intr.TimerManagers.Get(intr.GuildID).Reset(timerID, period)
			}
			settings.Period = period
		case "enabled":
			settings.Enabled = option.BoolValue()
		}
	}

	if err := settings.Store(intr.DB); err != nil {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}

	enabled := &dg.MessageEmbedField{
		Name:  "❌ Status:",
		Value: "Invites are disabled.",
	}
	if settings.Enabled {
		enabled.Name = "✅ Status:"
		enabled.Value = "Invites are enabled."
	}
	channel := &dg.MessageEmbedField{
		Name:  "#️⃣ Invite To Channel:",
		Value: "New members are invited to `no channel set`.",
	}
	if settings.ChannelID != "" {
		channel.Value = fmt.Sprintf("New members are invited to %s.", channels.Mention(settings.ChannelID))
	}
	maxAge := &dg.MessageEmbedField{
		Name:  "🕒 Max Invite Age:",
		Value: fmt.Sprintf("Invites expire after `%s`.", settings.MaxAge),
	}
	maxInvites := &dg.MessageEmbedField{
		Name:  "🔢 Max Invites Per Period:",
		Value: fmt.Sprintf("Members can generate `%d` invited per period.", settings.MaxInvites),
	}
	period := &dg.MessageEmbedField{
		Name:  "🔄 Period Duration:",
		Value: fmt.Sprintf("Period resets every `%s` per member.\nChanging this resets the period for all members.", durations.String(settings.Period)),
	}
	embed.Fields = append(embed.Fields, enabled, channel, maxAge, maxInvites, period)

	return errors.WithStack(intr.EditResponseEmbeds(nil, &embed.MessageEmbed))
}

//slashCommandInvitesInviter - A subcommand to view who invited a member to the guild.
var slashCommandInvitesInviter = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionSubCommand,
	Name:        "inviter",
	Description: "Shows who invited a particular server member.",
	Options: []*dg.ApplicationCommandOption{
		{
			Type:        dg.ApplicationCommandOptionUser,
			Name:        "member",
			Description: "The member in question",
			Required:    true,
		},
	},
}

//slashInvitesInviter shows who invited a member to a guild, if known.
func slashInvitesInviter(intr states.Interaction, options []*dg.ApplicationCommandInteractionDataOption) error {

	member := options[0].UserValue(intr.Session)
	record, err := RetrieveRecord(intr.DB, intr.GuildID, member.ID)
	if err != nil {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}

	embed := embeds.NewDefault(intr.DB, intr.GuildID)
	embed.Timestamp = time.Now().Format(time.RFC3339)
	embed.Title = "🛂 Invite Information"
	embed.Footer.Text = "This is only an educated guess.\nDiscord does not make invite tracking easy."
	if record.InvitedByID == "" {
		embed.Description = fmt.Sprintf("%s was invited by %s.", member.Mention(), "`Unknown`")
	} else {
		embed.Description = fmt.Sprintf("%s was invited by %s.", member.Mention(), members.Mention(record.InvitedByID))
	}

	return intr.EditResponseEmbeds(nil, &embed.MessageEmbed)
}
