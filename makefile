BUILDPATH=$(CURDIR)
GO = $(shell which go)
GOINSTALl = $(GO) install
GOCLEAN = $(GO) clean

SOURCE = main.go
EXENAME = bot
DATABASE = ./data.db
LOG = ./log.log

APPID = 123456789012345678
DBKEY = 12345678901234567890123456789012
BOTTOKEN = PlaceYourDevTokenHereDoNotUseProductionToken

clean:
	rm -f $(BUILDPATH)/$(EXENAME) $(DATABASE) $(LOG)

run:
	env APPID=$(APPID) BOTTOKEN=$(BOTTOKEN) DBKEY=$(DBKEY) GOOS=linux GOARCH=amd64 go run $(SOURCE) $(DATABASE) $(LOG)

build:
	env GOOS=linux GOARCH=amd64 go build -o $(BUILDPATH)/$(EXENAME) $(SOURCE)