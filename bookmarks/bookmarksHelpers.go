package bookmarks

import (
	"strings"

	dg "github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
	"gitlab.com/teterski-softworks/godbot/messages"
	"gitlab.com/teterski-softworks/godbot/states"
)

//bookmarkHelper sends an embed of the message to the member's DMs.
func bookmarkHelper(session *dg.Session, member *dg.Member, message *dg.Message, guildID string, respondWithLink bool) (responseEmbeds *dg.MessageEmbed, err error) {

	guild, err := session.Guild(guildID)
	if err != nil {
		return states.ResponseGenericError(err), errors.WithStack(err)
	}

	author, _ := session.GuildMember(guild.ID, message.Author.ID)
	//Ignore error, author member object can be null, if author no longer member of guild

	bookmark := New(*guild, *message, *member, author)
	st, err := bookmark.Send(session)
	if err != nil {
		cause := errors.Cause(err).Error()
		if strings.HasPrefix(cause, "HTTP 403 Forbidden") {
			return ResponseBookmarksEnableDMs(), nil
		}
		return states.ResponseGenericError(err), errors.WithStack(err)
	}
	if respondWithLink {
		message := messages.NewFromReference(st.Reference())
		message.GuildID = guildID
		return ResponseBookmarkSaved(message.Link()), nil
	}
	return nil, nil

}
