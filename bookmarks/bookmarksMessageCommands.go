package bookmarks

import (
	dg "github.com/bwmarrin/discordgo"
	"gitlab.com/teterski-softworks/godbot/commands"
	"gitlab.com/teterski-softworks/godbot/states"
)

//MessageCommandBookmark - sends a bookmark of a message to a member's DMs.
var MessageCommandBookmark = commands.New(
	&dg.ApplicationCommand{
		Type: dg.MessageApplicationCommand,
		Name: "Bookmark 🔖",
	},
	true,
	func(intr states.Interaction, _ ...*dg.ApplicationCommandInteractionDataOption) error {
		commandData := intr.ApplicationCommandData()
		if err := intr.AcknowledgeEphemeral(); err != nil {
			return err
		}

		message := commandData.Resolved.Messages[commandData.TargetID]
		responseEmbeds, err := bookmarkHelper(intr.Session, intr.Member, message, intr.GuildID, true)
		return intr.EditResponseEmbeds(err, responseEmbeds)
	},
	"You can bookmark messages by reacting to a message with the 🔖 emoji"+
		" or by left clicking on the message and going to `Apps > Bookmark 🔖`."+
		" Make sure your privacy settings allow DMs from this server!",
)
