package bookmarks

import (
	"fmt"

	dg "github.com/bwmarrin/discordgo"
	"gitlab.com/teterski-softworks/godbot/embeds"
)

var (
	ResponseBookmarkSaved = func(messageLink string) *dg.MessageEmbed {
		embed := embeds.New("", "", "")
		embed.Title = "✅ Bookmark Saved 🔖"
		embed.Color = embeds.ColorGreen
		embed.Description = fmt.Sprintf("Your bookmark has been saved in your DMs. [Link](%s)", messageLink)
		return &embed.MessageEmbed
	}

	ResponseBookmarksEnableDMs = func() *dg.MessageEmbed {
		embed := embeds.New("", "", "")
		embed.Title = "⛔ DMs Disabled"
		embed.Color = embeds.ColorRed
		embed.Description = "To save bookmarks, please enable DMs from server members in your privacy settings."
		return &embed.MessageEmbed
	}
)
