package bookmarks

import (
	"fmt"
	"time"

	dg "github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
	"gitlab.com/teterski-softworks/godbot/states"
)

//MessageReactionAddHandler handles bookmarks created through message reactions.
func MessageReactionAddHandler(s *states.State, event *dg.MessageReactionAdd) error {
	message, err := s.ChannelMessage(event.ChannelID, event.MessageID)
	if err != nil {
		_, err := s.SendEmbeds(event.ChannelID, nil, errors.WithStack(err), states.ResponseGenericError(err))
		return err
	}

	responseEmbed, err := bookmarkHelper(s.Session, event.Member, message, event.GuildID, false)
	if responseEmbed == nil && err == nil {
		//Do not send any messages if the bookmark has been saved successfully.
		return nil
	}

	responseEmbed.Description += fmt.Sprintf("\n\nThis message will be deleted <t:%d:R>.", time.Now().Add(time.Minute*2).Unix())

	//Send the error responseEmbed.
	data := &dg.MessageSend{
		Content: event.Member.Mention(),
		Embeds:  []*dg.MessageEmbed{responseEmbed},
	}
	st, err := s.ChannelMessageSendComplex(event.ChannelID, data)
	if err != nil {
		return errors.WithStack(err)
	}

	//TODO: use some sort of temp message abstraction for this. could be very helpful for other things.
	//Delete the response after some time.
	time.AfterFunc(time.Minute*2, func() {
		s.ChannelMessageDelete(st.ChannelID, st.ID)
	})

	return err
}
