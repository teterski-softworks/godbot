package bookmarks

import (
	"fmt"
	"strings"
	"time"

	dg "github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
	"gitlab.com/teterski-softworks/godbot/embeds"
)

const CustomIDDeleteBookmark string = "deleteBookmark"

//Bookmark represents an embedded bookmark message.
type Bookmark struct {
	guild   dg.Guild
	message dg.Message
	member  dg.Member
	author  *dg.Member

	embeds struct {
		message *dg.MessageEmbed
		images  []*dg.MessageEmbed
		embeds  []*dg.MessageEmbed
	}
}

//Bookmark returns a new bookmark pointing to the message provided.
//guild - the guild the message is from
//message - the message the bookmark is of
//memberID - the ID of the member who requested the bookmark.
//author - the full member object of the message's author, if available, otherwise null.
func New(guild dg.Guild, message dg.Message, member dg.Member, author *dg.Member) Bookmark {
	bookmark := Bookmark{
		guild:   guild,
		message: message,
		member:  member,
		author:  author,
	}
	bookmark.bookmarkAttachments()
	bookmark.bookmarkMessage()
	bookmark.bookmarkEmbeds()
	return bookmark
}

//template returns the embed template used for all bookmark embeds.
func (b Bookmark) template() *dg.MessageEmbed {

	name := b.message.Author.String()
	if b.author != nil {
		if b.author.Nick != "" {
			name = b.author.Nick + " [" + name + "]"
		}
	}

	template := embeds.New(b.guild.ID, b.message.ChannelID, "")
	template.URL = b.Link()
	template.Author.Name = name
	template.Author.IconURL = b.message.Author.AvatarURL("")

	template.Footer.Text = b.guild.Name
	template.Timestamp = b.message.Timestamp.Format(time.RFC3339)
	template.Footer.IconURL = b.guild.IconURL()
	template.Footer.ProxyIconURL = b.guild.IconURL()
	return &template.MessageEmbed
}

//bookmarkMessage generates information on the bookmark's corresponding message.
func (b *Bookmark) bookmarkMessage() {

	b.embeds.message = b.template()

	//Embed the message text with a link to the original message.
	if b.message.Content != "" {
		var messageField dg.MessageEmbedField
		messageField.Name = "Message"

		//Either display the whole message if it is shorter than the max allowed length of 1024 or trim it.
		lengthLimit := 995
		if len(b.message.Content) > lengthLimit {
			messageField.Value = b.message.Content[:lengthLimit] + "\n**...\n[Message is too long]**"
		} else {
			messageField.Value = b.message.Content
		}
		b.embeds.message.Fields = append(b.embeds.message.Fields, &messageField)
	}

	//Embed the first sticker in the message as the bookmark's thumbnail
	if len(b.message.StickerItems) != 0 {
		sticker := fmt.Sprintf("https://media.discordapp.net/stickers/%s.webp", b.message.StickerItems[0].ID)
		b.embeds.message.Thumbnail.URL = sticker
	}

	if len(b.message.Attachments) != 0 {

		var attachmentsField dg.MessageEmbedField
		attachmentsField.Name = "Attachments"

		var videoCount, imageCount, fileCount int
		for _, attachment := range b.message.Attachments {
			switch strings.Split(attachment.ContentType, "/")[0] {
			case "video":
				videoCount++
			case "image":
				imageCount++
			default:
				fileCount++
			}
		}
		if videoCount != 0 {
			attachmentsField.Value += fmt.Sprintf("%d Videos ", videoCount)
		}
		if imageCount != 0 {
			attachmentsField.Value += fmt.Sprintf("%d Images ", imageCount)
		}
		if fileCount != 0 {
			attachmentsField.Value += fmt.Sprintf("%d Files ", fileCount)
		}

		b.embeds.message.Fields = append(b.embeds.message.Fields, &attachmentsField)

		//We have to manually add an image to this embed (as opposed to the image embed)
		//if we want it to show the image on some mobile clients.
		b.embeds.message.Image = &dg.MessageEmbedImage{
			URL:      b.message.Attachments[0].URL,
			ProxyURL: b.message.Attachments[0].ProxyURL,
		}
	}
}

//bookmarkAttachments generates information on the bookmark's corresponding message's attachments.
func (b *Bookmark) bookmarkAttachments() {

	for i := 1; i < len(b.message.Attachments) && len(b.embeds.images) <= 4; i++ {

		if strings.HasPrefix(b.message.Attachments[i].ContentType, "image/") {

			imageEmbed := b.template()
			imageEmbed.Image = &dg.MessageEmbedImage{
				URL:      b.message.Attachments[i].URL,
				ProxyURL: b.message.Attachments[i].ProxyURL,
			}
			b.embeds.images = append(b.embeds.images, imageEmbed)
		}
	}
}

//bookmarkEmbeds generates information on the bookmark's corresponding message's embeds.
func (b *Bookmark) bookmarkEmbeds() {
	//Embed any message embeds
	b.embeds.embeds = append(b.embeds.embeds, b.message.Embeds...)
}

//messageEmbeds generates an embed of the bookmark.
func (b Bookmark) messageEmbeds() []*dg.MessageEmbed {
	var embeds []*dg.MessageEmbed

	if b.embeds.message != nil {
		embeds = append(embeds, b.embeds.message)
	}
	embeds = append(embeds, b.embeds.images...)
	embeds = append(embeds, b.embeds.embeds...)

	//Ignore any embeds past embed limit
	if len(embeds) > 10 {
		embeds = embeds[:10]
	}

	return embeds
}

//Link returns the URL of the bookmarks corresponding message.
func (b Bookmark) Link() string {
	return fmt.Sprintf("https://discord.com/channels/%s/%s/%s", b.guild.ID, b.message.ChannelID, b.message.ID)
}

//Send directly messages the bookmark to the member who requested the bookmark.
func (b Bookmark) Send(session *dg.Session) (st *dg.Message, err error) {

	//Connect to the member's DMs
	channel, err := session.UserChannelCreate(b.member.User.ID)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	//Send the embed-ed bookmarks to the user's DM.
	data := dg.MessageSend{
		Embeds: b.messageEmbeds(),
		Components: []dg.MessageComponent{
			b.newBookmarkOptionMenu(),
		},
	}

	st, err = session.ChannelMessageSendComplex(channel.ID, &data)
	if err != nil {
		return nil, errors.WithStack(err)
	}
	return st, nil
}

//newBookmarkOptionMenu returns an ActionsRow with two Buttons:
func (b Bookmark) newBookmarkOptionMenu() dg.ActionsRow {

	var actionRow dg.ActionsRow
	linkButton := dg.Button{
		Label: "Link to message",
		Style: dg.LinkButton,
		URL:   b.Link(),
	}
	deleteButton := dg.Button{
		Label:    "Delete",
		Style:    dg.SecondaryButton,
		CustomID: CustomIDDeleteBookmark,
	}
	actionRow.Components = append(actionRow.Components, linkButton, deleteButton)
	return actionRow
}
