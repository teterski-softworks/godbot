package tests

import (
	"database/sql"
	"os"
	"reflect"
	"testing"

	"github.com/pkg/errors"
	"gitlab.com/teterski-softworks/godbot/db"
)

//TODO: comment
type DBaccessible interface {
	Store(db.ReadWriter) (err error)
	Delete(db.ReadWriter) (found bool, err error)
}

//TODO
type TestDatabaseAccessFlags struct {
	CloseDatabase,
	DeletedDatabase,
	RetrieveFail,
	StoreNew,
	RetrieveNew,
	StoreEdit,
	RetrieveEdited,
	Delete bool
}

var DefaultTestDatabaseAccessFlags = TestDatabaseAccessFlags{
	true, true, true, true, true, true, true, true,
}

//TODO: comment
func TestDatabaseAccess[Type any](
	t *testing.T,
	newValue DBaccessible,
	updatedValue DBaccessible,
	createDatabaseTables []func(db.ReadWriter) error,
	retrieve func(db.Reader) (Type, error),
	flags ...TestDatabaseAccessFlags,
) {

	flag := DefaultTestDatabaseAccessFlags
	if len(flags) != 0 {
		flag = flags[0] //TODO
	}

	DB := InitDatabase(t, createDatabaseTables)
	defer CloseDatabase(t, DB)

	if flag.CloseDatabase {
		t.Run("Closed Database", func(t *testing.T) {

			closedDB := ClosedDatabase(t)

			//Testing if retrieving on closed DB fails
			if _, err := retrieve(closedDB); err == nil {
				t.Fatalf(MissingError, "Retrieve()")
			}
			//Testing if storing on closed DB fails
			if err := newValue.Store(closedDB); err == nil {
				t.Fatalf(MissingError, "Store()")
			}

			//Testing if deleting from closed DB fails
			if _, err := newValue.Delete(closedDB); err == nil {
				t.Fatalf(MissingError, "Delete()")
			}
		})
	}

	if flag.DeletedDatabase {
		t.Run("Deleted Database", func(t *testing.T) {

			deletedDB := DeletedDatabase(t, createDatabaseTables)

			//TODO: is this the right error we are looking for?
			//Testing if retrieving on closed DB fails
			if _, err := retrieve(deletedDB); !errors.Is(err, sql.ErrNoRows) {
				t.Fatalf(MissingError, "Retrieve()")
			}
			//Testing if storing on closed DB fails
			if err := newValue.Store(deletedDB); err == nil {
				t.Fatalf(MissingError, "Store()")
			}

			//Testing if deleting from closed DB SUCCEEDS but returns no rows deleted!
			if deleted, err := newValue.Delete(deletedDB); !errors.Is(err, sql.ErrNoRows) {
				t.Fatalf(MissingError, "Delete()")
			} else if deleted {
				//deleted should be false, because no actual rows from the DB where deleted, since the DB does not exist
				t.Fatalf(WrongValue, "Delete()", false, deleted)
			}
		})
	}

	if flag.RetrieveFail {
		t.Run("Retrieve Fail", func(t *testing.T) {
			//Testing if retrieving a non existing entry fails
			if _, err := retrieve(DB); err == nil {
				t.Fatalf(MissingError, "Retrieve()")
			}
		})
	}

	if flag.StoreNew {
		//Testing if Store method INSERTS the existing entry.
		t.Run("Store New", func(t *testing.T) {
			if err := newValue.Store(DB); err != nil {
				t.Fatalf("%+v", err)
			}
		})
	}

	if flag.RetrieveNew {
		//Testing if Retrieve method SELECTS the existing entry.
		t.Run("Retrieve New", func(t *testing.T) {
			result, err := retrieve(DB)
			if err != nil {
				t.Fatalf("%+v", err)
			}

			if !reflect.DeepEqual(newValue, result) {
				t.Fatalf(WrongValue, "Retrieve()", newValue, result)
			}
		})
	}
	if flag.StoreEdit {
		//Testing if Store method UPDATES the existing entry.
		t.Run("Store Edit", func(t *testing.T) {
			if err := updatedValue.Store(DB); err != nil {
				t.Fatalf("%+v", err)
			}
		})
	}
	if flag.RetrieveEdited {
		t.Run("Retrieve Edited", func(t *testing.T) {

			result, err := retrieve(DB)
			if err != nil {
				t.Fatalf("%+v", err)
			}

			if !reflect.DeepEqual(updatedValue, result) {
				t.Fatalf(WrongValue, "Retrieve()", updatedValue, result)
			}
		})
	}
	if flag.Delete {
		//Testing if Delete method DELETES the existing entry.
		t.Run("Delete", func(t *testing.T) {

			found, err := newValue.Delete(DB)
			if err != nil {
				t.Fatalf("%+v", err)
			}
			if !found {
				t.Fatalf(NoRowsAffected, "Delete()")
			}

			if _, err := retrieve(DB); err == nil {
				t.Fatalf(MissingError, "Retrieve()")
			} else if errors.Cause(err).Error() != sql.ErrNoRows.Error() {
				t.Fatalf(WrongError, "Retrieve()", sql.ErrNoRows.Error(), errors.Cause(err).Error())
			}

			if !found {
				t.Fatalf(WrongValue, "Delete()", true, found)
			}

		})
	}
}

//TODO
func InitDatabase(t *testing.T, createDatabaseTables []func(db.ReadWriter) error) db.Database {
	var DBFilePath = t.TempDir() + "/db_test.db"
	var DBKey = "key"

	DB, err := db.Open(DBFilePath, DBKey)
	if err != nil {
		t.Fatalf("%+v", err)
	}

	t.Run("CreateDatabaseTables", func(t *testing.T) {
		for _, createTable := range createDatabaseTables {
			if err = createTable(DB); err != nil {
				t.Fatalf("%+v", err)
			}
		}
	})

	return DB
}

func ClosedDatabase(t *testing.T) db.Database {
	var DBFilePath = t.TempDir() + "/db_test.db"
	var DBKey = "key"

	DB, err := db.Open(DBFilePath, DBKey)
	if err != nil {
		t.Fatalf("%+v", err)
	}
	CloseDatabase(t, DB)
	return DB
}

func DeletedDatabase(t *testing.T, createDatabaseTables []func(db.ReadWriter) error) db.Database {
	var DBFilePath = t.TempDir() + "/db_test.db"
	var DBKey = "key"

	DB, err := db.Open(DBFilePath, DBKey)
	if err != nil {
		t.Fatalf("%+v", err)
	}
	for _, createTable := range createDatabaseTables {
		if err = createTable(DB); err != nil {
			t.Fatalf("%+v", err)
		}
	}

	if err = os.Remove(DBFilePath); err != nil {
		t.Fatalf("%+v", err)
	}
	return DB
}

//TODO
func CloseDatabase(t *testing.T, DB db.Database) {
	if err := DB.Close(); err != nil {
		t.Fatalf("%+v", err)
	}
}
