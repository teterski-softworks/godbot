package tests

const (
	WrongValue     = "%s resulted in wrong value.\nExpected: \"%+v\",\nReceived: \"%+v\""
	MissingError   = "%s did not error out when expected."
	NoRowsAffected = "%s did not affect any DB rows."
	WrongError     = "%s resulted in wrong error. Expected: \"%s\", Received: \"%s\"."
)
