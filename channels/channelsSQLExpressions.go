package channels

var (
	SQLSelectChannel string = `
	SELECT guildID, channelID FROM channels WHERE guildID=?1 AND channelID=?2;`

	SQLSelectGuildChannels string = `
	SELECT channelID FROM channels WHERE guildID=?1;`

	SQLInsertChannel string = `
	INSERT OR IGNORE INTO channels(guildID, channelID) VALUES(?1, ?2);`

	SQLDeleteChannel string = `
	DELETE FROM channels WHERE guildID=?1 AND channelID=?2;`
)
