package channels

import (
	"database/sql"
	"fmt"

	dg "github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
	"gitlab.com/teterski-softworks/godbot/db"
	"gitlab.com/teterski-softworks/godbot/guilds"
	"gitlab.com/teterski-softworks/godbot/states"
)

//Channel represents a Discord Channel.
type Channel struct {
	guilds.Guild
	ChannelID string
}

//New returns a new Channel for the given channelID and guild.
func New(guildID string, channelID string) Channel {
	return Channel{
		Guild:     guilds.Guild{GuildID: guildID},
		ChannelID: channelID,
	}
}

//Retrieve returns a Channel if it is in the DB.
func Retrieve(DB db.Reader, guildID string, channelID string) (channel Channel, err error) {

	row, err := DB.QueryRow(SQLSelectChannel, guildID, channelID)
	if err != nil {
		return channel, err
	}
	if err = row.Scan(&channel.GuildID, &channel.ChannelID); err != nil {
		return channel, err
	}

	return channel, nil
}

//RetrieveAll returns a list of all Channels stored in the DB for a guild.
func RetrieveAll(DB db.Reader, guildID string) (channels []Channel, err error) {
	rows, err := DB.Query(SQLSelectGuildChannels, guildID)
	if err != nil {
		return channels, err
	}

	channel := Channel{Guild: guilds.New(guildID)}
	for rows.Next() {
		if err := rows.Scan(&channel.ChannelID); err != nil {
			return channels, errors.WithStack(err)
		}
		channels = append(channels, channel)
	}

	return channels, nil
}

//Store saves a Channel to the DB.
func (c Channel) Store(DB db.ReadWriter) error {
	t := DB.NewTransaction()
	if err := c.Guild.Store(t); err != nil {
		return err
	}
	t.Queue(SQLInsertChannel, c.GuildID, c.ChannelID)
	return t.Commit()
}

//Delete removes all Channel related data from the DB.
func (c Channel) Delete(DB db.ReadWriter) error {
	return DB.QueueAndCommit(SQLDeleteChannel, c.GuildID, c.ChannelID)
}

//Mention returns a mention string for the provided Channel ID.
func Mention(channelID string) string {
	return fmt.Sprintf("<#%s>", channelID)
}

//RemoveDeletedChannels deletes channels from the DB which have been removed from the given guild.
func RemoveDeletedChannels(s *states.State, guildID string) error {
	dbChannels, err := RetrieveAll(s.DB, guildID)
	if err != nil {
		return err
	}
	if errors.Is(err, sql.ErrNoRows) {
		return nil
	}

	guildChannels := s.GuildChannels(guildID)

	for _, dbChannel := range dbChannels {
		if dbChannel.ChannelID == "" {
			//Ignore blank channels. They are required to have empty channel settings option.
			continue
		}

		match := false
		for _, guildChannel := range guildChannels {
			if guildChannel.ID == dbChannel.ChannelID {
				match = true
				break
			}
		}
		if !match {
			if err := dbChannel.Delete(s.DB); err != nil {
				return err
			}
		}
	}
	return nil
}

//InCategoryList returns true if the given channel is in categoryIDs or if the channel's parent is in categoryIDs.
func InCategoryList(session *dg.Session, channelID string, categoryIDs []string) (bool, error) {
	channel, err := session.Channel(channelID)
	if err != nil {
		return false, err
	}
	for _, category := range categoryIDs {
		if channel.ID == category || channel.ParentID == category {
			return true, nil
		}
	}
	return false, err
}
