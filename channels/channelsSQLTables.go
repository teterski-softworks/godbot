package channels

import "gitlab.com/teterski-softworks/godbot/db"

func CreateDatabaseTables(DB db.ReadWriter) error {
	t := DB.NewTransaction()
	t.Queue(SQLCreateTableChannels)
	return t.Commit()
}

var (
	SQLCreateTableChannels string = `
	CREATE TABLE IF NOT EXISTS channels(
		guildID   TEXT NOT NULL REFERENCES guilds(guildID) ON DELETE CASCADE,
		channelID TEXT NOT NULL UNIQUE,

		PRIMARY KEY (guildID, channelID)
	);`
)
