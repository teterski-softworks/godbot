package channels

import (
	dg "github.com/bwmarrin/discordgo"
	"gitlab.com/teterski-softworks/godbot/states"
)

//ChannelDeleteHandler deletes a channel from the DB when it is deleted from a server.
func ChannelDeleteHandler(s *states.State, event *dg.ChannelDelete) error {
	s.Printf("C[%s] deleted from G[%s].", event.Channel.ID, event.GuildID)
	if err := New(event.GuildID, event.Channel.ID).Delete(s.DB); err != nil {
		return err
	}
	return nil
}
