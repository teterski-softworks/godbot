package info

import (
	"database/sql"
	"errors"
	"fmt"
	"strings"

	dg "github.com/bwmarrin/discordgo"
	"gitlab.com/teterski-softworks/godbot/channels"
	"gitlab.com/teterski-softworks/godbot/commands"
	"gitlab.com/teterski-softworks/godbot/counters"
	"gitlab.com/teterski-softworks/godbot/db"
	"gitlab.com/teterski-softworks/godbot/embeds"
	"gitlab.com/teterski-softworks/godbot/leveling"
	"gitlab.com/teterski-softworks/godbot/members"
	"gitlab.com/teterski-softworks/godbot/states"
)

//SlashCommandInfo displays information about a member.
var SlashCommandInfo = commands.New(
	&dg.ApplicationCommand{
		Type:        dg.ChatApplicationCommand,
		Name:        "info",
		Description: "Displays information about a member.",
		Options: []*dg.ApplicationCommandOption{
			{
				Type:        dg.ApplicationCommandOptionUser,
				Name:        "member",
				Description: "The server member whose information to display.",
				Required:    false,
			},
		},
	},
	true,
	func(intr states.Interaction, options ...*dg.ApplicationCommandInteractionDataOption) (err error) {

		if err := intr.Acknowledge(); err != nil {
			return err
		}

		//If no arguments are passed, we return info on the message author.
		member := intr.Member
		if len(options) == 1 {
			//Otherwise retrieve the passed through ID (only the first one, extra IDs are ignored).

			var user *dg.User
			member, user, err = members.GetMemberOrUserFromID(intr.Session, intr.GuildID, options[0].UserValue(intr.Session).ID)
			if err != nil {
				return intr.EditResponseEmbeds(err, states.ResponseGenericError(err))
			}

			//User is no longer a member of the server.
			if member == nil {
				description := fmt.Sprintf("We do not have any information about:\n %s `%s#%s` `ID:\u00A0%s`", user.Mention(), user.Username, user.Discriminator, user.ID)
				embed := embeds.NewDefault(intr.DB, intr.GuildID)
				embed.Title = "Member Information"
				embed.Description = description
				embed.Thumbnail = &dg.MessageEmbedThumbnail{URL: user.AvatarURL("")}

				return intr.EditResponseEmbeds(nil, &embed.MessageEmbed)
			}
		}

		description := fmt.Sprintf("Here is all the information we have about:\n %s `%s#%s` `ID:\u00A0%s`", member.User.Mention(), member.User.Username, member.User.Discriminator, member.User.ID)

		levelRecord, err := leveling.Retrieve(intr.DB, intr.GuildID, member.User.ID)
		if err != nil {
			return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
		} else if errors.Is(err, sql.ErrNoRows) {
			levelRecord = leveling.New(intr.GuildID, member.User.ID)
		}

		xpBarLength := 10
		startXP := leveling.ExpRequired(levelRecord.Level)
		requiredXP := leveling.ExpRequired(levelRecord.Level + 1)
		levelField := &dg.MessageEmbedField{
			Name:   fmt.Sprintf("Level %d (%d/%dxp)", levelRecord.Level, levelRecord.XP, requiredXP),
			Inline: false,
		}
		progress := int(float32(levelRecord.XP-startXP) / float32(requiredXP-startXP) * float32(xpBarLength))
		//TODO! string.Repeat can error out here because progress can be negative?!
		//TODO: caused by setting the members XP low enough that it is under the members current level.
		levelField.Value = fmt.Sprintf("%s%s", strings.Repeat("🟩", progress), strings.Repeat("⬜", xpBarLength-progress))

		counterChannelIDs, periodScores, allTimeScores, err := counters.GetMemberAllCounterScores(intr.DB, intr.GuildID, member.User.ID)
		if err != nil {
			return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
		}

		countersField := &dg.MessageEmbedField{
			Name:   "Counters:",
			Inline: false,
		}
		for i := range counterChannelIDs {
			countersField.Value = fmt.Sprintf("%s This Period: `%d` All Time: `%d`", channels.Mention(counterChannelIDs[i]), periodScores[i], allTimeScores[i])
		}

		joinedField := &dg.MessageEmbedField{
			Name:   "Joined:",
			Value:  fmt.Sprintf("<t:%[1]d:R>\n<t:%[1]d:D>", member.JoinedAt.Unix()),
			Inline: true,
		}

		var fields []*dg.MessageEmbedField
		fields = append(fields, levelField)
		if len(counterChannelIDs) != 0 {
			fields = append(fields, countersField)
		}
		fields = append(fields, joinedField)
		if member.PremiumSince != nil {
			fields = append(fields, &dg.MessageEmbedField{
				Name:   "Boosting since:",
				Value:  fmt.Sprintf("<t:%[1]d:R>\n<t:%[1]d:D>", member.PremiumSince.Unix()),
				Inline: true,
			})
		}

		embed := embeds.NewDefault(intr.DB, intr.GuildID)
		embed.Title = "Member Information"
		embed.Description = description
		embed.Fields = fields
		embed.Thumbnail = &dg.MessageEmbedThumbnail{URL: member.AvatarURL("")}

		return intr.EditResponseEmbeds(nil, &embed.MessageEmbed)
	},
)
