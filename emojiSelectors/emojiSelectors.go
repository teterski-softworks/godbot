package emojiSelectors

import (
	dg "github.com/bwmarrin/discordgo"
	"gitlab.com/teterski-softworks/godbot/concurrentMaps"
)

/*EmojiSelection listens to MessageReactionAdd events and returns the emoji that was reacted with.
It is intended as a way for users to easily select emojis, rather than having to pass them through as a string parameter to a slash command.*/
type EmojiSelection interface {
	NewSelection(channelID string, messageID string)
	GetSelection(channelID string, messageID string) dg.Emoji
	MessageReactionAddHandler(session *dg.Session, event *dg.MessageReactionAdd) error
}

//EmojiSelector is an implementation of EmojiSelection.
type EmojiSelector struct {
	selectors concurrentMaps.Map[string, chan dg.Emoji]
}

//New returns a new EmojiSelection.
func New() EmojiSelection {
	return &EmojiSelector{
		selectors: concurrentMaps.New[string, chan dg.Emoji](),
	}
}

//NewSelection creates a new chan in the emoji selector for the given channel and message which waits for an emoji.
func (s *EmojiSelector) NewSelection(channelID string, messageID string) {
	key := channelID + messageID
	s.selectors.Store(key, make(chan dg.Emoji))
}

//GetSelection halts program execution and waits for an emoji to be provided by the user. Once provided, it will return the emoji.
func (s *EmojiSelector) GetSelection(channelID string, messageID string) dg.Emoji {
	key := channelID + messageID
	emoji := <-s.selectors.Get(key)
	close(s.selectors.Get(key))
	s.selectors.Delete(key)
	return emoji
}

//MessageReactionAddHandler checks if an emoji selection menu was reacted to. If so, places the emoji into the EmojiSelectors corresponding chan for the channel and message.
func (s *EmojiSelector) MessageReactionAddHandler(session *dg.Session, event *dg.MessageReactionAdd) error {
	emojiChannel, ok := s.selectors.GetOK(event.ChannelID + event.MessageID)
	if !ok {
		return nil
	}
	emojiChannel <- event.Emoji
	//Remove the reaction
	return session.MessageReactionRemove(event.ChannelID, event.MessageID, event.Emoji.APIName(), event.UserID)
}

//Embed returns an embed which can be reacted to as a way to select an emoji.
func Embed(color int) *dg.MessageEmbed {
	//TODO: make use embeds package (resolve dependency cycle)
	return &dg.MessageEmbed{
		Title:       "🙂 Emoji Selector",
		Color:       color,
		Description: "React to this post with the emoji you wish to use.",
	}
}
