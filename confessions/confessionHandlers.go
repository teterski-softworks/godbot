package confessions

import (
	"gitlab.com/teterski-softworks/godbot/states"
)

//MessageCreateHandler checks if a message was made in a confession channel, and anonymizes it if so.
func MessageCreateHandler(intr states.Interaction) (bool, error) {

	//Check if the message was in a confession channel
	settings, err := RetrieveSettings(intr.DB, intr.GuildID)
	if err != nil {
		return false, err
	}
	match := false
	channelIndex := 0
	for i, channel := range settings.Channels {
		match = (intr.ChannelID == channel.ChannelID)
		channelIndex = i
		if match {
			break
		}
	}
	if !match {
		return false, nil
	}

	//Delete the original message
	if err := intr.ChannelMessageDelete(intr.ChannelID, intr.Message.ID); err != nil {
		_, err = intr.SendEmbeds(intr.ChannelID, intr.Message.Reference(), err, states.ResponseBotPermission())
		return true, err
	}

	//Check if the confession isn't empty (it could be only an image, which we do not support)
	if intr.Message.Content == "" {
		return true, nil
	}

	//Post an anonymous version of the message
	confession := New(intr.Message, settings.Channels[channelIndex].EmojiID, settings.Channels[channelIndex].EmojiName)
	if settings.Channels[channelIndex].Embedded {
		embed := confession.Embed(intr.DB)
		st, err := intr.SendEmbeds(intr.ChannelID, nil, nil, embed)
		if err != nil {
			return true, err
		}
		confession.Message.ID = st.ID
		embed = confession.Embed(intr.DB)
		if _, err := intr.ChannelMessageEditEmbed(intr.ChannelID, st.ID, embed); err != nil {
			return true, err
		}
	} else {
		header, err := intr.SendMessage("**"+confession.Emoji.MessageFormat()+" "+confession.Message.ID+"**", intr.ChannelID, nil, nil)
		if err != nil {
			return true, err
		}
		st, err := intr.SendMessage(confession.Content, intr.ChannelID, nil, nil)
		if err != nil {
			return true, err
		}
		if _, err := intr.ChannelMessageEdit(intr.ChannelID, header.ID, "**"+confession.Emoji.MessageFormat()+" "+st.ID+"**"); err != nil {
			return true, err
		}
		confession.Message.ID = st.ID
	}

	//Log the message
	if settings.LogChannelID == "" {
		return true, nil
	}
	confessionLog, err := confession.EmbedLog(intr.State, intr.GuildID)
	if err != nil {
		return true, err
	}
	_, err = intr.SendEmbeds(settings.LogChannelID, nil, nil, confessionLog)
	if err != nil {
		return true, err
	}

	return true, nil
}
