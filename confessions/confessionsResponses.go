package confessions

import (
	"fmt"
	"time"

	dg "github.com/bwmarrin/discordgo"
	"gitlab.com/teterski-softworks/godbot/channels"
	"gitlab.com/teterski-softworks/godbot/embeds"
)

var (
	ResponseConfessionNotFound = func(channelID string, messageID string) *dg.MessageEmbed {
		embed := embeds.New("", "", "")
		embed.Title = "❌ Confession Not Found"
		embed.Color = embeds.ColorRed
		embed.Description = fmt.Sprintf("Unable to find confession in channel %s with ID: %s.", channels.Mention(channelID), messageID)
		embed.Timestamp = time.Now().Format(time.RFC3339)
		return &embed.MessageEmbed
	}
)
