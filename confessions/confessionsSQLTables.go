package confessions

import "gitlab.com/teterski-softworks/godbot/db"

func CreateDatabaseTables(DB db.ReadWriter) error {
	t := DB.NewTransaction()
	t.Queue(SQLCreateTableConfessionSettings)
	t.Queue(SQLCreateTableConfessionChannels)
	return t.Commit()
}

var (
	SQLCreateTableConfessionSettings string = `
	CREATE TABLE IF NOT EXISTS confessionSettings(
		guildID    		TEXT NOT NULL PRIMARY KEY REFERENCES guild(guildID),
		logChannelID	TEXT DEFAULT "" REFERENCES channels(channelID) ON DELETE SET DEFAULT
	);`

	SQLCreateTableConfessionChannels string = `
	CREATE TABLE IF NOT EXISTS confessionChannels(

		guildID    		TEXT NOT NULL REFERENCES guild(guildID),
		channelID  		TEXT NOT NULL REFERENCES channels(channelID) ON DELETE CASCADE,
		embedded        BOOLEAN DEFAULT FALSE,
		emojiID			TEXT DEFAULT "",
		emojiName 		TEXT DEFAULT "",

		PRIMARY KEY (guildID, channelID)
	);`
)
