package confessions

import (
	dg "github.com/bwmarrin/discordgo"
	"gitlab.com/teterski-softworks/godbot/channels"
	"gitlab.com/teterski-softworks/godbot/commands"
	"gitlab.com/teterski-softworks/godbot/db"
	"gitlab.com/teterski-softworks/godbot/states"
)

//SlashCommandAdminConfession - An admin-only command group for confession related commands.
var SlashCommandAdminConfession = commands.New(
	&dg.ApplicationCommand{
		Type:        dg.ChatApplicationCommand,
		Name:        "admin_confession",
		Description: "Admin-only confession commands.",
		Options: []*dg.ApplicationCommandOption{
			SlashCommandAdminConfessionSettings,
		},
	},
	false,
	func(intr states.Interaction, options ...*dg.ApplicationCommandInteractionDataOption) (err error) {

		if err := intr.Acknowledge(); err != nil {
			return err
		}

		switch options[0].Name {
		case SlashCommandAdminConfessionSettings.Name:
			return SlashAdminConfessionSettings(intr, options[0].Options)
		}
		return intr.EditResponseEmbeds(nil, states.ResponseGenericError(nil))
	},
)

//SlashCommandAdminConfessionSettings - An admin-only subcommand for viewing and updating confession settings.
var SlashCommandAdminConfessionSettings = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionSubCommand,
	Name:        "settings",
	Description: "View and change settings for confession channels.",
	Options: []*dg.ApplicationCommandOption{
		{
			Type:         dg.ApplicationCommandOptionChannel,
			Name:         "channel_add",
			Description:  "The channel in which messages will be anonymized.",
			ChannelTypes: []dg.ChannelType{dg.ChannelTypeGuildText},
		},
		{
			Type:        dg.ApplicationCommandOptionInteger,
			Name:        "toggle_embed",
			Description: "Toggle message embedding for the selected channel index.",
			MinValue:    &one,
		},
		{
			Type:        dg.ApplicationCommandOptionInteger,
			Name:        "channel_emoji",
			Description: "Change the emoji for the selected channel index.",
			MinValue:    &one,
		},
		{
			Type:        dg.ApplicationCommandOptionInteger,
			Name:        "remove_channel",
			Description: "Removes the selected channel index.",
			MinValue:    &one,
		},
		{
			Type:         dg.ApplicationCommandOptionChannel,
			Name:         "log_channel",
			Description:  "The channel in which confessions will be logged.",
			ChannelTypes: []dg.ChannelType{dg.ChannelTypeGuildText},
		},
		{
			Type:        dg.ApplicationCommandOptionBoolean,
			Name:        "clear_log_channel",
			Description: "Set to true to clear log channel setting.",
		},
	},
}
var one float64 = 1

//SlashAdminConfessionSettings views and updates a guild's confession settings.
func SlashAdminConfessionSettings(intr states.Interaction, options []*dg.ApplicationCommandInteractionDataOption) (err error) {

	settings, err := RetrieveSettings(intr.DB, intr.GuildID)
	if err != nil {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}
	for _, option := range options {
		switch option.Name {

		case "channel_add":
			settings.Channels = append(settings.Channels, Channel{
				Channel:  channels.New(intr.GuildID, option.ChannelValue(intr.Session).ID),
				Embedded: false,
			})

		case "toggle_embed":
			index := int(option.IntValue()) - 1
			if index > len(settings.Channels) {
				continue
			}
			settings.Channels[index].Embedded = !settings.Channels[index].Embedded

		case "channel_emoji":

			index := int(option.IntValue()) - 1
			if index > len(settings.Channels) {
				continue
			}

			//TODO
			emoji, err := intr.RequestEmoji()
			if err != nil {
				return err
			}

			settings.Channels[index].EmojiID = emoji.ID
			settings.Channels[index].EmojiName = emoji.Name

		case "remove_channel":
			index := int(option.IntValue()) - 1
			if index > len(settings.Channels) {
				continue
			}
			settings.Channels = append(settings.Channels[:index], settings.Channels[index+1:]...)

		case "log_channel":
			settings.LogChannelID = option.ChannelValue(intr.Session).ID

		case "clear_log_channel":
			if option.BoolValue() {
				settings.LogChannelID = ""
			}
		}
	}
	if err := settings.Store(intr.DB); err != nil {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}
	settings, err = RetrieveSettings(intr.DB, intr.GuildID)
	if err != nil {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}

	return intr.EditResponseEmbeds(nil, settings.Embed(intr.DB))
}
