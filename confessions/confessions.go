package confessions

import (
	"fmt"
	"time"

	dg "github.com/bwmarrin/discordgo"
	"gitlab.com/teterski-softworks/godbot/channels"
	"gitlab.com/teterski-softworks/godbot/db"
	"gitlab.com/teterski-softworks/godbot/embeds"
	"gitlab.com/teterski-softworks/godbot/members"
	"gitlab.com/teterski-softworks/godbot/states"
)

//Confession represents an anonymous confession message.
type Confession struct {
	dg.Message
	Emoji dg.Emoji
}

//New returns a new Confession from the proved message. The emoji info is used for the confession header.
func New(message *dg.Message, emojiID string, emojiName string) Confession {
	return Confession{
		Message: *message,
		Emoji: dg.Emoji{
			ID:   emojiID,
			Name: emojiName,
		},
	}
}

//Embed returns an embedded version of the Confession.
func (c Confession) Embed(DB db.Reader) *dg.MessageEmbed {
	embed := embeds.NewDefault(DB, c.GuildID)
	embed.Title = c.Emoji.MessageFormat() + " " + c.Message.ID
	embed.Description = c.Content
	embed.Footer.Text = "All confessions are logged for moderation purposes."
	embed.Timestamp = c.Timestamp.Format(time.RFC3339)
	return &embed.MessageEmbed
}

//EmbedLog returns an embedded log of the Confession, showing who pasted the Confession.
func (c Confession) EmbedLog(state *states.State, guildID string) (*dg.MessageEmbed, error) {
	embed := embeds.NewDefault(state.DB, guildID)
	embed.Title = c.Emoji.MessageFormat() + " " + c.Message.ID
	embed.URL = c.Link()
	embed.Timestamp = c.Timestamp.Format(time.RFC3339)
	embed.Description = c.Content

	if member, user, err := members.GetMemberOrUserFromID(state.Session, c.GuildID, c.Author.ID); err != nil {
		return nil, err
	} else if member != nil {
		if member.Nick != "" {
			embed.Author.Name = member.Nick + " [" + member.User.String() + "]"
		} else {
			embed.Author.Name = member.User.String()
		}
		embed.Author.IconURL = member.AvatarURL("")
	} else {
		embed.Author.Name = user.String()
		embed.Author.IconURL = user.AvatarURL("")
	}

	embed.AddField("Author", members.Mention(c.Author.ID)+"\n`"+c.Author.ID+"`", true)
	embed.AddField("Channel", channels.Mention(c.ChannelID), true)

	return &embed.MessageEmbed, nil
}

//TODO: replace with general method
//Link returns th URL of the Confession
func (c Confession) Link() string {
	return fmt.Sprintf("https://discord.com/channels/%s/%s/%s", c.GuildID, c.ChannelID, c.Message.ID)
}
