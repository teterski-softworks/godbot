package confessions

import (
	"database/sql"
	"fmt"

	dg "github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
	"gitlab.com/teterski-softworks/godbot/channels"
	"gitlab.com/teterski-softworks/godbot/db"
	"gitlab.com/teterski-softworks/godbot/embeds"
	"gitlab.com/teterski-softworks/godbot/guilds"
)

//Settings represents a set of confession settings for a guild.
type Settings struct {
	GuildID      string
	LogChannelID string
	Channels     []Channel
}

//Channel represents a confessions channel.
type Channel struct {
	channels.Channel
	Embedded  bool   //If true, confessions will be posted as an embedded message.
	EmojiID   string //Used for the confession header.
	EmojiName string //Used for the confession header.
}

//NewSettings returns a new set of settings for the guild.
func NewSettings(guildID string) Settings {
	return Settings{GuildID: guildID}
}

//RetrieveSettings returns the Settings for the given Guild.
func RetrieveSettings(DB db.Reader, guildID string) (Settings, error) {
	s := Settings{GuildID: guildID}

	row, err := DB.QueryRow(SQLSelectGuildConfessionSettings, guildID)
	if err != nil {
		return s, err
	}
	if err = row.Scan(&s.LogChannelID); err != nil && !errors.Is(err, sql.ErrNoRows) {
		return s, errors.WithStack(err)
	}

	s.Channels, err = RetrieveChannels(DB, guildID)
	if err != nil {
		return s, err
	}

	return s, nil
}

//RetrieveChannels returns a list of all confession channels in the guild.
func RetrieveChannels(DB db.Reader, guildID string) (channels []Channel, err error) {

	rows, err := DB.Query(SQLSelectGuildConfessionChannels, guildID)
	if err != nil {
		return channels, err
	}
	var channel Channel
	channel.GuildID = guildID
	for rows.Next() {
		if err = rows.Scan(
			&channel.ChannelID,
			&channel.Embedded,
			&channel.EmojiID,
			&channel.EmojiName,
		); err != nil && !errors.Is(err, sql.ErrNoRows) {
			return channels, errors.WithStack(err)
		}
		channels = append(channels, channel)
	}

	return channels, nil
}

//Store saves Settings to the DB.
func (s Settings) Store(DB db.ReadWriter) error {
	t := DB.NewTransaction()

	if err := guilds.New(s.GuildID).Store(t); err != nil {
		return err
	}

	t.Queue(SQLUpsertConfessionSettings, s.GuildID, s.LogChannelID)

	//Delete current confession channels
	t.Queue(SQLDeleteGuildConfessionChannels, s.GuildID)

	//Save the list of confession channels.
	for _, channel := range s.Channels {
		if err := channel.Channel.Store(t); err != nil {
			return err
		}
		t.Queue(SQLInsertConfessionChannel, s.GuildID, channel.ChannelID, channel.Embedded, channel.EmojiID, channel.EmojiName)
	}

	return t.Commit()
}

//Embed returns an embed showing the confession settings.
func (s Settings) Embed(DB db.Reader) *dg.MessageEmbed {
	embed := embeds.NewDefault(DB, s.GuildID)
	embed.Title = "🛐 Confession Settings ⚙️"

	logField := &dg.MessageEmbedField{
		Name:  "📜 Log Channel",
		Value: "The channel into which all confessions are logged:\n",
	}
	if s.LogChannelID == "" {
		logField.Value += "`None set`"
	} else {
		logField.Value += channels.Mention(s.LogChannelID)
	}
	embed.Fields = append(embed.Fields, logField)

	channelsField := &dg.MessageEmbedField{
		Name:  "#️⃣ Channels",
		Value: "The following channels will delete users' messages and repost them anonymously:\n",
	}
	if len(s.Channels) == 0 {
		channelsField.Value += "`None set`"
	} else {
		for i, channel := range s.Channels {
			mode := "Plain Text"
			if channel.Embedded {
				mode = "Embedded"
			}
			emoji := dg.Emoji{
				ID:   channel.EmojiID,
				Name: channel.EmojiName,
			}
			channelsField.Value += fmt.Sprintf("%d. %s %s `%s`\n", i+1, emoji.MessageFormat(), channels.Mention(channel.ChannelID), mode)
		}
	}
	embed.Fields = append(embed.Fields, channelsField)

	return &embed.MessageEmbed
}
