package confessions

var (
	SQLSelectGuildConfessionChannels string = `
	SELECT channelID, embedded, emojiID, emojiName FROM confessionChannels WHERE guildID = ?1 ORDER BY channelID;`

	SQLSelectGuildConfessionSettings string = `
	SELECT logChannelID FROM confessionSettings WHERE guildID = ?1;`

	SQLInsertConfessionChannel string = `
	INSERT INTO confessionChannels(guildID, channelID, embedded, emojiID, emojiName)
	VALUES (?1, ?2, ?3, ?4, ?5) ON CONFLICT DO
	UPDATE SET

	embedded  =?3,
	emojiID   =?4,
	emojiName =?5
	
	WHERE guildID = ?1 AND channelID = ?2;`

	SQLUpsertConfessionSettings string = `
	INSERT INTO confessionSettings(guildID, logChannelID)
	VALUES (?1, ?2)	ON CONFLICT DO
	UPDATE SET
	logChannelID  = ?2
	WHERE guildID = ?1;`

	SQLDeleteGuildConfessionChannels string = `
	DELETE FROM confessionChannels WHERE guildID = ?1;`

	SQLDeleteConfessionChannel string = `
	DELETE FROM confessionChannels WHERE guildID = ?1 AND channelID = ?2;`
)
