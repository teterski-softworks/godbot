package counters

const (
	SQLUpsertCounter string = `
	INSERT INTO counters(
		guildID,
		channelID,

		allTime_count,
		allTime_reward,
		allTime_winner,

		periodic_count,
		periodic_reward,
		periodic_winner,
		periodic_duration,
		periodic_end,
		
		leaderBoard_channelID,
		leaderBoard_msgID,

		lastMessageID,

		timerID
	)
	VALUES (?1,?2,?3,?4,?5,?6,?7,?8,?9,?10,?11,?12,?13,?14)
	ON CONFLICT (guildID, channelID) DO
	UPDATE SET

	allTime_count         =(?3),
	allTime_reward        =(?4),
	allTime_winner        =(?5),

	periodic_count        =(?6),
	periodic_reward       =(?7),
	periodic_winner       =(?8),
	periodic_duration     =(?9),
	periodic_end          =(?10),
	
	leaderBoard_channelID =(?11),
	leaderBoard_msgID     =(?12),

	lastMessageID         =(?13),

	timerID               =(?14)
	
	WHERE guildID=(?1) AND channelID=(?2);`

	SQLDeleteCounter string = `
	DELETE FROM counters
	WHERE guildID=(?1) AND channelID=(?2);`

	SQLSelectCounter string = `
	SELECT

	allTime_count,
	allTime_reward,
	allTime_winner,

	periodic_count,
	periodic_reward,
	periodic_winner,
	periodic_duration,
	periodic_end,
	
	leaderBoard_channelID,
	leaderBoard_msgID,

	lastMessageID,

	timerID

	FROM counters WHERE guildID=(?1) AND channelID=(?2);`

	SQLSelectAllGuildCounters string = `
	SELECT

	channelID,

	allTime_count,
	allTime_reward,
	allTime_winner,

	periodic_count,
	periodic_reward,
	periodic_winner,
	periodic_duration,
	periodic_end,
	
	leaderBoard_channelID,
	leaderBoard_msgID,

	lastMessageID,

	timerID

	FROM counters WHERE guildID=(?1);`

	SQLSelectAllCounterAllTimeCounts string = `
	SELECT channelID, allTime_count
	FROM counters
	WHERE guildID=(?1);`

	SQLSelectAllCounterScoresOfMember string = `
	SELECT

	channelID,
	allTime_count,
	periodic_count

	FROM counterPerMember
	WHERE guildID=(?1) AND memberID=(?2);`

	SQLSelectCounterScoreOfMember string = `
	SELECT

	allTime_count,
	periodic_count

	FROM counterPerMember
	WHERE guildID=(?1) AND channelID=(?2) AND memberID=(?3);`

	SQLSelectCounterNewWinners string = `
	WITH highestPeriodic AS(
		SELECT MAX(periodic_count), memberID AS periodic_Winner
		FROM counterPerMember
		WHERE guildID=(?1) AND channelID=(?2)
	),
	highestAllTime AS(
		SELECT MAX(allTime_count), memberID AS AllTime_Winner
		FROM counterPerMember
		WHERE guildID=(?1) AND channelID=(?2)
	)
	SELECT * FROM
	(SELECT IFNULL(periodic_Winner, "") FROM highestPeriodic)
	CROSS JOIN
	(SELECT IFNULL(AllTime_Winner, "") FROM highestAllTime);`

	SQLSelectCounterPeriodicScores string = `
	SELECT periodic_count, memberID
	FROM counterPerMember
	WHERE guildID=(?1) AND channelID=(?2)
	ORDER BY periodic_count DESC LIMIT (?3);`

	SQLSelectCounterAllTimeScores string = `
	SELECT allTime_count, memberID
	FROM counterPerMember
	WHERE guildID=(?1) AND channelID=(?2)
	ORDER BY allTime_count DESC LIMIT (?3);`

	SQLIncrementMemberCounter string = `
	INSERT INTO counterPerMember(
		
		guildID,
		channelID,
		memberID,

		periodic_count,
		allTime_count
	)
	VALUES (?1,?2,?3,1,1)

	ON CONFLICT(guildID, channelID, memberID) DO
	UPDATE SET
	periodic_count = periodic_count + 1,
	allTime_count  = allTime_count  + 1

	WHERE guildID=(?1) AND channelID=(?2) AND memberID=(?3);`

	SQLUpsetCountOfMember string = `INSERT INTO counterPerMember(
		
		guildID,
		channelID,
		memberID,

		periodic_count,
		allTime_count
	)
	VALUES (?1,?2,?3,?4,?5)

	ON CONFLICT(guildID, channelID, memberID) DO
	UPDATE SET
	periodic_count = (?4),
	allTime_count  = (?5)

	WHERE guildID=(?1) AND channelID=(?2) AND memberID=(?3);`

	SQLChangeChannelMemberCounters string = `
	UPDATE counterPerMember
	SET	channelID=(?3)
	WHERE guildID=(?1) AND channelID=(?2);`

	SQLResetAllPeriodMemberCounters string = `
	UPDATE counterPerMember
	SET periodic_count=0
	WHERE guildID=(?1) AND channelID=(?2);`
)
