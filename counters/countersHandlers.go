package counters

import (
	"database/sql"
	"strconv"
	"strings"

	dg "github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
	"gitlab.com/teterski-softworks/godbot/db"
	"gitlab.com/teterski-softworks/godbot/states"
	"gitlab.com/teterski-softworks/godbot/webhooks"
)

//Handler handles messages send to a channel with a counter in it.
func MessageCreateHandler(intr states.Interaction, webhookManager webhooks.Manager) error {

	message := intr.Message

	guildID := intr.Message.GuildID
	channelID := intr.Message.ChannelID
	messageID := intr.Message.ID
	authorID := intr.Message.Author.ID
	reference := message.Reference()

	counter, err := Retrieve(intr.DB, guildID, channelID)
	if err != nil {
		//Check if a counter exists in the channel the message was sent in.
		//TODO: could we do this more efficiently through some sort cache, rather than querying the DB each time a message is sent?
		if errors.Is(err, sql.ErrNoRows) {
			return nil //stop execution if no counter exists.
		}
		_, err := intr.SendEmbeds(channelID, reference, err, db.ResponseDBError(err))
		return err
	}

	content := strings.Split(message.Content, " ")[0]
	number, err := strconv.Atoi(content)
	if err != nil || number != counter.AllTime.Count+1 {
		//Delete the original message if the number in the message is one higher than the current count.
		if err := intr.ChannelMessageDelete(channelID, messageID); err != nil {
			_, err := intr.SendEmbeds(channelID, reference, errors.WithStack(err), states.ResponseGenericError(err))
			return err
		}
		//Otherwise, do nothing and stop event handling.
		return nil
	}

	t := intr.DB.NewTransaction()

	//Increment counter
	if err := counter.IncrementMemberCounts(t, authorID); err != nil {
		return err
	}
	counter.AllTime.Count++
	counter.Periodic.Count++

	//Impersonate the member
	var webhookParams dg.WebhookParams
	webhookParams.Content = message.Content
	for _, file := range message.Attachments {
		webhookParams.Content = message.Content + "\n" + file.ProxyURL
	}
	st, err := intr.Impersonate(webhookParams, guildID, authorID, channelID, webhookManager)
	if err != nil {
		_, err := intr.SendEmbeds(channelID, reference, errors.WithStack(err), states.ResponseGenericError(err))
		return err
	}
	//Update the last processed message ID
	counter.lastMessageID = st.ID

	//Delete the original message
	if err := intr.ChannelMessageDelete(channelID, messageID); err != nil {
		_, err := intr.SendEmbeds(channelID, st.Reference(), errors.WithStack(err), states.ResponseGenericError(err))
		return err
	}

	//Save the updated counter
	if err := counter.Store(t); err != nil {
		_, err := intr.SendEmbeds(channelID, st.Reference(), err, db.ResponseDBError(err))
		return err
	}

	//Commit the transaction
	if err = t.Commit(); err != nil {
		intr.SendEmbeds(channelID, st.Reference(), err, db.ResponseDBError(err))
		return err
	}

	return nil
}
