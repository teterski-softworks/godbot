package counters

import (
	"database/sql"
	"fmt"
	"strings"
	"time"

	dg "github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
	"gitlab.com/teterski-softworks/godbot/channels"
	"gitlab.com/teterski-softworks/godbot/db"
	"gitlab.com/teterski-softworks/godbot/durations"
	"gitlab.com/teterski-softworks/godbot/embeds"
	"gitlab.com/teterski-softworks/godbot/members"
	"gitlab.com/teterski-softworks/godbot/roles"
	"gitlab.com/teterski-softworks/godbot/states"
	"gitlab.com/teterski-softworks/godbot/webhooks"
)

//TODO: make order of periodic score vs alltime score consistent
//TODO: decide on a single name for score/count and make it consistent throughout

//Counter stores two counts, a periodic counter that resets when the period runs out
//and an all time counter that never resets.
type Counter struct {
	//Primary keys
	channels.Channel

	//Contains basic counter info for the all time counter
	AllTime counterBase

	//Contains basic counter info for the periodic counter
	Periodic struct {
		counterBase
		time.Duration
		End time.Time
	}

	//The specific message that contains the leader board for the counter.
	LeaderBoard struct {
		ChannelID string
		MsgID     string
	}

	//The ID of the last message in the counter channel that was handled by the counter.
	lastMessageID string

	//The ID of the timer that fires every period.
	timerID int
}

//counterBase stores basic information about a counter.
type counterBase struct {
	Count  int
	Reward string
	Winner string
}

//New returns a new Counter for the given guild ID and channel ID.
func New(guildID string, channelID string) Counter {
	return Counter{Channel: channels.New(guildID, channelID)}
}

//Retrieve gets information from the DB pertaining to the counter in the given channel.
func Retrieve(DB db.Reader, guildID string, channelID string) (Counter, error) {
	c := Counter{Channel: channels.New(guildID, channelID)}
	row, err := DB.QueryRow(SQLSelectCounter, c.GuildID, c.ChannelID)
	if err != nil {
		return c, err
	}

	if err = row.Scan(
		&c.AllTime.Count,
		&c.AllTime.Reward,
		&c.AllTime.Winner,

		&c.Periodic.Count,
		&c.Periodic.Reward,
		&c.Periodic.Winner,
		&c.Periodic.Duration,
		&c.Periodic.End,

		&c.LeaderBoard.ChannelID,
		&c.LeaderBoard.MsgID,

		&c.lastMessageID,

		&c.timerID,
	); err != nil {
		return c, errors.WithStack(err)
	}

	return c, nil
}

//RetrieveAll returns all counters in a guild.
func RetrieveAll(DB db.Reader, guildID string) ([]Counter, error) {
	var counters []Counter

	//Retrieve a list of all role menus in the DB
	rows, err := DB.Query(SQLSelectAllGuildCounters, guildID)
	if err != nil {
		return counters, err
	}

	for rows.Next() {
		c := New(guildID, "")
		if err = rows.Scan(
			&c.ChannelID,
			&c.AllTime.Count,
			&c.AllTime.Reward,
			&c.AllTime.Winner,

			&c.Periodic.Count,
			&c.Periodic.Reward,
			&c.Periodic.Winner,
			&c.Periodic.Duration,
			&c.Periodic.End,

			&c.LeaderBoard.ChannelID,
			&c.LeaderBoard.MsgID,

			&c.lastMessageID,

			&c.timerID,
		); err != nil {
			return counters, errors.WithStack(err)
		}
		counters = append(counters, c)
	}

	return counters, nil
}

//Store saves the counter in the given database.
func (c Counter) Store(DB db.ReadWriter) error {

	t := DB.NewTransaction()

	if err := c.Channel.Store(t); err != nil {
		return err
	}

	t.Queue(SQLUpsertCounter,
		c.GuildID,
		c.ChannelID,

		c.AllTime.Count,
		c.AllTime.Reward,
		c.AllTime.Winner,

		c.Periodic.Count,
		c.Periodic.Reward,
		c.Periodic.Winner,
		c.Periodic.Duration,
		c.Periodic.End,

		c.LeaderBoard.ChannelID,
		c.LeaderBoard.MsgID,

		c.lastMessageID,

		c.timerID,
	)

	return t.Commit()
}

//Delete removes a Counter from the DB.
//If the counter is already not in the provided database, then found returns false. Otherwise, returns true.
func (c Counter) Delete(DB db.ReadWriter) (found bool, err error) {

	if _, err = Retrieve(DB, c.GuildID, c.ChannelID); err != nil {
		return false, err
	}
	if err = DB.QueueAndCommit(SQLDeleteCounter, c.GuildID, c.ChannelID); err != nil {
		return false, err
	}
	return true, nil
}

//ChangeChannel changes the channel that the timer is assigned to.
func (c *Counter) ChangeChannel(DB db.ReadWriter, channelID string) error {

	t := DB.NewTransaction()

	//Create a copy of the counter in the new channel
	newCounter := *c
	newCounter.ChannelID = channelID
	if err := newCounter.Store(DB); err != nil {
		return err
	}

	// Update existing member counting records to new channel
	t.Queue(SQLChangeChannelMemberCounters, c.GuildID, c.ChannelID, newCounter.ChannelID)

	//Delete the old counter
	if _, err := c.Delete(t); err != nil {
		return err
	}

	if err := t.Commit(); err != nil {
		return err
	}

	*c = newCounter

	return nil
}

//Embed returns an embed showing information about the counter.
func (c Counter) Embed(intr states.Interaction) (*dg.MessageEmbed, error) {

	//Generate embed fields
	var countField dg.MessageEmbedField
	countField.Name = "🧮 Counts:"
	countField.Value += fmt.Sprintf("`%4d` All time\n", c.AllTime.Count)
	countField.Value += fmt.Sprintf("`%4d` Period\n", c.Periodic.Count)
	countField.Inline = true

	var periodField dg.MessageEmbedField
	periodField.Name = "🕒 Period:"
	periodField.Value += fmt.Sprintf("Duration: `%s`\n", durations.String(c.Periodic.Duration))
	periodField.Value += fmt.Sprintf("Reset: <t:%d:R>\n", c.Periodic.End.Unix())
	periodField.Value += fmt.Sprintf("Reset date: <t:%d>\n", c.Periodic.End.Unix())
	periodField.Inline = true

	var rewardsField dg.MessageEmbedField
	rewardsField.Name = "🏆 Rewards:"
	link := fmt.Sprintf("https://discord.com/channels/%s/%s/%s", intr.GuildID, c.LeaderBoard.ChannelID, c.LeaderBoard.MsgID)

	winnerAllTime := ""
	if c.AllTime.Winner != "" {
		winnerAllTime = members.Mention(c.AllTime.Winner)
	}

	winnerPeriodic := ""
	if c.Periodic.Winner != "" {
		winnerPeriodic = members.Mention(c.Periodic.Winner)
	}

	rewardsField.Value += fmt.Sprintf("Scoreboard: %s [Link](%s)\n", channels.Mention(c.LeaderBoard.ChannelID), link)
	rewardsField.Value += fmt.Sprintf("All time: %s %s\n", roles.Mention(c.AllTime.Reward), winnerAllTime)
	rewardsField.Value += fmt.Sprintf("Period: %s %s\n", roles.Mention(c.Periodic.Reward), winnerPeriodic)

	//Get all time scores
	counts, members, err := c.AllTimesScores(intr.State, 10)
	if err != nil {
		return nil, err
	}
	var allTimeScores dg.MessageEmbedField
	allTimeScores.Name = "🎖️ Scores - All time:"
	allTimeScores.Inline = true
	for i := range members {
		allTimeScores.Value += fmt.Sprintf("**#%d.** `%d` %s\n", i+1, counts[i], members[i])
	}
	if allTimeScores.Value == "" {
		allTimeScores.Value = "`N/A`"
	}

	//Get all period scores
	counts, members, err = c.PeriodScores(intr.State, 10)
	if err != nil {
		return nil, err
	}
	var periodScores dg.MessageEmbedField
	periodScores.Name = "🏅 Scores - Periodic Counts:"
	periodScores.Inline = true
	for i := range members {
		periodScores.Value += fmt.Sprintf("**#%d.** `%d` %s\n", i+1, counts[i], members[i])
	}
	if periodScores.Value == "" {
		periodScores.Value = "`N/A`"
	}

	//Get previous winners
	if c.Periodic.Winner != "" {
		member, err := intr.Session.User(c.Periodic.Winner)
		if err != nil {
			return nil, err
		}
		c.Periodic.Winner = member.Mention()
	}
	if c.AllTime.Winner != "" {
		member, err := intr.Session.User(c.AllTime.Winner)
		if err != nil {
			return nil, err
		}
		c.AllTime.Winner = member.Mention()
	}

	embed := embeds.NewDefault(intr.DB, intr.GuildID)
	embed.Title = "🔢 Counter Info"
	embed.Description = "Below are some statistics about the " + channels.Mention(c.ChannelID) + " counter."
	embed.Timestamp = time.Now().Format(time.RFC3339)
	embed.Fields = append(embed.Fields,
		&countField,
		&periodField,
		&rewardsField,
		&periodScores,
		&allTimeScores,
	)
	return &embed.MessageEmbed, nil
}

//GetEveryAllTimeCount gets the all time counts for all counters.
func GetEveryAllTimeCount(DB db.Reader, guildID string) (counterChannelIDs []string, countsAllTime []int, err error) {

	rows, err := DB.Query(SQLSelectAllCounterAllTimeCounts, guildID)
	if err != nil {
		return counterChannelIDs, countsAllTime, err
	}

	var channelID string
	var count int
	for rows.Next() {

		if err = rows.Scan(&channelID, &count); err != nil {
			return counterChannelIDs, countsAllTime, errors.WithStack(err)
		}

		counterChannelIDs = append(counterChannelIDs, channelID)
		countsAllTime = append(countsAllTime, count)
	}

	return counterChannelIDs, countsAllTime, nil
}

//UpdateWinners updates the winners for a counter.
//This will also update the DB counter information with the current values of Counter.
func (c *Counter) UpdateWinners(DB db.ReadWriter, session *dg.Session) error {

	//Remove reward roles from previous winners
	if c.Periodic.Winner != "" {
		//*Ignore error: member might have left
		//TODO?: maybe we can check for other errors though
		session.GuildMemberRoleRemove(c.GuildID, c.Periodic.Winner, c.Periodic.Reward)
	}
	if c.AllTime.Winner != "" {
		//*Ignore error: member might have left
		//TODO?: maybe we can check for other errors though
		session.GuildMemberRoleRemove(c.GuildID, c.AllTime.Winner, c.AllTime.Reward)
	}

	//Calculates the new winners
	row, err := DB.QueryRow(SQLSelectCounterNewWinners, c.GuildID, c.ChannelID)
	if err != nil {
		return err
	}
	if err = row.Scan(&c.Periodic.Winner, &c.AllTime.Winner); err != nil {
		return err
	}

	if err := c.Store(DB); err != nil {
		return err
	}

	//Assign reward roles
	if c.Periodic.Winner != "" {
		//TODO: errors out if member is banned -> why arent the member's count records being cleared?
		if err = session.GuildMemberRoleAdd(c.GuildID, c.Periodic.Winner, c.Periodic.Reward); err != nil {
			return errors.WithStack(err)
		}
	}
	if c.AllTime.Winner != "" {
		//TODO: errors out if member is banned
		if err = session.GuildMemberRoleAdd(c.GuildID, c.AllTime.Winner, c.AllTime.Reward); err != nil {
			return errors.WithStack(err)
		}
	}

	return nil
}

//IncrementMemberCounts increases the member's periodic and total count by one.
func (c Counter) IncrementMemberCounts(DB db.ReadWriter, memberID string) error {

	t := DB.NewTransaction()
	if err := members.New(c.GuildID, memberID).Store(t); err != nil { //Add member to DB if not in it already
		return err
	}
	t.Queue(SQLIncrementMemberCounter, c.GuildID, c.ChannelID, memberID) //Apply the changes counter
	return t.Commit()
}

//SetMemberScore updates a members score.
func (c Counter) SetMemberScore(DB db.ReadWriter, memberID string, periodicScore int, allTimeScore int) error {

	t := DB.NewTransaction()
	//Save the member to the DB because the score relies on it as a foreign key and member might not yet be in DB.
	if err := members.New(c.GuildID, memberID).Store(t); err != nil {
		return err
	}
	t.Queue(SQLUpsetCountOfMember, c.GuildID, c.ChannelID, memberID, periodicScore, allTimeScore)
	return t.Commit()
}

//PeriodReset resets the periodic counters and period end time for the counter.
//This will also update the DB counter information with the current values of Counter.
func (c *Counter) PeriodReset(DB db.ReadWriter, timerManager states.TimerManager) error {

	t := DB.NewTransaction()

	c.Periodic.End = time.Now().Add(c.Periodic.Duration)
	c.Periodic.Count = 0
	if err := c.Store(t); err != nil {
		return err
	}
	t.Queue(SQLResetAllPeriodMemberCounters, c.GuildID, c.ChannelID)

	return t.Commit()
}

//UpdateLeaderBoard updates the counters leader board.
func (c *Counter) UpdateLeaderBoard(s *states.State, limit int) error {

	embed, err := c.LeaderBoardEmbed(s, limit)
	if err != nil {
		return err
	}

	//Update the leader board message
	_, err = s.ChannelMessageEditEmbed(c.LeaderBoard.ChannelID, c.LeaderBoard.MsgID, embed)
	if err != nil {
		cause := errors.Cause(err).Error()
		if strings.HasPrefix(cause, "HTTP 404 Not Found") {
			//leader board was deleted, so we post a new one and save the new message ID.
			msg, err := s.ChannelMessageSendEmbed(c.LeaderBoard.ChannelID, embed)
			if err != nil {
				return errors.WithStack(err)
			}
			c.LeaderBoard.MsgID = msg.ID
			return c.Store(s.DB)
		}
		return errors.WithStack(err)
	}
	return nil
}

//PeriodScores gets the top period scores for the counter.
func (c Counter) PeriodScores(s *states.State, limit int) (counts []int, memberIDs []string, err error) {
	return c.getCounterScores(s, SQLSelectCounterPeriodicScores, limit)
}

//PeriodScores gets the top all time scores for the counter.
func (c Counter) AllTimesScores(s *states.State, limit int) (counts []int, memberIDs []string, err error) {
	return c.getCounterScores(s, SQLSelectCounterAllTimeScores, limit)
}

//getCounterScores is a helper function which gets scores for the counter based on the query passed through.
func (c Counter) getCounterScores(s *states.State, query string, limit int) (counts []int, memberIDs []string, err error) {

	rows, err := s.DB.Query(query, c.GuildID, c.ChannelID, limit)
	if err != nil {
		return counts, memberIDs, err
	}
	var memberID string
	var count int
	for rows.Next() {
		if err = rows.Scan(&count, &memberID); err != nil {
			return counts, memberIDs, errors.WithStack(err)
		}
		counts = append(counts, count)
		memberIDs = append(memberIDs, memberID)
	}

	//Convert member IDs to member mentions.
	for m := range memberIDs {
		member, err := s.GuildMember(c.GuildID, memberIDs[m])
		if err != nil {
			return counts, memberIDs, err
		}
		memberIDs[m] = member.Mention()
	}

	return counts, memberIDs, nil
}

//Alarm resets the period and updates and posts the leader board for a counter.
func (c Counter) Alarm(s *states.State, e chan<- error) {

	t := s.DB.NewTransaction()

	//Get most current counter info
	counter, err := Retrieve(t, c.GuildID, c.ChannelID)
	if err != nil {
		e <- err
		return
	}

	//Update counter with new winners
	if err = counter.UpdateWinners(t, s.Session); err != nil {
		e <- err
		return
	}

	//Update scores to leaderboard channel
	if err = counter.UpdateLeaderBoard(s, 10); err != nil {
		e <- err
		return
	}

	//Reset all periodic counts.
	if err := counter.PeriodReset(t, s.TimerManagers.Get(counter.GuildID)); err != nil {
		e <- err
		return
	}

	//Commit any changes to the DB.
	if err := t.Commit(); err != nil {
		e <- err
		return
	}

}

//LeaderBoardEmbed returns an embed displaying a leaderboard for the current counter.
func (c Counter) LeaderBoardEmbed(s *states.State, limit int) (*dg.MessageEmbed, error) {
	counts, members, err := c.AllTimesScores(s, limit)
	if err != nil {
		return nil, err
	}

	var allTimeScores dg.MessageEmbedField
	allTimeScores.Name = "All time:"
	allTimeScores.Inline = true
	for i := range members {
		allTimeScores.Value += fmt.Sprintf("**#%d.** `%d` %s\n", i+1, counts[i], members[i])
	}
	if len(members) == 0 {
		allTimeScores.Value = "`N/A`"
	}

	counts, members, err = c.PeriodScores(s, limit)
	if err != nil {
		return nil, err
	}

	var periodicScores dg.MessageEmbedField
	periodicScores.Name = "This period:"
	periodicScores.Inline = true
	for i := range members {
		periodicScores.Value += fmt.Sprintf("**#%d.** `%d` %s\n", i+1, counts[i], members[i])
	}
	if len(members) == 0 {
		periodicScores.Value = "`N/A`"
	}

	embed := embeds.NewDefault(s.DB, c.GuildID)
	embed.Title = "🏆 Scoreboard"
	embed.Description = "Scores for " + channels.Mention(c.ChannelID) + "."
	embed.Timestamp = time.Now().Format(time.RFC3339)
	embed.Fields = append(embed.Fields,
		&periodicScores,
		&allTimeScores,
	)

	return &embed.MessageEmbed, nil
}

//GetMemberCounterScore returns the members score for the counter.
func (counter Counter) GetMemberCounterScore(DB db.Reader, guildID string, channelID string, memberID string) (allTimeScore int, periodScore int, err error) {

	row, err := DB.QueryRow(SQLSelectCounterScoreOfMember, guildID, channelID, memberID)
	if err != nil {
		return allTimeScore, periodScore, err
	}

	if err = row.Scan(&allTimeScore, &periodScore); err != nil && !errors.Is(err, sql.ErrNoRows) {
		return allTimeScore, periodScore, errors.WithStack(err)
	}

	return allTimeScore, periodScore, nil
}

//GetMemberAllCounterScores returns a member's statistics for all counters in a guild.
//`channelIDs` - channels containing counters. The index of the scores correlates to the index of the channel ID in this array.
//`periodScores` - an array of scores for counters for their current period.
//`allTimeScores` - an array of scores for counters for all time.
func GetMemberAllCounterScores(DB db.Reader, guildID string, memberID string) (channelIDs []string, periodScores []int, allTimeScores []int, err error) {

	rows, err := DB.Query(SQLSelectAllCounterScoresOfMember, guildID, memberID)
	if err != nil {
		return channelIDs, periodScores, allTimeScores, err
	}

	for rows.Next() {
		var channelID string
		var allTimeCount, periodicCount int
		if err = rows.Scan(
			&channelID,
			&allTimeCount,
			&periodicCount,
		); err != nil {
			return channelIDs, periodScores, allTimeScores, errors.WithStack(err)
		}

		channelIDs = append(channelIDs, channelID)
		allTimeScores = append(allTimeScores, allTimeCount)
		periodScores = append(periodScores, periodicCount)
	}

	return channelIDs, periodScores, allTimeScores, nil
}

/*
InitializeTimers resumes counter period timers.

Any errors that the timers encounter AFTER they are resumed are returned through the channel.
Other errors related to resuming the timers themselves are returned through the regular error.
*/
func InitializeTimers(s *states.State, guildID string) (chan error, error) {

	counters, err := RetrieveAll(s.DB, guildID)
	if err != nil {
		return nil, err
	}
	//We do not have any running timers.
	if len(counters) == 0 {
		return nil, nil
	}

	e := make(chan error, len(counters))
	for _, c := range counters {
		c.timerID = s.TimerManagers.Get(guildID).New(time.Until(c.Periodic.End), s, c.Alarm, e)
		if err = c.Store(s.DB); err != nil {
			s.TimerManagers.Get(guildID).Delete(c.timerID)
			return e, err
		}
	}
	return e, nil
}

//CatchUpOnMissedMessages checks if there are any new messages in any channels with a counter, and handles them, so the counter is up to date.
func CatchUpOnMissedMessages(s *states.State, webhookManager webhooks.Manager, guildID string) error {

	counters, err := RetrieveAll(s.DB, guildID)
	if err != nil {
		return err
	}

	for _, counter := range counters {

		//Retrieve a list of all messages since the latest processed message.
		var messages []*dg.Message
		lastMessageID := counter.lastMessageID //store lastMessageID in separate variable, so it doesn't get updated if we run into an error
		if lastMessageID == "" {
			//If no lastMessage has been set yet (counter is empty, newly set up)
			//then we do not need to catch up on any messages
			return nil
		}

		for {
			batch, err := s.ChannelMessages(counter.ChannelID, 100, "", lastMessageID, "")
			if err != nil {
				return err
			}
			if len(batch) == 0 {
				break
			}
			lastMessageID = batch[0].ID
			messages = append(messages, batch...)
		}
		counter.lastMessageID = lastMessageID
		if err := counter.Store(s.DB); err != nil {
			return err
		}

		//Handle the messages from oldest to newest.
		for i := range messages {
			i := len(messages) - i - 1 //reverse the order of the index
			if messages[i].Author.Bot {
				continue
			}
			messages[i].GuildID = guildID
			intr := states.NewInteraction(s, dg.Interaction{Message: messages[i]})
			if err := MessageCreateHandler(intr, webhookManager); err != nil {
				return err
			}
		}
	}

	return nil
}
