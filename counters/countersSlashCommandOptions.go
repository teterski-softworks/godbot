package counters

import dg "github.com/bwmarrin/discordgo"

var optionChannel = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionChannel,
	Name:        "channel",
	Description: "The channel in which the counter is set up in.",
	Required:    true,
	ChannelTypes: []dg.ChannelType{
		dg.ChannelTypeGuildText,
	},
}
