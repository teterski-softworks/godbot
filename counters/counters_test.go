package counters_test

import (
	"crypto/md5"
	"encoding/hex"
	"math"
	"math/rand"
	"reflect"
	"testing"
	"time"

	"gitlab.com/teterski-softworks/godbot/counters"
	"gitlab.com/teterski-softworks/godbot/db"
	"gitlab.com/teterski-softworks/godbot/members"
	"gitlab.com/teterski-softworks/godbot/tests"
)

var guildID, channelID = "guildID", "channelID"
var createTables = []func(db.ReadWriter) error{counters.CreateDatabaseTables, members.CreateDatabaseTables}

//TODO
func TestDatabaseAccess(t *testing.T) {

	expected := counters.New(guildID, channelID)

	//TODO: generate random values for counter.

	//TODO: generate new random values for counter
	updated := expected
	updated.AllTime.Winner = "allTimeWinnerMemberID"
	updated.LeaderBoard.ChannelID = "newChannelID2"
	updated.Periodic.Duration = time.Hour * 24

	retrieve := func(DB db.Reader) (counters.Counter, error) {
		return counters.Retrieve(DB, guildID, channelID)
	}

	tests.TestDatabaseAccess(
		t,
		expected, updated,
		createTables,
		retrieve,
	)
}

func TestChangeChannel(t *testing.T) {

	DB := tests.InitDatabase(t, createTables)
	defer tests.CloseDatabase(t, DB)

	expected := counters.New(guildID, channelID)
	if err := expected.Store(DB); err != nil {
		t.Fatalf("%+v", err)
	}

	if err := expected.ChangeChannel(DB, "newChannelID"); err != nil {
		t.Fatalf("%+v", err)
	}

	//TODO: change channel to a channel that is already in use.

	t.Run("Retrieve Edited", func(t *testing.T) {

		received, err := counters.Retrieve(DB, guildID, "newChannelID")
		if err != nil {
			t.Fatalf("%+v", err)
		}

		if !reflect.DeepEqual(expected, received) {
			t.Fatalf("\nRetrieve() returned wrong value.\nExpected: %v\nReceived: %v", expected, received)
		}
	})

}

//TODO
func TestUpdateMemberCount(t *testing.T) {
	DB := tests.InitDatabase(t, createTables)
	defer tests.CloseDatabase(t, DB)

	counter := counters.New(guildID, channelID)
	if err := counter.Store(DB); err != nil {
		t.Fatalf("%+v", err)
	}

	memberIDs := make(map[string]int)
	hash := md5.New()

	for len(memberIDs) < 40 {
		hash.Write([]byte{byte(rand.Intn(math.MaxInt8))})
		id := hex.EncodeToString(hash.Sum(nil))
		memberIDs[id] = rand.Int()
	}

	t.Run("IncrementMemberCounts", func(t *testing.T) {
		for id := range memberIDs {
			counter.IncrementMemberCounts(DB, id)
		}
	})

	t.Run("GetMemberCounts", func(t *testing.T) {
		//TODO
		t.SkipNow()
	})

	t.Run("SetMemberTotalCount", func(t *testing.T) {
		//TODO
		t.SkipNow()
	})
	t.Run("SetMemberPeriodicCount", func(t *testing.T) {
		//TODO
		t.SkipNow()
	})

	t.Run("GetEveryAllTimeCount", func(t *testing.T) {
		//TODO
		t.SkipNow()
		/*counterChannelIDs, countsAllTime, err := counters.GetEveryAllTimeCount(DB, guildID)
		if err != nil {
			t.Fatalf("%+v", err)
		}*/

	})

	//TODO!
}

func TestInitializeTimers(t *testing.T) {
	//TODO
	t.SkipNow()
}

func TestSlashCommands(t *testing.T) {
	//TODO
	t.SkipNow()
}
