package counters

import (
	"fmt"

	dg "github.com/bwmarrin/discordgo"
	"gitlab.com/teterski-softworks/godbot/channels"
	"gitlab.com/teterski-softworks/godbot/embeds"
)

var (
	ResponseCounterCreated = func(channelID string) *dg.MessageEmbed {
		embed := embeds.New("", "", "")
		embed.Title = "✅ Counter Created 🔢"
		embed.Color = embeds.ColorGreen
		embed.Description = fmt.Sprintf("A new counter in channel %s has been created.", channels.Mention(channelID))
		return &embed.MessageEmbed
	}

	ResponseCounterDeleted = func(channelID string) *dg.MessageEmbed {
		embed := embeds.New("", "", "")
		embed.Title = "✅ Counter Deleted 🚮"
		embed.Color = embeds.ColorGreen
		embed.Description = fmt.Sprintf("Counter has been removed from %s.", channels.Mention(channelID))
		return &embed.MessageEmbed
	}

	ResponseCounterNotFound = func(channelID string) *dg.MessageEmbed {
		embed := embeds.New("", "", "")
		embed.Title = "❌ Counter Not Found"
		embed.Color = embeds.ColorRed
		embed.Description = fmt.Sprintf("There is no counter set up in %s.", channels.Mention(channelID))
		return &embed.MessageEmbed
	}

	ResponseCounterAlreadyExists = func(channelID string) *dg.MessageEmbed {
		embed := embeds.New("", "", "")
		embed.Title = "❌ Already Exists"
		embed.Color = embeds.ColorRed
		embed.Description = fmt.Sprintf("A counter is already set up in %s.", channels.Mention(channelID))
		return &embed.MessageEmbed
	}

	ResponsePeriodReset = func(channelID string) *dg.MessageEmbed {
		embed := embeds.New("", "", "")
		embed.Title = "✅ Period Reset 🕒"
		embed.Color = embeds.ColorGreen
		embed.Description = fmt.Sprintf("%s counter's period has been reset.", channels.Mention(channelID))
		return &embed.MessageEmbed
	}
)
