package counters

import (
	"database/sql"
	"fmt"
	"time"

	dg "github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
	"gitlab.com/teterski-softworks/godbot/commands"
	"gitlab.com/teterski-softworks/godbot/db"
	"gitlab.com/teterski-softworks/godbot/durations"
	"gitlab.com/teterski-softworks/godbot/embeds"
	"gitlab.com/teterski-softworks/godbot/states"
)

//SlashCommandCounter - an application command group function for counter related functionality.
var SlashCommandCounter = commands.New(
	&dg.ApplicationCommand{
		Type:        dg.ChatApplicationCommand,
		Name:        "counter",
		Description: "Counter related commands. Admin only.",
		Options: []*dg.ApplicationCommandOption{
			slashCommandCounterNew,
			slashCommandCounterInfo,
			slashCommandCounterEdit,
			slashCommandCounterRemove,
			slashCommandCounterResetPeriod,
			slashCommandCounterMember,
		},
	},
	false,
	func(intr states.Interaction, options ...*dg.ApplicationCommandInteractionDataOption) (err error) {

		if err := intr.Acknowledge(); err != nil {
			return err
		}

		switch options[0].Type {
		case dg.ApplicationCommandOptionSubCommand:
			switch options[0].Name {
			case slashCommandCounterNew.Name:
				return slashCounterNew(intr, options[0].Options)
			case slashCommandCounterInfo.Name:
				return slashCounterInfo(intr, options[0].Options)
			case slashCommandCounterEdit.Name:
				return slashCounterEdit(intr, options[0].Options)
			case slashCommandCounterRemove.Name:
				return slashCounterRemove(intr, options[0].Options)
			case slashCommandCounterResetPeriod.Name:
				return slashCounterResetPeriod(intr, options[0].Options)
			case slashCommandCounterMember.Name:
				return slashCounterMember(intr, options[0].Options)
			}
		}
		return intr.EditResponseEmbeds(nil, states.ResponseGenericError(nil))
	},
)

//slashCommandCounterNew - A subcommand to create a new counter.
var slashCommandCounterNew = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionSubCommand,
	Name:        "new",
	Description: "Creates a new counter.",
	Options: []*dg.ApplicationCommandOption{
		{
			Type:        dg.ApplicationCommandOptionChannel,
			Name:        "channel",
			Description: "The channel to be used for the new counter.",
			Required:    true,
			ChannelTypes: []dg.ChannelType{
				dg.ChannelTypeGuildText,
			},
		}, {
			Type:        dg.ApplicationCommandOptionString,
			Name:        "period",
			Description: "The period duration used for the periodic count. Must be in format `0w0d0h0m0s`",
			Required:    true,
		}, {
			Type:        dg.ApplicationCommandOptionRole,
			Name:        "role_reward_alltime",
			Description: "The role to be rewarded to the member with the highest all time counter.",
			Required:    true,
		}, {
			Type:        dg.ApplicationCommandOptionRole,
			Name:        "role_reward_period",
			Description: "The role to be rewarded to the member with the highest counter in the period.",
			Required:    true,
		}, {
			Type:        dg.ApplicationCommandOptionChannel,
			Name:        "leaderboard_channel",
			Description: "The channel to be used for posting the leader board.",
			Required:    true,
			ChannelTypes: []dg.ChannelType{
				dg.ChannelTypeGuildText,
			},
		},
	},
}

//slashCounterNew creates a counter in a channel.
func slashCounterNew(intr states.Interaction, options []*dg.ApplicationCommandInteractionDataOption) error {

	var err error

	channel := options[0].ChannelValue(intr.Session)
	duration := options[1].StringValue()

	guildID := intr.GuildID

	//Check if a counter already exists in the channel
	if _, err := Retrieve(intr.DB, guildID, channel.ID); err == nil {
		//A counter already exists
		return intr.EditResponseEmbeds(errors.WithStack(err), ResponseCounterAlreadyExists(channel.ID))
	}

	counter := New(guildID, channel.ID)
	counter.AllTime.Reward = options[2].RoleValue(intr.Session, guildID).ID
	counter.Periodic.Reward = options[3].RoleValue(intr.Session, guildID).ID
	counter.Periodic.Duration, err = durations.Parse(duration)
	if err != nil {
		return intr.EditResponseEmbeds(nil, durations.ResponseInvalidDurationError())
	}
	counter.Periodic.End = time.Now().Add(counter.Periodic.Duration)
	counter.LeaderBoard.ChannelID = options[4].ChannelValue(intr.Session).ID

	//Create a leader board
	embed, err := counter.LeaderBoardEmbed(intr.State, 10)
	if err != nil {
		return intr.EditResponseEmbeds(err, states.ResponseGenericError(err))
	}

	msg, err := intr.ChannelMessageSendEmbed(counter.LeaderBoard.ChannelID, embed)
	if err != nil {
		return intr.EditResponseEmbeds(errors.WithStack(err), states.ResponseGenericError(err))
	}
	counter.LeaderBoard.MsgID = msg.ID

	e := make(chan error, 1)
	defer close(e)
	counter.timerID = intr.TimerManagers.Get(intr.GuildID).New(counter.Periodic.Duration, intr.State, counter.Alarm, e)

	if err = counter.Store(intr.DB); err != nil {
		intr.TimerManagers.Get(intr.GuildID).Delete(counter.timerID)
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}

	if err = intr.EditResponseEmbeds(nil, ResponseCounterCreated(channel.ID)); err != nil {
		return err
	}
	return <-e
}

//slashCommandCounterInfo - A subcommand to show counter info.
var slashCommandCounterInfo = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionSubCommand,
	Name:        "info",
	Description: "Views information on a counter.",
	Options: []*dg.ApplicationCommandOption{
		optionChannel,
	},
}

//slashCounterInfo shows information about a counter in a channel.
func slashCounterInfo(intr states.Interaction, options []*dg.ApplicationCommandInteractionDataOption) error {

	channel := options[0].ChannelValue(intr.Session)
	guildID := intr.GuildID

	counter, err := Retrieve(intr.DB, guildID, channel.ID)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return intr.EditResponseEmbeds(nil, ResponseCounterNotFound(channel.ID))
		}
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}

	embed, err := counter.Embed(intr)
	if err != nil {
		return intr.EditResponseEmbeds(err, states.ResponseGenericError(err))
	}

	return intr.EditResponseEmbeds(nil, embed)
}

//slashCommandCounterEdit - A subcommand to edit a counter.
var slashCommandCounterEdit = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionSubCommand,
	Name:        "edit",
	Description: "Views information on a counter.",
	Options: []*dg.ApplicationCommandOption{
		optionChannel,
		{
			Type:        dg.ApplicationCommandOptionChannel,
			Name:        "new_channel",
			Description: "The channel to move the counter to.",
			ChannelTypes: []dg.ChannelType{
				dg.ChannelTypeGuildText,
			},
		}, {
			Type:        dg.ApplicationCommandOptionString,
			Name:        "period",
			Description: "The period duration used for the periodic count. Must be in format `0w0d0h0m0s`",
		}, {
			Type:        dg.ApplicationCommandOptionRole,
			Name:        "role_reward_alltime",
			Description: "The role to be rewarded to the member with the highest all time counter.",
		}, {
			Type:        dg.ApplicationCommandOptionRole,
			Name:        "role_reward_period",
			Description: "The role to be rewarded to the member with the highest counter in the period.",
		}, {
			Type:        dg.ApplicationCommandOptionChannel,
			Name:        "leaderboard_channel",
			Description: "The channel to be used for posting the leader board.",
			ChannelTypes: []dg.ChannelType{
				dg.ChannelTypeGuildText,
			},
		}, {
			Type:        dg.ApplicationCommandOptionInteger,
			Name:        "current_count",
			Description: "Set the current count of the counter.",
		},
	},
}

//slashCounterEdit edits a counter.
func slashCounterEdit(intr states.Interaction, options []*dg.ApplicationCommandInteractionDataOption) error {

	channel := options[0].ChannelValue(intr.Session)
	guildID := intr.GuildID

	counter, err := Retrieve(intr.DB, guildID, channel.ID)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return intr.EditResponseEmbeds(nil, ResponseCounterNotFound(channel.ID))
		}
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}

	//Apply channel move first, so that all other changes are properly applied after
	for _, option := range options {
		switch option.Name {
		case "new_channel":
			if err := counter.ChangeChannel(intr.DB, option.ChannelValue(intr.Session).ID); err != nil {
				return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
			}
			counter.ChannelID = option.ChannelValue(intr.Session).ID
		}
	}
	for _, option := range options {
		switch option.Name {
		case "period":
			duration, err := durations.Parse(option.StringValue())
			if err != nil {
				return intr.EditResponseEmbeds(nil, durations.ResponseInvalidDurationError())
			}
			counter.Periodic.Duration = duration
			counter.Periodic.End = time.Now().Add(duration)
			intr.TimerManagers.Get(intr.GuildID).Reset(counter.timerID, duration)
		case "role_reward_alltime":
			counter.AllTime.Reward = option.RoleValue(intr.Session, intr.GuildID).ID
		case "role_reward_period":
			counter.Periodic.Reward = option.RoleValue(intr.Session, intr.GuildID).ID
		case "leaderboard_channel":

			counter.LeaderBoard.ChannelID = option.ChannelValue(intr.Session).ID

			embed, err := counter.LeaderBoardEmbed(intr.State, 10)
			if err != nil {
				return intr.EditResponseEmbeds(err, states.ResponseGenericError(err))
			}
			msg, err := intr.ChannelMessageSendEmbed(counter.LeaderBoard.ChannelID, embed)
			if err != nil {
				return intr.EditResponseEmbeds(err, states.ResponseGenericError(err))
			}
			counter.LeaderBoard.MsgID = msg.ID

		case "current_count":
			counter.AllTime.Count = int(option.IntValue())
		}
	}
	if err := counter.Store(intr.DB); err != nil {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}

	embed, err := counter.Embed(intr)
	if err != nil {
		return intr.EditResponseEmbeds(err, states.ResponseGenericError(err))
	}
	return intr.EditResponseEmbeds(nil, embed)
}

//slashCommandCounterRemove - A subcommand to remove a counter from a channel.
var slashCommandCounterRemove = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionSubCommand,
	Name:        "remove",
	Description: "Removes a counter from a channel.",
	Options: []*dg.ApplicationCommandOption{
		optionChannel,
	},
}

//slashCounterRemove deleted a counter, and all of its info, from a channel.
func slashCounterRemove(intr states.Interaction, options []*dg.ApplicationCommandInteractionDataOption) error {

	//TODO: add confirmation menu

	counter, err := Retrieve(intr.DB, intr.GuildID, options[0].ChannelValue(intr.Session).ID)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return intr.EditResponseEmbeds(nil, ResponseCounterNotFound(counter.ChannelID))
		}
		return err
	}

	//Delete the timer associated with the counter
	intr.TimerManagers.Get(intr.GuildID).Delete(counter.timerID)

	if _, err := counter.Delete(intr.DB); err != nil {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}

	return intr.EditResponseEmbeds(nil, ResponseCounterDeleted(counter.ChannelID))
}

//slashCommandCounterResetPeriod - A subcommand to restart a counter's period.
var slashCommandCounterResetPeriod = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionSubCommand,
	Name:        "reset_period",
	Description: "Resets a counter's period timer.",
	Options: []*dg.ApplicationCommandOption{
		optionChannel,
	},
}

//slashCounterResetPeriod restarts a counter's period.
func slashCounterResetPeriod(intr states.Interaction, options []*dg.ApplicationCommandInteractionDataOption) error {

	channel := options[0].ChannelValue(intr.Session)
	guildID := intr.GuildID

	counter, err := Retrieve(intr.DB, guildID, channel.ID)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return intr.EditResponseEmbeds(nil, ResponseCounterNotFound(channel.ID))
		}
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}

	if !intr.TimerManagers.Get(intr.GuildID).Fire(counter.timerID) {
		err = errors.New("unable to fire counter timer")
		return intr.EditResponseEmbeds(err, states.ResponseGenericError(err))
	}

	return intr.EditResponseEmbeds(nil, ResponsePeriodReset(counter.ChannelID))
}

//slashCommandCounterMember - A subcommand for updating a member's count.
var slashCommandCounterMember = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionSubCommand,
	Name:        "member",
	Description: "Updates a member's score for a counter.",
	Options: []*dg.ApplicationCommandOption{
		optionChannel,
		{
			Type:        dg.ApplicationCommandOptionUser,
			Name:        "member",
			Description: "The member whose scores to edit.",
			Required:    true,
		},
		{
			Type:        dg.ApplicationCommandOptionString,
			Name:        "type",
			Description: "The score type to edit.",
			Required:    true,
			Choices: []*dg.ApplicationCommandOptionChoice{
				{Name: "Period", Value: "period"},
				{Name: "All Time", Value: "alltime"},
			},
		},
		{
			Type:        dg.ApplicationCommandOptionInteger,
			Name:        "count",
			Description: "The number to which to set the member's total count to.",
			Required:    true,
		},
	},
}

//slashCounterMember updates a member's count
func slashCounterMember(intr states.Interaction, options []*dg.ApplicationCommandInteractionDataOption) error {

	guildID := intr.GuildID
	channel := options[0].ChannelValue(intr.Session)
	member := options[1].UserValue(intr.Session)
	mode := options[2].StringValue()
	count := int(options[3].IntValue())

	counter, err := Retrieve(intr.DB, guildID, channel.ID)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return intr.EditResponseEmbeds(nil, ResponseCounterNotFound(channel.ID))
		}
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}

	allTimeScore, periodScore, err := counter.GetMemberCounterScore(intr.DB, guildID, channel.ID, member.ID)
	if err != nil {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}

	if mode == "period" {
		periodScore = count
	}
	if mode == "alltime" {
		allTimeScore = count
	}

	t := intr.DB.NewTransaction()

	if err := counter.SetMemberScore(t, member.ID, periodScore, allTimeScore); err != nil {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}

	//Update counter with new winners
	if err = counter.UpdateWinners(t, intr.Session); err != nil {
		return err
	}

	//Update scores to leaderboard channel
	if err = counter.UpdateLeaderBoard(intr.State, 10); err != nil {
		return err
	}

	//Respond to the user
	embed := embeds.NewDefault(intr.DB, intr.GuildID)
	embed.Title = "✅ Score Updated"
	embed.Thumbnail.URL = member.AvatarURL("")
	embed.Timestamp = time.Now().Format(time.RFC3339)
	embed.Description = fmt.Sprintf("The current %s scores for %s have been updated to:", channel.Mention(), member.Mention())
	embed.Fields = append(embed.Fields,
		&dg.MessageEmbedField{
			Name:   "All Time Score",
			Value:  fmt.Sprintf("`%d`", allTimeScore),
			Inline: true,
		}, &dg.MessageEmbedField{
			Name:   "This Period",
			Value:  fmt.Sprintf("`%d`", periodScore),
			Inline: true,
		})

	if err = intr.EditResponseEmbeds(nil, &embed.MessageEmbed); err != nil {
		return err
	}
	return t.Commit()
}
