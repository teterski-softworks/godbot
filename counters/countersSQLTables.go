package counters

import "gitlab.com/teterski-softworks/godbot/db"

//CreateDatabaseTables creates the required database tables for the counters package.
func CreateDatabaseTables(DB db.ReadWriter) error {
	t := DB.NewTransaction()
	t.Queue(SQLCreateTableCounters)
	t.Queue(SQLCreateTableCounterPerMember)
	return t.Commit()
}

var (
	SQLCreateTableCounters string = `
	CREATE TABLE IF NOT EXISTS counters(
		guildID               TEXT NOT NULL,
		channelID             TEXT NOT NULL,

		allTime_count         UNSIGNED INT DEFAULT 0,
		allTime_reward        TEXT NOT NULL,
		allTime_winner        TEXT DEFAULT "",

		periodic_count        UNSIGNED INT DEFAULT 0,
		periodic_reward       TEXT NOT NULL,
		periodic_winner       TEXT DEFAULT "",
		periodic_duration     INT NOT NULL,
		periodic_end          DATETIME NOT NULL,
		
		leaderBoard_channelID TEXT NOT NULL,
		leaderBoard_msgID     TEXT NOT NULL,

		lastMessageID         TEXT NOT NULL,

		timerID               INT NOT NULL,
		
		PRIMARY KEY(guildID, channelID),
		FOREIGN KEY(guildID, channelID) REFERENCES channels(guildID, channelID) ON DELETE CASCADE
		//TODO: allow null in case channel/message gets deleted?
		--FOREIGN KEY(guildID, leaderBoard_channelID)  REFERENCES channels(guildID, channelID) ON DELETE SET DEFAULT,
		--FOREIGN KEY(guildID, leaderBoard_channelID, leaderBoard_msgID)  REFERENCES messages(guildID, channelID, messageID) ON DELETE SET DEFAULT
	);`

	SQLCreateTableCounterPerMember string = `
	CREATE TABLE IF NOT EXISTS counterPerMember(
		guildID       TEXT NOT NULL,
		channelID     TEXT NOT NULL,
		memberID      TEXT NOT NULL,
		
		allTime_count  INT DEFAULT 0,
		periodic_count INT DEFAULT 0,

		PRIMARY KEY (guildID, channelID, memberID),
		FOREIGN KEY (guildID, channelID) REFERENCES counters(guildID, channelID) ON DELETE CASCADE,
		FOREIGN KEY (guildID, memberID) REFERENCES members(guildID, memberID) ON DELETE CASCADE
	);`
)
