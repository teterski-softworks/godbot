package helpMenus

import (
	"strconv"
	"strings"

	dg "github.com/bwmarrin/discordgo"
	"gitlab.com/teterski-softworks/godbot/states"
)

//PageSelectionHandler handles the selection of pages for a help menu.
func PageSelectionHandler(intr states.Interaction, h *HelpMenu, customID string, customIDPrefix string) error {

	page, err := strconv.Atoi(strings.TrimPrefix(customID, customIDPrefix))
	if err != nil {
		return intr.RespondEphemeralEmbeds(err, states.ResponseGenericError(err))
	}

	response := &dg.InteractionResponse{
		Type: dg.InteractionResponseUpdateMessage,
		Data: &dg.InteractionResponseData{
			Embeds:     h.Embeds(page),
			Components: h.Components(page),
		},
	}
	return intr.InteractionRespond(&intr.Interaction, response)
}
