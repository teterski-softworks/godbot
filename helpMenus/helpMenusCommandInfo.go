package helpMenus

import (
	"fmt"

	dg "github.com/bwmarrin/discordgo"
	"gitlab.com/teterski-softworks/godbot/commands"
)

//MenuInfo maps command names with the corresponding command help information.
type MenuInfo map[ /*command name*/ string]commandInfo

//commandInfo stores information about an application command.
type commandInfo struct {
	description      string
	subCommandGroups []subCommandGroupInfo
	subCommands      []subCommandInfo
	options          []optionInfo
}

//subCommandGroupInfo stores information about a subcommand group.
type subCommandGroupInfo struct {
	name        string
	description string
	subCommands []subCommandInfo
}

//subCommandGroupInfo stores information about a subcommand.
type subCommandInfo struct {
	name        string
	description string
	options     []optionInfo
}

//optionInfo stores information about an application command option.
type optionInfo struct {
	name        string
	description string
	required    bool
}

//NewMenuInfo generates and returns a MenuInfo map from the provided Commander array.
func NewMenuInfo(commandList []commands.Commander) MenuInfo {
	info := make(map[string]commandInfo)
	for _, c := range commandList {
		cmdInfo := commandInfo{
			description: c.Description(),
		}
		for _, option := range c.ApplicationCommand().Options {

			if option.Type == dg.ApplicationCommandOptionSubCommandGroup {
				cmdInfo.subCommandGroups = append(cmdInfo.subCommandGroups, subCommandGroupInfoFromOption(option))
				continue
			}

			if option.Type == dg.ApplicationCommandOptionSubCommand {
				cmdInfo.subCommands = append(cmdInfo.subCommands, subCommandInfoFromOption(option))
				continue
			}

			optionInfo := optionInfo{
				name:        option.Name,
				description: option.Description,
				required:    option.Required,
			}
			cmdInfo.options = append(cmdInfo.options, optionInfo)
		}
		info[c.Name()] = cmdInfo
	}
	return info
}

//commandHelp generates help menu information for a command.
func (info MenuInfo) commandHelp(commandName string) string {
	var optionList string
	for _, option := range info[commandName].options {
		required := ""
		if !option.required {
			required = " (optional)"
		}
		optionList += fmt.Sprintf("`%s%s`: *%s*\n", option.name, required, option.description)
	}
	return fmt.Sprintf("**```%s```**%s\n%s\n", commandName, info[commandName].description, optionList)
}

//subCommandHelp generates help menu information for a sub command.
func (info MenuInfo) subCommandHelp(commandName string, groupName string, group []subCommandInfo) string {
	var details string
	if groupName != "" {
		groupName += " "
	}
	for _, subCommand := range group {
		var optionList string
		for _, option := range subCommand.options {
			required := ""
			if !option.required {
				required = " (optional)"
			}
			optionList += fmt.Sprintf("`%s%s`: *%s*\n", option.name, required, option.description)
		}
		details += fmt.Sprintf("**```%s %s%s```**%s\n%s\n", commandName, groupName, subCommand.name, subCommand.description, optionList)
	}
	return details
}

//subCommandGroupHelp generates help menu information for a sub command group.
func (info MenuInfo) subCommandGroupHelp(commandName string) string {
	var details string
	for _, group := range info[commandName].subCommandGroups {
		details += info.subCommandHelp(commandName, group.name, group.subCommands)
	}
	return details
}

//subCommandInfoFromOption retrieves sub command group help menu information from a command option.
func subCommandGroupInfoFromOption(option *dg.ApplicationCommandOption) subCommandGroupInfo {
	info := subCommandGroupInfo{
		name:        option.Name,
		description: option.Description,
	}
	for _, option := range option.Options {
		if option.Type == dg.ApplicationCommandOptionSubCommand {
			info.subCommands = append(info.subCommands, subCommandInfoFromOption(option))
		}
	}
	return info
}

//subCommandInfoFromOption retrieves sub command help menu information from a command option.
func subCommandInfoFromOption(option *dg.ApplicationCommandOption) subCommandInfo {
	info := subCommandInfo{
		name:        option.Name,
		description: option.Description,
	}
	for _, option := range option.Options {
		optionInfo := optionInfo{
			name:        option.Name,
			description: option.Description,
			required:    option.Required,
		}
		info.options = append(info.options, optionInfo)
	}
	return info
}
