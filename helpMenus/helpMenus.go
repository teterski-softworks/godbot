package helpMenus

import (
	"sort"

	dg "github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
	"gitlab.com/teterski-softworks/godbot/commands"
	"gitlab.com/teterski-softworks/godbot/db"
	"gitlab.com/teterski-softworks/godbot/embeds"
	"gitlab.com/teterski-softworks/godbot/states"
)

const (
	CustomIDPrefixHelp      = "help"
	CustomIDPrefixHelpAdmin = "helpAdmin"
)

//HelpMenu is a menu that provides information on commands.
type HelpMenu struct {
	MenuInfo           //Maps command group names with the corresponding command help information.
	commands.Commander //The command to call the help menu with.
	embeds.Book
}

//New returns a new HelpMenu.
func New(name string, title string, customID string, description string, defaultPermissions bool, commandList []commands.Commander) *HelpMenu {
	var h HelpMenu
	h.MenuInfo = NewMenuInfo(commandList)
	h.Book = h.generateBook(title, customID)
	function := func(intr states.Interaction, options ...*dg.ApplicationCommandInteractionDataOption) (err error) {
		if err := intr.Acknowledge(); err != nil {
			return err
		}
		embed := h.Book.Embeds(0)[0]
		embedSettings, err := embeds.RetrieveSettings(intr.DB, intr.GuildID)
		if err != nil {
			return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
		}
		embed.Color = embedSettings.DefaultColor
		response := dg.WebhookEdit{
			Embeds:     []*dg.MessageEmbed{embed},
			Components: h.Book.Components(0),
		}
		if _, err := intr.State.InteractionResponseEdit(&intr.Interaction, &response); err != nil {
			return errors.Wrapf(err, "unable to respond")
		}
		return nil
	}
	h.generateCommander(name, description, defaultPermissions, function, commandList)
	return &h
}

//generateCommander is a helper function that generates the HelpMenuAdvanced's Commander which is used for calling the help menu.
//The command's parameter choices are generated based on what command groups have been added to the HelpMenuAdvanced.
func (h *HelpMenu) generateCommander(name string, description string, defaultPermissions bool, function commands.CmdFunc, commandList []commands.Commander) {

	h.Commander = commands.New(
		&dg.ApplicationCommand{
			Type:        dg.ChatApplicationCommand,
			Name:        name,
			Description: description,
		},
		defaultPermissions,
		function,
	)
}

//Adds a Commander's info to the Help menu.
func (h *HelpMenu) Add(c commands.Commander) {
	cmdInfo := commandInfo{
		description: c.Description(),
	}
	for _, option := range c.ApplicationCommand().Options {

		if option.Type == dg.ApplicationCommandOptionSubCommandGroup {
			cmdInfo.subCommandGroups = append(cmdInfo.subCommandGroups, subCommandGroupInfoFromOption(option))
			continue
		}

		if option.Type == dg.ApplicationCommandOptionSubCommand {
			cmdInfo.subCommands = append(cmdInfo.subCommands, subCommandInfoFromOption(option))
			continue
		}

		optionInfo := optionInfo{
			name:        option.Name,
			description: option.Description,
			required:    option.Required,
		}
		cmdInfo.options = append(cmdInfo.options, optionInfo)
	}
	h.MenuInfo[c.Name()] = cmdInfo
}

//generateBook creates the book of embeds which is used to display the help menu.
func (info MenuInfo) generateBook(title string, customID string) embeds.Book {

	embed := embeds.New("", "", "")
	embed.Title = title
	book := embeds.NewBook(embed, customID)

	var cmdNames []string
	for name := range info {
		cmdNames = append(cmdNames, name)
	}
	sort.Strings(cmdNames)

	for _, name := range cmdNames {
		var description string
		commandHelp := info.commandHelp(name)
		subCommandGroupHelp := info.subCommandGroupHelp(name)
		subCommandHelp := info.subCommandHelp(name, "", info[name].subCommands)
		if subCommandGroupHelp == "" {
			if subCommandHelp == "" {
				description += commandHelp
			} else if subCommandHelp != "" {
				description += subCommandHelp
			}
		} else {
			description += subCommandGroupHelp + subCommandHelp
		}
		book.Add(description)
	}

	return book
}
