package db_test

import (
	"testing"

	"gitlab.com/teterski-softworks/godbot/db"
)

var testDBFilePath = "/db_test.db"
var testDBKey = "key"

func TestInterfaces(t *testing.T) {
	//Ensures structs implement the correct interfaces
	var _ db.Database = new(db.DB)
	var _ db.ReadWriter = new(db.DB)
	var _ db.Reader = new(db.DB)

	var _ db.Transaction = new(db.TransactionQueue)
	var _ db.ReadWriter = new(db.TransactionQueue)
	var _ db.Reader = new(db.TransactionQueue)

	var _ db.Encryption = new(db.Encrypter)
}

func TestDatabase(t *testing.T) {
	var DB db.Database
	var err error
	testDBFilePath = t.TempDir() + testDBFilePath
	tableSize := 4

	//(Database).Close() error checking
	CloseDatabase := func(t *testing.T) {
		if err := DB.Close(); err != nil {
			t.Fatalf("%+v", err)
		}
	}
	defer t.Run("CloseDatabase", CloseDatabase)

	//(Database).Open() error checking
	OpenDatabase := func(t *testing.T) {
		if DB, err = db.Open(testDBFilePath, testDBKey); err != nil {
			t.Fatalf("%+v", err)
		}
	}

	//Tests table creation
	CreateTable := func(t *testing.T) {
		if err := DB.QueueAndCommit("CREATE TABLE test(number INT NOT NULL);"); err != nil {
			t.Fatalf("%+v", err)
		}
	}

	//Tests inserting into table
	PopulateTable := func(t *testing.T) {
		transaction := DB.NewTransaction()
		for i := 1; i <= tableSize; i++ {
			transaction.Queue("INSERT INTO test(number) VALUES (?1);", i)
		}
		if err := transaction.Commit(); err != nil {
			t.Fatalf("%+v", err)
		}
	}

	//Tests successful querying table
	QueryTable := func(t *testing.T) {

		rows, err := DB.Query("SELECT * FROM test ORDER BY number DESC;")
		if err != nil {
			t.Fatalf("%+v", err)
		}

		var received int
		expected := tableSize
		for rows.Next() {
			if rows.Scan(&received); err != nil {
				t.Fatalf("%+v", err)
			}
			if received != expected {
				t.Fatalf("(*DB).Query() did not match expected result. Expected %d, Received %d", expected, received)
			}
			expected--
		}
		if expected != 0 {
			t.Fatal("incorrect number of table entries")
		}
	}

	//Tests successful querying a single row from a table
	//TODO: redo
	QueryRow := func(t *testing.T) {

		expected := tableSize

		row, err := DB.QueryRow("SELECT * FROM test WHERE number=(?);", expected)
		if err != nil {
			t.Fatalf("%+v", err)
		}
		var received int
		if err := row.Scan(&received); err != nil {
			t.Fatalf("%+v", err)
		}
		if received != expected {
			t.Fatalf("(Reader).QueryRow() did not match expected result. Expected %d, Received %d", expected, received)
		}
	}

	//(*TransactionQueue).exec() - (*sql.DB).Prepare() error - Missing database
	MissingDB := func(t *testing.T) {
		//This creates a database that will be deleted from disk once the function finishes running.
		//not so much used as test, but more so as a way to test error handling in case of database deletion from disk.
		t.Run("CreateAndDeleteDatabase", func(t *testing.T) {
			pathWillBeClearedAfterUse := t.TempDir() + testDBFilePath
			if DB, err = db.Open(pathWillBeClearedAfterUse, testDBKey); err != nil {
				t.Fatalf("%+v", err)
			}
		})
		if err := DB.NewTransaction().Commit(); err == nil {
			t.Fatal("(*TransactionQueue).exec() did not error out when expected")
		}
	}

	//Ensures SQL errors are properly handled.
	SQLErrors := func(t *testing.T) {

		t.Run("Query", func(t *testing.T) {

			//(db.Reader).Query() - (*sql.Stmt).Query() error - Incorrect number of arguments
			if _, err := DB.Query("SELECT * FROM test WHERE number=(?1);"); err == nil {
				t.Fatal("(*TransactionQueue).exec() did not error out when expected")
			}
			//(db.Reader).Query() - (*sql.Stmt).Query() error - Incorrect number of arguments
			if _, err := DB.Query("SELECT * FROM test WHERE number=(?1);", 1, "extra parameter"); err == nil {
				t.Fatal("(*TransactionQueue).exec() did not error out when expected")
			}

			//(db.Reader).Query() - (*sql.Stmt).Query() error - Invalid table name.
			if _, err = DB.Query("SELECT * FROM invalidTableName;"); err == nil {
				t.Fatal("(db.Reader).Query() did not error out when expected")
			}
			//(db.Reader).QueryRow() - (*sql.Stmt).Query() error - Invalid table name.
			if _, err = DB.QueryRow("SELECT * FROM invalidTableName;"); err == nil {
				t.Fatal("(Reader).QueryRow() did not error out when expected")
			}

			//(db.Reader).Query() - (*sql.Stmt).Query() error - Invalid column name.
			if _, err = DB.Query("SELECT * FROM test WHERE invalidColumnName;"); err == nil {
				t.Fatal("(db.Reader).Query() did not error out when expected")
			}
			//(db.Reader).QueryRow() - (*sql.Stmt).Query() error - Invalid column name.
			if _, err = DB.QueryRow("SELECT * FROM test WHERE invalidColumnName=(?);"); err == nil {
				t.Fatal("(Reader).QueryRow() did not error out when expected")
			}

			//TODO: doesn't error out on multiple statements.
			//(*db.Reader).Query() - (*sql.Stmt).Query() error - Incorrect number of arguments
			/*if _, err := DB.NewTransaction().Query("SELECT * FROM test WHERE number=(?1); SELECT * FROM test WHERE number=(?2);", 1); err == nil {
				fmt.Printf("%+v\n", err)
				t.Fatal("(*TransactionQueue).exec() did not error out when expected")
			}*/

		})

		t.Run("QueueAndCommit", func(t *testing.T) {
			//(*TransactionQueue).exec() - (*sql.DB).Prepare() error - Syntax error
			if err := DB.QueueAndCommit("InvalidSQL;"); err == nil {
				t.Fatal("(*TransactionQueue).exec() did not error out when expected")
			}
			//(*TransactionQueue).exec() - (*sql.Stmt).Exec() error - Incorrect number of arguments
			if err := DB.QueueAndCommit("INSERT INTO test(number) VALUES (?1);"); err == nil {
				t.Fatal("(*TransactionQueue).exec() did not error out when expected")
			}
			//(*TransactionQueue).exec() - (*sql.Stmt).Exec() error - Incorrect number of arguments
			if err := DB.QueueAndCommit("INSERT INTO test(number) VALUES (?1);", 1, "extra parameter"); err == nil {
				t.Fatal("(*TransactionQueue).exec() did not error out when expected")
			}

			//(*TransactionQueue).exec() - (*sql.Stmt).Query() error - Constraint failure
			if err := DB.QueueAndCommit("INSERT INTO test(number) VALUES (?1);", nil); err == nil {
				t.Fatal("(*TransactionQueue).exec() did not error out when expected")
			}
		})

		//we need to repeat the same tests with Queue and Commit separately
		t.Run("Queue first, and then Commit", func(t *testing.T) {
			transaction := DB.NewTransaction()
			//(*TransactionQueue).exec() - (*sql.DB).Prepare() error - Syntax error
			transaction.Queue("(*TransactionQueue).exec() did not error out when expected")
			if err := transaction.Commit(); err == nil {
				t.Fatal("(*TransactionQueue).exec() did not error out when expected")
			}
			//(*TransactionQueue).exec() - (*sql.Stmt).Exec() error - Incorrect number of arguments
			transaction.Queue("INSERT INTO test(number) VALUES (?1);")
			if err := transaction.Commit(); err == nil {
				t.Fatal("(*TransactionQueue).exec() did not error out when expected")
			}
			//(*TransactionQueue).exec() - (*sql.Stmt).Exec() error - Incorrect number of arguments
			transaction.Queue("INSERT INTO test(number) VALUES (?1);", 1, "extra parameter")
			if err := transaction.Commit(); err == nil {
				t.Fatal("(*TransactionQueue).exec() did not error out when expected")
			}

			//(*TransactionQueue).exec() - (*sql.Stmt).Query() error - Constraint failure
			transaction.Queue("INSERT INTO test(number) VALUES (?1);", nil)
			if err := transaction.Commit(); err == nil {
				t.Fatal("(*TransactionQueue).exec() did not error out when expected")
			}
		})
	}

	//(Transaction).Commit() - makes sure that transactions can only be committed once.
	TransactionErrors := func(t *testing.T) {

		transaction := DB.NewTransaction()
		if err := transaction.Commit(); err != nil {
			t.Fatalf("%+v", err)
		}
		if err := transaction.Commit(); err == nil {
			t.Fatal("(Transaction).Commit() did not error out when expected")
		}

		//TODO!: Why wont this error out?
		//if _, err := DB.NewTransaction().QueueAndCommit(db.SQLBeginTransaction); err == nil {
		//	t.Fatal("(*TransactionQueue).exec() did not error out when expected")
		//}
		transaction = DB.NewTransaction()
		transaction.Queue(db.SQLBeginTransaction)
		if err := transaction.Commit(); err == nil {
			t.Fatal("(*TransactionQueue).exec() did not error out when expected")
		}

		if err := DB.QueueAndCommit(db.SQLCommitTransaction); err == nil {
			t.Fatal("(*TransactionQueue).exec() did not error out when expected")
		}
		transaction = DB.NewTransaction()
		transaction.Queue(db.SQLCommitTransaction)
		if err := transaction.Commit(); err == nil {
			t.Fatal("(*TransactionQueue).exec() did not error out when expected")
		}

		if err := DB.QueueAndCommit(db.SQLRollbackTransaction); err == nil {
			t.Fatal("(*TransactionQueue).exec() did not error out when expected")
		}
		transaction = DB.NewTransaction()
		transaction.Queue(db.SQLRollbackTransaction)
		if err := transaction.Commit(); err == nil {
			t.Fatal("(*TransactionQueue).exec() did not error out when expected")
		}
	}

	//TODO
	NestedTransaction := func(t *testing.T) {
		t.Run("Commit", func(t *testing.T) {
			t.Run("Parent Transaction", func(t *testing.T) {
				transaction := DB.NewTransaction()
				for i := tableSize + 1; i <= tableSize*2; i++ {
					transaction.Queue("INSERT INTO test(number) VALUES (?1);", i)
				}
				t.Run("Nested Transaction", func(t *testing.T) {
					nestedTransaction := transaction.NewTransaction()
					for i := (tableSize * 2) + 1; i <= tableSize*3; i++ {
						nestedTransaction.Queue("INSERT INTO test(number) VALUES (?1);", i)
					}
					if err := nestedTransaction.Commit(); err != nil {
						t.Fatalf("%+v", err)
					}
				})
				if err := transaction.Commit(); err != nil {
					t.Fatalf("%+v", err)
				}
			})
			tableSize *= 3
			t.Run("Check table", QueryTable)
		})

		//TODO
		t.Run("Rollback", func(t *testing.T) {
			t.Run("Parent Transaction", func(t *testing.T) {
				transaction := DB.NewTransaction()
				tableSize++
				transaction.Queue("INSERT INTO test(number) VALUES (?1);", tableSize)

				t.Run("Nested Transaction", func(t *testing.T) {
					nestedTransaction := transaction.NewTransaction()
					nestedTransaction.Queue("INSERT INTO test(number) VALUES (?1);", tableSize+1)
					//No commit of nested transaction
				})
				if err := transaction.Commit(); err != nil {
					t.Fatalf("%+v", err)
				}
			})
			t.Run("Check table", QueryRow)
		})

		//TODO! not implemented
		t.Run("Parallel", func(t *testing.T) {
			t.SkipNow() //TODO: remove
			transaction := DB.NewTransaction()
			t.Run("", func(t *testing.T) {
				t.Parallel()
			})
			t.Run("", func(t *testing.T) {
				t.Parallel()
			})
			t.Run("", func(t *testing.T) {
				t.Parallel()
			})
			if err := transaction.Commit(); err != nil {
				t.Fatalf("%+v", err)
			}
		})
	}

	t.Run("MissingDB", MissingDB)

	t.Run("OpenDatabase", OpenDatabase)
	t.Run("CreateTable", CreateTable)
	t.Run("PopulateTable", PopulateTable)
	t.Run("QueryTable", QueryTable)
	t.Run("QueryRow", QueryRow)

	t.Run("SQLErrors", SQLErrors)
	t.Run("TransactionErrors", TransactionErrors)
	t.Run("NestedTransaction", NestedTransaction)
}

func TestEncryption(t *testing.T) {

	type testStruct struct {
		Int      int
		String   string
		Bool     bool
		IntArray []int
	}
	type NonEncryptableStruct struct{ bool } //Struct with no exported fields
	invalidStruct := NonEncryptableStruct{true}
	var nilPointer *testStruct

	expected := testStruct{
		Int:      100,
		String:   "foo",
		Bool:     true,
		IntArray: []int{1, 2, 3, 4, 5},
	}

	var encryption db.Encryption
	var err error

	NewEncryption := func(t *testing.T) {
		encryption, err = db.NewEncryption(testDBKey)
		if err != nil {
			t.Fatalf("%+v", err)
		}
	}

	var data []byte
	//(Encryption).Encrypt
	Encrypt := func(t *testing.T) {
		t.Run("Unable to encrypt", func(t *testing.T) {
			if _, err = encryption.Encrypt(invalidStruct); err == nil {
				t.Fatal("Encrypt() did not error out when expected")
			}
		})
		t.Run("Nil pointer", func(t *testing.T) {
			if _, err = encryption.Encrypt(nilPointer); err == nil {
				t.Fatal("Encrypt() did not error out when expected")
			}
		})
		t.Run("Successful encryption", func(t *testing.T) {
			if data, err = encryption.Encrypt(expected); err != nil {
				t.Fatalf("%+v", err)
			}
		})
	}

	//(Encryption).Decrypt
	Decrypt := func(t *testing.T) {
		t.Run("Unable to decrypt", func(t *testing.T) {
			if err = encryption.Decrypt(&invalidStruct, data); err == nil {
				t.Fatal("Decrypt() did not error out when expected")
			}
		})
		t.Run("Not a pointer to struct", func(t *testing.T) {
			if err = encryption.Decrypt(expected, data); err == nil {
				t.Fatal("Decrypt() did not error out when expected")
			}
		})
		t.Run("Invalid data length", func(t *testing.T) {
			if err = encryption.Decrypt(expected, []byte{}); err == nil {
				t.Fatal("Decrypt() did not error out when expected")
			}
		})
		t.Run("Successful decryption", func(t *testing.T) {
			if err = encryption.Decrypt(&expected, data); err != nil {
				t.Fatalf("%+v", err)
			}
		})
		//TODO: corrupted byte fuzz test?
		t.Run("Invalid data - corrupted data", func(t *testing.T) {
			for i, j := 0, len(data)-1; i < j; i, j = i+1, j-1 {
				data[i], data[j] = data[j], data[i]
			}
			if err = encryption.Decrypt(&expected, data); err == nil {
				t.Fatal("Decrypt() did not error out when expected")
			}
		})
	}

	t.Run("NewEncryption", NewEncryption)
	t.Run("Encrypt", Encrypt)
	t.Run("Decrypt", Decrypt)
}
