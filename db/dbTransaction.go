package db

import (
	"database/sql"
	"fmt"
	"sync"

	"github.com/pkg/errors"
)

//Transaction represents a concurrently safe, nestable database transaction.
type Transaction interface {
	//ReadWriter is an interface for reading/querying and writing data to a database.
	ReadWriter
	//Commit commits the TransactionQueue expressions to its parent Transaction,
	//if one exists, or directly to the database if the TransactionQueue is not nested.
	Commit() error
	//Queue adds a SQL expression to the TransactionQueue.
	Queue(expression string, parameters ...any)

	//exec executes an expression with the provided parameters in the given database.
	exec(expression string, parameters ...any) (*sql.Result, error)
	//append is a helper function which adds Expressions to the the TransactionQueue
	append(...Expression)

	//Helper functions for locking/unlocking the transaction for concurrency safety.
	lock()
	unlock()
}

//ReadWriter is an interface for reading/querying and writing data to a database.
type ReadWriter interface {
	//Reader is an interface for reading/querying data from a database.
	Reader
	//NewTransaction creates a new database transaction.
	NewTransaction() Transaction
	//QueueAndCommit creates a nested Transaction, adds a SQL expression to it, and commits the nested Transaction.
	QueueAndCommit(expression string, parameters ...any) error
}

//TransactionQueue is a concurrently safe, nestable database transaction.
type TransactionQueue struct {
	*DB                      //The database to which the transaction belongs to.
	parent      Transaction  //If this TransactionQueue is nested within another Transaction, will contain pointer to said parent.
	committed   bool         //True only if the Transaction has already been committed.
	mutex       sync.Mutex   //Concurrency handling mutex.
	expressions []Expression //A list of SQL expressions that make up the transaction.
}

//Expression represents a SQL database expression.
type Expression struct {
	//The program stack trace of when the Expression was created.
	//If the expression causes a runtime error, this stack can be used to
	//trace the error back to the moment in time the Expression was created.
	stack error
	//The underlining SQL expression.
	expression string
	//A list of parameters which will fill any placeholder tokens in the query.
	parameters []any
}

//NewTransaction creates a new database transaction.
func (t *TransactionQueue) NewTransaction() Transaction {
	return &TransactionQueue{
		DB:     t.DB,
		parent: t,
	}
}

//Commit commits the TransactionQueue expressions to its parent Transaction,
//if one exists, or directly to the database if the TransactionQueue is not nested.
func (t *TransactionQueue) Commit() error {

	if t.committed {
		return errors.New("this transaction has already been committed")
	}

	t.lock()
	defer t.unlock()

	if t.parent != nil {
		t.parent.lock()
		defer t.parent.unlock()
		t.parent.append(t.expressions...)
		return nil
	}

	t.DB.lock()
	defer t.DB.unlock()
	if err := t.begin(); err != nil {
		return err
	}

	for _, e := range t.expressions {
		if _, err := t.exec(e.expression, e.parameters...); err != nil {
			return t.rollback(errors.WithMessage(err, fmt.Sprintf("%+v", e.stack)))
		}
	}
	if err := t.commit(); err != nil {
		return err
	}

	t.committed = true

	return nil
}

//Queue adds a SQL expression to the TransactionQueue.
func (t *TransactionQueue) Queue(expression string, parameters ...any) {

	t.append(Expression{
		stack:      errors.New("queue stack:"),
		expression: expression,
		parameters: parameters,
	})
}

//QueueAndCommit creates a nested Transaction, adds a SQL expression to it, and commits the nested Transaction.
//The TransactionQueue itself is not committed however.
func (t *TransactionQueue) QueueAndCommit(expression string, parameters ...any) error {
	tChild := t.NewTransaction()
	tChild.Queue(expression, parameters...)
	return tChild.Commit()
}

//Begin starts a new SQL transaction which can be either committed or rolled back.
func (t *TransactionQueue) begin() error {
	if _, err := t.exec(SQLBeginTransaction); err != nil {
		return errors.WithStack(err)
	}
	return nil
}

//Rollback rolls back all previous SQL transactions to the point where the transaction began.
//	e - the error that caused the rollback
func (t *TransactionQueue) rollback(e error) error {

	if _, err := t.exec(SQLRollbackTransaction); err != nil {
		//TODO?: should we wrap 'e' instead?
		return errors.Wrapf(err, "unable to rollback from error %s", errors.Cause(e).Error())
	}
	return e
}

//TransactionCommit commits all previous SQL transactions to the database.
func (t *TransactionQueue) commit() error {
	if _, err := t.exec(SQLCommitTransaction); err != nil {
		return t.rollback(err)
	}
	return nil
}

//exec executes an expression with the provided parameters in the given database.
func (t *TransactionQueue) exec(expression string, parameters ...any) (*sql.Result, error) {

	stmt, err := t.DB.Prepare(expression)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	result, err := stmt.Exec(parameters...)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	if err := stmt.Close(); err != nil {
		return nil, errors.WithStack(err)
	}
	return &result, nil

}

//append is a helper function which adds Expressions to the the TransactionQueue
func (t *TransactionQueue) append(expression ...Expression) {
	t.expressions = append(t.expressions, expression...)
}

//lock is a helper function for locking the TransactionQueue to provide concurrency handling.
func (t *TransactionQueue) lock() {
	t.mutex.Lock()
}

//unlock is a helper function for unlocking the TransactionQueue to provide concurrency handling.
func (t *TransactionQueue) unlock() {
	t.mutex.Unlock()
}
