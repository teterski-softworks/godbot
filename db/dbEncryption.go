package db

import (
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"crypto/md5"
	"crypto/rand"
	"encoding/gob"
	"encoding/hex"
	"hash"
	"io"

	"github.com/pkg/errors"
)

/*//TODO:
might want to create a check to see if provided password is correct for an existing database
that way if password is incorrect, bot immediately tells the user so.*/

//Encryption is an interface that allows the encryption/decryption of a database.
type Encryption interface {
	//Encrypt returns `toEncrypt` as an encrypted array of bytes.
	Encrypt(toEncrypt any) (encryptedData []byte, err error)
	//Decrypt reads the encrypted byte array, decrypts it, and stores it in `toDecrypt`.
	Decrypt(toDecrypt any, encryptedData []byte) (err error)
}

//Encrypter allows the encryption/decryption of data.
type Encrypter struct {
	gcm  cipher.AEAD
	hash hash.Hash
}

//NewEncryption returns a new Encrypter that uses the given password.
func NewEncryption(password string) (Encryption, error) {
	var e Encrypter
	e.hash = md5.New()
	block, err := aes.NewCipher(e.hashString(password))
	if err != nil {
		return e, errors.WithStack(err)
	}
	e.gcm, err = cipher.NewGCM(block)
	if err != nil {
		return e, errors.WithStack(err)
	}

	return e, nil
}

//Encrypt returns `toEncrypt` as an encrypted array of bytes.
func (e Encrypter) Encrypt(toEncrypt any) (encryptedData []byte, err error) {

	//Recover from nil pointer panic
	defer func() {
		if r := recover(); r != nil {
			err = errors.Errorf("cannot encode nil pointer of type %T", toEncrypt)
		}
	}()

	nonce := make([]byte, e.gcm.NonceSize())
	if _, err = io.ReadFull(rand.Reader, nonce); err != nil {
		return encryptedData, errors.WithStack(err)
	}

	buffer := bytes.Buffer{}
	if err = gob.NewEncoder(&buffer).Encode(toEncrypt); err != nil {
		return encryptedData, errors.WithStack(err)
	}

	encryptedData = e.gcm.Seal(nonce, nonce, buffer.Bytes(), nil)
	return encryptedData, nil
}

//Decrypt reads the encrypted byte array, decrypts it, and stores it in `toDecrypt`.
func (e Encrypter) Decrypt(toDecrypt any, encryptedData []byte) (err error) {

	//check to see if cipherData is larger than nonce size in order to avoid panic on errors.
	if len(encryptedData) <= e.gcm.NonceSize() {
		return errors.Errorf("encrypted data not long enough to decipher, is %d bytes, must be at least %d bytes", len(encryptedData), e.gcm.NonceSize())
	}

	nonce := encryptedData[:e.gcm.NonceSize()]

	decryptedBytes, err := e.gcm.Open(nil, nonce, encryptedData[e.gcm.NonceSize():], nil)
	if err != nil {
		return errors.WithStack(err)
	}

	if err = gob.NewDecoder(bytes.NewBuffer(decryptedBytes)).Decode(toDecrypt); err != nil {
		return errors.WithStack(err)
	}

	return nil
}

//hashString returns the MD5 hash of the given string.
func (e Encrypter) hashString(key string) []byte {
	e.hash.Write([]byte(key))
	return []byte(hex.EncodeToString(e.hash.Sum(nil)))
}
