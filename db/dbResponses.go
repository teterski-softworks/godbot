package db

import (
	dg "github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
)

var (
	ResponseDBError = func(e error) *dg.MessageEmbed {
		//TODO: use embeds package, but resolve import cycle first
		return &dg.MessageEmbed{
			Title:       "❌ Database Error ⚠️",
			Color:       0xdd2e44,
			Description: "The database responded with the error:\n```" + errors.Cause(e).Error() + "```\n",
			Footer: &dg.MessageEmbedFooter{
				Text: "Please ask a bot administrator to look at the logs for more information.",
			},
		}
	}
)
