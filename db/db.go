package db

import (
	"database/sql"
	"sync"

	_ "github.com/mattn/go-sqlite3"

	"github.com/pkg/errors"
)

//TODO: Throw error if selecting in queue and inserting/deleting in query

//Database represents a database capable of executing and querying statements,
//and of beginning, rolling back, and committing transactions.
type Database interface {
	ReadWriter    //ReadWriter is an interface for reading/querying and writing data to a database.
	Close() error //Close closes the database.
}

//Reader is an interface for reading/querying data from a database.
type Reader interface {
	//Encryption is an interface that allows the encryption/decryption of a database.
	Encryption
	//Query executes a query with the provided arguments in the given database.
	Query(query string, parameters ...any) (*sql.Rows, error)
	//QueryRow executes a query (returning only a single row) with the provided arguments in the given database.
	QueryRow(query string, parameters ...any) (*sql.Row, error)
}

//DB is a wrapper around sql.DB which allows for easier concurrency handling and database encryption.
type DB struct {
	*sql.DB
	mutex      sync.Mutex //Concurrency handling mutex.
	Encryption            //Encryption is an interface that allows the encryption/decryption of a database.
}

//Open opens an encrypted database at the given path using the provided encryption key.
func Open(database string, key string) (Database, error) {

	db, err := sql.Open("sqlite3", database)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	Encrypter, err := NewEncryption(key)
	if err != nil {
		return nil, err
	}
	DB := DB{
		DB:         db,
		mutex:      sync.Mutex{},
		Encryption: Encrypter,
	}

	if err := DB.PragmaForeignKeysOn(); err != nil {
		return nil, err
	}

	return &DB, nil
}

//Close closes the database.
func (db *DB) Close() error {
	db.mutex.Lock()
	defer db.mutex.Unlock()
	return db.DB.Close()
}

//PragmaForeignKeysOn enables the use of foreign keys in the database. Used for SQLite specifically.
//This is required as PRAGMAs cannot be changed in a transaction.
func (db *DB) PragmaForeignKeysOn() error {
	stmt, err := db.Prepare(SQLPragmaForeignKeysOn)
	if err != nil {
		return errors.WithStack(err)
	}
	if _, err = stmt.Exec(); err != nil {
		return errors.WithStack(err)
	}
	if err := stmt.Close(); err != nil {
		return errors.WithStack(err)
	}
	return nil
}

//NewTransaction creates a new database transaction.
func (db *DB) NewTransaction() Transaction {
	return &TransactionQueue{DB: db, parent: nil}
}

//Query executes a query with the provided arguments in the given database.
func (db *DB) Query(query string, parameters ...any) (*sql.Rows, error) {

	db.lock()
	defer db.unlock()

	stmt, err := db.DB.Prepare(query)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	rows, err := stmt.Query(parameters...)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	if err := stmt.Close(); err != nil {
		return nil, errors.WithStack(err)
	}

	return rows, nil
}

//Exec executes a query (returning only a single row) with the provided arguments in the given database.
func (db *DB) QueryRow(query string, parameters ...any) (*sql.Row, error) {

	db.lock()
	defer db.unlock()

	stmt, err := db.DB.Prepare(query)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	row := stmt.QueryRow(parameters...)

	if err = stmt.Close(); err != nil {
		return nil, errors.WithStack(err)
	}

	return row, nil
}

//QueueAndCommit creates a nested Transaction, adds a SQL expression to it, and commits the nested Transaction.
func (db *DB) QueueAndCommit(expression string, parameters ...any) error {
	t := db.NewTransaction()
	t.Queue(expression, parameters...)
	return t.Commit()
}

//locks the DB.
func (db *DB) lock() {
	db.mutex.Lock()
}

//unlocks the DB.
func (db *DB) unlock() {
	db.mutex.Unlock()
}
