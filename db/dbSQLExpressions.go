package db

var (
	SQLPragmaForeignKeysOn string = "PRAGMA foreign_keys = ON;"

	SQLBeginTransaction    string = "BEGIN TRANSACTION;"
	SQLRollbackTransaction string = "ROLLBACK TRANSACTION;"
	SQLCommitTransaction   string = "COMMIT TRANSACTION;"
)
