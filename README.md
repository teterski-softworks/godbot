## Go.D.Bot - <ins>Go</ins> <ins>D</ins>iscord <ins>Bot</ins> Framework.

Godbot is a framework for making general purpose Discord bots in Go. The framework is intended to run on Linux systems through Docker. See below for a list of features. The framework makes use of an embedded SQLite DBMS, as SQLite's limitations are not exceeded, allowing for easy and lightweight deployment.

**Note:** This is not a development repository. Features are developed through the repositories of the various bots that make use of this framework. Any new features or code that would be useful for the framework is then added here as needed.


## Table of Contents

[TOC]

## License
The following code, or its previous versions, is **not** currently open source, it is non-free (referring to liberty). All rights to this framework as reserved by Teterski Softworks Inc. The code is mainly published for demonstration purposes as part of a portfolio.

Please see the [LICENSE](/LICENSE) file for the framework's full software license.

This framework depends on the Discord API bindings provided by the [github.com/bwmarrin/discordgo](https://github.com/bwmarrin/discordgo) package by [bwmarrin](https://github.com/bwmarrin), which is licensed under the [BSD-3 Clause Revised License](https://github.com/bwmarrin/discordgo/blob/master/LICENSE).
This framework currently also depends on the SQLite driver in the [github.com/mattn/go-sqlite3](https://github.com/mattn/go-sqlite3) package by [mattn](https://github.com/mattn), which is licensed under the [MIT license](https://pkg.go.dev/github.com/mattn/go-sqlite3@v1.14.13?tab=licenses).

## Building
A bot can be ran by running either `make run`, or by generating an executable using `make build` first and then running it instead.

Make sure to read [how to create a Discord app](https://discord.com/developers/docs/getting-started#creating-an-app) in the official Discord documentation. You will need to follow the first of these steps to recieve your required `APPID` and `TOKEN`. Once your app is created, invite it to the server.
**For best results, make sure the bot has Admin privilages given to it through an assigned [role](https://support.discord.com/hc/en-us/articles/214836687-Role-Management-101).** (This will change in future releases.)

Requirements:
- [Go 1.18 or newer.](https://go.dev/dl/)
- Provide a valid [Application ID](https://discord.com/developers/docs/getting-started#creating-an-app) (`APPID`) and [token](https://discord.com/developers/docs/getting-started#configuring-a-bot) (`TOKEN`) in the [makefile](/makefile).
- A database key (`DBKEY`) must also be provided for proper database encryption (currently not optional, might be changed in future releases). A key must be either 16, 24, or 32 characters in length.
- The bot must be given both the `bot` and `application.commands` [scopes](https://discord.com/developers/docs/getting-started#adding-scopes-and-permissions).
- If running an executable, make sure to set the `APPID`, `TOKEN`, and `DBKEY` environmental variables.

**Note:** Due to the frameworks current license, any bots made using this framework may only be ran for demonstration purposes (as a demonstration of the framework itself), and may not be ran for either personal or commercial use, or be deployed on a Discord server.

## Features
Below is an alphabetical list of bot features the framework contains.

### Bookmarks
Bookmarks are an easy way to keep track of a message for later. When a bookmark is created, a link to that message is sent to the user. The link contains some information about the message, such as its text, who posted it, on which server, the time the message was sent, and any images or embeds the message might have had.<br>
![Bookmark](/docs/img/bookmarks-bookmark.jpg "A bookmark in a user's direct messages.")

Bookmarks can be made one of two ways:

* Right clicking a message and going to `Apps -> Bookmark🔖`.
    ![Bookmark - Apps Menu](/docs/img/bookmarks-apps.jpg "A bookmark made through the Apps menu.")
<br>
* Reacting to a message with a <img src="https://emojipedia-us.s3.dualstack.us-west-1.amazonaws.com/thumbs/320/twitter/322/bookmark_1f516.png" height="16px" alt="🔖"/> emoji (supported on all platforms).
    ![Bookmark - Emoji](/docs/img/bookmarks-emoji.jpg "A bookmark made by reacting with a bookmark emoji to a message.")

Once a bookmark is no longer needed, it can be easily deleted.

### Counters
Counters convert a regular Discord text channel into a way to help keep track, or count, of something. This can be anything that fits your server: the number of times you have worked out, cooked a meal, the number of git issues closed, or the number of times you have done anything productive. It can be anything that your community cares about and wants to work together towards.

When a counter is set up in a channel, each message in the channel must start with a number, and each message's number must be one higher than the message before. The first message in that channel, once the counter is created, must start with 1.

Counters keep track of two numbers per participating member, an all time count (the number of times this member has counted since the beginning of time), and a periodic count (the number of times this member has counted since the beginning of the last period). A period is a duration of time, set by a server admin, after which the periodic count is reset back to zero.

Counters can have role rewards associated with them, with rewards being automatically assigned to winners after the end of each period. A role reward is a role that is assigned to members who counted the highest. There are currently two possible rewards. One reward is a role that will be automatically assigned to the member with the highest all time count. Another reward will be assigned to the member with the highest count during that specific period.

This system allows to reward long term participants as well as those who are new to the server and still would like to get rewarded for participating. Otherwise, if only an all time highest count reward was allowed, new members would be unwilling to participate, as they would have a long way to go before they are able to catch up to an older member with a much higher count.

An automatic scoreboard can be set up by a server admin. This scoreboard will automatically update and show the top 10 members with highest all time counts, as well as the top 10 members with highest counts for the current period.

Server admins can set up multiple counters as long as they are in separate channels.

![Counter](/docs/img/counter.jpg) ![Counter Info](/docs/img/counter-info.jpg)

### Embeds
Discord embeds are a way to neatly present a message with a lot of data.
The bot allows for the easy creation and editing of embeds.
Any message in the server can be right clicked by an admin, after which they can go to `Apps -> Embed - New 📄` in order to turn it into an embed. Embeds can also be created using a command. Once created, embeds can also be fully edited. Embeds can be saved and reposted, in case they get deleted from the channel. This can also be used to first create and edit the embed in a private channel, and then reposting it to a public channel once an admin has finished setting the embed up and is ready to show it publicly. Embeds can easily be made to support multiple pages through which a user can flip through. Because Discord embeds have a set maximum character length, the framework provides an automatic way to split content between pages without going over this character limit.

![Embed](/docs/img/embed.jpg "An example of an embed.") ![Embed List](/docs/img/embed-list.jpg "A list of saved embeds.")

### Help Menus
Help Menus provide a way to easily see helpful information about the bot's commands. Help menus are auto generated from the slash and message commands added by the bot to the Discord server. Two types of help menus exist, a general purpose help menu which can be accessed by all server members, as well as an admin-only help menu. This provides a neat way of filtering out admin only command information from general users, who do not have access to admin functionality.

![Admin Help Menu](/docs/img/helpmenu-admin.jpg "An example of a possible admin help menu.") ![Help Menu](/docs/img/helpmenu.jpg "An example of a possible help menu.")

### Invites
The bot allows server admins an easy way to manage server invites. This can be extremely helpful if you wish to limit the number of new users to your server (if you want that cozy tightly knit small server feeling), or simply helpful if you want to prevent raids.

Server admins can toggle if members can use the bot to generate new invites to the server. Server admins can also set the max number of invites a member can generate in a specified period of time. This period of time can also be set by a server admin.

When a new member joins a server using an invite generated through the bot, a server admin can check which other member provided them with that invite. This can be extremely helpful in the case of a server raid or resolving a string of malicious members continuously joining the server.

![Invite Settings](/docs/img/invite-settings.jpg "A window showing the bot's invite generation settings.") ![Invite](/docs/img/invite.jpg "An invite generated through the use of the bot.") ![Invite Info](/docs/img/invite-info.jpg "A window showing who invited a member.")

![Invite Error](/docs/img/invite-error.jpg "A member has used up their allowed number of invites for the period.") ![Invite Disabled](/docs/img/invite-disabled.jpg "Invite generation has been disabled by a server admin.")

### Leveling
Leveling systems can be a fun way to reward member engagement on your server. Put simply, when enabled, server members gain experience points when they interact on the server. Server admins can set a base experience rate for the entire server, meaning that members will get a specific amount of experience when chatting anywhere on the server. Channels and categories can have their own bonus exp rates set, in case you want to encourage more interaction on specific channels. Conversely, channels and channel categories can also be excluding from giving members any experience.
Server admins can set bonus exp rates for the type of message a member sends, with a separate bonuses if a message contains only text, contains an image, or contains a video. What better way to encourage server members to post more funny cat videos.

To discourage spam, server admins can also set a cool down before members are able to get experience again and set a percentage chance of getting experience for their message.

Reward roles can be assigned to particular levels, so that members automatically are assigned a role when they reach a certain level. These rewards can either be stacked, meaning the member retains their reward roles from previous levels, or be mutually exclusive, meaning that the member will only retain the most recent mutual exclusive reward. Mutual exclusive rewards are only mutually exclusive to other mutually exclusive rewards, and not so to stacked rewards. This means that a member can still retain any reward roles that are stacked rewards if they receive a new mutual exclusive reward.

A special reward can be assigned to the member with the highest number of experience points.

Server admins can enable an automatic scoreboard, which shows the current top 10 most experienced server members.

![Leveling Settings](/docs/img/leveling-settings.jpg "A window showing the bot's leveling settings.")
![Level Up](/docs/img/leveling-level-up.jpg "A level up message assigning rewards to a member for their experience.") ![Level Auto Scoreboard](/docs/img/leveling-scoreboard.jpg "An automatic scoreboard posted and updated as per the leveling settings.")

### Logging
When enabled, if a message is deleted or edited, a log is sent to a selected log channel, showing what the message looked like before it was deleted/edited. This is an almost essential tool in moderating a server, allowing for a way to see if a message previously posted violated any of the server's or Discord's rules, as Discord does not provide any way to easily check this type of information.

The log shows who posted the message, what the message looked like before being deleted/edited, and what files were attached to it. 

Logging can also be enabled for mutes, warns, and bans, which sends a message to their designated log channel when a member a mute/warn/ban is applied/removed.

Channels and channel categories can be excluded from message logging.

There are many things going on in the background to make message logging possible and efficient. If interested, read [Log Scheduler](#log-scheduler) for more info.

| ![Deleted Message](/docs/img/logs-deleted-message.jpg "A log of a deleted message.")| ![Log Settings](/docs/img/log-settings.jpg "A window showing log settings.")<br>![Edited Message](/docs/img/logs-edited-message.jpg "A log of an edited message.") |
|-------------------------------------------------------------------------------------|---------------------------------------|

### Messages
The bot can be used to send messages to the server, or even reply to existing messages from other members. This can be useful if you need a more "official" feel to your messages, and a regular message from a server admin might not cut it. Any message previously sent by the bot can also be edited.

![Message](/docs/img/messages.jpg "A message send by the bot, as well as a reply made by the bot to a message.")

### Role Menus
Role menus are an easy way for server members to select what roles they want on your server. These roles could specify what their interests are, who they identify as, or even as simple as what part of the world they are from.

Role menus can be attached to any message previously posted by the bot, including embeds.
These menus can have required roles associated with them, meaning that a server member must already be assigned some specific role(s) before being able to use a particular role menu. Additionally, there are ways to associate roles with a role menu that would prevent a member from being able to interact with the role menu if that member has the specified role(s). 

Role menus can allow members to either select all the roles within them, or be made mutually exclusive, where only one role from the whole menu may be selected at a time.
    
This allows server admins to create intricate role selection systems for their server, that enforce requirements and consistency for roles.

![Role Menu](/docs/img/rolemenu-selected.jpg "A role menu for timezones, with the Mountain Time option selected.") ![Role Menu Options](/docs/img/rolemenu-selection.jpg "A list of options in the role menu.") ![Role Menu List](/docs/img/rolemenu-list.jpg "A list of role menus on the server.")

![Ignored Role](/docs/img/rolemenu-ignored-role.jpg "A message stating the member has a role preventing them from using the role menu.") ![Missing Required Role](/docs/img/rolemenu-required-role.jpg "A message stating the member lacks a required role to use the role menu.")

### Stats
A statistics system is in place that allows admins to view very basic usage statistics about how much use bot commands are getting, as well as other information about the bot, such as the number of members whose information is stored in the internal database, an overview of the server's counters, and any timed events such as temp bans/mutes. This can be helpful in determining if a bot feature is getting proper user engagement.

![Stats](/docs/img/stats.jpg "A screen displaying some statistics about the bot.")

### Moderation
Features that aid in the moderation of a server.

#### Mutes
Server members can be muted. Mutes can be either temporary, and expire after a set duration, or permanent (or be removed manually after some time, if you do not want to set a duration). Mutes can removed at any time.
    
Mutes can work in one of three ways. The bot can automatically assigning a role that prevents a member from speaking in the server. It can also remove a role from the member that grants them permission from interacting from the server. The bot can also do both, where it adds a mute specific role, and removes another role. The roles to add/remove can be manually configured through the bot's settings. When a mute expires/is removed, any removed roles are re-added, and any added roles are removed.

![Mute](/docs/img/mute.jpg "A mute message.") ![Unmute](/docs/img/unmute.jpg "An unmute message.")
![Mute Settings](/docs/img/mute-settings.jpg "A window showing mute settings.")

#### Warnings
Server members can be warned if they brake a rule. Warnings can have optional reasons assigned to them. All warnings are saved, so when need be, a server admin can check what previous warnings on the server a Discord account has.
    
Warnings can be removed if need be.

![Warnings List](/docs/img/warn-list.jpg "A list of previous warnings for a member.") ![Warning](/docs/img/warning.jpg)

#### Bans
Server member can be banned from a server. Bans can have optional reasons assigned to them, and can be either temporary or permanent. If a ban is temporary, a user can rejoin the server once the ban expires. A permanent ban can be manually lifted by a server admin. When banning a member, through the use of an optional parameter, the member's messages can be also automatically deleted (or if need be, only messages within a certain recent period of time). All bans are saved, so when need be, a server admin can check what previous bans on the server a Discord account has. Even bans and unbans not made through the bot can easily be tracked and logged by the bot.

![Ban](/docs/img/ban.jpg "A ban message.") ![Unban](/docs/img/unban.jpg "An uban message.")

![Ban List](/docs/img/ban-list.jpg "A list of previous bans for a member.")


## Background Features
These are features that are not visible to end users, but provide important functionality in the background.

### Database Encryption
The bot's database encrypts any potential personally identifiable information using either AES-128, AES-192, or AES-256 encryption (depending on length of database key provided). This helps keep any bots made with the framework within Discord's Privacy Policy. The framework provides an easy mechanism to specify which data should be encrypted.

### Nested Transaction Queue for SQLite
Because SQLite does not allow to start transactions within transaction, a custom transaction queue is implemented in this framework. The queue makes use of the program stack to allow for concurrently-safe, nested transactions.

### Webhook Management
Webhooks are another way to post message to channels in Discord. They can be used to post a message using a sort of pseudo account, the name and avatar of which can be customized. They can be a very powerful tool when developing a bot. A webhook is created to post messages to a specific channel. If you wish to use the webhook to post to a different channel, either the existing webhook needs to be modified to use that different channel, or a new webhook needs to be made.

A Discord bot is only allowed to create a certain number of webhooks within a day (if this number is exceeded, the bot is timed out for a whole day before being allowed to create webhooks again). Additionally, a bot can only have 10 webhooks associated with it at a time. As a result, webhook usage can feel very limiting, especially on a large server.

This framework provides an easy way to create and manage webhooks, ensuring that you always have a webhook ready when you need it without exceeding any allocated quotas:
- The bot will reuse a webhook if a channel already has one.
- If the channel has no webhooks created for it, a new one will be made.
- If the bot runs out of available webhooks to create, it will reassign the least recently used webhook to the channel that needs it.

### Application Command Management
Application commands are the main way in which users interact with the bot on a Discord server. When setting up a bot, the bot has to add its application commands to the Discord servers it is a member of (this only needs to be done once, or when updating the command). Not only can this be time consuming during the launch of a bot (easily taking up to several minutes even with a small number of commands), but Discord also limits its bots in the number of application commands they can add in a day. If this number is exceeded, the bot is timed out for a whole day before being allowed to add new application commands to the server. This seriously impedes development.

The solution is to only add application commands when needed, such as if it is a new command or if the command has been updated in some way. Unfortunately, the Discord API does not provide an automatic or easy way of doing so. It is up to the bot's developer(s) to keep track of if and when an application command should be added or updated. Keeping track of this manually also impedes development, as not only is this tedious, but is a huge annoyance each time the developer makes a mistake and has to wait for the command to be needlessly re-added.

Fortunately, this framework provides an automated way which handles this for the developer, ensuring a fast bot start up time during development and deployment.

### Audit Log Tracking
The framework provides an easy way to keep track of processed audit log entries. For each channel, the last audit log entry of a specific action type can be saved. This allows to filter out old/unnecessary/handled entries when querying the audit log of a Discord server.

### Log Scheduler
Message logging is a very expensive operation. While Discord provides a way to be notified of when a message is deleted/edited, Discord does not provide any sort of information about the message content itself. As a result, messages that are to be logged need to be saved first. That way when a message is deleted/edited, this saved record can be pulled up and a server admin can see what the message used to look like.

This creates a new problem, that is not necessarily evident at first: when exactly are these messages supposed to be saved?

Messages can be saved as they are posted to the server, but the bot also needs a way to keep track of any messages that where posted from before when the bot joined the server. Additionally, the bot could either gain or lose access to different channels or categories. As a result, the bot could have records of messages it no longer has any way of detecting if they are deleted/edited from the server, as well as channels filled with messages the bot does not yet have any records for.

Simply covering these edge cases does not solve the issue entirely. There is a major cost associated with these sort of edge cases that is not being accounted for.

As an example, let's say a server admin is doing a rework of the server's permissions. During the course of twenty minutes, the admin is continuously changing the permissions for various channels. This means that the messages the bot has access too are constantly changing; one minute the bot might be saving a channel it just got access to, only to then be denied access to the channel again a minute later. Conversely, the bot might temporarily lose access to an entire section of channels. To make matters even more difficult, this scenario might be happening on multiple other servers which the bot is a part of. This could result in hundreds of thousands of wasteful requests being made by the bot to Discord.
        
Instead of constantly retrieving and deleting records, the bot keeps track of which channels it had access to, and queries changes within a period of time. If another change is detected, the timer for the period is reset, and the bot waits for more changes until the timer runs out. Once the timer runs out, the bot then checks what channels it has access to at that moment, and applies the appropriate changes, by either deleting records that are no longer needed or by retrieving and saving new messages it has access to. This results in a very efficient system of logging messages.

This framework provides a ready made log scheduler that automatically implements this system.

![Log Scheduler Diagram](/docs/graphs/Log%20Scheduler.svg "A diagram of the log scheduler system.")

### Restarting
If the bot is down, any scheduled events that were meant to fire during this time, such as a ban that is set to expire, a period ending for a counter, or so on, will automatically fire when the bot goes back online. Any other scheduled events that should not have yet fired, will fire as they should when their time comes.
