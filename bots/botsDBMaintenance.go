package bots

import (
	"sync"

	"gitlab.com/teterski-softworks/godbot/bans"
	"gitlab.com/teterski-softworks/godbot/channels"
	"gitlab.com/teterski-softworks/godbot/counters"
	"gitlab.com/teterski-softworks/godbot/guilds"
	"gitlab.com/teterski-softworks/godbot/guilds/guildRemovers"
	"gitlab.com/teterski-softworks/godbot/invites"
	"gitlab.com/teterski-softworks/godbot/logs/logSchedulers"
	"gitlab.com/teterski-softworks/godbot/members"
	"gitlab.com/teterski-softworks/godbot/messages"
	"gitlab.com/teterski-softworks/godbot/mutes"
	"gitlab.com/teterski-softworks/godbot/roles"
)

//DBMaintenance runs a clean on the database for any non-needed or outdated entries.
func (b *Bot) DBMaintenance(guildDBMaintenance func(b *Bot, guildID string)) {

	b.State.Printf("Cleaning database.")
	//Save all connected guilds to DB, as there could be more since last time the bot was online
	for _, guild := range b.State.State.Guilds {
		if err := guilds.New(guild.ID).Store(b.State.DB); err != nil {
			b.LogError(err)
		}
	}

	//Retrieve a full list of all guilds the bot is on.
	guilds, err := guilds.RetrieveAll(b.State.DB)
	if err != nil {
		b.LogError(err)
		return
	}

	var wg sync.WaitGroup
	wg.Add(len(guilds))
	for _, guild := range guilds {
		go func(guildID string) {
			b.State.Printf("Performing DB maintenance on entries for G[%s].", guildID)
			guildDBMaintenance(b, guildID)
			wg.Done()
		}(guild.GuildID)
	}
	//wait for all guilds to finished before continuing
	wg.Wait()
}

//GuildDBMaintenance runs a clean on the database for any non-needed or outdated entries relating to the guild.
func GuildDBMaintenance(b *Bot, guildID string) {

	if err := b.RemoveInconsequentialRecords(guildID); err != nil {
		b.LogError(err)
	}

	if err := b.RemoveOldRecords(guildID); err != nil {
		b.LogError(err)
	}

	//Check if any channels have been deleted
	if err := channels.RemoveDeletedChannels(b.State, guildID); err != nil {
		b.LogError(err)
	}

	//Check if any messages have been deleted - not for logging.
	if err := messages.RemoveDeletedMessages(b.State, guildID); err != nil {
		b.LogError(err)
	}

	//Check if any roles have been deleted
	if err := roles.RemoveDeletedRoles(b.State, guildID); err != nil {
		b.LogError(err)
	}

	//Guild remover timers
	e, err := guildRemovers.InitializeTimers(b.State, guildID)
	go b.LogChannelErrors(e, err)

	//Ban timers
	e, err = bans.InitializeTimers(b.State, guildID)
	go b.LogChannelErrors(e, err)

	//Mute timers
	e, err = mutes.InitializeTimers(b.State, guildID)
	go b.LogChannelErrors(e, err)

	//Counter - catch up on missed messages
	if err := counters.CatchUpOnMissedMessages(b.State, b.WebhookManagers.Get(guildID), guildID); err != nil {
		b.LogError(err)
	}
	//Counter - timers
	e, err = counters.InitializeTimers(b.State, guildID)
	go b.LogChannelErrors(e, err)

	//Invite timers
	e, err = invites.InitializeTimers(b.State, guildID)
	go b.LogChannelErrors(e, err)

	//Clean and populate log records if needed
	b.State.LogRecordSchedulers.Get(guildID).ScheduleGuildUpdate(logSchedulers.UpdateTypeRefresh)
	b.State.LogRecordSchedulers.Get(guildID).ForceUpdate()
	b.State.LogRecordSchedulers.Range(
		func(_ string, scheduler *logSchedulers.Scheduler) {
			go b.LogChannelErrors(scheduler.ErrChan, nil)
		},
	)
}

//RemoveOldRecords removes any records of members who are no longer members of the server.
func (b *Bot) RemoveOldRecords(guildID string) error {

	memberList, err := members.RetrieveAll(b.State.DB, guildID)
	if err != nil {
		return err
	}
	for _, memberID := range memberList {
		if _, err := b.State.GuildMember(guildID, memberID); err == nil {
			continue
		}
		if _, err := members.New(guildID, memberID).Delete(b.State.DB); err != nil {
			return err
		}
	}

	return nil
}

//RemoveInconsequentialRecords deletes records that have default values
//and do not provide any useful relationships between tables.
func (b *Bot) RemoveInconsequentialRecords(guildID string) error {
	return b.State.DB.QueueAndCommit(members.SQLCleanMembers, guildID)
}

//LogChannelErrors logs any errors that are received from the channel.
func (b *Bot) LogChannelErrors(e chan error, err error) {
	if err != nil {
		b.LogError(err)
	}
	for err := range e {
		if err != nil {
			b.LogError(err)
		}
	}
	close(e)
}
