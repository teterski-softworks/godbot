package bots

import (
	"io"
	"log"
	"os"

	"github.com/pkg/errors"
	"gitlab.com/teterski-softworks/godbot/db"
)

//OpenLog opens and returns the logs file for the Bot.
func OpenLog(logPath string) (l *log.Logger, err error) {

	file, err := os.OpenFile(logPath, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		return l, errors.Wrap(err, "unable to create bot log")
	}

	writer := io.MultiWriter(os.Stdout, file)
	return log.New(writer, "", log.LstdFlags), nil
}

//initializeDatabase connects to the Bot's database.
func (b *Bot) initializeDatabase(dbPath string, key string, tables []func(DB db.ReadWriter) error) error {

	b.State.Printf("Opening DB[%s].", dbPath)

	var err error
	b.State.DB, err = db.Open(dbPath, key)
	if err != nil {
		return err
	}

	for _, createTable := range tables {
		if err := createTable(b.State.DB); err != nil {
			return err
		}
	}
	return nil
}
