package bots

import (
	dg "github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
	"gitlab.com/teterski-softworks/godbot/commands"
	"gitlab.com/teterski-softworks/godbot/concurrentMaps"
	"gitlab.com/teterski-softworks/godbot/db"
	"gitlab.com/teterski-softworks/godbot/emojiSelectors"
	"gitlab.com/teterski-softworks/godbot/guilds"
	"gitlab.com/teterski-softworks/godbot/helpMenus"
	"gitlab.com/teterski-softworks/godbot/logs"
	"gitlab.com/teterski-softworks/godbot/logs/logSchedulers"
	"gitlab.com/teterski-softworks/godbot/states"
	"gitlab.com/teterski-softworks/godbot/webhooks"
)

//Bot represents a Discord bot.
type Bot struct {
	State           *states.State
	ApplicationID   string
	WebhookManagers concurrentMaps.Map[ /*GuildID*/ string, webhooks.Manager]

	CommandManager commands.Manager

	HelpMenu      *helpMenus.HelpMenu
	HelpMenuAdmin *helpMenus.HelpMenu
}

//New returns a new Bot struct.
func New(token, applicationID, dbPath, dbKey, logPath string,
	intents dg.Intent,
	tables []func(DB db.ReadWriter) error,
	GuildDBMaintenance func(b *Bot, guildID string),
	AddHandlers func(*Bot),
	cmds, cmdsAdmin []commands.Commander,

	SetStatusBooting func(state *states.State) error,
	SetStatusReady func(state *states.State) error,
) (*Bot, error) {

	log, err := OpenLog(logPath)
	if err != nil {
		return &Bot{}, err
	}
	b := &Bot{
		State:           states.New(log),
		ApplicationID:   applicationID,
		WebhookManagers: concurrentMaps.New[string, webhooks.Manager](),
	}

	if err := b.initializeDatabase(dbPath, dbKey, tables); err != nil {
		return b, err
	}

	//Connects to Discord
	if _, err := b.Connect(token, intents); err != nil {
		return b, err
	}
	if err := SetStatusBooting(b.State); err != nil {
		return b, err
	}

	if err := b.addCommands(cmds, cmdsAdmin); err != nil {
		return b, err
	}

	b.DBMaintenance(GuildDBMaintenance)

	b.State.Println("Adding handlers.")
	AddHandlers(b)
	if err := SetStatusReady(b.State); err != nil {
		return b, err
	}
	b.State.Println("Bot has launched")

	return b, nil
}

//Connect the Bot to Discord via a token.
func (b *Bot) Connect(token string, intents dg.Intent) (*dg.User, error) {
	b.State.Println("Connecting to Discord.")
	var err error
	b.State.Session, err = dg.New("Bot " + token)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	b.State.Identify.Intents = intents
	if err := b.State.Open(); err != nil {
		return nil, errors.WithStack(err)
	}

	for _, g := range b.State.State.Guilds {
		b.ConnectGuild(g.ID)
	}

	b.State.Println("Connected.")
	return b.State.State.User, nil
}

//TODO! we need an analogous method to this to de-allocate these resources when the bot is removed from a guild (except the timerManger).
//ConnectGuild allocates the required resources for the guild.
func (b *Bot) ConnectGuild(guildID string) error {

	b.State.Printf("Detected connection to G[%s].", guildID)

	guild := guilds.New(guildID)
	if err := guild.Store(b.State.DB); err != nil {
		return err
	}

	//Add webhook managers
	webhookManager, err := webhooks.New(webhooks.MaxWebhooks, guild.GuildID, b.ApplicationID, b.State.Session)
	if err != nil {
		return err
	}
	b.WebhookManagers.Store(guild.GuildID, webhookManager)

	//Initialize timer managers
	b.State.TimerManagers.Store(guild.GuildID, states.NewTimerManager())
	//Initialize log record schedulers
	b.State.LogRecordSchedulers.Store(guild.GuildID,
		logSchedulers.New(func() error { return logs.ApplyLogRecordGuildUpdates(b.State, guild.GuildID) }),
	)

	//Add emoji selectors menu handler for the guild.
	b.State.EmojiSelectors.Store(guild.GuildID, emojiSelectors.New())

	b.State.Printf("Allocated resources for G[%s].", guildID)
	return nil
}

//Close safely terminates the Bot and the closes the database.
func (b *Bot) Close() error {
	if err := b.State.Close(); err != nil {
		return errors.WithStack(err)
	}
	b.State.Println("Session closed.")
	if err := b.State.DB.Close(); err != nil {
		return errors.WithStack(err)
	}
	b.State.Println("Database closed.")
	return nil
}

func (b *Bot) addCommands(cmds []commands.Commander, cmdsAdmin []commands.Commander) error {

	b.State.Println("Adding application commands.")

	b.State.Println("Creating help menus.")
	//Generate the help menus
	b.HelpMenu = helpMenus.New("help", "📔 Help Menu", helpMenus.CustomIDPrefixHelp, "Shows the help menu.", true, cmds)
	cmds = append(cmds, b.HelpMenu.Commander)
	b.HelpMenuAdmin = helpMenus.New("admin_help", "📕 Admin Help Menu", helpMenus.CustomIDPrefixHelpAdmin, "Shows the admin-only help menu.", false, cmdsAdmin)
	cmdsAdmin = append(cmdsAdmin, b.HelpMenuAdmin.Commander)
	for _, cmd := range cmds {
		b.HelpMenu.Add(cmd)
	}
	for _, cmd := range cmdsAdmin {
		b.HelpMenuAdmin.Add(cmd)
	}

	b.State.Println("Initializing command manager.")
	b.CommandManager = commands.NewManager(b.State.Session, b.State.Logger, b.ApplicationID, cmds, cmdsAdmin)

	if err := b.CommandManager.ClearAllCommandsAddedStatus(b.State.DB); err != nil {
		return err
	}

	for _, guild := range b.State.State.Guilds {
		if err := b.CommandManager.AddCommandsToGuild(b.State.DB, b.State.Session, guild.ID); err != nil {
			return err
		}
	}

	if err := b.CommandManager.DeleteNotAddedCommands(b.State.DB, b.State.State.Guilds); err != nil {
		return err
	}

	return nil
}

//LogError writes an error to the Bot's log.
func (b *Bot) LogError(err error) {
	b.State.Printf("[ERROR]\n%+v", err)
}
