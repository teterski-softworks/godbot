package bots

import (
	"os"
	"os/signal"
	"strings"
	"syscall"

	dg "github.com/bwmarrin/discordgo"
	"gitlab.com/teterski-softworks/godbot/autoReacts"
	"gitlab.com/teterski-softworks/godbot/bans"
	"gitlab.com/teterski-softworks/godbot/bookmarks"
	"gitlab.com/teterski-softworks/godbot/channels"
	"gitlab.com/teterski-softworks/godbot/confessions"
	"gitlab.com/teterski-softworks/godbot/counters"
	"gitlab.com/teterski-softworks/godbot/embeds"
	"gitlab.com/teterski-softworks/godbot/guilds/guildRemovers"
	"gitlab.com/teterski-softworks/godbot/helpMenus"
	"gitlab.com/teterski-softworks/godbot/invites"
	"gitlab.com/teterski-softworks/godbot/leveling"
	"gitlab.com/teterski-softworks/godbot/logs"
	"gitlab.com/teterski-softworks/godbot/members"
	"gitlab.com/teterski-softworks/godbot/messages"
	"gitlab.com/teterski-softworks/godbot/respondChannels"
	"gitlab.com/teterski-softworks/godbot/roleMenus"
	"gitlab.com/teterski-softworks/godbot/roles"
	"gitlab.com/teterski-softworks/godbot/starBoards"
	"gitlab.com/teterski-softworks/godbot/states"
)

//AddHandlers adds the default event handlers for the Bot.
func AddHandlers(b *Bot) {
	b.State.Println("Adding handlers.")
	b.State.AddHandler(b.interactionCreateHandler)
	b.State.AddHandler(b.guildMemberAddHandler)
	b.State.AddHandler(b.guildMemberRemoveHandler)
	b.State.AddHandler(b.guildMemberUpdateHandler)
	b.State.AddHandler(b.messageCreateHandler)
	b.State.AddHandler(b.messageEditHandler)
	b.State.AddHandler(b.messageDeleteHandler)
	b.State.AddHandler(b.messageReactionAddHandler)
	b.State.AddHandler(b.channelDeleteHandler)
	b.State.AddHandler(b.channelUpdateHandler)
	b.State.AddHandler(b.inviteDeleteHandler)
	b.State.AddHandler(b.guildRoleUpdateHandler)
	b.State.AddHandler(b.guildRoleDeleteHandler)
	b.State.AddHandler(b.guildBanAddHandler)
	b.State.AddHandler(b.guildBanRemoveHandler)
	b.State.AddHandler(b.guildCreateHandler)
	b.State.AddHandler(b.guildDeleteHandler)
}

//SignalHandler intercepts all system signals that would interrupt, quit, terminate, or kill the process
//and ensures safe bot termination.
func (b *Bot) SignalHandler() {
	signals := make(chan os.Signal, 1)
	done := make(chan bool, 1)
	defer close(done)
	defer close(signals)
	signal.Notify(signals, os.Interrupt, syscall.SIGTERM, syscall.SIGQUIT)
	go func() {
		b.State.Println("Detected signal [" + (<-signals).String() + "]. Terminating.")
		b.Close()
		os.Exit(0)
		done <- true
	}()
	<-done
}

//interactionCreateHandler is called each time the bot receive a InteractionCreate event (when a member interacts with a component)
func (b *Bot) interactionCreateHandler(session *dg.Session, event *dg.InteractionCreate) {

	b.State.Session = session
	intr := states.NewInteraction(b.State, *event.Interaction)

	switch event.Type {
	case dg.InteractionApplicationCommand:
		if err := b.CommandManager.CommandHandler(intr); err != nil {
			b.LogError(err)
		}

	case dg.InteractionMessageComponent:
		event.Message.GuildID = event.GuildID

		customID := event.MessageComponentData().CustomID
		//Custom ID exact match
		switch customID {
		case bookmarks.CustomIDDeleteBookmark:
			if err := b.State.ChannelMessageDelete(event.Message.ChannelID, event.Message.ID); err != nil {
				b.LogError(err)
			}

		case roleMenus.CustomIDSelectionMenu:
			if err := roleMenus.SelectionMenuHandler(intr, event); err != nil {
				b.LogError(err)
			}

		}

		//Help menus
		if strings.HasPrefix(customID, helpMenus.CustomIDPrefixHelpAdmin) {
			if err := helpMenus.PageSelectionHandler(intr, b.HelpMenuAdmin, customID, helpMenus.CustomIDPrefixHelpAdmin); err != nil {
				b.LogError(err)
			}
		} else if strings.HasPrefix(customID, helpMenus.CustomIDPrefixHelp) {
			if err := helpMenus.PageSelectionHandler(intr, b.HelpMenu, customID, helpMenus.CustomIDPrefixHelp); err != nil {
				b.LogError(err)
			}
		}

		//Custom ID prefixes
		if strings.HasPrefix(customID, embeds.CustomIDPrefixList) {
			if err := embeds.ListPageSelectionHandler(intr, customID); err != nil {
				b.LogError(err)
			}
		} else if strings.HasPrefix(customID, roleMenus.CustomIDPrefixList) {
			if err := roleMenus.ListPageSelectionHandler(intr, customID); err != nil {
				b.LogError(err)
			}
		}

		//Custom ID separators
		if strings.Contains(customID, bans.CustomIDSeparatorList) {
			if err := bans.ListPageSelectionHandler(intr, customID); err != nil {
				b.LogError(err)
			}
		}

	case dg.InteractionModalSubmit:
		//Nothing here yet
	}
}

//messageCreateHandler is called each time the bot receives a MessageCreate event (when a new message is sent to the server).
func (b *Bot) messageCreateHandler(session *dg.Session, event *dg.MessageCreate) {

	b.State.Session = session
	intr := states.NewInteraction(b.State, dg.Interaction{
		GuildID:   event.GuildID,
		ChannelID: event.ChannelID,
		Message:   event.Message,
	})

	//Log the message
	if err := logs.MessageCreateHandler(b.State, event); err != nil {
		b.LogError(err)
	}

	//Skip processing any messages that are created by a bot except for logs.
	if event.Author.Bot {
		return
	}

	//Give XP for the message
	if err := leveling.MessageCreateHandler(intr); err != nil {
		b.LogError(err)
	}

	//Handle any confessions
	if confession, err := confessions.MessageCreateHandler(intr); err != nil {
		b.LogError(err)
	} else if confession {
		return //Do not handle the event any further if the message was a confession.
	}

	//Handle any respond channel messages
	if err := respondChannels.MessageCreateHandler(intr, b.WebhookManagers.Get(intr.GuildID)); err != nil {
		b.LogError(err)
	}

	//Handle any counters
	if err := counters.MessageCreateHandler(intr, b.WebhookManagers.Get(event.GuildID)); err != nil {
		b.LogError(err)
	}

	//Handle auto reacts
	if err := autoReacts.MessageCreateHandler(intr); err != nil {
		b.LogError(err)
	}
}

//messageEditHandler is called each time the bot receives a MessageUpdate event (when a message is edited).
func (b *Bot) messageEditHandler(session *dg.Session, event *dg.MessageUpdate) {
	b.State.Session = session

	if event.Author == nil {
		//Discord can edit messages (such as adding embeds for links)
		//in which case the edit does not have an author.
		return
	}
	if err := logs.MessageEditHandler(b.State, event); err != nil {
		b.LogError(err)
	}
}

//messageDeleteHandler is called each time the bot receives a MessageDelete event (when a message is deleted).
func (b *Bot) messageDeleteHandler(session *dg.Session, event *dg.MessageDelete) {
	b.State.Session = session
	//Delete the message from any entries from the DB.
	if err := messages.MessageDeleteHandler(b.State, event); err != nil {
		b.LogError(err)
	}
	//Log the deleted message.
	if err := logs.MessageDeleteHandler(b.State, event); err != nil {
		b.LogError(err)
	}
}

//messageReactionAddHandler is called each time the bot receives a MessageReactionAdd event (when a user reacts to a message).
func (b *Bot) messageReactionAddHandler(session *dg.Session, event *dg.MessageReactionAdd) {
	b.State.Session = session

	if event.Member.User.Bot {
		return
	}

	//Check if any emoji selections are pending
	if err := b.State.EmojiSelectors.Get(event.GuildID).MessageReactionAddHandler(session, event); err != nil {
		b.LogError(err)
	}

	b.State.Session = session
	switch event.Emoji.APIName() {
	case "🔖":
		if err := bookmarks.MessageReactionAddHandler(b.State, event); err != nil {
			b.LogError(err)
		}
	}

	//Check for star boards
	if err := starBoards.MessageReactionAddHandler(b.State, event); err != nil {
		b.LogError(err)
	}
}

//channelDeleteHandler is called each time the bot receives a ChannelDelete event (when a channel is deleted).
func (b *Bot) channelDeleteHandler(session *dg.Session, event *dg.ChannelDelete) {
	b.State.Session = session
	//Delete the channel from any DB entries.
	if err := channels.ChannelDeleteHandler(b.State, event); err != nil {
		b.LogError(err)
	}
	//Clear log related updates and cache for the channel
	logs.ChannelDeleteHandler(b.State, event)
}

//guildMemberAddHandler is called each time the bot receives a GuildMemberAdd event (when a member joins a guild).
func (b *Bot) guildMemberAddHandler(session *dg.Session, event *dg.GuildMemberAdd) {
	b.State.Session = session
	//Check for invites
	if err := invites.GuildMemberAddHandler(b.State, event); err != nil {
		b.LogError(err)
	}
}

//guildMemberUpdateHandler is called each time the bot receives a GuildMemberUpdate event (when a member's data is changed).
func (b *Bot) guildMemberUpdateHandler(session *dg.Session, event *dg.GuildMemberUpdate) {
	b.State.Session = session
	//Check if bot's permissions have changed and if it affects logging
	if err := logs.GuildMemberUpdateHandler(b.State, event); err != nil {
		b.LogError(err)
	}
}

//inviteDeleteHandler is called each time the bot receives a InviteDelete event (when an invite is deleted from a guild).
func (b *Bot) inviteDeleteHandler(session *dg.Session, event *dg.InviteDelete) {
	b.State.Session = session
	//TODO: Requires "Manage Channels" permissions.
	if err := invites.InviteDeleteHandler(b.State, event); err != nil {
		b.LogError(err)
	}
}

//guildMemberRemoveHandler is called each time the bot receives a GuildMemberRemove event (when a member leaves the server)
func (b *Bot) guildMemberRemoveHandler(session *dg.Session, event *dg.GuildMemberRemove) {
	b.State.Session = session
	//Delete all of the members records.
	if _, err := members.New(event.GuildID, event.User.ID).Delete(b.State.DB); err != nil {
		b.LogError(err)
	}
}

//channelUpdateHandler is called each time the bot receives a ChannelUpdate event (when a channel is updated).
func (b *Bot) channelUpdateHandler(session *dg.Session, event *dg.ChannelUpdate) {
	b.State.Session = session
	//Check if logging is affected
	if err := logs.ChannelUpdateHandler(b.State, event); err != nil {
		b.LogError(err)
	}
}

//guildRoleDeleteHandler is called each time the bot receives a GuildRoleDelete event (when a role is deleted).
func (b *Bot) guildRoleDeleteHandler(session *dg.Session, event *dg.GuildRoleDelete) {
	b.State.Session = session
	//Delete the role from all DB entries.
	if err := roles.GuildRoleDeleteHandler(b.State, event); err != nil {
		b.LogError(err)
	}
	//*NOTE: guild role deletion is handled through the guildMemberUpdateHandler
}

//guildRoleUpdateHandler is called each time the bot receives a GuildRoleUpdate event (when a role is updated).
func (b *Bot) guildRoleUpdateHandler(session *dg.Session, event *dg.GuildRoleUpdate) {
	b.State.Session = session
	//Check if logging is affected
	if err := logs.GuildRoleUpdateHandler(b.State, event); err != nil {
		b.LogError(err)
	}
}

//guildBanAddHandler is called each time the bot receives a GuildBanAdd event (when a member gets banned).
func (b *Bot) guildBanAddHandler(session *dg.Session, event *dg.GuildBanAdd) {
	b.State.Session = session
	if err := bans.GuildBanAddHandler(b.State, event); err != nil {
		b.LogError(err)
	}
}

//guildBanRemoveHandler is called each time the bot receives a GuildBanRemove event (when a member gets unbanned).
func (b *Bot) guildBanRemoveHandler(session *dg.Session, event *dg.GuildBanRemove) {
	b.State.Session = session
	if err := bans.GuildBanRemoveHandler(b.State, event); err != nil {
		b.LogError(err)
	}
}

//guildCreateHandler is called each time the bot receives a GuildCreate event (when bot connects to a guild, not including on startup).
func (b *Bot) guildCreateHandler(session *dg.Session, event *dg.GuildCreate) {

	if err := b.ConnectGuild(event.Guild.ID); err != nil {
		b.LogError(err)
	}
	GuildDBMaintenance(b, event.Guild.ID)

	if err := guildRemovers.GuildCreateHandler(b.State, b.CommandManager, event); err != nil {
		b.LogError(err)
	}
}

//guildDeleteHandler is called each time the bot receives a GuildDelete event (when bot is disconnected from a guild).
func (b *Bot) guildDeleteHandler(session *dg.Session, event *dg.GuildDelete) {
	b.State.Printf("Bot removed from G[%s].", event.Guild.ID) //TODO: Move to b.DisconnectGuild(event.Guild.ID)
	//TODO: b.DisconnectGuild(event.Guild.ID)
	if err := guildRemovers.GuildDeleteHandler(b.State, b.CommandManager, event); err != nil {
		b.LogError(err)
	}
}
