package bots

import (
	dg "github.com/bwmarrin/discordgo"
	"gitlab.com/teterski-softworks/godbot/states"
)

//SetStatusBooting sets the Bot's status to booting and do not disturb.
var SetStatusBooting = func(state *states.State) error {

	var activity dg.Activity
	activity.Type = dg.ActivityTypeWatching
	activity.Name = "itself boot up ⏳"

	var data dg.UpdateStatusData
	data.Status = string(dg.StatusDoNotDisturb)
	data.Activities = append(data.Activities, &activity)
	return state.UpdateStatusComplex(data)
}

//SetStatusBooting sets the Bot's status to online.
var SetStatusReady = func(state *states.State) error {

	var data dg.UpdateStatusData
	data.Status = string(dg.StatusOnline)
	return state.UpdateStatusComplex(data)
}
