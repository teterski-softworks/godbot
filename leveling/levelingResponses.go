package leveling

import (
	"fmt"

	dg "github.com/bwmarrin/discordgo"
	"gitlab.com/teterski-softworks/godbot/embeds"
	"gitlab.com/teterski-softworks/godbot/members"
)

var (
	ResponseExpAdded = func(memberID string, xp int) *dg.MessageEmbed {
		embed := embeds.New("", "", "")
		embed.Title = "✅ XP Added"
		embed.Color = embeds.ColorGreen
		embed.Description = fmt.Sprintf("%s received `%dxp`.", members.Mention(memberID), xp)
		return &embed.MessageEmbed
	}

	ResponseExpSet = func(memberID string, xp int) *dg.MessageEmbed {
		embed := embeds.New("", "", "")
		embed.Title = "✅ Member XP Set"
		embed.Color = embeds.ColorGreen
		embed.Description = fmt.Sprintf("%s's exp has been set to: `%dxp`.", members.Mention(memberID), xp)
		return &embed.MessageEmbed
	}
)
