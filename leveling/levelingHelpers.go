package leveling

import (
	"database/sql"

	"github.com/pkg/errors"
	"gitlab.com/teterski-softworks/godbot/db"
	"gitlab.com/teterski-softworks/godbot/states"
)

//TODO: deprecate
//showSettings shows the current leveling settings.
func showSettings(intr states.Interaction, settings Settings) error {
	return intr.EditResponseEmbeds(nil, settings.Embed(intr.DB))
}

//RetrieveMostExperiencedMemberID returns the ID of the member with the most XP on the server.
func RetrieveMostExperiencedMemberID(DB db.Reader, guildID string) (string, error) {
	var memberID string
	row, err := DB.QueryRow(SQLSelectLevelingMostExperienced, guildID)
	if err != nil {
		return memberID, err
	}
	if err = row.Scan(&memberID); !errors.Is(err, sql.ErrNoRows) && err != nil {
		return memberID, errors.WithStack(err)
	}
	return memberID, nil
}
