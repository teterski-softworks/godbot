package leveling

import (
	"fmt"
	"math"
	"time"

	dg "github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
	"gitlab.com/teterski-softworks/godbot/db"
	"gitlab.com/teterski-softworks/godbot/embeds"
	"gitlab.com/teterski-softworks/godbot/members"
	"gitlab.com/teterski-softworks/godbot/roles"
	"gitlab.com/teterski-softworks/godbot/states"
)

//Record stores a member's level and experience information.
type Record struct {
	members.Member

	Level       int
	XP          int
	LastUpdated time.Time
}

//New creates a new leveling record for the provided member in the guild.
func New(guildID string, memberID string) Record {
	return Record{
		Member:      members.New(guildID, memberID),
		Level:       1,
		XP:          0,
		LastUpdated: time.Now(),
	}
}

//Retrieve returns a Record for the given member.
func Retrieve(DB db.Reader, guildID string, memberID string) (Record, error) {

	record := New(guildID, memberID)

	row, err := DB.QueryRow(SQLSelectLevelingRecord, record.GuildID, record.MemberID)
	if err != nil {
		return record, err
	}

	if err = row.Scan(
		&record.Level,
		&record.XP,
		&record.LastUpdated,
	); err != nil {
		return record, errors.WithStack(err)
	}

	return record, nil
}

//Store saves a Record to the DB.
func (r Record) Store(DB db.ReadWriter) error {

	t := DB.NewTransaction()

	if err := r.Member.Store(t); err != nil {
		return err
	}

	t.Queue(SQLUpsertLevelingRecord,
		r.GuildID,
		r.MemberID,

		r.Level,
		r.XP,
		r.LastUpdated,
	)

	return t.Commit()
}

//Delete deletes a leveling record from the database. Returns true if the record actually existed in the database and was deleted, false otherwise.
func (r Record) Delete(DB db.ReadWriter) (found bool, err error) {

	if _, err = Retrieve(DB, r.GuildID, r.MemberID); err != nil {
		return false, err
	}
	if err = DB.QueueAndCommit(SQLDeleteLevelingRecord, r.GuildID, r.MemberID); err != nil {
		return false, err
	}
	return true, nil
}

//AddXP gives a member experience.
//`leveledUp` returns as true only if adding the xp resulted in a level up. Returns false otherwise.
//`mostXP` returns as true if adding the xp resulted in the member having the most xp on the server. Returns false otherwise.
func (r *Record) AddXP(s *states.State, xp int, settings Settings) (leveledUp bool, mostXP bool, err error) {

	//Check who currently has the most experience in the server.
	previousMostExperiencedMemberID, err := RetrieveMostExperiencedMemberID(s.DB, r.GuildID)
	if err != nil {
		return leveledUp, mostXP, errors.WithStack(err)
	}

	r.XP += xp
	r.LastUpdated = time.Now()
	leveledUp = r.levelUp()
	if leveledUp {
		if err = r.applyRewards(settings, s.Session); err != nil {
			return leveledUp, mostXP, err
		}
	}

	//TODO: probably want to move this out of here. too much of a side effect
	if err = r.Store(s.DB); err != nil {
		return leveledUp, mostXP, err
	}

	//Check if user has the most experience in the server
	//If so, apply the reward role.
	currentMostExperiencedMemberID, err := RetrieveMostExperiencedMemberID(s.DB, r.GuildID)
	if err != nil {
		return leveledUp, mostXP, errors.WithStack(err)
	}

	if currentMostExperiencedMemberID != previousMostExperiencedMemberID &&
		currentMostExperiencedMemberID == r.MemberID {
		mostXP = true
		//There might not be a reward role set for members with most XP
		if _, ok := settings.Rewards[rewardLevelMostExperience]; !ok {
			return leveledUp, mostXP, nil
		}
		if err := s.GuildMemberRoleAdd(r.GuildID, r.MemberID, settings.Rewards[rewardLevelMostExperience].RoleID); err != nil {
			return leveledUp, mostXP, errors.WithStack(err)
		}
		//It is possible that no one had any xp before, therefor there wont be a previous member with most xp.
		if previousMostExperiencedMemberID != "" {
			if err := s.GuildMemberRoleRemove(r.GuildID, previousMostExperiencedMemberID, settings.Rewards[rewardLevelMostExperience].RoleID); err != nil {
				return leveledUp, mostXP, errors.WithStack(err)
			}
		}
	}

	return leveledUp, mostXP, nil
}

//levelUp checks if enough xp has been reached to meet the next level.
//If so, increases the member's level and returns true.
//Returns false otherwise.
func (r *Record) levelUp() bool {

	if r.XP < ExpRequired(r.Level+1) {
		return false
	}
	r.Level++
	r.levelUp()
	return true
}

//applyRewards checks if the member qualifies for any role rewards, and applies all rewards they qualify for.
func (r *Record) applyRewards(settings Settings, session *dg.Session) error {

	//TODO!: What if role has been deleted?
	//TODO? add a handler that removes it as a reward?

	//TODO! What if a new reward was added for which members already have the required levels for

	var latestMutuallyExclusiveRewardLevel int

	//apply any stack reward roles
	for level, reward := range settings.Rewards {
		if level > r.Level {
			continue
		}

		if reward.Mode == rewardModeMostExperience {
			continue
		}

		if reward.Mode == rewardModeMutuallyExclusive {
			//keep track of the latest mutual exclusive reward
			//we want to make sure its still assigned to the member
			if level > latestMutuallyExclusiveRewardLevel {
				latestMutuallyExclusiveRewardLevel = level
			}
			continue
		}

		//Apply any stacked reward roles
		if err := session.GuildMemberRoleAdd(r.GuildID, r.MemberID, reward.RoleID); err != nil {
			return errors.WithStack(err)
		}
	}

	//remove any previous mutually exclusive reward roles
	for level, reward := range settings.Rewards {

		if reward.Mode == rewardModeMostExperience {
			continue
		}

		if reward.Mode != rewardModeMutuallyExclusive ||
			level > r.Level ||
			level == latestMutuallyExclusiveRewardLevel {
			continue
		}

		if err := session.GuildMemberRoleRemove(r.GuildID, r.MemberID, reward.RoleID); err != nil {
			return errors.WithStack(err)
		}
	}

	if latestMutuallyExclusiveRewardLevel == 0 {
		return nil
	}

	//Apply the highest mutually exclusive reward
	if err := session.GuildMemberRoleAdd(r.GuildID, r.MemberID, settings.Rewards[latestMutuallyExclusiveRewardLevel].RoleID); err != nil {
		return errors.WithStack(err)
	}

	return nil
}

//ExpRequired returns the amount of xp required to reach the given level.
func ExpRequired(level int) int {
	requiredXP := 0
	for lvl := 1; lvl < level; lvl++ {
		requiredXP += lvl + int(300*math.Pow(2, float64(lvl)/7))
	}
	return requiredXP / 4
}

//levelUpEmbed returns an embed stating the member has leveled up.
func (r *Record) levelUpEmbed(DB db.Reader, settings Settings) *dg.MessageEmbed {
	embed := embeds.NewDefault(DB, r.GuildID)
	embed.Title = "🥳 Level Up!"
	embed.Description += fmt.Sprintf("Congrats %s! You are now level **%d**!", members.Mention(r.MemberID), r.Level)
	if reward, ok := settings.Rewards[r.Level]; ok {
		embed.Description += fmt.Sprintf("\nYou have received the following reward:\n%s", roles.Mention(reward.RoleID))
	}
	embed.Footer.Text = fmt.Sprintf("Only %dxp to go until your next level!", ExpRequired(r.Level+1)-r.XP)
	embed.Timestamp = time.Now().Format(time.RFC3339)
	return &embed.MessageEmbed
}

//mostXPEmbed returns an embed stating the member has the most xp on the server.
func (r *Record) mostXPEmbed(DB db.Reader, settings Settings) *dg.MessageEmbed {
	embed := embeds.NewDefault(DB, r.GuildID)
	embed.Title = "🥇 Most Experience!"
	embed.Description += fmt.Sprintf("Congrats %s!\nWith `%dxp`, you now have the most experience on the server!", members.Mention(r.MemberID), r.XP)
	if reward, ok := settings.Rewards[rewardLevelMostExperience]; ok {
		embed.Description += fmt.Sprintf("\nYou have received the following reward:\n%s", roles.Mention(reward.RoleID))
	}
	embed.Timestamp = time.Now().Format(time.RFC3339)
	return &embed.MessageEmbed
}
