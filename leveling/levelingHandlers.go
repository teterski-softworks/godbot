package leveling

import (
	"database/sql"
	"math/rand"
	"strings"
	"time"

	"github.com/pkg/errors"
	"gitlab.com/teterski-softworks/godbot/states"
)

//TODO: add handler for channel deletion that checks if scoreboard channel has been deleted

//MessageCreateHandler, when a member sends a message, checks for and applies how much XP the member should receive for the message.
func MessageCreateHandler(intr states.Interaction) error {

	message := intr.Message
	guildID := message.GuildID
	channelID := message.ChannelID

	settings, err := RetrieveSettings(intr.DB, guildID)
	if err != nil {
		return err
	}

	if !settings.Enabled {
		return nil
	}

	//Check if the channel is ignored or is ignored category
	for _, match := range settings.IgnoreChannelIDs {
		if match == channelID {
			return nil
		}
		if channel, err := intr.Channel(channelID); err != nil {
			return errors.WithStack(err)
		} else if match == channel.ParentID {
			return nil
		}
	}

	record, err := Retrieve(intr.DB, guildID, message.Author.ID)
	if err != nil {
		return err
	} else if errors.Is(err, sql.ErrNoRows) {
		record = New(guildID, message.Author.ID)
	}

	//Check if the member is still on cool down.
	if record.LastUpdated.Add(settings.Cooldown).After(time.Now()) {
		return nil
	}

	//Apply xp per chance
	if float32(rand.Intn(100)) > settings.Chance {
		return nil
	}

	xp := settings.BaseXP
	if message.Content != "" {
		xp += settings.TextXP
	}

	//get the current scoreboard so we can check if the scoreboard has changed after
	//member gained xp and needs to be updated
	scoreboardBefore, err := RetrieveScoreboard(intr.DB, guildID)
	if err != nil {
		return err
	}

	for _, attachment := range message.Attachments {
		if strings.HasPrefix(attachment.ContentType, "image") {
			xp += settings.ImageXP
			continue
		}
		if strings.HasPrefix(attachment.ContentType, "video") {
			xp += settings.VideoXP
			continue
		}
	}

	if channelXP, ok := settings.ChannelRates[channelID]; ok {
		xp += channelXP
	}

	//update the xp for the user.
	leveledUp, mostXP, err := record.AddXP(intr.State, xp, settings)
	if err != nil {
		return err
	}

	if leveledUp {
		embed := record.levelUpEmbed(intr.DB, settings)
		if _, err = intr.ChannelMessageSendEmbed(channelID, embed); err != nil {
			return errors.WithStack(err)
		}
	}
	if mostXP {
		embed := record.mostXPEmbed(intr.DB, settings)
		if _, err = intr.ChannelMessageSendEmbed(channelID, embed); err != nil {
			return errors.WithStack(err)
		}
	}

	//TODO: what if original scoreboard has been deleted -> post a new one -> make this into a method we can use elsewhere too
	//update the scoreboard if it has changed
	scoreboardNew, err := RetrieveScoreboard(intr.DB, guildID)
	if err != nil {
		return err
	}
	if !scoreboardNew.Equals(scoreboardBefore) {
		_, err = intr.ChannelMessageEditEmbed(settings.ScoreboardChannelID, settings.ScoreboardMessageID, scoreboardNew.Embed(intr.DB))
		if err != nil {
			return errors.WithStack(err)
		}
	}

	return nil
}
