package leveling

var (
	SQLSelectLevelingRecord string = `
	SELECT
		level,
		xp,
		lastUpdated
	FROM levelingRecords
	WHERE guildID=(?1) AND memberID=(?2);`

	SQLSelectLevelingSettings string = `
	SELECT
		enabled,
		scoreboardChannelID,
		scoreboardMessageID,
		chance,
		cooldown,
		baseXP,
		textXP,
		imageXP,
		videoXP
	FROM levelingSettings
	WHERE guildID=(?1);`

	SQLSelectLevelingRewards string = `
	SELECT level, roleID, mode
	FROM levelingRewards
	WHERE guildID=(?1);`

	SQLSelectLevelingIgnoreChannels string = `
	SELECT channelID
	FROM levelingIgnore
	WHERE guildID=(?1);`

	SQLSelectLevelingChannelRates string = `
	SELECT channelID, xp
	FROM levelingChannelRates
	WHERE guildID=(?1);`

	SQLSelectLevelingScores string = `
	SELECT memberID, level, xp
	FROM levelingRecords
	WHERE guildID=(?1)
	ORDER BY xp DESC
	LIMIT 10;`

	SQLSelectLevelingMostExperienced string = `
	SELECT memberID
	FROM levelingRecords
	WHERE guildID=(?1)
	ORDER BY xp DESC
	LIMIT 1;`

	SQLUpsertLevelingRecord string = `
	INSERT INTO levelingRecords (
		guildID,
		memberID,

		level,
		xp,
		lastUpdated
	) VALUES (?1, ?2, ?3, ?4, ?5)
	ON CONFLICT (guildID, memberID) DO
	UPDATE SET
		level       =(?3),
		xp          =(?4),
		lastUpdated =(?5)
	WHERE guildID=(?1) AND memberID=(?2);`

	SQLUpsertLevelingSettings string = `
	INSERT INTO levelingSettings (
		guildID,

		enabled,
		scoreboardChannelID,
		scoreboardMessageID,
		chance,
		cooldown,

		baseXP,
		textXP,
		imageXP,
		videoXP
	) VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9, ?10)
	ON CONFLICT (guildID) DO
	UPDATE SET
		enabled  = (?2),
		scoreboardChannelID = (?3),
		scoreboardMessageID = (?4),
		chance   = (?5),
		cooldown = (?6),

		baseXP   = (?7),
		textXP   = (?8),
		imageXP  = (?9),
		videoXP  = (?10)
	WHERE guildID=(?1);`

	SQLInsertLevelingIgnoreChannel string = `
	INSERT OR IGNORE INTO levelingIgnore (guildID, channelID)
	VALUES (?1, ?2);`

	SQLInsertLevelingRewards string = `
	INSERT INTO levelingRewards (
		guildID,
		level,
		roleID,
		mode
	) VALUES (?1, ?2, ?3, ?4);`

	SQLInsertLevelingChannelRates string = `
	INSERT INTO levelingChannelRates (guildID, channelID, xp)
	VALUES (?1, ?2, ?3);`

	SQLDeleteLevelingRecord string = `
	DELETE FROM levelingRecords
	WHERE guildID=(?1) AND memberID=(?2);`

	SQLDeleteLevelingIgnoreChannels string = `
	DELETE FROM levelingIgnore
	WHERE guildID=(?1);`

	SQLDeleteLevelingRewards string = `
	DELETE FROM levelingRewards
	WHERE guildID=(?1);`

	SQLDeleteLevelingChannelRates string = `
	DELETE FROM levelingChannelRates
	WHERE guildID=(?1);`
)
