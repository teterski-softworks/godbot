package leveling

import (
	"database/sql"
	"fmt"
	"sort"
	"time"

	dg "github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
	"gitlab.com/teterski-softworks/godbot/channels"
	"gitlab.com/teterski-softworks/godbot/db"
	"gitlab.com/teterski-softworks/godbot/durations"
	"gitlab.com/teterski-softworks/godbot/embeds"
	"gitlab.com/teterski-softworks/godbot/guilds"
	"gitlab.com/teterski-softworks/godbot/messages"
	"gitlab.com/teterski-softworks/godbot/roles"
)

//Settings represents server settings for the leveling system.
type Settings struct {
	GuildID string

	Enabled             bool
	ScoreboardChannelID string
	ScoreboardMessageID string
	Chance              float32
	Cooldown            time.Duration

	BaseXP  int
	TextXP  int
	ImageXP int
	VideoXP int

	ChannelRates     map[ /*ChannelID*/ string]int
	Rewards          map[ /*level*/ int]Reward
	IgnoreChannelIDs []string
}

//rewardMode is the mode by which a reward is applied.
type rewardMode string

const (
	rewardLevelMostExperience int = 0

	//All applicable stack rewards are given.
	rewardModeStack rewardMode = "Stack"

	//Only the highest level mutually exclusive reward is given.
	rewardModeMutuallyExclusive rewardMode = "Mutually Exclusive"

	//Only the member with most experience can get this reward.
	rewardModeMostExperience rewardMode = "Most Experience"
)

//Reward represents a role reward.
type Reward struct {
	RoleID string
	Mode   rewardMode
}

//RetrieveSettings returns the Settings for the server.
func RetrieveSettings(DB db.Reader, guildID string) (Settings, error) {
	var settings Settings
	settings.GuildID = guildID
	settings.ChannelRates = make(map[string]int)
	settings.Rewards = make(map[int]Reward)
	row, err := DB.QueryRow(SQLSelectLevelingSettings, guildID)
	if err != nil {
		return settings, err
	}

	if err = row.Scan(
		&settings.Enabled,

		&settings.ScoreboardChannelID,
		&settings.ScoreboardMessageID,
		&settings.Chance,
		&settings.Cooldown,

		&settings.BaseXP,
		&settings.TextXP,
		&settings.ImageXP,
		&settings.VideoXP,
	); errors.Is(err, sql.ErrNoRows) {
		return settings, nil
	} else if err != nil {
		return settings, errors.WithStack(err)
	}

	rows, err := DB.Query(SQLSelectLevelingRewards, guildID)
	if err != nil {
		return settings, err
	}
	for rows.Next() {
		var level int
		var roleID, mode string
		if err = rows.Scan(&level, &roleID, &mode); errors.Is(err, sql.ErrNoRows) {
			if err = rows.Close(); err != nil {
				return settings, errors.WithStack(err)
			}
			continue
		} else if err != nil {
			return settings, errors.WithStack(err)
		}

		settings.Rewards[level] = Reward{RoleID: roleID, Mode: rewardMode(mode)}
	}

	rows, err = DB.Query(SQLSelectLevelingChannelRates, guildID)
	if err != nil {
		return settings, err
	}
	for rows.Next() {
		var channelID string
		var xp int
		if err = rows.Scan(&channelID, &xp); errors.Is(err, sql.ErrNoRows) {
			if err = rows.Close(); err != nil {
				return settings, errors.WithStack(err)
			}
			continue
		} else if err != nil {
			return settings, errors.WithStack(err)
		}
		settings.ChannelRates[channelID] = xp
	}

	rows, err = DB.Query(SQLSelectLevelingIgnoreChannels, guildID)
	if err != nil {
		return settings, err
	}
	for rows.Next() {
		var channelID string
		if err = rows.Scan(&channelID); errors.Is(err, sql.ErrNoRows) {
			if err = rows.Close(); err != nil {
				return settings, errors.WithStack(err)
			}
			continue
		} else if err != nil {
			return settings, errors.WithStack(err)
		}

		settings.IgnoreChannelIDs = append(settings.IgnoreChannelIDs, channelID)
	}

	return settings, err
}

//Store saves the Settings to the DB.
func (settings Settings) Store(DB db.ReadWriter) error {
	t := DB.NewTransaction()

	if err := guilds.New(settings.GuildID).Store(t); err != nil {
		return err
	}
	if err := messages.New(settings.GuildID, settings.ScoreboardChannelID, settings.ScoreboardMessageID).Store(t); err != nil {
		return err
	}

	t.Queue(SQLUpsertLevelingSettings,
		settings.GuildID,

		settings.Enabled,
		settings.ScoreboardChannelID,
		settings.ScoreboardMessageID,
		settings.Chance,
		settings.Cooldown,

		settings.BaseXP,
		settings.TextXP,
		settings.ImageXP,
		settings.VideoXP,
	)

	t.Queue(SQLDeleteLevelingIgnoreChannels, settings.GuildID)
	for _, channelID := range settings.IgnoreChannelIDs {
		if err := channels.New(settings.GuildID, channelID).Store(t); err != nil {
			return err
		}
		t.Queue(SQLInsertLevelingIgnoreChannel, settings.GuildID, channelID)
	}

	t.Queue(SQLDeleteLevelingRewards, settings.GuildID)
	for level, reward := range settings.Rewards {
		if err := roles.New(settings.GuildID, reward.RoleID).Store(t); err != nil {
			return err
		}
		t.Queue(SQLInsertLevelingRewards, settings.GuildID, level, reward.RoleID, reward.Mode)
	}

	t.Queue(SQLDeleteLevelingChannelRates, settings.GuildID)
	for channelID, xp := range settings.ChannelRates {
		if err := channels.New(settings.GuildID, channelID).Store(t); err != nil {
			return err
		}
		t.Queue(SQLInsertLevelingChannelRates, settings.GuildID, channelID, xp)
	}

	return t.Commit()
}

//RemoveIgnore removes a channel from the leveling Settings's ignore list.
func (settings *Settings) RemoveIgnore(channelID string) {
	var ignoredChannels []string
	for _, match := range settings.IgnoreChannelIDs {
		if match == channelID {
			continue
		}
		ignoredChannels = append(ignoredChannels, match)
	}
	settings.IgnoreChannelIDs = ignoredChannels
}

//Embed returns an embed displaying the current leveling Settings.
func (settings Settings) Embed(DB db.Reader) *dg.MessageEmbed {

	embed := embeds.NewDefault(DB, settings.GuildID)
	embed.Title = "🆙 Leveling Settings ⚙️"
	embed.Timestamp = time.Now().Format(time.RFC3339)

	embed.Description += "\\▪️ Experience is calculated by summing base xp, xp for any media in the post (text, image, video), and xp for the channel/category.\n"
	embed.Description += "\\▪️ Experience per level is calculated using the following [formula](https://wikimedia.org/api/rest_v1/media/math/render/png/bfd9a87d1b927c62f6aaa47d3101c42a8f398a5f).\n"
	embed.Description += "\\▪️ All eligible rewards are reapplied when a member levels up.\n"
	embed.Description += fmt.Sprintf("\\▪️ Members can have all `%s` rewards that are equal to or lower than their level.\n", rewardModeStack)
	embed.Description += fmt.Sprintf("\\▪️ Only a single `%s` reward is applied: the one with the highest eligible level.\n", rewardModeMutuallyExclusive)
	embed.Description += fmt.Sprintf("\\▪️ Only a single `%s` reward can be set, and the level will always be set to `%d`, regardless of what settings is used.", rewardModeMostExperience, rewardLevelMostExperience)

	status := &dg.MessageEmbedField{
		Name:  "❌ Status:",
		Value: "Leveling is currently `disabled`.",
	}
	if settings.Enabled {
		status.Name = "✅ Status:"
		status.Value = "Leveling is currently `enabled`."
	}

	cooldown := &dg.MessageEmbedField{
		Name:  "🕒 Cooldown:",
		Value: fmt.Sprintf("Members have to wait `%s` before xp can be received again.", durations.String(settings.Cooldown)),
	}

	chance := &dg.MessageEmbedField{
		Name:  "🎲 Chance:",
		Value: fmt.Sprintf("Members have a `%.2f%%` chance to receive xp for their post.", settings.Chance),
	}

	scoreboard := &dg.MessageEmbedField{
		Name:  "🏆 Auto Scoreboard ❌",
		Value: "Automatically updating scoreboard is `disabled`.\n",
	}
	if settings.ScoreboardChannelID != "" {
		scoreboard.Name = "🏆 Auto Scoreboard ✅"
		scoreboard.Value = fmt.Sprintf("Scoreboard will automatically update to channel: %s ", channels.Mention(settings.ScoreboardChannelID))
		scoreboard.Value += fmt.Sprintf("[Link](https://discord.com/channels/%s/%s/%s)\n", settings.GuildID, settings.ScoreboardChannelID, settings.ScoreboardMessageID)
	}

	expRates := &dg.MessageEmbedField{
		Name: "➕ Experience Rates:",
		Value: fmt.Sprintf("Base: `%dxp`,\tText: `%dxp`,\tImage: `%dxp`,\tVideo: `%dxp`",
			settings.BaseXP, settings.TextXP, settings.ImageXP, settings.VideoXP),
	}

	embed.Fields = append(embed.Fields, status, cooldown, chance, scoreboard, expRates)

	if len(settings.ChannelRates) != 0 {
		channelExpRates := &dg.MessageEmbedField{}
		channelExpRates.Name = "#️⃣ Channel/Category Experience Rates:"

		for channelID, xp := range settings.ChannelRates {
			channelExpRates.Value += fmt.Sprintf("`%dxp` %s\n", xp, channels.Mention(channelID))
		}
		embed.Fields = append(embed.Fields, channelExpRates)
	}

	if len(settings.IgnoreChannelIDs) != 0 {
		ignoredChannels := &dg.MessageEmbedField{}
		ignoredChannels.Name = "🚫 Ignored Channels/Categories:"

		for _, channelID := range settings.IgnoreChannelIDs {
			ignoredChannels.Value += channels.Mention(channelID) + "\n"
		}
		embed.Fields = append(embed.Fields, ignoredChannels)
	}

	if len(settings.Rewards) != 0 {
		//sort the rewards by level
		var levels []int
		for level := range settings.Rewards {
			levels = append(levels, level)
		}
		sort.Ints(levels)

		rewards := &dg.MessageEmbedField{}
		rewards.Name = "🏅 Rewards:"

		for _, level := range levels {
			reward := settings.Rewards[level]
			rewards.Value += fmt.Sprintf("Level %d: %s Mode: `%s`\n", level, roles.Mention(reward.RoleID), reward.Mode)
		}
		embed.Fields = append(embed.Fields, rewards)
	}

	return &embed.MessageEmbed
}
