package leveling

import (
	"database/sql"

	dg "github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
	"gitlab.com/teterski-softworks/godbot/commands"
	"gitlab.com/teterski-softworks/godbot/db"
	"gitlab.com/teterski-softworks/godbot/durations"
	"gitlab.com/teterski-softworks/godbot/states"
)

//SlashCommandLeveling - A command group for leveling related commands.
var SlashCommandLeveling = commands.New(
	&dg.ApplicationCommand{
		Type:        dg.ChatApplicationCommand,
		Name:        "leveling",
		Description: "Leveling related commands. Admin only.",
		Options: []*dg.ApplicationCommandOption{
			slashCommandLevelingSettings,
			slashCommandLevelingReward,
			slashCommandLevelingIgnore,
			slashCommandLevelingChannelRate,
			slashCommandLevelingExp,
			slashCommandLevelingScoreboard,
		},
	},
	false,
	func(intr states.Interaction, options ...*dg.ApplicationCommandInteractionDataOption) (err error) {

		if err := intr.Acknowledge(); err != nil {
			return err
		}

		switch options[0].Name {
		case slashCommandLevelingSettings.Name:
			return slashLevelingSettings(intr, options[0].Options)
		case slashCommandLevelingReward.Name:
			return slashLevelingReward(intr, options[0].Options)
		case slashCommandLevelingIgnore.Name:
			return slashLevelingIgnore(intr, options[0].Options)
		case slashCommandLevelingChannelRate.Name:
			return slashLevelingChannelRate(intr, options[0].Options)
		case slashCommandLevelingExp.Name:
			return slashLevelingExp(intr, options[0].Options)
		case slashCommandLevelingScoreboard.Name:
			return slashLevelingScoreboard(intr, options[0].Options)
		}

		return intr.EditResponseEmbeds(err, states.ResponseGenericError(err))
	},
)

//slashCommandLevelingSettings - A subcommand to edit leveling settings.
var slashCommandLevelingSettings = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionSubCommand,
	Name:        "settings",
	Description: "Sets and shows the settings for leveling.",
	Options: []*dg.ApplicationCommandOption{
		{
			Type:        dg.ApplicationCommandOptionBoolean,
			Name:        "enabled",
			Description: "Enabled leveling for the server.",
		}, {
			Type:        dg.ApplicationCommandOptionString,
			Name:        "cooldown",
			Description: "The cooldown before a member can receive XP again. Must be in format `0w0d0h0m0s`",
		}, {
			Type:        dg.ApplicationCommandOptionNumber,
			Name:        "chance",
			Description: "The percentage chance of a member receiving XP when posting a message.",
			MinValue:    &minChance,
			MaxValue:    100,
		}, {
			Type:        dg.ApplicationCommandOptionInteger,
			Name:        "base_xp",
			Description: "The base amount of XP received.",
			MinValue:    &minXP,
		}, {
			Type:        dg.ApplicationCommandOptionInteger,
			Name:        "text_xp",
			Description: "The amount of extra XP received if the message contains text.",
			MinValue:    &minXP,
		}, {
			Type:        dg.ApplicationCommandOptionInteger,
			Name:        "image_xp",
			Description: "The amount of extra XP received if the message contains an image.",
			MinValue:    &minXP,
		}, {
			Type:        dg.ApplicationCommandOptionInteger,
			Name:        "video_xp",
			Description: "The amount of extra XP received if the message contains a video.",
			MinValue:    &minXP,
		},
	},
}
var minChance float64 = 0
var minXP float64 = 0

//slashLevelingSettings edits leveling settings.
func slashLevelingSettings(intr states.Interaction, options []*dg.ApplicationCommandInteractionDataOption) error {

	settings, err := RetrieveSettings(intr.DB, intr.GuildID)
	if err != nil {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}

	for _, option := range options {
		switch option.Name {
		case "enabled":
			settings.Enabled = option.BoolValue()
		case "cooldown":
			settings.Cooldown, err = durations.Parse(option.StringValue())
			if err != nil {
				return intr.EditResponseEmbeds(nil, durations.ResponseInvalidDurationError())
			}
		case "chance":
			settings.Chance = float32(option.FloatValue())
		case "base_xp":
			settings.BaseXP = int(option.IntValue())
		case "text_xp":
			settings.TextXP = int(option.IntValue())
		case "image_xp":
			settings.ImageXP = int(option.IntValue())
		case "video_xp":
			settings.VideoXP = int(option.IntValue())
		}
	}

	if err := settings.Store(intr.DB); err != nil {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}

	return showSettings(intr, settings)
}

//slashCommandLevelingReward - A subcommand group for leveling reward related commands.
var slashCommandLevelingReward = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionSubCommandGroup,
	Name:        "reward",
	Description: "Adds or removes role rewards for reaching certain levels.",
	Options: []*dg.ApplicationCommandOption{
		slashCommandLevelingRewardAdd,
		slashCommandLevelingRewardRemove,
	},
}

//slashLevelingReward handles leveling reward related command functionality.
func slashLevelingReward(intr states.Interaction, options []*dg.ApplicationCommandInteractionDataOption) error {
	switch options[0].Name {
	case slashCommandLevelingRewardAdd.Name:
		return slashLevelingRewardAdd(intr, options[0].Options)
	case slashCommandLevelingRewardRemove.Name:
		return slashLevelingRewardRemove(intr, options[0].Options)
	}

	return intr.EditResponseEmbeds(nil, states.ResponseGenericError(nil))
}

//slashCommandLevelingRewardAdd - A subcommand to add leveling rewards.
var slashCommandLevelingRewardAdd = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionSubCommand,
	Name:        "add",
	Description: "Adds a role reward for reaching a certain level.",
	Options: []*dg.ApplicationCommandOption{
		{
			Type:        dg.ApplicationCommandOptionInteger,
			Name:        "level",
			Description: "The level to which the role reward will be added.",
			Required:    true,
			MinValue:    &minLevel,
		}, {
			Type:        dg.ApplicationCommandOptionRole,
			Name:        "role",
			Description: "The role to be rewarded for reaching the level.",
			Required:    true,
		}, {
			Type:        dg.ApplicationCommandOptionString,
			Name:        "mode",
			Description: "The type of role reward. See leveling settings for more info.",
			Choices: []*dg.ApplicationCommandOptionChoice{
				{
					Name:  string(rewardModeStack),
					Value: rewardModeStack,
				}, {
					Name:  string(rewardModeMutuallyExclusive),
					Value: rewardModeMutuallyExclusive,
				}, {
					Name:  string(rewardModeMostExperience),
					Value: rewardModeMostExperience,
				},
			},
		},
	},
}
var minLevel float64 = 2

//slashLevelingRewardAdd adds a leveling role reward.
func slashLevelingRewardAdd(intr states.Interaction, options []*dg.ApplicationCommandInteractionDataOption) error {

	settings, err := RetrieveSettings(intr.DB, intr.GuildID)
	if err != nil {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}
	level := int(options[0].IntValue())
	roleReward := options[1].RoleValue(intr.Session, intr.GuildID)
	mode := rewardModeStack
	if len(options) > 2 {
		mode = rewardMode(options[2].StringValue())
	}

	if mode == rewardModeMostExperience {
		settings.Rewards[0] = Reward{RoleID: roleReward.ID, Mode: mode}
	} else {
		settings.Rewards[level] = Reward{RoleID: roleReward.ID, Mode: mode}
	}

	if err := settings.Store(intr.DB); err != nil {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}

	return showSettings(intr, settings)
}

//slashCommandLevelingRewardRemove - A subcommand to remove leveling rewards.
var slashCommandLevelingRewardRemove = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionSubCommand,
	Name:        "remove",
	Description: "Removes a role reward.",
	Options: []*dg.ApplicationCommandOption{
		{
			Type:        dg.ApplicationCommandOptionInteger,
			Name:        "level",
			Description: "The level from which the role reward will be removed.",
			Required:    true,
		},
	},
}

//slashLevelingRewardRemove removes a role leveling reward.
func slashLevelingRewardRemove(intr states.Interaction, options []*dg.ApplicationCommandInteractionDataOption) error {

	settings, err := RetrieveSettings(intr.DB, intr.GuildID)
	if err != nil {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}

	level := int(options[0].IntValue())
	delete(settings.Rewards, level)

	if err := settings.Store(intr.DB); err != nil {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}

	return showSettings(intr, settings)
}

//slashCommandLevelingIgnore - A subcommand group for ignoring channels/category from the leveling system.
var slashCommandLevelingIgnore = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionSubCommandGroup,
	Name:        "ignore",
	Description: "Ignores/un-ignores a channel of category from giving XP.",
	Options: []*dg.ApplicationCommandOption{
		slashCommandLevelingIgnoreAdd,
		slashCommandLevelingIgnoreRemove,
	},
}

//slashLevelingIgnore handles ignoring of channels/category from the leveling system.
func slashLevelingIgnore(intr states.Interaction, options []*dg.ApplicationCommandInteractionDataOption) error {
	switch options[0].Name {
	case slashCommandLevelingIgnoreAdd.Name:
		return slashLevelingIgnoreAdd(intr, options[0].Options)
	case slashCommandLevelingIgnoreRemove.Name:
		return slashLevelingIgnoreRemove(intr, options[0].Options)
	}

	return intr.EditResponseEmbeds(nil, states.ResponseGenericError(nil))
}

//slashCommandLevelingIgnoreAdd - A subcommand for marking a channel/category as ignored from the leveling system.
var slashCommandLevelingIgnoreAdd = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionSubCommand,
	Name:        "add",
	Description: "Ignores a channel of category from giving XP.",
	Options: []*dg.ApplicationCommandOption{
		{
			Type:        dg.ApplicationCommandOptionChannel,
			Name:        "channel_or_category",
			Description: "The channel/category to ignore",
			Required:    true,
		},
	},
}

//slashLevelingIgnoreAdd marks a channel/category as ignored from the leveling system.
func slashLevelingIgnoreAdd(intr states.Interaction, options []*dg.ApplicationCommandInteractionDataOption) error {
	settings, err := RetrieveSettings(intr.DB, intr.GuildID)
	if err != nil {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}
	channel := options[0].ChannelValue(intr.Session)
	settings.IgnoreChannelIDs = append(settings.IgnoreChannelIDs, channel.ID)

	if err := settings.Store(intr.DB); err != nil {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}

	//We need to retrieve the settings again, to deduplicate ignored channel list
	settings, err = RetrieveSettings(intr.DB, intr.GuildID)
	if err != nil {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}

	return showSettings(intr, settings)
}

//slashCommandLevelingIgnoreRemove - A subcommand for no longer ignoring a channel/category from the leveling system.
var slashCommandLevelingIgnoreRemove = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionSubCommand,
	Name:        "remove",
	Description: "Removes a channel from the leveling ignore list.",
	Options: []*dg.ApplicationCommandOption{
		{
			Type:        dg.ApplicationCommandOptionChannel,
			Name:        "channel",
			Description: "The channel to no longer ignore.",
			Required:    true,
		},
	},
}

//slashLevelingIgnoreRemove no longer ignores a channel/category from the leveling system.
func slashLevelingIgnoreRemove(intr states.Interaction, options []*dg.ApplicationCommandInteractionDataOption) error {
	settings, err := RetrieveSettings(intr.DB, intr.GuildID)
	if err != nil {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}
	channel := options[0].ChannelValue(intr.Session)
	settings.RemoveIgnore(channel.ID)

	if err := settings.Store(intr.DB); err != nil {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}

	return showSettings(intr, settings)
}

//slashCommandLevelingChannelRate - A subcommand for setting a channel's/category's xp rate.
var slashCommandLevelingChannelRate = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionSubCommand,
	Name:        "channel_xp_rate",
	Description: "Sets the XP rate for a channel or category.",
	Options: []*dg.ApplicationCommandOption{
		{
			Type:        dg.ApplicationCommandOptionChannel,
			Name:        "channel_or_category",
			Description: "The channel/category to set the XP rate for.",
			Required:    true,
		}, {
			Type:        dg.ApplicationCommandOptionInteger,
			Name:        "xp",
			Description: "The amount of extra XP received from posting in the channel/category.",
			Required:    true,
			MinValue:    &minXP,
		},
	},
}

//slashLevelingChannelRate sets a channel's/category's xp rate.
func slashLevelingChannelRate(intr states.Interaction, options []*dg.ApplicationCommandInteractionDataOption) error {

	settings, err := RetrieveSettings(intr.DB, intr.GuildID)
	if err != nil {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}

	channel := options[0].ChannelValue(intr.Session)
	xp := options[1].IntValue()

	if xp == 0 {
		delete(settings.ChannelRates, channel.ID)
	} else {
		settings.ChannelRates[channel.ID] = int(xp)
	}

	if err := settings.Store(intr.DB); err != nil {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}

	return showSettings(intr, settings)
}

//slashCommandLevelingExp - A subcommand group for commands relating to member experience.
var slashCommandLevelingExp = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionSubCommandGroup,
	Name:        "exp",
	Description: "Commands relating to member experience.",
	Options: []*dg.ApplicationCommandOption{
		slashCommandLevelingExpAdd,
		slashCommandLevelingExpSet,
	},
}

//slashLevelingExp edits a member's experience.
func slashLevelingExp(intr states.Interaction, options []*dg.ApplicationCommandInteractionDataOption) error {
	switch options[0].Name {
	case slashCommandLevelingExpAdd.Name:
		return slashLevelingExpAdd(intr, options[0].Options)
	case slashCommandLevelingExpSet.Name:
		return slashLevelingExpSet(intr, options[0].Options)
	}

	return intr.EditResponseEmbeds(nil, states.ResponseGenericError(nil))
}

//slashCommandLevelingExpAdd - A subcommand to add experience to a member.
var slashCommandLevelingExpAdd = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionSubCommand,
	Name:        "add",
	Description: "Adds experience to a member.",
	Options: []*dg.ApplicationCommandOption{
		{
			Type:        dg.ApplicationCommandOptionUser,
			Name:        "member",
			Description: "The member to add exp to.",
			Required:    true,
		},
		{
			Type:        dg.ApplicationCommandOptionInteger,
			Name:        "exp",
			Description: "The amount of exp to add.",
			Required:    true,
		},
	},
}

//slashLevelingExpAdd gives a member the specified amount of experience.
func slashLevelingExpAdd(intr states.Interaction, options []*dg.ApplicationCommandInteractionDataOption) error {

	member := options[0].UserValue(intr.Session)
	exp := int(options[1].IntValue())

	settings, err := RetrieveSettings(intr.DB, intr.GuildID)
	if err != nil {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}

	record, err := Retrieve(intr.DB, intr.GuildID, member.ID)
	if err != nil {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	} else if errors.Is(err, sql.ErrNoRows) {
		record = New(intr.GuildID, member.ID)
	}

	record.Level = 0 //Reset their level so it can be properly recalculated if exp was removed.
	//TODO: someway to automatically remove any non eligible rewards if exp was removed.
	//TODO: if xp was removed, check who now has the most exp
	if _, _, err = record.AddXP(intr.State, exp, settings); err != nil {
		return intr.EditResponseEmbeds(err, states.ResponseGenericError(err))
	}
	if err = record.Store(intr.DB); err != nil {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}

	return intr.EditResponseEmbeds(nil, ResponseExpAdded(member.ID, exp))
}

//slashCommandLevelingExpSet - A subcommand to set a member's experience.
var slashCommandLevelingExpSet = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionSubCommand,
	Name:        "set",
	Description: "Sets  a member's experience.",
	Options: []*dg.ApplicationCommandOption{
		{
			Type:        dg.ApplicationCommandOptionUser,
			Name:        "member",
			Description: "The member to set the exp for.",
			Required:    true,
		},
		{
			Type:        dg.ApplicationCommandOptionInteger,
			Name:        "exp",
			Description: "The amount of exp to set.",
			Required:    true,
		},
	},
}

//slashLevelingExpSet changes the member's experience to the amount specified.
func slashLevelingExpSet(intr states.Interaction, options []*dg.ApplicationCommandInteractionDataOption) error {
	member := options[0].UserValue(intr.Session)
	exp := int(options[1].IntValue())

	settings, err := RetrieveSettings(intr.DB, intr.GuildID)
	if err != nil {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}

	record, err := Retrieve(intr.DB, intr.GuildID, member.ID)
	if err != nil {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	} else if errors.Is(err, sql.ErrNoRows) {
		record = New(intr.GuildID, member.ID)
	}

	record.XP = 0
	if _, _, err = record.AddXP(intr.State, exp, settings); err != nil {
		return intr.EditResponseEmbeds(err, states.ResponseGenericError(err))
	}

	if err = record.Store(intr.DB); err != nil {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}

	return intr.EditResponseEmbeds(nil, ResponseExpSet(member.ID, exp))
}

//slashCommandLevelingScoreboard - A subcommand group for leveling scoreboard related functions.
var slashCommandLevelingScoreboard = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionSubCommandGroup,
	Name:        "scoreboard",
	Description: "Commands relating to the leveling scoreboard.",
	Options: []*dg.ApplicationCommandOption{
		slashCommandLevelingScoreboardSet,
		slashCommandLevelingScoreboardDisable,
		slashCommandLevelingScoreboardView,
	},
}

//slashLevelingScoreboard handles scoreboard related command functionality.
func slashLevelingScoreboard(intr states.Interaction, options []*dg.ApplicationCommandInteractionDataOption) error {
	switch options[0].Name {
	case slashCommandLevelingScoreboardView.Name:
		return slashLevelingScoreboardView(intr, options[0].Options)
	case slashCommandLevelingScoreboardSet.Name:
		return slashLevelingScoreboardSet(intr, options[0].Options)
	case slashCommandLevelingScoreboardDisable.Name:
		return slashLevelingScoreboardDisable(intr, options[0].Options)
	}

	return intr.EditResponseEmbeds(nil, states.ResponseGenericError(nil))
}

//slashCommandLevelingScoreboardSet - A subcommand for settings the channel to which the auto-updating scoreboard is posted.
var slashCommandLevelingScoreboardSet = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionSubCommand,
	Name:        "set",
	Description: "Sets the channel to which a scoreboard will be automatically posted and updated.",
	Options: []*dg.ApplicationCommandOption{
		{
			Type:        dg.ApplicationCommandOptionChannel,
			Name:        "channel",
			Description: "The channel to be used for the automatic scoreboard.",
			Required:    true,
		},
	},
}

//slashLevelingScoreboardSet sets the channel to which the auto-updating scoreboard is posted.
func slashLevelingScoreboardSet(intr states.Interaction, options []*dg.ApplicationCommandInteractionDataOption) error {

	channel := options[0].ChannelValue(intr.Session)
	settings, err := RetrieveSettings(intr.DB, intr.GuildID)
	if err != nil {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}
	settings.ScoreboardChannelID = channel.ID

	//Post a new scoreboard in the channel.
	scoreboard, err := RetrieveScoreboard(intr.DB, intr.GuildID)
	if err != nil {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}
	message, err := intr.ChannelMessageSendEmbed(settings.ScoreboardChannelID, scoreboard.Embed(intr.DB))
	if err != nil {
		return intr.EditResponseEmbeds(errors.WithStack(err), states.ResponseGenericError(err))
	}

	//get the message ID, and save it.
	settings.ScoreboardMessageID = message.ID

	if err = settings.Store(intr.DB); err != nil {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}
	return showSettings(intr, settings)
}

//slashCommandLevelingScoreboardDisable - A subcommand for disabling the auto-updating scoreboard.
var slashCommandLevelingScoreboardDisable = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionSubCommand,
	Name:        "disable",
	Description: "Disables any previously set automatic scoreboard.",
}

//slashLevelingScoreboardDisable disables the auto-updating scoreboard.
func slashLevelingScoreboardDisable(intr states.Interaction, _ []*dg.ApplicationCommandInteractionDataOption) error {

	settings, err := RetrieveSettings(intr.DB, intr.GuildID)
	if err != nil {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}
	settings.ScoreboardChannelID = ""
	settings.ScoreboardMessageID = ""

	if err = settings.Store(intr.DB); err != nil {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}
	return showSettings(intr, settings)
}

//slashCommandLevelingScoreboardView - A subcommand for showing the current scoreboard.
var slashCommandLevelingScoreboardView = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionSubCommand,
	Name:        "view",
	Description: "Views the current scoreboard.",
}

//slashLevelingScoreboardView shows the current scoreboard.
func slashLevelingScoreboardView(intr states.Interaction, options []*dg.ApplicationCommandInteractionDataOption) error {
	scoreboard, err := RetrieveScoreboard(intr.DB, intr.GuildID)
	if err != nil {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}

	return intr.EditResponseEmbeds(nil, scoreboard.Embed(intr.DB))
}
