package leveling

import "gitlab.com/teterski-softworks/godbot/db"

//CreateDatabaseTables creates the required database tables for the embeds package.
func CreateDatabaseTables(DB db.ReadWriter) error {
	t := DB.NewTransaction()
	t.Queue(SQLCreateTableLevelingRecords)
	t.Queue(SQLCreateTableLevelingSettings)
	t.Queue(SQLCreateTableLevelingRewards)
	t.Queue(SQLCreateTableLevelingIgnore)
	t.Queue(SQLCreateTableLevelingChannelRates)
	return t.Commit()
}

var (
	SQLCreateTableLevelingRecords string = `
	CREATE TABLE IF NOT EXISTS levelingRecords(
		guildID       TEXT NOT NULL,
		memberID      TEXT NOT NULL,

		level		  INT DEFAULT 1,
		xp            INT DEFAULT 0,
		lastUpdated   DATETIME NOT NULL,

		PRIMARY KEY (guildID, memberID),
		FOREIGN KEY (guildID, memberID) REFERENCES members(guildID, memberID) ON DELETE CASCADE
	);`

	//TODO: channel constraints
	SQLCreateTableLevelingSettings string = `
	CREATE TABLE IF NOT EXISTS levelingSettings(
		guildID		        TEXT  NOT NULL PRIMARY KEY REFERENCES guilds(guildID) ON DELETE CASCADE,

		enabled		        BOOL  DEFAULT FALSE,
		scoreboardChannelID TEXT  DEFAULT "",
		scoreboardMessageID TEXT  DEFAULT "",
		chance		        FLOAT DEFAULT 0,
		cooldown	        INT   DEFAULT 0,
		baseXP		        INT   DEFAULT 0,
		textXP		        INT   DEFAULT 0,
		imageXP 	        INT   DEFAULT 0,
		videoXP		        INT   DEFAULT 0

		FOREIGN KEY (guildID, scoreboardChannelID, scoreboardMessageID) REFERENCES messages(guildID, channelID, messageID) ON DELETE SET DEFAULT
	);`

	SQLCreateTableLevelingRewards string = `
	CREATE TABLE IF NOT EXISTS levelingRewards(
		guildID   TEXT NOT NULL REFERENCES levelingSettings(guildID) ON DELETE CASCADE,
		level     INT  NOT NULL,
		roleID    TEXT NOT NULL,
		mode      TEXT NOT NULL,

		PRIMARY KEY (guildID, level),
		FOREIGN KEY (guildID, roleID) REFERENCES roles(guildID, roleID) ON DELETE CASCADE
	);`

	SQLCreateTableLevelingIgnore string = `
	CREATE TABLE IF NOT EXISTS levelingIgnore(
		guildID     TEXT NOT NULL REFERENCES levelingSettings(guildID) ON DELETE CASCADE,
		channelID   TEXT NOT NULL REFERENCES channels(channelID) ON DELETE CASCADE,
		
		PRIMARY KEY (guildID, channelID)
	);`

	SQLCreateTableLevelingChannelRates string = `
	CREATE TABLE IF NOT EXISTS levelingChannelRates(
		guildID     TEXT NOT NULL REFERENCES levelingSettings(guildID) ON DELETE CASCADE,
		channelID   TEXT NOT NULL REFERENCES channels(channelID) ON DELETE CASCADE,
		xp			INT  NOT NULL,

		PRIMARY KEY (guildID, channelID)
	);`
)
