package leveling_test

import (
	"testing"
	"time"

	"gitlab.com/teterski-softworks/godbot/db"
	"gitlab.com/teterski-softworks/godbot/leveling"
	"gitlab.com/teterski-softworks/godbot/members"
	"gitlab.com/teterski-softworks/godbot/tests"
)

var guildID, memberID = "guildID", "memberID"
var createTables = []func(db.ReadWriter) error{members.CreateDatabaseTables, leveling.CreateDatabaseTables}

//TODO
func TestDatabaseAccess(t *testing.T) {

	expected := leveling.New(guildID, memberID)
	expected.LastUpdated = expected.LastUpdated.Truncate(time.Second).UTC() //SQLite converts time to UTC format when storing it
	updated := expected
	updated.Level = 2
	updated.XP = 100
	updated.LastUpdated = time.Now().Add(3 * time.Hour).Truncate(time.Second).UTC()

	retrieve := func(DB db.Reader) (leveling.Record, error) {
		return leveling.Retrieve(DB, guildID, memberID)
	}

	tests.TestDatabaseAccess(
		t,
		expected, updated,
		createTables,
		retrieve,
	)
}
