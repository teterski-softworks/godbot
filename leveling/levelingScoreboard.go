package leveling

import (
	"database/sql"
	"fmt"
	"time"

	dg "github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
	"gitlab.com/teterski-softworks/godbot/db"
	"gitlab.com/teterski-softworks/godbot/embeds"
	"gitlab.com/teterski-softworks/godbot/members"
	"gitlab.com/teterski-softworks/godbot/messages"
)

//Scoreboard represents a scoreboard of the members members with most xp descending.
type Scoreboard struct {
	messages.Message
	MemberIDs []string
	Levels    []int
	XP        []int
}

//RetrieveScoreboard returns a scoreboard for the given server.
func RetrieveScoreboard(DB db.Reader, guildID string) (Scoreboard, error) {

	var scoreboard Scoreboard
	scoreboard.GuildID = guildID

	rows, err := DB.Query(SQLSelectLevelingScores, guildID)
	if err != nil {
		return scoreboard, err
	}

	index := 0
	for rows.Next() {
		var memberID string
		var level, xp int
		if err = rows.Scan(&memberID, &level, &xp); errors.Is(err, sql.ErrNoRows) {
			break
		} else if err != nil {
			return scoreboard, errors.WithStack(err)
		}
		scoreboard.MemberIDs = append(scoreboard.MemberIDs, memberID)
		scoreboard.Levels = append(scoreboard.Levels, level)
		scoreboard.XP = append(scoreboard.XP, xp)
		index++
	}

	return scoreboard, nil
}

//Embed returns an embed that shows the scoreboard.
func (s Scoreboard) Embed(DB db.Reader) *dg.MessageEmbed {
	embed := embeds.NewDefault(DB, s.GuildID)
	embed.Title = "🏆 Level Scoreboard"
	embed.Timestamp = time.Now().Format(time.RFC3339)
	embed.Description = "Top 10 highest levels in the server:\n"
	if len(s.MemberIDs) == 0 {
		embed.Description = "There are no members with XP yet."
	}
	for i, memberID := range s.MemberIDs {
		embed.Description += fmt.Sprintf("**#%d** %s Level:**`%d`** `%dxp`\n", i+1, members.Mention(memberID), s.Levels[i], s.XP[i])
	}
	return &embed.MessageEmbed
}

//Equals returns true if `compare` is equal to the scoreboard. Returns false otherwise.
func (s Scoreboard) Equals(compare Scoreboard) bool {

	if s.Message != compare.Message {
		return false
	}

	if len(s.MemberIDs) != len(compare.MemberIDs) {
		return false
	}

	for i, memberID := range s.MemberIDs {
		if memberID != compare.MemberIDs[i] {
			return false
		}
	}

	for i, xp := range s.XP {
		if xp != compare.XP[i] {
			return false
		}
	}

	return true
}
