package warnings

import "gitlab.com/teterski-softworks/godbot/db"

//CreateDatabaseTables creates the required database tables for the warnings package.
func CreateDatabaseTables(DB db.ReadWriter) error {
	t := DB.NewTransaction()
	t.Queue(SQLCreateTableWarnings)
	return t.Commit()
}

var (
	SQLCreateTableWarnings string = `
	CREATE TABLE IF NOT EXISTS warnings(
		guildID         TEXT NOT NULL REFERENCES guilds(guildID) ON DELETE CASCADE,
		memberID        TEXT NOT NULL,
		encryptedData   BLOB NOT NULL

		--No primary key
	);`
)
