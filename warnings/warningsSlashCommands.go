package warnings

import (
	"strconv"
	"strings"

	dg "github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
	"gitlab.com/teterski-softworks/godbot/commands"
	"gitlab.com/teterski-softworks/godbot/db"
	"gitlab.com/teterski-softworks/godbot/embeds"
	"gitlab.com/teterski-softworks/godbot/logs"
	"gitlab.com/teterski-softworks/godbot/states"
)

//SlashCommandWarn issues warnings to members.
var SlashCommandWarn = commands.New(
	&dg.ApplicationCommand{
		Type:        dg.ChatApplicationCommand,
		Name:        "warn",
		Description: "Warn related commands. Admin only.",
		Options: []*dg.ApplicationCommandOption{
			slashCommandWarnMember,
			slashCommandWarnRemove,
			slashCommandWarnList,
		},
	},
	false,
	func(intr states.Interaction, options ...*dg.ApplicationCommandInteractionDataOption) (err error) {

		if err := intr.Acknowledge(); err != nil {
			return err
		}

		switch options[0].Name {
		case slashCommandWarnMember.Name:
			return slashWarnMember(intr, options[0].Options)
		case slashCommandWarnRemove.Name:
			return slashWarnRemove(intr, options[0].Options)
		case slashCommandWarnList.Name:
			return slashWarnList(intr, options[0].Options)
		}
		return intr.EditResponseEmbeds(err, states.ResponseGenericError(err))
	},
)

//slashCommandWarnMember - A subcommand to issue warning to members.
var slashCommandWarnMember = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionSubCommand,
	Name:        "member",
	Description: "Warns a member.",
	Options: []*dg.ApplicationCommandOption{
		{
			Type:        dg.ApplicationCommandOptionUser,
			Name:        "member",
			Description: "The member who will be warned.",
			Required:    true,
		},
		{
			Type:        dg.ApplicationCommandOptionString,
			Name:        "reason",
			Description: "The reason for the warning.",
		},
	},
}

//slashWarnMember issues a warning to a member.
func slashWarnMember(intr states.Interaction, options []*dg.ApplicationCommandInteractionDataOption) (err error) {

	member := options[0].UserValue(intr.Session)
	reason := ReasonNone
	if len(options) == 2 {
		//TODO: make a function
		//Sanitize input so it doesn't mess up the warn list.
		reason = strings.ReplaceAll(options[1].StringValue(), "```", "")
		reason = strings.ReplaceAll(reason, "|", "")
		reason = strings.ReplaceAll(reason, "`", "")
		reason = strings.ReplaceAll(reason, "~", "")
		reason = strings.ReplaceAll(reason, "_", "")
		reason = strings.ReplaceAll(reason, "*", "")
	}

	warning := New(
		intr.GuildID,
		member.ID,
		intr.Member.User.ID,
		intr.Member.User.String(),
		reason,
	)
	if err := warning.Store(intr.DB); err != nil {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}

	embed := warning.Embed(intr.State)
	if err := logs.LogWarning(intr.State, intr.GuildID, embed); err != nil {
		return intr.EditResponseEmbeds(err, states.ResponseGenericError(err))
	}
	//Respond to the user
	return intr.EditResponseEmbeds(nil, embed)
}

//slashCommandWarnRemove - A subcommand to remove issued warnings.
var slashCommandWarnRemove = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionSubCommand,
	Name:        "remove",
	Description: "Removes a warning from a member.",
	Options: []*dg.ApplicationCommandOption{
		{
			Type:        dg.ApplicationCommandOptionInteger,
			Name:        "warning_id",
			Description: "The ID of the warning to remove.",
			Required:    true,
		},
	},
}

//slashWarnRemove removes a previously issued warning.
func slashWarnRemove(intr states.Interaction, options []*dg.ApplicationCommandInteractionDataOption) (err error) {
	warningID := strconv.Itoa(int(options[0].IntValue()))
	warning := Warning{WarningID: warningID}
	if found, err := warning.Delete(intr.DB); err != nil {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	} else if !found {
		return intr.EditResponseEmbeds(err, ResponseWarningNotFound(warningID))
	}
	return intr.EditResponseEmbeds(err, ResponseWarningRemoved())
}

//slashCommandWarnList - A subcommand to view a member's previous warnings.
var slashCommandWarnList = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionSubCommand,
	Name:        "list",
	Description: "Lists a member's previous warnings.",
	Options: []*dg.ApplicationCommandOption{
		{
			Type:        dg.ApplicationCommandOptionUser,
			Name:        "member",
			Description: "The member whose warnings to list.",
			Required:    true,
		},
	},
}

//slashWarnList shows a member's previous warnings.
func slashWarnList(intr states.Interaction, options []*dg.ApplicationCommandInteractionDataOption) (err error) {

	user := options[0].UserValue(intr.Session)
	list, err := embeds.NewMemberList(intr.State, intr.GuildID, user.ID, ListTitle, CustomIDSeparatorList, RetrieveAllMember)
	if err != nil {
		return intr.EditResponseEmbeds(err, states.ResponseGenericError(err))
	}

	response := &dg.WebhookEdit{
		Embeds:     list.Embeds(0),
		Components: list.Components(0),
	}
	if _, err := intr.InteractionResponseEdit(&intr.Interaction, response); err != nil {
		return errors.WithStack(err)
	}
	return nil
}
