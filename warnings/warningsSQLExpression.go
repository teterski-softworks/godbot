package warnings

const (
	SQLSelectWarning string = `
	SELECT guildID, memberID, encryptedData
	FROM warnings
	WHERE rowID=(?1);`

	SQLSelectWarnsOfMember string = `
	SELECT rowID, encryptedData
	FROM warnings
	WHERE guildID=(?1) AND memberID=(?2)
	ORDER BY rowID ASC;`

	SQLInsertWarning string = `
	INSERT INTO warnings(guildID, memberID, encryptedData)
	VALUES (?1, ?2, ?3);`

	SQLDeleteWarning string = `
	DELETE FROM warnings
	WHERE rowID=(?1);`
)
