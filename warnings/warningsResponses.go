package warnings

import (
	"fmt"

	dg "github.com/bwmarrin/discordgo"
	"gitlab.com/teterski-softworks/godbot/embeds"
)

var (
	ResponseWarningRemoved = func() *dg.MessageEmbed {
		embed := embeds.New("", "", "")
		embed.Title = "✅ Warning Removed 🚮"
		embed.Color = embeds.ColorGreen
		embed.Description = "The warning has been removed."
		return &embed.MessageEmbed
	}

	ResponseWarningNotFound = func(warningID string) *dg.MessageEmbed {
		embed := embeds.New("", "", "")
		embed.Title = "❌ Warning Not Found"
		embed.Color = embeds.ColorRed
		embed.Description = fmt.Sprintf("Unable to find warning with ID: `%s`.", warningID)
		return &embed.MessageEmbed
	}
)
