package warnings

import (
	"fmt"
	"time"

	dg "github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
	"gitlab.com/teterski-softworks/godbot/db"
	"gitlab.com/teterski-softworks/godbot/embeds"
	"gitlab.com/teterski-softworks/godbot/guilds"
	"gitlab.com/teterski-softworks/godbot/members"
	"gitlab.com/teterski-softworks/godbot/states"
)

var (
	ListTitle             = "⚠️ Warnings for %s"
	CustomIDSeparatorList = "warnList"
)

//Warning represents a record of warning issued to a member.
type Warning struct {
	WarningID string
	members.Member
	Encrypted
}

//Encrypted contains the encrypted part of the Warning.
type Encrypted struct {
	WarnedByMemberID       string
	WarnedByMemberUsername string
	Reason                 string
	Timestamp              time.Time
}

//New returns a new warning record from the provided information.
func New(guildID string, memberID string, warnedByMemberID string, warnedByMemberUsername string, reason string) Warning {
	var w Warning
	w.GuildID = guildID
	w.MemberID = memberID
	w.WarnedByMemberID = warnedByMemberID
	w.WarnedByMemberUsername = warnedByMemberUsername
	w.Reason = reason
	w.Timestamp = time.Now()
	return w
}

//Retrieve gets a warning records for the given warning ID.
func Retrieve(DB db.Reader, warningID string) (Warning, error) {
	var warning Warning
	row, err := DB.QueryRow(SQLSelectWarning, warningID)
	if err != nil {
		return warning, err
	}
	var encryptedData []byte
	if err = row.Scan(&warning.GuildID, &warning.MemberID, &encryptedData); err != nil {
		return warning, errors.WithStack(err)
	}
	if err = DB.Decrypt(&warning, encryptedData); err != nil {
		return warning, err
	}
	return warning, nil
}

//RetrieveAllMember gets all of a member's warning records for the given server from the database.
func RetrieveAllMember(DB db.Reader, guildID string, memberID string) ([]Warning, error) {

	var warnings []Warning
	rows, err := DB.Query(SQLSelectWarnsOfMember, guildID, memberID)
	if err != nil {
		return warnings, err
	}
	for rows.Next() {
		w := New(guildID, memberID, "", "", "")
		var encryptedData []byte
		if err = rows.Scan(&w.WarningID, &encryptedData); err != nil {
			return warnings, errors.WithStack(err)
		}
		if err = DB.Decrypt(&w, encryptedData); err != nil {
			return warnings, err
		}
		warnings = append(warnings, w)
	}

	return warnings, nil
}

//Store saves a warning record in the given database.
func (w Warning) Store(DB db.ReadWriter) error {

	t := DB.NewTransaction()

	if err := guilds.New(w.GuildID).Store(t); err != nil {
		return err
	}

	encryptedData, err := t.Encrypt(w)
	if err != nil {
		return err
	}
	t.Queue(SQLInsertWarning, w.GuildID, w.MemberID, encryptedData)

	return t.Commit()
}

//Delete removes a warning from the DB.
//If the warning is already not in the provided database, then found returns false. Otherwise, returns true.
func (w Warning) Delete(DB db.ReadWriter) (found bool, err error) {

	if _, err = Retrieve(DB, w.WarningID); err != nil {
		return false, err
	}
	if err = DB.QueueAndCommit(SQLDeleteWarning, w.WarningID); err != nil {
		return false, err
	}
	return true, nil
}

//Embed returns an embed showing the warning.
func (w Warning) Embed(s *states.State) *dg.MessageEmbed {
	embed := embeds.New("", "", "")
	embed.Color = embeds.ColorYellow
	member, user, err := members.GetMemberOrUserFromID(s.Session, w.GuildID, w.MemberID)
	if err == nil {
		if member != nil {
			embed.Thumbnail.URL = member.AvatarURL("")
		} else if user != nil {
			embed.Thumbnail.URL = user.AvatarURL("")
		}
	}
	embed.Title = "⚠️ Member Warned"
	embed.Timestamp = time.Now().Format(time.RFC3339)
	embed.Description += fmt.Sprintf("%s has been warned by %s.\n\n", members.Mention(w.MemberID), members.Mention(w.WarnedByMemberID))
	embed.Description += fmt.Sprintf("**Reason:** %s", w.Reason)

	return &embed.MessageEmbed
}

//Returns a formatted string with information about the Warning. Used in menus showing a list of all member warning.
func (w Warning) Info() string {
	info := fmt.Sprintf("**Warning ID:** `%s` - <t:%d>\n", w.WarningID, w.Timestamp.Unix())
	info += fmt.Sprintf("**Warned by:** %s `%s`\n", w.WarnedByMemberUsername, w.WarnedByMemberID)
	info += fmt.Sprintf("**Reason:** %s\n", w.Reason)
	return info
}
