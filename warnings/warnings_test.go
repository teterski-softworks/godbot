package warnings_test

import (
	"testing"
	"time"

	"gitlab.com/teterski-softworks/godbot/db"
	"gitlab.com/teterski-softworks/godbot/tests"
	"gitlab.com/teterski-softworks/godbot/warnings"
)

var guildID, memberID, warnedByMemberID, warnedByMemberUsername, reason, warningID = "guildID", "memberID", "warnedByMemberID", "warnedByMemberUsername", "reason", "1"
var createTables = []func(db.ReadWriter) error{warnings.CreateDatabaseTables}

//TODO
func TestDatabaseAccess(t *testing.T) {

	expected := warnings.New(guildID, memberID, warnedByMemberID, warnedByMemberUsername, reason)
	expected.Timestamp = expected.Timestamp.Round(time.Nanosecond)
	updated := warnings.Warning{}
	flags := tests.DefaultTestDatabaseAccessFlags
	flags.RetrieveEdited = false
	flags.StoreEdit = false
	flags.Delete = false //TODO: need custom test because of unique warningIDs.

	retrieve := func(DB db.Reader) (warnings.Warning, error) {
		return warnings.Retrieve(DB, warningID)
	}

	tests.TestDatabaseAccess(
		t,
		expected, updated,
		createTables,
		retrieve,
		flags,
	)
}
