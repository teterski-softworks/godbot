package webhooks

import (
	"fmt"

	dg "github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
)

//TODO: add logging

//MaxWebhooks is the maximum number of webhooks allowed by Discord for the Bot per guild
const MaxWebhooks int = 10

//Manager keeps track of what webhooks exist in a guild to ensure that the bot does not go above
//its maximum number of allowed webhooks, and helps reduce the number of webhook related requests the
//bot sends to Discord (so we do not hit the hard rate limit)
//It always ensures we are able to get a webhook for a given channel when needed.
type Manager interface {
	//Returns a webhook for the channel.
	GetWebhook(channelID string, name string, avatar string, session *dg.Session) (*dg.Webhook, error)
}

//listLRU is an ordered doubly-linked list of webhooks, where the most recently used
//webhook is the head, and the least recently used webhook is the tail.
//New webhooks are added as the list's head, and once it is full, it follows the
//"least recently used" policy to edit the tail into the webhook we need.
type listLRU struct {
	size    int
	maxSize int
	head    *listNode
}

//listNode forms a doubly-linked list by pointing to the node before it, and the one after it.
//Each node contains a Discord webhook.
type listNode struct {
	*dg.Webhook
	previous *listNode
	next     *listNode
}

//New returns a new WebhookManager that follows the least recently used policy for editing and reusing existing webhooks.
//It checks which webhooks already exist in the guild, and adds them to it's list.
//Webhooks that are redundant, are removed from the guild.
func New(maxSize int, guildID string, appID string, session *dg.Session) (Manager, error) {

	//Create the list
	list := &listLRU{
		size:    0,
		maxSize: maxSize,
		head:    nil,
	}

	//Get any existing webhooks
	webhooks, err := session.GuildWebhooks(guildID)
	if err != nil {
		return list, errors.WithStack(err)
	}

	//Add any existing webhooks to the list and delete redundant webhooks.
	for _, wh := range webhooks {

		//Check if webhooks belong to this bot
		if wh.ApplicationID != appID {
			continue
		}

		//Check if the list already has a webhook with the same info.
		//If so, delete the webhook from the server
		found := (nil != list.retrieve(wh.ChannelID, wh.Name, wh.Avatar))
		if found {
			if err := session.WebhookDelete(wh.ID); err != nil {
				return list, errors.WithStack(err)
			}
			continue
		}

		//Check if we have reached the allowed max size of the list
		//if so, simply delete any remaining webhooks
		if list.size >= maxSize {
			if err := session.WebhookDelete(wh.ID); err != nil {
				return list, errors.WithStack(err)
			}
			continue
		}

		//Add the existing webhook to our list
		list.addWebhook(wh)
	}

	return list, nil

}

//GetWebhook returns either an existing webhook, if one is found in the list, or creates and returns a new one.
func (list *listLRU) GetWebhook(channelID string, name string, avatar string, session *dg.Session) (*dg.Webhook, error) {

	node := list.retrieve(channelID, name, avatar)
	if node != nil {

		//If webhook has been deleted from the guild, without listLRU knowing, create a new webhook
		if st, _ := session.Webhook(node.ID); st == nil {
			node, err := list.createNewWebhook(channelID, name, avatar, session)
			return node.Webhook, err
		}
		return node.Webhook, nil
	}

	//No matching webhook was found, so a new one is made.
	node, err := list.createNewWebhook(channelID, name, avatar, session)
	return node.Webhook, err
}

//createNewWebhook creates and adds a new webhook to the head of the list.
//If the list is full, it edits the webhook in its tail, and makes it the new head.
func (list *listLRU) createNewWebhook(channelID string, name string, avatar string, session *dg.Session) (*listNode, error) {

	//If list is not full, create a new webhook
	if list.size < list.maxSize {

		//Create a new webhook.
		newWebhook, err := session.WebhookCreate(channelID, name, avatar)
		if err != nil {
			return nil, errors.WithStack(err)
		}
		list.addWebhook(newWebhook)
		return list.head, nil
	}

	//Edit the least recently used webhook to use the desired channel and set it as the head.
	node := list.popTail(session)
	if _, err := session.WebhookEdit(node.ID, name, avatar, channelID); err != nil {
		return node, errors.WithStack(err)
	}
	node.ChannelID = channelID
	list.addWebhook(node.Webhook)
	return node, nil

}

//addWebhook adds a webhook to the head of the list.
func (list *listLRU) addWebhook(webhook *dg.Webhook) {
	list.size++
	node := &listNode{webhook, nil, list.head}
	if list.head != nil {
		list.head.previous = node
	}
	list.head = node
}

//retrieve searches the list for a webhook for the given channel ID, name, and avatar.
//If one is found, it moves that webhook to the head of the list.
//Returns nil if no matching webhook is found.
func (list *listLRU) retrieve(channelID, name, avatar string) *listNode {

	for node := list.head; node != nil; node = node.next {

		if !node.matches(channelID, name, avatar) {
			continue
		}

		//The node is the head already
		if node == list.head {
			return list.head
		}

		//Otherwise, remove the node from its current location
		node.previous.next = node.next
		if node.next != nil {
			node.next.previous = node.previous
		}

		//Place the node as the new head
		node.previous = nil
		node.next = list.head
		list.head.previous = node
		list.head = node
		return list.head

	}
	return nil
}

//tail returns the least recently used webhook of the list.
func (list listLRU) tail() *listNode {
	node := list.head
	for node.next != nil {
		node = node.next
	}
	return node
}

//popTail removes the last node in the list and returns it.
func (list *listLRU) popTail(session *dg.Session) *listNode {
	node := list.tail()
	node.previous.next = nil
	node.previous = nil
	list.size--

	return node
}

//matches checks if a node's webhook has the required channelID, name, and avatar.
//Returns true if all three match, false otherwise.
func (node listNode) matches(channelID, name, avatar string) bool {
	return node.ChannelID == channelID && node.Name == name
}

//print is used for debugging to print out the contents of a listLRU
func (list *listLRU) print() {
	fmt.Println("Webhooks Start", list.size, "/", list.maxSize)
	for node := list.head; node != nil; node = node.next {
		fmt.Printf("-> %s %p %+v\n", node.ChannelID, node, node)
	}
	fmt.Println("Webhooks End")
}
