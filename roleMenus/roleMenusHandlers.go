package roleMenus

import (
	"strconv"
	"strings"

	dg "github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
	"gitlab.com/teterski-softworks/godbot/db"
	"gitlab.com/teterski-softworks/godbot/embeds"
	"gitlab.com/teterski-softworks/godbot/roles"
	"gitlab.com/teterski-softworks/godbot/states"
)

//ListPageSelectionHandler allows for switching between pages in a role menu list.
func ListPageSelectionHandler(intr states.Interaction, customID string) error {

	list, err := embeds.NewGuildList(intr.DB, intr.GuildID, ListTitle, CustomIDPrefixList, RetrieveAll)
	if err != nil {
		return intr.RespondEphemeralEmbeds(err, db.ResponseDBError(err))
	}

	page, err := strconv.Atoi(strings.TrimPrefix(customID, CustomIDPrefixList))
	if err != nil {
		return intr.RespondEphemeralEmbeds(err, states.ResponseGenericError(err))
	}

	response := &dg.InteractionResponse{
		Type: dg.InteractionResponseUpdateMessage,
		Data: &dg.InteractionResponseData{
			Embeds:     list.Embeds(page),
			Components: list.Components(page),
		},
	}
	return intr.InteractionRespond(&intr.Interaction, response)
}

//SelectionMenuHandler handles role selection through a selection menu.
func SelectionMenuHandler(intr states.Interaction, event *dg.InteractionCreate) error {

	componentData := event.MessageComponentData()
	guildID := event.GuildID
	channelID := event.ChannelID
	messageID := event.Message.ID
	member := event.Member

	if err := intr.AcknowledgeEphemeral(); err != nil {
		return err
	}

	menu, err := Retrieve(intr.DB, guildID, channelID, messageID)
	if err != nil {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}

	//Check if clear is selected first. Allow members to clear roles even if they are ignored.
	for _, selection := range componentData.Values {
		if selection != "clear" {
			continue
		}

		for _, option := range menu.Roles {
			if err = intr.GuildMemberRoleRemove(guildID, member.User.ID, option.RoleID); err != nil {
				return intr.EditResponseEmbeds(errors.WithStack(err), states.ResponseBotPermission())
			}
		}

		return intr.EditResponseEmbeds(nil, ResponseSelectionCleared())
	}

	//Check if member has a role preventing them from using the role menu.
	for _, roleID := range member.Roles {
		for _, ignored := range menu.RolesIgnored {
			if roleID != ignored {
				continue
			}
			return intr.EditResponseEmbeds(nil, ResponseHasIgnoredRole(ignored))
		}
	}

	//Check if member has all the required roles to use the role menu.
	for _, required := range menu.RolesRequired {
		hasRole := false
		for _, roleID := range member.Roles {
			if roleID != required {
				continue
			}
			hasRole = true
		}
		if !hasRole {
			//Member is missing a required role for using the role menu.
			return intr.EditResponseEmbeds(nil, ResponseNeedsRequiredRole(required))
		}
	}

	//Assign the selected roles.
	var rolesAssigned string
	for _, option := range menu.Roles {

		//Check if a role is not selected and therefore should be removed
		for _, selection := range componentData.Values {
			if option.RoleID == selection {
				//Add the selected role to the user
				if err = intr.GuildMemberRoleAdd(guildID, member.User.ID, option.RoleID); err != nil {
					return intr.EditResponseEmbeds(errors.WithStack(err), states.ResponseBotPermission())
				}
				rolesAssigned += roles.Mention(option.RoleID) + "\n"
				break
			}
		}
	}

	embed := embeds.New("", "", "")
	embed.Title = "✅ Roles Assigned:"
	embed.Color = embeds.ColorGreen
	embed.Description = rolesAssigned

	if err = intr.EditResponseEmbeds(nil, &embed.MessageEmbed); err != nil {
		return errors.WithStack(err)
	}
	return nil
}
