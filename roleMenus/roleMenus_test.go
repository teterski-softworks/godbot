package roleMenus_test

import (
	"testing"

	"gitlab.com/teterski-softworks/godbot/db"
	"gitlab.com/teterski-softworks/godbot/roleMenus"
	"gitlab.com/teterski-softworks/godbot/tests"
)

var guildID, channelID, messageID = "guildID", "channelID", "messageID"
var createTables = []func(db.ReadWriter) error{roleMenus.CreateDatabaseTables}

//TODO
func TestDatabaseAccess(t *testing.T) {

	expected := roleMenus.New(guildID, channelID, messageID)
	expected.AddRole("role1", "description1")
	expected.AddRole("role2", "description2")
	expected.AddRole("role3", "description3")
	expected.AddRole("role4", "description4")
	expected.RolesIgnored = []string{"ignored1", "ignored2", "ignored3", "ignored4"}
	expected.RolesRequired = []string{"required1", "required2", "required3", "required4"}
	updated := expected
	//TODO: random values for updated
	updated.MutuallyExclusive = true

	retrieve := func(DB db.Reader) (roleMenus.Menu, error) {
		return roleMenus.Retrieve(DB, guildID, channelID, messageID)
	}

	tests.TestDatabaseAccess(
		t,
		expected, updated,
		createTables,
		retrieve,
	)
}
