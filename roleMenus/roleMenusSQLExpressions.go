package roleMenus

var (
	SQLSelectRoleMenu string = `
	SELECT mutuallyExclusive
	FROM roleMenus
	WHERE guildID=(?1) AND channelID=(?2) AND messageID=(?3);`

	SQLSelectGuildRoleMenus string = `
	SELECT channelID, messageID, mutuallyExclusive
	FROM roleMenus
	WHERE guildID=(?1)
	ORDER BY channelID, messageID;`

	SQLSelectRoleMenuOptions string = `
	SELECT roleID, emojiID, encryptedData
	FROM roleMenuOptions
	WHERE guildID=(?1) AND channelID=(?2) AND messageID=(?3)
	ORDER BY orderIndex ASC;`

	SQLSelectRoleMenuRequiredRoles string = `
	SELECT roleID
	FROM roleMenuRequiredRoles
	WHERE guildID=(?1) AND channelID=(?2) AND messageID=(?3);`

	SQLSelectRoleMenuIgnoredRoles string = `
	SELECT roleID
	FROM roleMenuIgnoredRoles
	WHERE guildID=(?1) AND channelID=(?2) AND messageID=(?3);`

	SQLUpsertRoleMenu string = `
	INSERT INTO roleMenus(
		guildID,
		channelID,
		messageID,

		mutuallyExclusive
	)VALUES(?1, ?2, ?3, ?4)
	ON CONFLICT (guildID, channelID, messageID) DO
	UPDATE SET mutuallyExclusive=(?4)
	WHERE guildID=(?1) AND channelID=(?2) AND messageID=(?3);`

	SQLInsertRoleMenuOption string = `
	INSERT OR IGNORE INTO roleMenuOptions(
		guildID,
		channelID,
		messageID,

		orderIndex,

		roleID,
		emojiID,
		encryptedData
	)VALUES(?1, ?2, ?3, ?4, ?5, ?6, ?7);`

	SQLInsertRoleMenuRequiredRole string = `
	INSERT OR IGNORE INTO roleMenuRequiredRoles(
		guildID,
		channelID,
		messageID,

		roleID
	)VALUES(?1, ?2, ?3, ?4);`

	SQLInsertRoleMenuIgnoredRole string = `
	INSERT OR IGNORE INTO roleMenuIgnoredRoles(
		guildID,
		channelID,
		messageID,

		roleID
	)VALUES(?1, ?2, ?3, ?4);`

	SQLDeleteRoleMenu string = `
	DELETE FROM roleMenus
	WHERE guildID=(?1) AND channelID=(?2) AND messageID=(?3);`

	SQLDeleteRoleMenuOption string = `
	DELETE FROM roleMenuOptions
	WHERE guildID=(?1) AND channelID=(?2) AND messageID=(?3);`

	SQLDeleteRoleMenuRequiredRoles string = `
	DELETE FROM roleMenuRequiredRoles
	WHERE guildID=(?1) AND channelID=(?2) AND messageID=(?3);`

	SQLDeleteRoleMenuIgnoredRole string = `
	DELETE FROM roleMenuIgnoredRoles
	WHERE guildID=(?1) AND channelID=(?2) AND messageID=(?3);`
)
