package roleMenus

import (
	"fmt"

	dg "github.com/bwmarrin/discordgo"
	"gitlab.com/teterski-softworks/godbot/embeds"
	"gitlab.com/teterski-softworks/godbot/roles"
)

var (
	ResponseCreated = func() *dg.MessageEmbed {
		embed := embeds.New("", "", "")
		embed.Title = "✅ Role Menu Created"
		embed.Color = embeds.ColorGreen
		embed.Description = "Role menu has been created."
		return &embed.MessageEmbed
	}

	ResponseUpdated = func() *dg.MessageEmbed {
		embed := embeds.New("", "", "")
		embed.Title = "✅ Role Menu Updated"
		embed.Color = embeds.ColorGreen
		embed.Description = "Role menu has been updated."
		return &embed.MessageEmbed
	}

	ResponseDeleted = func() *dg.MessageEmbed {
		embed := embeds.New("", "", "")
		embed.Title = "✅ Role Menu Deleted 🚮"
		embed.Color = embeds.ColorGreen
		embed.Description = "Role menu has been deleted."
		return &embed.MessageEmbed
	}

	ResponseNotFound = func() *dg.MessageEmbed {
		embed := embeds.New("", "", "")
		embed.Title = "❌ Role Menu Not Found"
		embed.Color = embeds.ColorRed
		embed.Description = "Unable to find the role menu with the provided information."
		return &embed.MessageEmbed
	}

	ResponseIndexOutOfRange = func() *dg.MessageEmbed {
		embed := embeds.New("", "", "")
		embed.Title = "❌ Role Index Out Of Range"
		embed.Color = embeds.ColorRed
		embed.Description = "The provided index is out of range for this role menu."
		return &embed.MessageEmbed
	}

	ResponseAlreadyExists = func() *dg.MessageEmbed {
		embed := embeds.New("", "", "")
		embed.Title = "❌ Role Menu Already Exists"
		embed.Color = embeds.ColorRed
		embed.Description = "A role menu already exists here."
		return &embed.MessageEmbed
	}

	ResponseOptionAdded = func(roleID string) *dg.MessageEmbed {
		embed := embeds.New("", "", "")
		embed.Title = "✅ Role Added To Menu"
		embed.Color = embeds.ColorGreen
		embed.Description = fmt.Sprintf("%s has been added to the role menu.", roles.Mention(roleID))
		return &embed.MessageEmbed
	}

	ResponseRequiredAdded = func(roleID string) *dg.MessageEmbed {
		embed := embeds.New("", "", "")
		embed.Title = "✅ Required Role Added To Menu"
		embed.Color = embeds.ColorGreen
		embed.Description = fmt.Sprintf("%s is now a required role for the role menu.", roles.Mention(roleID))
		return &embed.MessageEmbed
	}

	ResponseIgnoredAdded = func(roleID string) *dg.MessageEmbed {
		embed := embeds.New("", "", "")
		embed.Title = "✅ Ignored Role Added To Menu"
		embed.Color = embeds.ColorGreen
		embed.Description = fmt.Sprintf("Members with the %s role can no longer use the role menu.", roles.Mention(roleID))
		return &embed.MessageEmbed
	}

	ResponseNotRequired = func(roleID string) *dg.MessageEmbed {
		embed := embeds.New("", "", "")
		embed.Title = "❌ Role Is Not Required"
		embed.Color = embeds.ColorRed
		embed.Description = fmt.Sprintf("The role %s is not a required role for the role menu.", roles.Mention(roleID))
		return &embed.MessageEmbed
	}

	ResponseNotIgnored = func(roleID string) *dg.MessageEmbed {
		embed := embeds.New("", "", "")
		embed.Title = "❌ Role Is Not Ignored"
		embed.Color = embeds.ColorRed
		embed.Description = fmt.Sprintf("The role %s is not an ignored role for the role menu.", roles.Mention(roleID))
		return &embed.MessageEmbed
	}

	ResponseSelectionCleared = func() *dg.MessageEmbed {
		embed := embeds.New("", "", "")
		embed.Title = "✅ Roles Removed"
		embed.Color = embeds.ColorGreen
		embed.Description = "Removed all roles in this role menu from you."
		return &embed.MessageEmbed
	}

	ResponseNeedsRequiredRole = func(roleID string) *dg.MessageEmbed {
		embed := embeds.New("", "", "")
		embed.Title = "❌ Required Role Missing"
		embed.Color = embeds.ColorRed
		embed.Description = fmt.Sprintf("You need the %s role to use this role menu.", roles.Mention(roleID))
		return &embed.MessageEmbed
	}

	ResponseHasIgnoredRole = func(roleID string) *dg.MessageEmbed {
		embed := embeds.New("", "", "")
		embed.Title = "❌ Cannot Use This Role Menu"
		embed.Color = embeds.ColorRed
		embed.Description = fmt.Sprintf("You cannot use this role menu, because you have the %s role.", roles.Mention(roleID))
		return &embed.MessageEmbed
	}
)
