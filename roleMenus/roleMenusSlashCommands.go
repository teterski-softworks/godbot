package roleMenus

import (
	"database/sql"

	dg "github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
	"gitlab.com/teterski-softworks/godbot/commands"
	"gitlab.com/teterski-softworks/godbot/db"
	"gitlab.com/teterski-softworks/godbot/embeds"
	"gitlab.com/teterski-softworks/godbot/states"
)

//SlashCommandRoleMenu - A command group for role Menu related commands.
var SlashCommandRoleMenu = commands.New(
	&dg.ApplicationCommand{
		Type:        dg.ChatApplicationCommand,
		Name:        "rolemenu",
		Description: "Role menu related commands. Admin only.",
		Options: []*dg.ApplicationCommandOption{
			slashCommandRoleMenuNew,
			slashCommandRoleMenuAdd,
			slashCommandRoleMenuEdit,
			slashCommandRoleMenuRemove,
			slashCommandRoleMenuList,
		},
	},
	false,
	func(intr states.Interaction, options ...*dg.ApplicationCommandInteractionDataOption) (err error) {

		if err := intr.AcknowledgeEphemeral(); err != nil {
			return err
		}

		switch options[0].Name {
		case slashCommandRoleMenuNew.Name:
			return slashRoleMenuNew(intr, options[0].Options)
		case slashCommandRoleMenuAdd.Name:
			return slashRoleMenuAdd(intr, options[0].Options)
		case slashCommandRoleMenuEdit.Name:
			return slashRoleMenuEdit(intr, options[0].Options)
		case slashCommandRoleMenuRemove.Name:
			return slashRoleMenuRemove(intr, options[0].Options)
		case slashCommandRoleMenuList.Name:
			return slashRoleMenuList(intr, options[0].Options)
		}
		return intr.EditResponseEmbeds(nil, states.ResponseGenericError(nil))
	},
)

//slashCommandRoleMenuNew - A subcommand to make new role Menus.
var slashCommandRoleMenuNew = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionSubCommand,
	Name:        "new",
	Description: "Creates a new role menu.",
	Options: []*dg.ApplicationCommandOption{
		optionChannel,
		optionMessage,
	},
}

//slashRoleMenuNew creates a new role Menu.
func slashRoleMenuNew(intr states.Interaction, options []*dg.ApplicationCommandInteractionDataOption) error {

	channel := options[0].ChannelValue(intr.Session)
	messageID := options[1].StringValue()

	//Get the original message
	message, err := intr.ChannelMessage(channel.ID, messageID)
	if err != nil {
		return intr.EditResponseEmbeds(errors.WithStack(err), states.ResponseGenericError(err))
	}

	//Add the new role menu to it.
	return roleMenuNew(intr, intr.GuildID, message)
}

//slashCommandRoleMenuAdd - A subcommand to add roles to a role Menu.
var slashCommandRoleMenuAdd = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionSubCommandGroup,
	Name:        "add",
	Description: "Adds a role to a role menu.",
	Options: []*dg.ApplicationCommandOption{
		slashCommandRoleMenuAddRole,
		slashCommandRoleMenuAddRequired,
		slashCommandRoleMenuAddIgnore,
	},
}

//slashRoleMenuAdd adds a new role menu to an existing message.
func slashRoleMenuAdd(intr states.Interaction, options []*dg.ApplicationCommandInteractionDataOption) error {
	switch options[0].Name {
	case slashCommandRoleMenuAddRole.Name:
		return slashRoleMenuAddRole(intr, options[0].Options)
	case slashCommandRoleMenuAddRequired.Name:
		return slashRoleMenuAddRequired(intr, options[0].Options)
	case slashCommandRoleMenuAddIgnore.Name:
		return slashRoleMenuAddIgnore(intr, options[0].Options)
	}
	return intr.EditResponseEmbeds(nil, states.ResponseGenericError(nil))
}

//slashCommandRoleMenuAddRole - A subcommand to add roles to a role Menu.
var slashCommandRoleMenuAddRole = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionSubCommand,
	Name:        "role",
	Description: "Adds a role to a role menu.",
	Options: []*dg.ApplicationCommandOption{
		optionChannel,
		optionMessage,
		optionRole,
		optionDescription,
		{
			Type:        dg.ApplicationCommandOptionBoolean,
			Name:        "emote",
			Description: "If set to true, the bot will ask what emote to give the option.",
		},
	},
}

//slashRoleMenuAdd adds a role to a role Menu.
func slashRoleMenuAddRole(intr states.Interaction, options []*dg.ApplicationCommandInteractionDataOption) error {
	channel := options[0].ChannelValue(intr.Session)
	messageID := options[1].StringValue()
	role := options[2].RoleValue(intr.Session, channel.GuildID)
	description := options[3].StringValue()
	/*askForEmote := false
	if len(options) == 4 {
		askForEmote = true
	}*/
	//TODO!: continue from here!

	//Get the role menu
	menu, err := Retrieve(intr.DB, intr.GuildID, channel.ID, messageID)
	if err != nil {
		//check if role menu actually exists.
		if errors.Is(err, sql.ErrNoRows) {
			return intr.EditResponseEmbeds(nil, ResponseNotFound())
		}
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}

	menu.AddRole(role.ID, description)

	//Save the new role menu
	if err = menu.Store(intr.DB); err != nil {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}

	if err = menu.refresh(intr); err != nil {
		return intr.EditResponseEmbeds(err, states.ResponseGenericError(err))
	}

	return intr.EditResponseEmbeds(nil, ResponseOptionAdded(role.ID))
}

//slashCommandRoleMenuAddRequired - A subcommand to add a required role to a role Menu.
var slashCommandRoleMenuAddRequired = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionSubCommand,
	Name:        "required",
	Description: "Adds a required role to a role menu.",
	Options: []*dg.ApplicationCommandOption{
		optionChannel,
		optionMessage,
		optionRole,
	},
}

//slashRoleMenuAddRequired adds a required role to a role Menu.
func slashRoleMenuAddRequired(intr states.Interaction, options []*dg.ApplicationCommandInteractionDataOption) error {
	channel := options[0].ChannelValue(intr.Session)
	messageID := options[1].StringValue()
	role := options[2].RoleValue(intr.Session, channel.GuildID)

	//Get the role menu
	menu, err := Retrieve(intr.DB, intr.GuildID, channel.ID, messageID)
	if err != nil {
		//check if role menu actually exists.
		if errors.Is(err, sql.ErrNoRows) {
			return intr.EditResponseEmbeds(nil, ResponseNotFound())
		}
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}
	menu.RolesRequired = append(menu.RolesRequired, role.ID)

	//Save the new role menu
	if err = menu.Store(intr.DB); err != nil {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}

	return intr.EditResponseEmbeds(nil, ResponseRequiredAdded(role.ID))
}

//slashCommandRoleMenuAddIgnore - A subcommand to ignore a role from using a role Menu.
var slashCommandRoleMenuAddIgnore = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionSubCommand,
	Name:        "ignored",
	Description: "Prevents members with the specified role from using the role menu.",
	Options: []*dg.ApplicationCommandOption{
		optionChannel,
		optionMessage,
		optionRole,
	},
}

//slashRoleMenuAddIgnore ignores a role from being able to use the role Menu.
func slashRoleMenuAddIgnore(intr states.Interaction, options []*dg.ApplicationCommandInteractionDataOption) error {
	channel := options[0].ChannelValue(intr.Session)
	messageID := options[1].StringValue()
	role := options[2].RoleValue(intr.Session, channel.GuildID)

	//Get the role menu
	menu, err := Retrieve(intr.DB, intr.GuildID, channel.ID, messageID)
	if err != nil {
		//check if role menu actually exists.
		if errors.Is(err, sql.ErrNoRows) {
			return intr.EditResponseEmbeds(nil, ResponseNotFound())
		}
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}
	menu.RolesIgnored = append(menu.RolesIgnored, role.ID)

	//Save the new role menu
	if err = menu.Store(intr.DB); err != nil {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}

	return intr.EditResponseEmbeds(nil, ResponseIgnoredAdded(role.ID))
}

//slashCommandRoleMenuEdit - A subcommand group for role Menu editing related commands.
var slashCommandRoleMenuEdit = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionSubCommandGroup,
	Name:        "edit",
	Description: "Edits a role menu.",
	Options: []*dg.ApplicationCommandOption{
		slashCommandRoleMenuEditRole,
		slashCommandRoleMenuEditMenu,
	},
}

//slashRoleMenuEdit edits a role menu.
func slashRoleMenuEdit(intr states.Interaction, options []*dg.ApplicationCommandInteractionDataOption) error {
	switch options[0].Name {
	case slashCommandRoleMenuEditRole.Name:
		return slashRoleMenuEditRole(intr, options[0].Options)
	case slashCommandRoleMenuEditMenu.Name:
		return slashRoleMenuEditMenu(intr, options[0].Options)
	}
	return intr.EditResponseEmbeds(nil, states.ResponseGenericError(nil))
}

//slashCommandRoleMenuEditRole - A subcommand to edit a role option in a role Menu.
var slashCommandRoleMenuEditRole = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionSubCommand,
	Name:        "role",
	Description: "Edits a role in a role menu.",
	Options: []*dg.ApplicationCommandOption{
		optionChannel,
		optionMessage,
		optionIndex,
		optionRole,
		optionDescription,
	},
}

//slashRoleMenuEditRole edits a role option in a role Menu.
func slashRoleMenuEditRole(intr states.Interaction, options []*dg.ApplicationCommandInteractionDataOption) error {

	channel := options[0].ChannelValue(intr.Session)
	messageID := options[1].StringValue()
	index := int(options[2].IntValue()) - 1
	role := options[3].RoleValue(intr.Session, channel.GuildID)
	description := options[4].StringValue()

	menu, err := Retrieve(intr.DB, intr.GuildID, channel.ID, messageID)
	if err != nil {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}

	if index >= len(menu.Roles) || index < 0 {
		intr.EditResponseEmbeds(nil, ResponseIndexOutOfRange())
	}

	menu.Roles[index].RoleID = role.ID
	menu.Roles[index].Description = description

	if err = menu.Store(intr.DB); err != nil {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}

	if err = menu.refresh(intr); err != nil {
		return intr.EditResponseEmbeds(err, states.ResponseGenericError(err))
	}

	return intr.EditResponseEmbeds(nil, ResponseUpdated())
}

//slashCommandRoleMenuEditMenu - A subcommand to edit a role Menu.
var slashCommandRoleMenuEditMenu = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionSubCommand,
	Name:        "menu",
	Description: "Edits a role menu.",
	Options: []*dg.ApplicationCommandOption{
		optionChannel,
		optionMessage,
		{
			Type:        dg.ApplicationCommandOptionBoolean,
			Name:        "mutually_exclusive",
			Description: "Whether or not the roles in the menu mutually exclusive.",
			Required:    true,
		},
	},
}

//slashRoleMenuEditMenu edits a role menu.
func slashRoleMenuEditMenu(intr states.Interaction, options []*dg.ApplicationCommandInteractionDataOption) error {
	channel := options[0].ChannelValue(intr.Session)
	messageID := options[1].StringValue()
	mutuallyExclusive := options[2].BoolValue()

	menu, err := Retrieve(intr.DB, intr.GuildID, channel.ID, messageID)
	if err != nil {
		//check if role menu actually exists.
		if errors.Is(err, sql.ErrNoRows) {
			return intr.EditResponseEmbeds(nil, ResponseNotFound())
		}
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}
	menu.MutuallyExclusive = mutuallyExclusive

	if err = menu.Store(intr.DB); err != nil {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}

	if err = menu.refresh(intr); err != nil {
		return intr.EditResponseEmbeds(err, states.ResponseGenericError(err))
	}

	return intr.EditResponseEmbeds(nil, ResponseUpdated())
}

//slashCommandRoleMenuRemove - A subcommand to remove parts of or an entire role Menu.
var slashCommandRoleMenuRemove = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionSubCommandGroup,
	Name:        "remove",
	Description: "Removes a role from a role menu.",
	Options: []*dg.ApplicationCommandOption{
		slashCommandRoleMenuRemoveRole,
		slashCommandRoleMenuRemoveRequired,
		slashCommandRoleMenuRemoveIgnored,
		slashCommandRoleMenuRemoveMenu,
	},
}

//slashRoleMenuRemove removes parts of a role Menu.
func slashRoleMenuRemove(intr states.Interaction, options []*dg.ApplicationCommandInteractionDataOption) error {
	switch options[0].Name {
	case slashCommandRoleMenuRemoveRole.Name:
		return slashRoleMenuRemoveRole(intr, options[0].Options)
	case slashCommandRoleMenuRemoveRequired.Name:
		return slashRoleMenuRemoveRequired(intr, options[0].Options)
	case slashCommandRoleMenuRemoveIgnored.Name:
		return slashRoleMenuRemoveIgnored(intr, options[0].Options)
	case slashCommandRoleMenuRemoveMenu.Name:
		return slashRoleMenuRemoveMenu(intr, options[0].Options)
	}
	return intr.EditResponseEmbeds(nil, states.ResponseGenericError(nil))
}

//slashCommandRoleMenuRemoveRole - A subcommand to remove a role from a role Menu.
var slashCommandRoleMenuRemoveRole = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionSubCommand,
	Name:        "role",
	Description: "Removes a role from a role menu.",
	Options: []*dg.ApplicationCommandOption{
		optionChannel,
		optionMessage,
		optionIndex,
	},
}

//slashRoleMenuRemoveRole deletes a role from a role Menu.
func slashRoleMenuRemoveRole(intr states.Interaction, options []*dg.ApplicationCommandInteractionDataOption) error {
	channel := options[0].ChannelValue(intr.Session)
	messageID := options[1].StringValue()
	index := int(options[2].IntValue()) - 1

	menu, err := Retrieve(intr.DB, intr.GuildID, channel.ID, messageID)
	if err != nil {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}

	if index >= len(menu.Roles) || index < 0 {
		return intr.EditResponseEmbeds(nil, ResponseIndexOutOfRange())
	}

	menu.Roles = append(menu.Roles[:index], menu.Roles[index+1:]...)
	if err := menu.Store(intr.DB); err != nil {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}

	if err = menu.refresh(intr); err != nil {
		return intr.EditResponseEmbeds(err, states.ResponseGenericError(err))
	}

	return intr.EditResponseEmbeds(nil, ResponseUpdated())
}

//slashCommandRoleMenuRemoveRequired - a subcommand to remove a required role from a role Menu.
var slashCommandRoleMenuRemoveRequired = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionSubCommand,
	Name:        "required",
	Description: "Removes a required role from a role menu.",
	Options: []*dg.ApplicationCommandOption{
		optionChannel,
		optionMessage,
		{
			Type:        dg.ApplicationCommandOptionRole,
			Name:        "role",
			Description: "The role to remove as a requirement for the role menu.",
			Required:    true,
		},
	},
}

//slashRoleMenuRemoveRequired removes a required role from a role Menu.
func slashRoleMenuRemoveRequired(intr states.Interaction, options []*dg.ApplicationCommandInteractionDataOption) error {
	channel := options[0].ChannelValue(intr.Session)
	messageID := options[1].StringValue()
	role := options[2].RoleValue(intr.Session, intr.GuildID)

	menu, err := Retrieve(intr.DB, intr.GuildID, channel.ID, messageID)
	if err != nil {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}

	if !menu.RemoveRequired(role.ID) {
		return intr.EditResponseEmbeds(nil, ResponseNotRequired(role.ID))
	}
	if err := menu.Store(intr.DB); err != nil {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}

	return intr.EditResponseEmbeds(nil, ResponseUpdated())
}

//slashCommandRoleMenuRemoveIgnored - A subcommand to remove an ignored role from a role Menu.
var slashCommandRoleMenuRemoveIgnored = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionSubCommand,
	Name:        "ignored",
	Description: "Removes an ignored role from a role menu.",
	Options: []*dg.ApplicationCommandOption{
		optionChannel,
		optionMessage,
		{
			Type:        dg.ApplicationCommandOptionRole,
			Name:        "role",
			Description: "The role to remove as a requirement for the role menu.",
			Required:    true,
		},
	},
}

//slashRoleMenuRemoveIgnored removes an ignored role from a role Menu.
func slashRoleMenuRemoveIgnored(intr states.Interaction, options []*dg.ApplicationCommandInteractionDataOption) error {
	channel := options[0].ChannelValue(intr.Session)
	messageID := options[1].StringValue()
	role := options[2].RoleValue(intr.Session, intr.GuildID)

	menu, err := Retrieve(intr.DB, intr.GuildID, channel.ID, messageID)
	if err != nil {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}

	if !menu.RemoveIgnored(role.ID) {
		return intr.EditResponseEmbeds(nil, ResponseNotIgnored(role.ID))
	}

	if err := menu.Store(intr.DB); err != nil {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}

	return intr.EditResponseEmbeds(nil, ResponseUpdated())
}

//slashCommandRoleMenuRemoveMenu - A subcommand to remove a role Menu.
var slashCommandRoleMenuRemoveMenu = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionSubCommand,
	Name:        "menu",
	Description: "Deletes a role menu.",
	Options: []*dg.ApplicationCommandOption{
		optionChannel,
		optionMessage,
	},
}

//slashRoleMenuRemoveMenu deletes a role menu.
func slashRoleMenuRemoveMenu(intr states.Interaction, options []*dg.ApplicationCommandInteractionDataOption) error {

	channel := options[0].ChannelValue(intr.Session)
	messageID := options[1].StringValue()

	menu, err := Retrieve(intr.DB, intr.GuildID, channel.ID, messageID)
	if err != nil {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}

	if _, err = menu.Delete(intr.DB); err != nil {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}

	//Get the original message
	message, err := intr.ChannelMessage(menu.ChannelID, menu.MessageID)
	if err != nil {
		return errors.WithStack(err)
	}
	//Remove the role menu from the message
	var messageEdit dg.MessageEdit
	messageEdit.ID = message.ID
	messageEdit.Channel = message.ChannelID
	messageEdit.Content = &message.Content
	messageEdit.Embeds = message.Embeds
	messageEdit.Components = []dg.MessageComponent{}
	if _, err = intr.ChannelMessageEditComplex(&messageEdit); err != nil {
		return errors.WithStack(err)
	}

	return intr.EditResponseEmbeds(nil, ResponseDeleted())

}

//slashCommandRoleMenuList - A subcommand for listing all role Menus in the guild.
var slashCommandRoleMenuList = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionSubCommand,
	Name:        "list",
	Description: "Lists all role menus on the server.",
}

//slashRoleMenuList lists all role Menus in the guild.
func slashRoleMenuList(intr states.Interaction, options []*dg.ApplicationCommandInteractionDataOption) error {

	list, err := embeds.NewGuildList(intr.DB, intr.GuildID, ListTitle, CustomIDPrefixList, RetrieveAll)
	if err != nil {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}

	page := 0
	response := dg.WebhookEdit{
		Embeds:     list.Embeds(page),
		Components: list.Components(page),
	}
	if _, err = intr.InteractionResponseEdit(&intr.Interaction, &response); err != nil {
		return errors.Wrapf(err, "unable to respond")
	}
	return nil
}
