package roleMenus

import (
	dg "github.com/bwmarrin/discordgo"
	"gitlab.com/teterski-softworks/godbot/commands"
	"gitlab.com/teterski-softworks/godbot/states"
)

//MessageCommandRoleMenuNew - turns a text message into an embed.
var MessageCommandRoleMenuNew = commands.New(
	&dg.ApplicationCommand{
		Type: dg.MessageApplicationCommand,
		Name: "Role Menu - New ✅",
	},
	false,
	func(intr states.Interaction, _ ...*dg.ApplicationCommandInteractionDataOption) error {

		if err := intr.AcknowledgeEphemeral(); err != nil {
			return err
		}

		commandData := intr.ApplicationCommandData()
		message := commandData.Resolved.Messages[commandData.TargetID]

		return roleMenuNew(intr, intr.GuildID, message)
	},
	"A role menu can be added to any message previously sent by the bot. Simply left click on the message"+
		" and go to `Apps > Role Menu - New ✅`.",
)
