package roleMenus

import (
	"fmt"

	dg "github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
	"gitlab.com/teterski-softworks/godbot/channels"
	"gitlab.com/teterski-softworks/godbot/db"
	"gitlab.com/teterski-softworks/godbot/messages"
	"gitlab.com/teterski-softworks/godbot/roles"
	"gitlab.com/teterski-softworks/godbot/states"
)

//TODO: Auto update role menu if role in menu has been renamed!

//TODO: what if role in menu has been deleted from server?

//TODO: re-post role menu.

//TODO: do we want to delete role menu if channel gets deleted? could be really annoying...

var (
	ListTitle             = "🎭 Role Menus"
	CustomIDPrefixList    = "roleMenuList"
	CustomIDSelectionMenu = "roleMenuSelectionMenu"
)

//Menu represents a role selection menu.
type Menu struct {
	messages.Message
	MutuallyExclusive bool
	Roles             []menuOption
	RolesRequired     []string //The roles a member would need to have in order to be allowed to use the role menu.
	RolesIgnored      []string //The roles that prevent a member from being allowed to use the role menu.
}

//menuOption represents a role option for a role selection Menu.
type menuOption struct {
	RoleID  string
	EmojiID string
	Encrypted
}

//Encrypted stores the encrypted portion of menuOption.
type Encrypted struct {
	Description string
}

//New returns a new Menu.
func New(guildID string, channelID string, messageID string) Menu {
	var m Menu
	m.GuildID = guildID
	m.ChannelID = channelID
	m.MessageID = messageID
	return m
}

//Retrieve gets a Menu from the DB.
func Retrieve(DB db.Reader, guildID string, channelID string, messageID string) (Menu, error) {

	m := New(guildID, channelID, messageID)

	row, err := DB.QueryRow(SQLSelectRoleMenu, guildID, channelID, messageID)
	if err != nil {
		return m, err
	}

	if err = row.Scan(&m.MutuallyExclusive); err != nil {
		return m, errors.WithStack(err)
	}

	//Get the roles options
	rows, err := DB.Query(SQLSelectRoleMenuOptions, guildID, channelID, messageID)
	if err != nil {
		return m, err
	}
	for rows.Next() {
		var roleID, emojiID string
		var encryptedData []byte
		if err = rows.Scan(&roleID, &emojiID, &encryptedData); err != nil {
			return m, errors.WithStack(err)
		}

		roleOption := menuOption{
			RoleID:  roleID,
			EmojiID: emojiID,
		}
		if err = DB.Decrypt(&roleOption.Encrypted, encryptedData); err != nil {
			return m, err
		}
		m.Roles = append(m.Roles, roleOption)
	}

	//Get the required roles
	rows, err = DB.Query(SQLSelectRoleMenuRequiredRoles, m.GuildID, m.ChannelID, m.MessageID)
	if err != nil {
		return m, err
	}
	for rows.Next() {
		var roleID string
		if err = rows.Scan(&roleID); err != nil {
			return m, errors.WithStack(err)
		}
		m.RolesRequired = append(m.RolesRequired, roleID)
	}

	//Get the ignored roles
	rows, err = DB.Query(SQLSelectRoleMenuIgnoredRoles, m.GuildID, m.ChannelID, m.MessageID)
	if err != nil {
		return m, err
	}
	for rows.Next() {
		var roleID string
		if err = rows.Scan(&roleID); err != nil {
			return m, errors.WithStack(err)
		}
		m.RolesIgnored = append(m.RolesIgnored, roleID)
	}

	return m, nil
}

//RetrieveAll returns all role menus in the server.
func RetrieveAll(DB db.Reader, guildID string) ([]Menu, error) {
	var menus []Menu

	//Retrieve a list of all role menus in the DB
	rows, err := DB.Query(SQLSelectGuildRoleMenus, guildID)
	if err != nil {
		return menus, err
	}

	var channelIDs, messageIDs []string
	for rows.Next() {
		var channelID, messageID string
		var mutuallyExclusive bool
		if err = rows.Scan(&channelID, &messageID, &mutuallyExclusive); err != nil {
			return menus, errors.WithStack(err)
		}
		channelIDs = append(channelIDs, channelID)
		messageIDs = append(messageIDs, messageID)
	}

	for i := range messageIDs {
		menu, err := Retrieve(DB, guildID, channelIDs[i], messageIDs[i])
		if err != nil {
			return menus, err
		}
		menus = append(menus, menu)
	}
	return menus, nil
}

//Store saves the role menu into the DB.
func (m Menu) Store(DB db.ReadWriter) error {

	t := DB.NewTransaction()

	if err := channels.New(m.GuildID, m.ChannelID).Store(t); err != nil {
		return err
	}

	t.Queue(SQLUpsertRoleMenu,
		m.GuildID,
		m.ChannelID,
		m.MessageID,
		m.MutuallyExclusive,
	)

	//We need to delete all the roles for the menu, as their order might have changed.
	t.Queue(SQLDeleteRoleMenuOption, m.GuildID, m.ChannelID, m.MessageID)
	t.Queue(SQLDeleteRoleMenuRequiredRoles, m.GuildID, m.ChannelID, m.MessageID)
	t.Queue(SQLDeleteRoleMenuIgnoredRole, m.GuildID, m.ChannelID, m.MessageID)

	//Save all the roles in the correct order.
	for index, role := range m.Roles {

		if err := roles.New(m.GuildID, role.RoleID).Store(t); err != nil {
			return err
		}

		encryptedData, err := DB.Encrypt(role.Encrypted)
		if err != nil {
			return err
		}

		t.Queue(SQLInsertRoleMenuOption,
			m.GuildID,
			m.ChannelID,
			m.MessageID,
			index,
			role.RoleID,
			role.EmojiID,
			encryptedData,
		)
	}

	//Save all the required roles
	for _, roleID := range m.RolesRequired {
		if err := roles.New(m.GuildID, roleID).Store(t); err != nil {
			return err
		}
		t.Queue(SQLInsertRoleMenuRequiredRole,
			m.GuildID,
			m.ChannelID,
			m.MessageID,
			roleID,
		)
	}

	//Save all the ignored roles
	for _, roleID := range m.RolesIgnored {
		if err := roles.New(m.GuildID, roleID).Store(t); err != nil {
			return err
		}
		t.Queue(SQLInsertRoleMenuIgnoredRole,
			m.GuildID,
			m.ChannelID,
			m.MessageID,
			roleID,
		)
	}

	return t.Commit()
}

//Delete removes the saved Menu from the DB.
//If the menu is already not in the provided database, then found returns false. Otherwise, returns true.
func (m Menu) Delete(DB db.ReadWriter) (found bool, err error) {

	if _, err = Retrieve(DB, m.GuildID, m.ChannelID, m.MessageID); err != nil {
		return false, err
	}
	if err = DB.QueueAndCommit(SQLDeleteRoleMenu, m.GuildID, m.ChannelID, m.MessageID); err != nil {
		return false, err
	}
	return true, nil
}

//AddRole adds a role as an option to the role selection Menu.
//If the roleID is already part of the menu, it will overwrite the role's existing description with the one provided.
func (m *Menu) AddRole(roleID string, description string) {

	for i := range m.Roles {
		if m.Roles[i].RoleID == roleID {
			m.Roles[i].Description = description
			return
		}
	}

	m.Roles = append(m.Roles, menuOption{
		RoleID:    roleID,
		Encrypted: Encrypted{Description: description},
	})
}

//RemoveRequired removes a role from the required roles for the Menu.
func (m *Menu) RemoveRequired(roleID string) bool {
	var required []string
	removed := false
	for _, match := range m.RolesRequired {
		if match == roleID {
			removed = true
			continue
		}
		required = append(required, match)
	}
	m.RolesRequired = required
	return removed
}

//RemoveIgnored removes a role from the ignored roles for the Menu.
func (m *Menu) RemoveIgnored(roleID string) bool {
	var ignored []string
	removed := false
	for _, match := range m.RolesIgnored {
		if match == roleID {
			removed = true
			continue
		}
		ignored = append(ignored, match)
	}
	m.RolesIgnored = ignored
	return removed
}

//refresh re-posts the selection menu component to the Menu's associated Message.
func (m Menu) refresh(intr states.Interaction) error {

	//Get the original message
	message, err := intr.ChannelMessage(m.ChannelID, m.MessageID)
	if err != nil {
		return errors.WithStack(err)
	}

	//Add the role menu to the desired message
	var messageEdit dg.MessageEdit
	messageEdit.ID = message.ID
	messageEdit.Channel = message.ChannelID
	messageEdit.Content = &message.Content
	messageEdit.Embeds = message.Embeds

	component, err := m.generateRoleSelectionMenu(intr)
	if err != nil {
		return err
	}
	messageEdit.Components = append(messageEdit.Components, component)

	//Edit the message
	if _, err = intr.ChannelMessageEditComplex(&messageEdit); err != nil {
		return errors.WithStack(err)
	}

	return nil
}

//generateRoleSelectionMenu returns a selection menu component representing the role Menu.
func (m Menu) generateRoleSelectionMenu(intr states.Interaction) (dg.ActionsRow, error) {

	var selectMenu dg.SelectMenu

	selectMenu.CustomID = CustomIDSelectionMenu
	minOptions := 1
	maxOptions := len(m.Roles) + 1
	if m.MutuallyExclusive {
		maxOptions = minOptions
	}
	selectMenu.MinValues = &minOptions
	selectMenu.MaxValues = maxOptions
	if len(m.Roles) == 0 {
		selectMenu.Disabled = true
		selectMenu.Options = append(selectMenu.Options, dg.SelectMenuOption{
			Label:       "Please add roles to this role menu.",
			Value:       "0",
			Description: "Use the `/roleMenu add command` to add roles to this menu.",
			Emoji: dg.ComponentEmoji{
				Name: "❗",
			},
			Default: true,
		})
	}

	roles, err := intr.GuildRoles(intr.GuildID)
	if err != nil {
		return dg.ActionsRow{}, err
	}

	for _, r := range m.Roles {

		for _, role := range roles {
			if role.ID != r.RoleID {
				continue
			}
			selectMenu.Options = append(selectMenu.Options, dg.SelectMenuOption{
				Label:       role.Name,
				Value:       r.RoleID,
				Description: r.Description,
			})
			break
		}
	}

	selectMenu.Options = append(selectMenu.Options, dg.SelectMenuOption{
		Label:       "Clear any assigned roles.",
		Value:       "clear",
		Description: "This also ignores any selected roles.",
		Emoji: dg.ComponentEmoji{
			Name: "❌",
		},
	})

	return dg.ActionsRow{Components: []dg.MessageComponent{selectMenu}}, nil
}

//Returns a formatted string with information about the Menu. Used in menus showing a list of all role Menus.
func (m Menu) Info() string {

	mode := "Regular"
	if m.MutuallyExclusive {
		mode = "Mutually Exclusive"
	}

	info := fmt.Sprintf("**ID:** `%s` %s [[Link]](%s)\n", m.MessageID, channels.Mention(m.ChannelID), m.Link())
	info += fmt.Sprintf("Mode: `%s`\n", mode)
	if len(m.Roles) == 0 {
		info += "This menu does not have any roles yet.\n"
	}
	for i, roleOption := range m.Roles {
		info += fmt.Sprintf("%d. %s *%s*\n", i+1, roles.Mention(roleOption.RoleID), roleOption.Description)
	}
	if len(m.RolesRequired) != 0 {
		info += "**✅ Required:**\n"
		for _, roleID := range m.RolesRequired {
			info += roles.Mention(roleID) + "\n"
		}
	}
	if len(m.RolesIgnored) != 0 {
		info += "**❎ Ignored:**\n"
		for _, roleID := range m.RolesIgnored {
			info += roles.Mention(roleID) + "\n"
		}
	}
	return info
}
