package roleMenus

import (
	dg "github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
	"gitlab.com/teterski-softworks/godbot/db"
	"gitlab.com/teterski-softworks/godbot/states"
)

//roleMenuNew creates a new role selection Menu.
func roleMenuNew(intr states.Interaction, guildID string, message *dg.Message) error {

	//Check if a role menu already exists for the message
	if _, err := Retrieve(intr.DB, guildID, message.ChannelID, message.ID); err == nil {
		//A role menu already exists
		return intr.EditResponseEmbeds(nil, ResponseAlreadyExists())
	}

	//Create the role menu
	menu := New(guildID, message.ChannelID, message.ID)

	//Add the role menu to the desired message
	var messageEdit dg.MessageEdit
	messageEdit.ID = message.ID
	messageEdit.Channel = message.ChannelID
	messageEdit.Content = &message.Content
	messageEdit.Embeds = message.Embeds

	component, err := menu.generateRoleSelectionMenu(intr)
	if err != nil {
		return intr.EditResponseEmbeds(err, states.ResponseGenericError(err))
	}
	messageEdit.Components = append(messageEdit.Components, component)

	//Edit the message
	if _, err = intr.ChannelMessageEditComplex(&messageEdit); err != nil {
		return intr.EditResponseEmbeds(errors.WithStack(err), states.ResponseGenericError(err))
	}

	//Save the role menu
	if err := menu.Store(intr.DB); err != nil {
		return intr.EditResponseEmbeds(err, db.ResponseDBError(err))
	}

	return intr.EditResponseEmbeds(nil, ResponseCreated())
}
