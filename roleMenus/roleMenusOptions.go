package roleMenus

import dg "github.com/bwmarrin/discordgo"

//optionChannel - a required option for a text channel.
var optionChannel = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionChannel,
	Name:        "channel",
	Description: "The channel location of the role menu.",
	Required:    true,
	ChannelTypes: []dg.ChannelType{
		dg.ChannelTypeGuildText,
	},
}

//optionMessage - a required option for a message ID string.
var optionMessage = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionString,
	Name:        "message_id",
	Description: "The ID of the message to which the role menu belongs.",
	Required:    true,
}

//optionIndex - a required option for an index in a role menu.
var optionIndex = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionInteger,
	Name:        "index",
	Description: "The index of the role in question.",
	Required:    true,
	MinValue:    &roleIndex,
	MaxValue:    25,
}
var roleIndex float64 = 1

//optionRole - a required option for a role.
var optionRole = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionRole,
	Name:        "role",
	Description: "The role to add to the role menu.",
	Required:    true,
}

//optionDescription - a required option for a role description.
var optionDescription = &dg.ApplicationCommandOption{
	Type:        dg.ApplicationCommandOptionString,
	Name:        "description",
	Description: "A description of the role.",
	Required:    true,
}
