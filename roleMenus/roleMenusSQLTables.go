package roleMenus

import "gitlab.com/teterski-softworks/godbot/db"

//CreateDatabaseTables creates the required database tables for the roleMenu package.
func CreateDatabaseTables(DB db.ReadWriter) error {
	t := DB.NewTransaction()
	t.Queue(SQLCreateTableRoleMenus)
	t.Queue(SQLCreateTableRoleMenuOptions)
	t.Queue(SQLCreateTableRoleMenuRequiredRoles)
	t.Queue(SQLCreateTableRoleMenuIgnoredRoles)
	return t.Commit()
}

//TODO: do we really want to delete role menus if the channel gets deleted? could be annoying...

var (
	SQLCreateTableRoleMenus string = `
	CREATE TABLE IF NOT EXISTS roleMenus(
		guildID              TEXT NOT NULL,
		channelID            TEXT NOT NULL,
		messageID            TEXT NOT NULL,

		mutuallyExclusive    BOOL DEFAULT FALSE,

		PRIMARY KEY (guildID, channelID, messageID),
		FOREIGN KEY (guildID, channelID) REFERENCES channels(guildID, channelID) ON DELETE CASCADE
	);`

	SQLCreateTableRoleMenuOptions string = `
	CREATE TABLE IF NOT EXISTS roleMenuOptions(
		guildID         TEXT NOT NULL,
		channelID       TEXT NOT NULL,
		messageID       TEXT NOT NULL,
		
		orderIndex      INT NOT NULL,

		roleID          TEXT NOT NULL,
		emojiID         TEXT DEFAULT "",

		encryptedData   BLOB NOT NULL,

		PRIMARY KEY (guildID, channelID, messageID, roleID),
		UNIQUE (guildID, channelID, messageID, orderIndex),
		UNIQUE (guildID, channelID, messageID, roleID),
		FOREIGN KEY (guildID, channelID, messageID) REFERENCES roleMenus(guildID, channelID, messageID) ON DELETE CASCADE,
		--//TODO: message foreign key?
		FOREIGN KEY (guildID, roleID) REFERENCES roles(guildID, roleID) ON DELETE CASCADE
	);`

	SQLCreateTableRoleMenuRequiredRoles string = `
	CREATE TABLE IF NOT EXISTS roleMenuRequiredRoles(
		guildID       TEXT NOT NULL,
		channelID     TEXT NOT NULL,
		messageID     TEXT NOT NULL,

		roleID        TEXT NOT NULL,

		PRIMARY KEY (guildID, channelID, messageID, roleID),
		FOREIGN KEY (guildID, channelID, messageID) REFERENCES roleMenus(guildID, channelID, messageID) ON DELETE CASCADE,
		FOREIGN KEY (guildID, roleID) REFERENCES roles(guildID, roleID) ON DELETE CASCADE
	);`

	SQLCreateTableRoleMenuIgnoredRoles string = `
	CREATE TABLE IF NOT EXISTS roleMenuIgnoredRoles(
		guildID       TEXT NOT NULL,
		channelID     TEXT NOT NULL,
		messageID     TEXT NOT NULL,

		roleID        TEXT NOT NULL,

		PRIMARY KEY (guildID, channelID, messageID, roleID),
		FOREIGN KEY (guildID, channelID, messageID) REFERENCES roleMenus(guildID, channelID, messageID) ON DELETE CASCADE,
		FOREIGN KEY (guildID, roleID) REFERENCES roles(guildID, roleID) ON DELETE CASCADE
	);`
)
